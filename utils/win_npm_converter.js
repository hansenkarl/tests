require.main === module && main(process.argv.slice(2)); async function main(args) {
	const pwd = process.cwd() + "\\";

	const maps = { local: {}, run: {}, build: {} };
	Object.assign(maps, require(pwd + "npm.js"));

	require("fs").writeFileSync(pwd + "package.json", JSON.stringify({
		private: true,
		dependencies: maps.run,
		devDependencies: maps.build,
		...maps.config,
	}));
}
