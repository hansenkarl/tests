import { WebDriver } from "selenium-webdriver";

export type TestAction = (webdriver: WebDriver, context: string) => Promise<true | string>;

export type TestModule = [string, TestAction][];
