import { WebDriver } from "selenium-webdriver";

export function makeExecutable(path: string) {
	if (path.includes(" ")) {
		throw new Error("Path contained a whitespace!\n" + path);
	}
	require("child_process").spawn("chmod +x " + path, {
		shell: true, stdio: ["inherit", "inherit", "inherit"],
	});
}

export function timestamp() { return new Date().getTime(); }

export async function setWindowSize(webdriver: WebDriver, width: number, height: number) {
	return await webdriver.manage().window().setRect({ width, height });
}

export function sleep(ms: number): Promise<true> { return new Promise(r => setTimeout(() => { r(true); }, ms)); }
