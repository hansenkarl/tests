"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListedIdMap = exports.newIdAutoMap = exports.isId = exports.NewId = void 0;
/** Placeholder Id for Documents that have not gotten a generated Id yet. */
exports.NewId = "new";
function isId(x) { return typeof x == "string" && x.startsWith("x"); }
exports.isId = isId;
function newIdAutoMap(doc_constructor) {
    return new Proxy({}, {
        get: (target, property) => {
            if (isId(property) && target[property] === undefined)
                target[property] = doc_constructor(property);
            return target[property];
        },
    });
}
exports.newIdAutoMap = newIdAutoMap;
class ListedIdMap {
    constructor(arg) {
        this.list = [];
        if (typeof arg == "string")
            this.reset(JSON.parse(arg));
        else
            this.reset(arg);
    }
    reset(items) {
        items ?? (items = []);
        const map = {};
        items.forEach(item => map[item._id] = item);
        this.map = map;
        this.list.splice(0, this.list.length, ...items);
    }
    get(id) {
        return this.map[id];
    }
    add(x) {
        if (this.map[x._id])
            this.list.splice(this.list.findIndex(y => y._id == x._id), 1, x);
        else
            this.list.push(x);
        this.map[x._id] = x;
    }
    remove(x) {
        if (this.map[x]) {
            this.list.splice(this.list.findIndex(y => y._id == x), 1);
            delete this.map[x];
        }
    }
    toJSON() {
        return this.list;
    }
}
exports.ListedIdMap = ListedIdMap;
//# sourceMappingURL=u.js.map