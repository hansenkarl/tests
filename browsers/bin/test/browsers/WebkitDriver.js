"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebkitDriver = void 0;
const selenium_webdriver_1 = require("selenium-webdriver");
const remote = require("selenium-webdriver/remote");
const http = require("selenium-webdriver/http");
const createDriver = require("selenium-webdriver").createDriver;
const webKit_webDriver_location = "/usr/bin/WebKitWebDriver";
class WebkitDriver {
    build() {
        return createDriver(ImplementationDriver, {
            "webkitgtk:browserOptions": { binary: "/usr/bin/epiphany", args: ["--automation-mode"] },
            "browserName": "Epiphany",
            "version": "",
            "platform": "ANY",
            "pageLoadStrategy": "normal",
        });
    }
}
exports.WebkitDriver = WebkitDriver;
class ImplementationDriver extends selenium_webdriver_1.WebDriver {
    static createSession(options) {
        const service = new (class ServiceBuilder extends remote.DriverService.Builder {
            constructor() {
                super(webKit_webDriver_location);
                this.setLoopback(true);
            }
        })().build();
        const executor = new http.Executor(service.start().then((url) => new http.HttpClient(url)));
        return super.createSession(executor, options, () => service.kill());
    }
}
//# sourceMappingURL=WebkitDriver.js.map