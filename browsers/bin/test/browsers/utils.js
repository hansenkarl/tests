"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sleep = exports.setWindowSize = exports.timestamp = exports.makeExecutable = void 0;
function makeExecutable(path) {
    if (path.includes(" ")) {
        throw new Error("Path contained a whitespace!\n" + path);
    }
    require("child_process").spawn("chmod +x " + path, {
        shell: true, stdio: ["inherit", "inherit", "inherit"],
    });
}
exports.makeExecutable = makeExecutable;
function timestamp() { return new Date().getTime(); }
exports.timestamp = timestamp;
async function setWindowSize(webdriver, width, height) {
    return await webdriver.manage().window().setRect({ width, height });
}
exports.setWindowSize = setWindowSize;
function sleep(ms) { return new Promise(r => setTimeout(() => { r(true); }, ms)); }
exports.sleep = sleep;
//# sourceMappingURL=utils.js.map