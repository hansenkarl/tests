"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ui_testing = void 0;
const selenium_webdriver_1 = require("selenium-webdriver");
const config_1 = require("../config");
async function registerUser(webdriver, context) {
    await webdriver.get(config_1.url);
    await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".login")), config_1.slowly);
    await (await webdriver.findElement(selenium_webdriver_1.By.css("a"))).click();
    await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-page-register")), config_1.quickly);
    await webdriver.findElement(selenium_webdriver_1.By.css(".form .widget-textfield:nth-of-type(1) input")).sendKeys(config_1.reg_key);
    await webdriver.findElement(selenium_webdriver_1.By.css(".form .widget-textfield:nth-of-type(2) input")).sendKeys("firstname");
    await webdriver.findElement(selenium_webdriver_1.By.css(".form .widget-textfield:nth-of-type(3) input")).sendKeys("lastname");
    await webdriver.findElement(selenium_webdriver_1.By.css(".form .widget-textfield:nth-of-type(4) input")).sendKeys(context + "@sharklasers.com");
    await webdriver.findElement(selenium_webdriver_1.By.css(".form .widget-textfield:nth-of-type(5) input")).sendKeys("Password123");
    await (await webdriver.findElement(selenium_webdriver_1.By.css(".form button"))).click();
    await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".success")), config_1.quickly);
    await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".success button")), config_1.quickly);
}
exports.ui_testing = [
    ["can register a new user", async (webdriver, context) => {
            try {
                await webdriver.get(config_1.url);
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".login")), config_1.slowly);
                await (await webdriver.findElement(selenium_webdriver_1.By.css("a"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-page-register")), config_1.quickly);
                await webdriver.findElement(selenium_webdriver_1.By.css(".form #pageregister-reg_key-textfield input")).sendKeys(config_1.reg_key);
                await webdriver.findElement(selenium_webdriver_1.By.css(".form #pageregister-fname-textfield input")).sendKeys("firstname");
                await webdriver.findElement(selenium_webdriver_1.By.css(".form #pageregister-lname-textfield input")).sendKeys("lastname");
                await webdriver.findElement(selenium_webdriver_1.By.css(".form #pageregister-email-textfield input")).sendKeys(context + "@sharklasers.com");
                await webdriver.findElement(selenium_webdriver_1.By.css(".form #pageregister-password1-textfield input")).sendKeys("Password123");
                await webdriver.findElement(selenium_webdriver_1.By.css(".form #pageregister-password2-textfield input")).sendKeys("Password123");
                await (await webdriver.findElement(selenium_webdriver_1.By.css(".form #pageregister-btn_submit-button"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".success")), config_1.quickly);
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".success #pageregister-btn_login-button")), config_1.quickly);
                const exceptions = (await webdriver.executeScript("return G.exceptions"));
                if (exceptions.length === 0)
                    return true;
                else
                    return exceptions.join("\n");
            }
            catch (e) {
                return e.message;
            }
        }],
    ["can login without typing after registration", async (webdriver, context) => {
            try {
                (await webdriver.findElement(selenium_webdriver_1.By.css(".success #pageregister-btn_login-button"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-top-bar")), config_1.quickly);
                const exceptions = (await webdriver.executeScript("return G.exceptions"));
                if (exceptions.length === 0)
                    return true;
                else
                    return exceptions.join("\n");
            }
            catch (e) {
                return e.message;
            }
        }],
    ["can log out", async (webdriver, context) => {
            try {
                (await webdriver.findElement(selenium_webdriver_1.By.css(".v-top-bar #btn_main-menu"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-main-menu .mdc-drawer.mdc-drawer--modal.mdc-drawer--open")), config_1.quickly);
                (await webdriver.findElement(selenium_webdriver_1.By.css(".v-main-menu .mdc-list-item#mainmenu-item_logout-button"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".loginform")), config_1.quickly);
                const exceptions = (await webdriver.executeScript("return G.exceptions"));
                if (exceptions.length === 0)
                    return true;
                else
                    return exceptions.join("\n");
            }
            catch (e) {
                return e.message;
            }
        }],
    ["can login", async (webdriver, context) => {
            try {
                await webdriver.findElement(selenium_webdriver_1.By.css(".loginform #pagemain-login_email-textfield input")).sendKeys(context + "@sharklasers.com");
                await webdriver.findElement(selenium_webdriver_1.By.css(".loginform #pagemain-login_password-textfield input")).sendKeys("Password123");
                await (await webdriver.findElement(selenium_webdriver_1.By.css(".loginform #pagemain-btn_login-button"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-top-bar")), config_1.quickly);
                const exceptions = (await webdriver.executeScript("return G.exceptions"));
                if (exceptions.length === 0)
                    return true;
                else
                    return exceptions.join("\n");
            }
            catch (e) {
                return e.message;
            }
        }],
    ["can create an org", async (webdriver, context) => {
            try {
                await (await webdriver.findElement(selenium_webdriver_1.By.css(".page-content #pagemain-btn_createorg-button"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-page-form-org")), config_1.quickly);
                await webdriver.findElement(selenium_webdriver_1.By.css(".page-content input")).sendKeys("my new org");
                await webdriver.findElement(selenium_webdriver_1.By.css("select")).click();
                await (await webdriver.findElement(selenium_webdriver_1.By.css(".page-content .widget-button"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-big-calendar")), config_1.quickly);
                const exceptions = (await webdriver.executeScript("return G.exceptions"));
                if (exceptions.length === 0)
                    return true;
                else
                    return exceptions.join("\n");
            }
            catch (e) {
                return e.message;
            }
        }],
    ["can create a grp", async (webdriver, context) => {
            try {
                // todo: needs to be better in future
                await (await webdriver.findElement(selenium_webdriver_1.By.css("#sidenav-item_groups-row"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css("#pageorg-menu_new_grp-button")), config_1.quickly).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css("label")), config_1.quickly);
                await (await webdriver.findElement(selenium_webdriver_1.By.css("input.data"))).sendKeys("new grp");
                await (await webdriver.findElement(selenium_webdriver_1.By.css("button"))).click();
                await webdriver.wait(selenium_webdriver_1.until.elementLocated(selenium_webdriver_1.By.css(".v-cxt-menu-of-group")), config_1.quickly);
                const exceptions = (await webdriver.executeScript("return G.exceptions"));
                if (exceptions.length === 0)
                    return true;
                else
                    return exceptions.join("\n");
            }
            catch (e) {
                return e.message;
            }
        }],
];
//# sourceMappingURL=ui_testing.js.map