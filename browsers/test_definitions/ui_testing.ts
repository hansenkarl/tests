import { By, Key, until, WebDriver } from "selenium-webdriver";
import { reg_key, url, slowly, quickly } from "../config";
import { TestModule } from "../typings";

async function registerUser(webdriver: WebDriver, context: string) {
	await webdriver.get(url);
	await webdriver.wait(until.elementLocated(By.css(".login")), slowly);
	await (await webdriver.findElement(By.css("a"))).click();

	await webdriver.wait(until.elementLocated(By.css(".v-page-register")), quickly);
	await webdriver.findElement(By.css(".form .widget-textfield:nth-of-type(1) input")).sendKeys(reg_key);
	await webdriver.findElement(By.css(".form .widget-textfield:nth-of-type(2) input")).sendKeys("firstname");
	await webdriver.findElement(By.css(".form .widget-textfield:nth-of-type(3) input")).sendKeys("lastname");
	await webdriver.findElement(By.css(".form .widget-textfield:nth-of-type(4) input")).sendKeys(context + "@sharklasers.com");
	await webdriver.findElement(By.css(".form .widget-textfield:nth-of-type(5) input")).sendKeys("Password123");

	await (await webdriver.findElement(By.css(".form button"))).click();

	await webdriver.wait(until.elementLocated(By.css(".success")), quickly);
	await webdriver.wait(until.elementLocated(By.css(".success button")), quickly);
}

export const ui_testing: TestModule = [
	["can register a new user", async (webdriver, context) => {
		try {
			await webdriver.get(url);
			await webdriver.wait(until.elementLocated(By.css(".login")), slowly);
			await (await webdriver.findElement(By.css("a"))).click();

			await webdriver.wait(until.elementLocated(By.css(".v-page-register")), quickly);
			await webdriver.findElement(By.css(".form #pageregister-reg_key-textfield input")).sendKeys(reg_key);
			await webdriver.findElement(By.css(".form #pageregister-fname-textfield input")).sendKeys("firstname");
			await webdriver.findElement(By.css(".form #pageregister-lname-textfield input")).sendKeys("lastname");
			await webdriver.findElement(By.css(".form #pageregister-email-textfield input")).sendKeys(context + "@sharklasers.com");
			await webdriver.findElement(By.css(".form #pageregister-password1-textfield input")).sendKeys("Password123");
			await webdriver.findElement(By.css(".form #pageregister-password2-textfield input")).sendKeys("Password123");

			await (await webdriver.findElement(By.css(".form #pageregister-btn_submit-button"))).click();

			await webdriver.wait(until.elementLocated(By.css(".success")), quickly);
			await webdriver.wait(until.elementLocated(By.css(".success #pageregister-btn_login-button")), quickly);
			const exceptions: string[] = (await webdriver.executeScript("return G.exceptions"));
			if (exceptions.length === 0) return true;
			else return exceptions.join("\n");
		}
		catch (e) {
			return e.message;
		}
	}],
	["can login without typing after registration", async (webdriver, context) => {
		try {
			(await webdriver.findElement(By.css(".success #pageregister-btn_login-button"))).click();

			await webdriver.wait(until.elementLocated(By.css(".v-top-bar")), quickly);

			const exceptions: string[] = (await webdriver.executeScript("return G.exceptions"));
			if (exceptions.length === 0) return true;
			else return exceptions.join("\n");
		}
		catch (e) {
			return e.message;
		}
	}],
	["can log out", async (webdriver, context) => {
		try {
			(await webdriver.findElement(By.css(".v-top-bar #btn_main-menu"))).click();
			await webdriver.wait(until.elementLocated(By.css(".v-main-menu .mdc-drawer.mdc-drawer--modal.mdc-drawer--open")), quickly);
			(await webdriver.findElement(By.css(".v-main-menu .mdc-list-item#mainmenu-item_logout-button"))).click();
			await webdriver.wait(until.elementLocated(By.css(".loginform")), quickly);

			const exceptions: string[] = (await webdriver.executeScript("return G.exceptions"));
			if (exceptions.length === 0) return true;
			else return exceptions.join("\n");
		}
		catch (e) {
			return e.message;
		}
	}],
	["can login", async (webdriver, context) => {
		try {
			await webdriver.findElement(By.css(".loginform #pagemain-login_email-textfield input")).sendKeys(context + "@sharklasers.com");
			await webdriver.findElement(By.css(".loginform #pagemain-login_password-textfield input")).sendKeys("Password123");

			await (await webdriver.findElement(By.css(".loginform #pagemain-btn_login-button"))).click();
			await webdriver.wait(until.elementLocated(By.css(".v-top-bar")), quickly);

			const exceptions: string[] = (await webdriver.executeScript("return G.exceptions"));
			if (exceptions.length === 0) return true;
			else return exceptions.join("\n");
		}
		catch (e) {
			return e.message;
		}
	}],
	["can create an org", async (webdriver, context) => {
		try {
			await (await webdriver.findElement(By.css(".page-content #pagemain-btn_createorg-button"))).click();
			await webdriver.wait(until.elementLocated(By.css(".v-page-form-org")), quickly);
			await webdriver.findElement(By.css(".page-content input")).sendKeys("my new org");
			
			await webdriver.findElement(By.css("select"));
			await (await webdriver.findElement(By.css(".page-content .widget-button"))).click();
			await webdriver.wait(until.elementLocated(By.css(".v-big-calendar")), quickly);

			const exceptions: string[] = (await webdriver.executeScript("return G.exceptions"));
			if (exceptions.length === 0) return true;
			else return exceptions.join("\n");
		}
		catch (e) {
			return e.message;
		}
	}],
	["can create a grp", async (webdriver, context) => {
		try {
			// todo: needs to be better in future
			await (await webdriver.findElement(By.css("#sidenav-item_groups-row"))).click();
			await webdriver.wait(until.elementLocated(By.css("#pageorg-menu_new_grp-button")), quickly).click();
			await webdriver.wait(until.elementLocated(By.css("label")), quickly);
			await (await webdriver.findElement(By.css("input.data"))).sendKeys("new grp");
			await (await webdriver.findElement(By.css("button"))).click();
			await webdriver.wait(until.elementLocated(By.css(".v-cxt-menu-of-group")), quickly);

			const exceptions: string[] = (await webdriver.executeScript("return G.exceptions"));
			if (exceptions.length === 0) return true;
			else return exceptions.join("\n");
		}
		catch (e) {
			return e.message;
		}
	}],
];
