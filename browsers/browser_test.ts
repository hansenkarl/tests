const is_windows = process.platform == "win32";
const slash = is_windows ? "\\" : "/";

require("source-map-support").install({ environment: "node" });
require("module-alias").addAlias("@", __dirname + `${slash}..${slash}..${slash}src`);

import "json5/lib/register";
declare global { interface JSON { r: (x: string) => any, w: (x: any) => string } }
{ const j5 = require("json5"); JSON.r = j5.parse; JSON.w = (x) => j5.stringify(x, { space: "\t" }); }
declare global { interface String { replaceAll(searchValue: string | RegExp, replaceValue: string): string } }
String.prototype.replaceAll ??= function (search: any, replacement: any) { return this.split(search).join(replacement); };

import { Builder, WebDriver } from "selenium-webdriver";

import "@/data/u";

import * as firefox from "selenium-webdriver/firefox";
import * as chrome from "selenium-webdriver/chrome";
import { WebkitDriver } from "./WebkitDriver";
import { makeExecutable, timestamp, sleep } from "./utils";
import { TestModule } from "./typings";

import { ui_testing } from "./test_definitions/ui_testing";

const firefox_driver_path_windows = __dirname + "\\..\\..\\..\\drivers\\geckodriver.exe";
const chrome_driver_path_windows = __dirname + "\\..\\..\\..\\drivers\\chromedriver.exe";
const chrome_driver_path_linux = __dirname + "/../../../drivers/chromedriver";
if (!is_windows) makeExecutable(chrome_driver_path_linux);

type TestResult = { browser_name: BrowserName, count_success: number, count_total: number };
type BrowserName = "firefox" | "chrome" | "gnomeweb";
const tests: ((webdriver: WebDriver, browser_name: string) => Promise<boolean>)[] = [];
const current_before_each: (() => Promise<void>) | null = null;
const green = "\x1b[32m";
const red = "\x1b[31m";
const reset = "\x1b[0m";
let counter = 0;

async function runTests(webdriver: WebDriver, browser_name: BrowserName, tests: TestModule): Promise<TestResult> {
	const context = "context_" + timestamp() + "_" + counter++;
	let count = 0;
	try {
		for (const [test_name, test_action] of tests) {
			const test_failed = (error_msg: string, browser_name: string) => {
				console.log();
				console.log(red + "Test \"" + test_name + "\" failed for browser " + browser_name + reset);
				console.log(error_msg);
				console.log();
			};
			try {
				const result = await test_action(webdriver, context);
				if (result === true) {
					count++;
				}
				else {
					test_failed(result, browser_name);
					break;
				}
			}
			catch (err) {
				console.log("Error, while testing with browser: " + browser_name + ", " + test_name + "\n" + err);
				break;
			}
		}
		return { browser_name, count_success: count, count_total: tests.length };
	}
	catch (e) {
		console.log(e);
		return { browser_name, count_success: count, count_total: tests.length };
	}
	finally {
		if (browser_name === "gnomeweb") {
			await webdriver.close();
			await sleep(1000);
		}
		await webdriver.quit();
	}
}

async function createBrowserDrivers() {
	const browsers = [];

	if (is_windows) {
		browsers.push(["firefox", await new Builder().forBrowser("firefox").setFirefoxService(new firefox.ServiceBuilder(firefox_driver_path_windows)).build()]);
		browsers.push(["chrome", await new Builder().forBrowser("chrome").setChromeService(new chrome.ServiceBuilder(chrome_driver_path_windows)).build()]);
	}
	else {
		browsers.push(["firefox", await new Builder().forBrowser("firefox").build()]);
		browsers.push(["chrome", await new Builder().forBrowser("chrome").setChromeService(new chrome.ServiceBuilder(chrome_driver_path_linux)).build()]);
		browsers.push(["gnomeweb", new WebkitDriver().build()]);
	}

	return browsers;
}

(async function main() {
	const running_tests: Promise<TestResult>[] = [];
	const test_modules = [ui_testing];
	for (const test_module of test_modules) {
		const drivers = await createBrowserDrivers();
		for (const [browser_name, driver] of drivers) {
			running_tests.push(runTests(driver, browser_name, test_module));
		}
	}

	const results = {
		firefox: { count_success: 0, count_total: 0 },
		chrome: { count_success: 0, count_total: 0 },
		gnomeweb: { count_success: 0, count_total: 0 },
	};

	for (const run_test of running_tests) {
		const result = (results)[(await run_test).browser_name];
		result.count_success += (await run_test).count_success;
		result.count_total += (await run_test).count_total;
	}

	function printResult(browser_name: string, result: { count_success: number, count_total: number }) {
		const color_code = result.count_success === result.count_total ? green : red;
		console.log(browser_name + ": " + color_code + result.count_success + " out of " + result.count_total + " tests successful!" + reset);
	}

	printResult("firefox", results.firefox);
	printResult("chrome", results.chrome);
	if (!is_windows) {
		printResult("gnomeweb", results.gnomeweb);
		if (results.gnomeweb.count_success < results.gnomeweb.count_total) process.exitCode = 1;
	}
	if (results.firefox.count_success < results.firefox.count_total) process.exitCode = 1;
	if (results.chrome.count_success < results.chrome.count_total) process.exitCode = 1;
})();
