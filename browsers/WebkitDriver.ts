import { WebDriver } from "selenium-webdriver";
import * as remote from "selenium-webdriver/remote";
import * as http from "selenium-webdriver/http";
const createDriver = require("selenium-webdriver").createDriver;

const webKit_webDriver_location = "/usr/bin/WebKitWebDriver";

export class WebkitDriver {
	build() {
		return createDriver(ImplementationDriver, {
			"webkitgtk:browserOptions": { binary: "/usr/bin/epiphany", args: ["--automation-mode"] },
			"browserName": "Epiphany",
			"version": "",
			"platform": "ANY",
			"pageLoadStrategy": "normal",
		});
	}
}

class ImplementationDriver extends WebDriver {
	static createSession(options: any) {
		const service = new (class ServiceBuilder extends remote.DriverService.Builder {
			constructor() {
				super(webKit_webDriver_location);
				this.setLoopback(true);
			}
		})().build();
		const executor = new http.Executor(
			service.start().then((url) => new http.HttpClient(url))
		);

		return super.createSession(executor, options, () => service.kill());
	}
}
