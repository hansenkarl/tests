const is_windows = process.platform == "win32";
const slash = is_windows ? "\\" : "/";

require("source-map-support").install({ environment: "node" });

import "json5/lib/register";
declare global { interface JSON { r: (x: string) => any, w: (x: any) => string } }
{ const j5 = require("json5"); JSON.r = j5.parse; JSON.w = (x) => j5.stringify(x, { space: "\t" }); }
declare global { interface String { replaceAll(searchValue: string | RegExp, replaceValue: string): string } }
String.prototype.replaceAll ??= function (search: any, replacement: any) { return this.split(search).join(replacement); };

import { AllRequests } from "@/data/RequestProtocol";
import { timestamp } from "./utils";
import { url } from "./config";
import { RequestValidator } from "@/core/RequestValidator";
import { PromiseType } from "utility-types";
import fetch from "node-fetch";

const fetchTimeout = (url: string, ms: number, options: any) => {
	const controller = new AbortController();
	const promise = fetch(url, { signal: controller.signal, ...options });
	const timeout = setTimeout(() => controller.abort(), ms);
	return promise.finally(() => clearTimeout(timeout));
};
let cookie = "";
let current_test_req_set: Set<string>;

/**
 * 
 * @param action either request action or false to reset cookies
 * @param parameters 
 * @param files 
 */
export async function request<
	Action extends AllRequests["action"],
	Form extends Extract<AllRequests, { action: Action }>,
>(action: Action | false, parameters?: Form["params"]): Promise<PromiseType<ReturnType<Form["rpf"]>> & { is200: boolean }> {
	if (action === false) {
		cookie = "";
		return null as any;
	}

	return fetchTimeout(url + (true ? "?a=" + action : ""), 5000, {
		method: "post",
		body: JSON.stringify({ action, ...(parameters ?? {}) }),
		headers: {
			"Cookie": cookie,
			"Content-Type": "application/json",
		},
	}).then(async (response: any) => {
		if (response.status === 502 || response.status === 503 || response.status === 504) {
			console.log("The server or network is unavailable!");
			console.log(response.status);
			process.exit(1);
		}
		const set_cookie = response.headers.raw()["set-cookie"];
		if (set_cookie != null) {
			cookie = set_cookie[0].split(";")[0];
		}
		return new Proxy({ ...(await response.json()), is200: response.status === 200 }, {
			get: (target, step) => {
				if (step === "is200") {
					current_test_req_set.add(action);
				}
				return target[step];
			},
		});
	}).catch((err: any) => {
		console.log(err);
		if (err.type === "aborted" || err.code === "ECONNREFUSED") {
			console.log("The server or network is unavailable!");
			process.exit(1);
		}
		return { ok: false, is200: false };
	});
}

type main_category = "functionality" | "stability" | "security";
type test_category = main_category | main_category[] | "extra";
const tests: (() => Promise<{ passed: boolean, category: test_category, test_name: string }>)[] = [];
let current_before_each: ((context: string) => Promise<void>) | null = null;
const green = "\x1b[32m";
const red = "\x1b[31m";
const reset = "\x1b[0m";
let counter = 0;


export function createTest(test_name: string, category: test_category, test_action: (context: string) => Promise<true | string>) {
	const test_failed = (error_msg: string) => {
		console.log();
		console.log(red + "Test \"" + test_name + "\" failed!" + reset);
		console.log(error_msg);
		console.log();
	};

	const before_each = current_before_each;
	const test: () => Promise<{ passed: boolean, category: test_category, test_name: string }> = async () => {
		const context = "context_" + timestamp() + "_" + counter++;
		if (before_each) {
			await before_each(context);
		}

		try {
			const result = await test_action(context);
			if (category !== "extra" && current_test_req_set.size === 0) throw new Error("No 200 checks in this test!");
			
			if (result === true) {
				return { passed: true, category, test_name };
			}
			else {
				test_failed(result);
				return { passed: false, category, test_name };
			}
		}
		catch (err) {
			if (typeof err !== "string") {
				console.log(err.code);
				console.log("\nError in test: " + test_name);
				console.log(err);
			}
			else test_failed(err);
			return { passed: false, category, test_name };
		}
	};

	tests.push(test);
}

export function setBeforeEach(lambda: (context: string) => Promise<void>) {
	current_before_each = lambda;
}

type RequestCoverage = {
	functionality: number, // does it even work? is the UI okay?
	stability: number, // can the system handle dirty input? does it crash? is there any data corruption?
	security: number, // are permissions checked properly?
};
const coverage = {} as Record<string, RequestCoverage>;
const req_names = Object.keys(RequestValidator);
req_names.forEach(x => {
	coverage[x] = {
		functionality: 0,
		stability: 0,
		security: 0,
	};
});

async function runTests() {
	function markCorrectCoverage(current_test_req: string, category: main_category, test_name: string) {
		if (!coverage[current_test_req][category]) coverage[current_test_req][category]++;
		else throw new Error("Warning! Test \"" + test_name + "\" for request \"" + current_test_req + "\" is a duplicate test in category \"" + category + "\"!");
	}

	let count = 0;
	for (const test of tests) {
		current_test_req_set = new Set();
		const test_result = await test();
		if (test_result.passed) count++;
		if (test_result.category === "extra") continue;

		for (const current_test_req of current_test_req_set) {
			if (typeof test_result.category === "string") {
				markCorrectCoverage(current_test_req, test_result.category, test_result.test_name);
			}
			else {
				for (const category of test_result.category) {
					markCorrectCoverage(current_test_req, category, test_result.test_name);
				}
			}
		}
	}
	const color_code = count === tests.length ? green : red;
	console.log(color_code + count + " out of " + tests.length + " tests successful!" + reset);
	if (count < tests.length) process.exitCode = 1;
}

// imports tests and runs them
current_before_each = null;
import "./user_requests_tests/session_test";
current_before_each = null;
import "./user_requests_tests/org_test";
current_before_each = null;
import "./user_requests_tests/ticket_test";
current_before_each = null;
import "./user_requests_tests/misc_test";
current_before_each = null;
import "./user_requests_tests/group_test";
current_before_each = null;
import "./user_requests_tests/checklist_test";
current_before_each = null;
import "./user_requests_tests/chat_test";

async function main() {
	await runTests();
	function isMissingType(x: RequestCoverage) {
		return (!x.functionality || !x.stability || !x.security);
	}

	if (req_names.some(x => isMissingType(coverage[x]))) {
		console.log("Warning, some requests are missing tests!");

		req_names.forEach(x => {
			if (isMissingType(coverage[x])) {
				const func = (coverage[x].functionality ? green : red) + "functionality" + reset;
				const stab = (coverage[x].stability ? green : red) + "stability" + reset;
				const secu = (coverage[x].security ? green : red) + "security" + reset;
				console.log(`${func}  ${stab}  ${secu}  ${x}`);
			}
		});
	}
}

main();
