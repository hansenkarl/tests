import { createTest, request, setBeforeEach } from "../request_test";
import { Id } from "@/data/u";
import { createUser } from "../request_utils";
import { nameDirtyInputTest, testDirtyInputRequest, timestamp, verifyRequestAccepted } from "../utils";
import { Checklist, ChecklistForm, ChecklistSection, ChecklistTask } from "@/data/Checklist";

setBeforeEach(async (context) => {
	await request(false);
	await createUser(context, "user");
});

createTest("Can create a new checklist", "functionality", async (context) => {
	const title = "Tee seda!";
	const name = "Tegevus";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	const chklist = { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 1 } as const], items_latest: 1, activation: 1, duration: 1 } };
	const req = await request("chk_new", chklist);
	verifyRequestAccepted(req, "Tried to create a new checklist.");

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	return (checklists[0].title !== title) ? "New checklist was not created properly! Expected title: " + title + " Received title: " + checklists[0].title : true;
});

createTest("Can create a repeating checklist without time manually set", "extra", async (context) => {
	const days = { mon: true, tue: true, wed: true, thu: true, fri: true, sat: true, sun: true };
	const title = "Tee seda!";
	const name = "Tegevus";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	await request("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 1 }], items_latest: 1, activation: 1, duration: 0, schedule: { repeat: "daily", days } } });

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	for (const checklist of checklists) {
		if (checklist.status === "active") {
			if (checklist.schedule) return "Active checklist had an existing schedule!";
		}
		else {
			if (checklist.schedule?.repeat !== "daily") return "New checklist does not repeat daily! Expected repeat schedule: daily, Received repeat schedule: " + checklists[0].schedule?.repeat;
		}
	}

	return true;
});

createTest("Can edit an existing checklist", "functionality", async (context) => {
	const title = "Tee seda!";
	const name = "Tegevus";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	await request("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 1 }], items_latest: 1, activation: timestamp() - 2000, duration: 60 * 60 * 1000 } });
	const checklist = (await request("chk_getOfGroup", { org, grp })).data![0];
	const edited_time = timestamp() + 2000;
	checklist.activation = edited_time;
	checklist.duration = 15 * 60 * 1000;
	const req = await request("chk_edit", { org, doc: checklist });
	verifyRequestAccepted(req, "Tried to edit a checklist.");

	const req_get = await request("chk_get", { org, id: checklist._id });
	verifyRequestAccepted(req_get, "Tried to access modified checklist.");
	if (req_get.data!.status === "active") return "Checklist's status is active!";
	if (req_get.data!.activation !== edited_time) return "Activation time did not change correctly!";
	if (req_get.data!.duration !== checklist.duration) return "Duration did not change correctly!";

	return true;
});

createTest("Server accepts phantom, but does not store it", "extra", async (context) => {
	const title = "Tee seda!";
	const name = "Tegevus";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	const chklist = { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 } as const], items_latest: 0, is_phantom: true, activation: 1, duration: 1 } };
	const req = await request("chk_new", chklist);
	verifyRequestAccepted(req, "Tried to create a new checklist with an is_phantom field.");

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	return (checklists[0].is_phantom !== undefined) ? "Server stored an is_phantom field!" : true;
});

createTest("Can remove whitespaces from checklist's title", "extra", async (context) => {
	let title = " Tee seda!";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	await request("chk_new", { org, doc: { grp, title, items: [], activation: 1, duration: 1, items_latest: 0 } });

	title = "Tee seda! ";
	await request("chk_new", { org, doc: { grp, title, items: [], activation: 1, duration: 1, items_latest: 0 } });

	title = "Tee \n\n\t\t\n seda";
	await request("chk_new", { org, doc: { grp, title, items: [], activation: 1, duration: 1, items_latest: 0 } });

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	if (!checklists[0].title.startsWith("T")) return "The leading whitespace was not removed from checklist's title!";
	if (!checklists[1].title.endsWith("!")) return "The trailing whitespace was not removed from checklist's title!";
	if (checklists[2].title.includes("\n") || checklists[2].title.includes("\t")) return "Newlines were not removed from checklist's title!";

	return true;
});

createTest(nameDirtyInputTest("chk_new"), "stability", async (context) => {
	const days = { mon: true, tue: true, wed: true, thu: true, fri: true, sat: true, sun: true };
	const title = "Tee seda!";
	const name = "Tegevus";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	await request("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days } } });

	let dirty_result = await testDirtyInputRequest("chk_new was accepted with dirty parameters!",
		{ org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 } as const], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days } as const } },
		(dirty_object) => ["chk_new", dirty_object]);
	if (dirty_result !== true) return dirty_result;

	dirty_result = await testDirtyInputRequest("chk_new was accepted with dirty parameters!",
		{ name, section: false, key: 0, val: "new" } as const,
		(dirty_object) => ["chk_new", { org, doc: { grp, title, items: [dirty_object], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days } } }], { key: [123], val: [null, "", 123, true, false, [], {}, undefined] });
	if (dirty_result !== true) return dirty_result;

	dirty_result = await testDirtyInputRequest("chk_new was accepted with dirty parameters!",
		{ repeat: "daily", days } as const,
		(dirty_object) => ["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: dirty_object } }],
		{ võti: [null, undefined] });
	if (dirty_result !== true) return dirty_result;

	dirty_result = await testDirtyInputRequest("chk_new was accepted with dirty parameters!",
		{ repeat: "monthly", day: 1, from_end: false } as const,
		(dirty_object) => ["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: dirty_object } }],
		{ võti: [null, undefined], from_end: [false, true] });
	if (dirty_result !== true) return dirty_result;

	const days_exclusion_map = {} as any;
	for (const day in days) {
		days_exclusion_map[day] = [false, true];
	}
	dirty_result = await testDirtyInputRequest("chk_new was accepted with dirty parameters!", days,
		(dirty_object) => ["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days: dirty_object } } }],
		days_exclusion_map);
	if (dirty_result !== true) return dirty_result;

	await request("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 1, schedule: { repeat: "daily", days } } });

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	const _id = checklists[1]._id;
	const activation = checklists[1].activation;
	const overrides_phantom = { _id, activation };

	dirty_result = await testDirtyInputRequest("chk_new with overrides_phantom accepted dirty input!", overrides_phantom, (dirty_input) =>
		["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation, duration: 2, overrides_phantom: dirty_input } }],
	{ võti: [null, undefined] });
	if (dirty_result !== true) return dirty_result;

	return dirty_result;
});

createTest("Can override checklist phantoms", "functionality", async (context) => {
	const days = { mon: true, tue: true, wed: true, thu: true, fri: true, sat: true, sun: true };
	const title = "Tee seda!";
	const name = "Tegevus";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	await request("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: timestamp() + 100_000, duration: 1, schedule: { repeat: "daily", days } } });

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	const _id = checklists[0]._id;
	const activation = checklists[0].activation;
	const overrides_phantom = { _id, activation };

	await request("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation, duration: 2, overrides_phantom } });

	const req_res = (await request("chk_getOfGroup", { org, grp }));
	verifyRequestAccepted(req_res, "Tried to get all results in group, where overrides_phantom is defined.");

	const checklist = req_res.data!.pop();
	if (checklist!.duration !== 2) return "Checklist duration was not correct. Expected: 2, Received: " + checklist!.duration;
	if (!checklist!.overrides_phantom) return "overrides_phantom does not exist on checklist! Received: " + JSON.stringify(checklist);
	if (checklist!.overrides_phantom._id !== _id) return "overrides_phantom received an incorrect id! Expected: " + _id + ", Received: " + checklist!.overrides_phantom._id;
	if (checklist!.overrides_phantom.activation !== activation) return "overrides_phantom received an incorrect activation! Expected: " + activation + ", Received: " + checklist!.overrides_phantom.activation;

	return true;
});

createTest("Can set checklist item status", "functionality", async (context) => {
	const title = "Tee seda!";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	const chklist = {
		org, doc: {
			grp, title, items: [
				{ name: "tere", val: "new", section: false, key: 1 } as const,
				{ name: "hello", val: "new", section: false, key: 2 } as const,
				{ name: "privjet", val: "new", section: false, key: 3 } as const,
				{
					name: "konnichiwa uwu", val: "new", section: true, key: 4, tasks: [
						{ name: "senpai", val: "new", section: false, key: 5 } as const,
						{ name: "san", val: "new", section: false, key: 6 } as const,
						{ name: "chan", val: "new", section: false, key: 7 } as const,
					], description: ["Japanese gibberish"],
				},
			], items_latest: 1, activation: 1, duration: 1,
		} as ChecklistForm,
	};
	await request("chk_new", chklist);

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	const checklist = checklists[0];

	// initial set item status
	let req = await request("chk_setItemStatus", { org, id: checklist._id, index: 0, status: "done" as const, section: -1 });
	verifyRequestAccepted(req, "Tried to set checklist item status as \"done\".");

	req = await request("chk_setItemStatus", { org, id: checklist._id, index: 1, status: "fail" as const, section: -1 });
	verifyRequestAccepted(req, "Tried to set checklist item status as \"fail\".");

	await request("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "done" as const, section: -1 });
	req = await request("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "new" as const, section: -1 });
	verifyRequestAccepted(req, "Tried to set checklist item status as \"new\".");

	// section
	req = await request("chk_setItemStatus", { org, id: checklist._id, index: 0, status: "done" as const, section: 3 });
	verifyRequestAccepted(req, "Tried to set checklist item status as \"done\" within section.");

	req = await request("chk_setItemStatus", { org, id: checklist._id, index: 1, status: "fail" as const, section: 3 });
	verifyRequestAccepted(req, "Tried to set checklist item status as \"fail\" within section.");

	await request("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "done" as const, section: 3 });
	req = await request("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "new" as const, section: 3 });
	verifyRequestAccepted(req, "Tried to set checklist item status as \"new\" within section.");

	// final check
	const checklist_group = (await request("chk_getOfGroup", { org, grp })).data![0];
	if (checklist_group.items[0]?.val !== "done") return "Could not actually set checklist item value status as \"done\"!";
	if (checklist_group.items[1]?.val !== "fail") return "Could not actually set checklist item value status as \"fail\"!";
	if (checklist_group.items[2]?.val !== "new") return "Could not actually set checklist item value status as \"new\"!";
	if ((checklist_group.items[3] as ChecklistSection)?.tasks[0]?.val !== "done") return "Could not actually set checklist item value status as \"done\" within section!";
	if ((checklist_group.items[3] as ChecklistSection)?.tasks[1]?.val !== "fail") return "Could not actually set checklist item value status as \"fail\" within section!";
	if ((checklist_group.items[3] as ChecklistSection)?.tasks[2]?.val !== "new") return "Could not actually set checklist item value status as \"new\" within section!";

	return true;
});

createTest(nameDirtyInputTest("chk_setItemStatus"), "stability", async (context) => {
	const title = "Tee seda!";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	const chklist = {
		org, doc: {
			grp, title, items: [
				{ name: "task", val: "new", section: false, key: 1 } as const,
				{
					name: "section", val: "new", section: true, key: 2, description: ["section"] as string[],
					tasks: [{ name: "task1", val: "new", section: false, key: 3 }] as ChecklistTask[],
				} as const,
			], items_latest: 1, activation: 1, duration: 1,
		},
	};
	await request("chk_new", chklist);

	const checklists: Checklist[] = (await request("chk_getOfGroup", { org, grp })).data!;
	const checklist = checklists[0];

	let dirty_result = await testDirtyInputRequest(
		"There was a problem trying to set checklist task as completed!",
		{ org, id: checklist._id, index: 0, status: "done" as const, section: -1 },
		(dirty_object) => ["chk_setItemStatus", dirty_object]
	);
	if (dirty_result !== true) return dirty_result;

	dirty_result = await testDirtyInputRequest(
		"There was a problem trying to set checklist task status within section as completed!",
		{ org, id: checklist._id, index: 0, status: "done" as const, section: 1 },
		(dirty_object) => ["chk_setItemStatus", dirty_object]
	);
	return dirty_result;
});
