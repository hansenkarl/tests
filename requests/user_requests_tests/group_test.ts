import { createTest, request, setBeforeEach } from "../request_test";
import { Id } from "@/data/u";
import { createUser } from "../request_utils";
import { shiftTime, timestamp, verifyRequestAccepted } from "../utils";

setBeforeEach(async (context) => {
	await request(false);
	await createUser(context, "user");
});

createTest("Can create a group", "functionality", async (context) => {
	const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
	await request("login", user_obj);

	const name = "my new grp";
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
	const req = await request("grp_new", { org, name });
	verifyRequestAccepted(req, "Tried to create a new grp.");

	const grp = (await request("grp_getOfOrg", { org })).data![0];
	if (grp == undefined) return "Group was actually not created!";
	if (grp.name !== name) return "Group was not named correctly!";

	return true;
});

createTest("Can a group be asked to be deleted", "functionality", async (context) => {
	const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
	await request("login", user_obj);

	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
	await request("grp_new", { org, name: "new grp insta delete" });

	let groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	// mark for deletion
	let req = await request("grp_delete", { org, grp, confirm: true });
	verifyRequestAccepted(req, "Tried to mark a grp to be deleted.");

	groups = (await request("grp_getOfOrg", { org })).data!;
	if (groups[0].mfd < timestamp() + 1.8 * 86_400 * 1000) return "Group was not marked to be deleted at a correct time!\nExpected: " +
		new Date(timestamp() + 1.8 * 86_400 * 1000) + " or later\nReceived: " +
		new Date(groups[0].mfd);

	// unmark for deletion
	req = await request("grp_delete", { org, grp, confirm: false });
	groups = (await request("grp_getOfOrg", { org })).data!;
	verifyRequestAccepted(req, "Tried to cancel a grp from being deleted.");

	if (groups[0].mfd !== 0) return "Group was not unmarked to be deleted!";

	// test actual deletion by hacking time
	req = await request("grp_delete", { org, grp, confirm: true });

	groups = await shiftTime(user_obj, timestamp() + 3 * 86_400 * 1000, async () => (await request("grp_getOfOrg", { org })).data!);
	if (groups[0] != undefined) return "Group was not actually deleted!";

	return true;
});

createTest("Can delete tickets from one group without deleting tickets from other group", "stability", async (context) => {
	const title = "teine probleem";
	const text = "erakordselt teine probleem";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
	await request("grp_new", { org: org_id, name: "new grp insta delete" });
	await request("grp_new", { org: org_id, name: "irrelevant grp" });

	const groups = (await request("grp_getOfOrg", { org: org_id })).data!;
	const grp_id1 = groups[0]._id;
	const grp_id2 = groups[1]._id;

	await request("tkt_new", { doc: { grp: grp_id1, is_global: false, is_priority: false, title: "esimene probleem", text: "erakordselt esimene probleem" }, org: org_id });
	await request("tkt_new", { doc: { grp: grp_id2, is_global: false, is_priority: false, title, text }, org: org_id });

	const req = await request("grp_delete", { org: org_id, grp: grp_id1, confirm: true });
	verifyRequestAccepted(req, "Tried to delete tickets from first group!");

	const req_tkt = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id2 })).data!;
	return (req_tkt[0].title !== title || req_tkt[0].comments[0].text[0] !== text) ? "Deleting a group caused the entries from another group to also be deleted!" : true;
});

createTest("Can rename a group", "functionality", async (context) => {
	const name = "new group";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
	await request("grp_new", { org, name: "new grup" });

	let groups = (await request("grp_getOfOrg", { org })).data!;
	const grp = groups[0]._id;

	const req = await request("grp_rename", { org, grp, name });
	verifyRequestAccepted(req, "Tried to rename a grp.");

	groups = (await request("grp_getOfOrg", { org })).data!;
	const changed = groups[0].name;
	
	return (name !== changed) ? "Request grp_rename did not change the name of the group!\nReceived: " + changed + "\nExpected: " + name : true;
});
