import { createTest, request, setBeforeEach } from "../request_test";
import { Id, NewId } from "@/data/u";
import { createUser } from "../request_utils";
import { testDirtyInputRequest, timestamp, shiftTime, nameDirtyInputTest, verifyRequestAccepted, verifyRequestDenied } from "../utils";

setBeforeEach(async (context) => {
	await request(false);
	await createUser(context, "user");
});

const name = "new org";
const timezone = "Europe/Tallinn";

createTest("Can create a new organization", "functionality", async (context) => {
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });

	const req = await request("org_new", { name, timezone, options: {} });
	verifyRequestAccepted(req, "Tried to create an org.");

	return (req.data == undefined || req.data == NewId) ? "Could not create a new org!": true;
});

createTest("Can access updated problem list while internet hiked", "functionality", async (context) => {
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;

	// can access latest cursors
	let req = await request("org_getLatestCursors", { org });
	verifyRequestAccepted(req, "Tried to get latest cursors.");
	if (req.data!["tkt"] !== undefined) return "Request org_getLatestCursors did not return undefined!" + JSON.stringify(req.data);

	// should open most recently visited org
	const user = (await request("user")).data;
	if (user?.options.last_org !== org) return "User's last org was not set correctly!\nExpected: " + JSON.stringify(org) + "\nReceived: " + JSON.stringify(user?.options.last_org);

	// create a group and insert a ticket there
	await request("grp_new", { org, name: "new grp" });
	const groups = (await request("grp_getOfOrg", { org })).data!;
	const grp_id = groups[0]._id;
	await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "ticket", text: "text" }, org });
	const tkt = (await request("tkt_getOfGroup", { org, grp: grp_id })).data![0];

	// were the cursors correct?
	req = await request("org_getLatestCursors", { org });
	verifyRequestAccepted(req, "Tried to get updated latest cursors.");
	if (req.data!["tkt"] !== tkt._id) return "Request org_getLatestCursors did not return a ticket with correct id!\nExpected: " + tkt._id + "\nReceived: " + JSON.stringify(req.data);

	return true;
});

createTest(nameDirtyInputTest("org_getLatestCursors"), "stability", async (context) => {
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;
	return await testDirtyInputRequest("org_getLatestCursors accepted dirty input!", { org }, (dirty_input) => ["org_getLatestCursors", dirty_input]);
});

createTest(nameDirtyInputTest("org_new"), "stability", async (context) => {
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
	return await testDirtyInputRequest("org_new accepted dirty input!", { name, timezone, options: {} }, (dirty_input) => ["org_new", dirty_input]);
});

createTest("Can access, send, receive and accept invitations", "functionality", async (context) => {
	const user0 = context + "@sharklasers.com";
	const user1 = context + "1@sharklasers.com";

	await createUser(context + 1, "user");
	await request("login", { usr: user0, pwd: "Password123" });

	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;

	// non-existing invites
	let invitations = await request("org_getInvitations", { org });
	verifyRequestAccepted(invitations, "Tried to get non-existing invites.");

	let req = await request("invite", { org, email: user1 });
	verifyRequestAccepted(req, "Tried to send an invite.");

	invitations = await request("org_getInvitations", { org });
	verifyRequestAccepted(invitations, "Tried to get sent existing invites.");
	await request("logout");

	await request("login", { usr: user1, pwd: "Password123" });
	req = (await request("getInvitations"));
	verifyRequestAccepted(req, "Tried to get received existing invites.");

	req = await request("acceptInvitation", { inv: invitations.data![0]._id });
	verifyRequestAccepted(req, "Tries to accept invitations.");
	
	return true;
});

createTest("Can kick a user from an org", "functionality", async (context) => {
	const user0 = context + "@sharklasers.com";
	const user1 = context + "1@sharklasers.com";

	await createUser(context + 1, "user");
	await request("login", { usr: user0, pwd: "Password123" });

	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;
	await request("org_getInvitations", { org });
	await request("invite", { org, email: user1 });
	await request("logout");
	await request("login", { usr: user1, pwd: "Password123" });
	const pfl = (await request("user")).data!;

	const invitations = (await request("getInvitations"));
	await request("acceptInvitation", { inv: invitations.data![0]._id });
	await request("logout");
	await request("login", { usr: user0, pwd: "Password123" });

	const req = await request("org_kick", { org, user: pfl._id });
	verifyRequestAccepted(req, "Tried to kick a user from an org.");

	const cur_org = (await request("getOrgs")).data![0];
	if (cur_org.auth[pfl._id] !== undefined) return "User was not correctly removed from org auth table!";
	if (cur_org.users.includes(pfl._id)) return "User was not correctly removed from org users list!";

	else return true;
});

createTest(nameDirtyInputTest("org_kick"), "stability", async (context) => {
	const user0 = context + "@sharklasers.com";
	const user1 = context + "1@sharklasers.com";

	await createUser(context + 1, "user");
	await request("login", { usr: user0, pwd: "Password123" });

	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;

	await request("org_getInvitations", { org });
	await request("invite", { org, email: user1 });
	await request("logout");
	await request("login", { usr: user1, pwd: "Password123" });
	const pfl = (await request("user")).data!;

	const invitations = (await request("getInvitations"));

	await request("acceptInvitation", { inv: invitations.data![0]._id });
	await request("logout");
	await request("login", { usr: user0, pwd: "Password123" });

	return testDirtyInputRequest("Org_kick accepted dirty input!", { org, user: pfl._id }, (dirty_object) => ["org_kick", dirty_object]);
});

createTest(nameDirtyInputTest("invite, org_getInvitations and acceptInvitation"), "stability", async (context) => {
	const user0 = context + "@sharklasers.com";
	const user1 = context + "1@sharklasers.com";

	await createUser(context + 1, "user");
	await request("login", { usr: user0, pwd: "Password123" });

	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;
	let dirty_result = await testDirtyInputRequest("Invite, which included dirty input was sent!", { org, email: user1 }, (dirty_object) => ["invite", dirty_object]);
	if (dirty_result !== true) return dirty_result;

	let req = await request("invite", { org, email: user1 });
	req = await request("invite", { org, email: user1 });
	if (req.ok) return "Can create multiple invitations for one user!";

	dirty_result = await testDirtyInputRequest("Request, which gets all invitations, accepted dirty input!", { org }, (dirty_object) => ["org_getInvitations", dirty_object]);
	if (dirty_result !== true) return dirty_result;

	await request("logout");

	await request("login", { usr: user1, pwd: "Password123" });
	const req2 = (await request("getInvitations"));

	dirty_result = await testDirtyInputRequest("Invitation was accepted with dirty input!", { inv: req2.data![0]._id }, (dirty_object) => ["acceptInvitation", dirty_object]);
	if (dirty_result !== true) return dirty_result;

	return true;
});

createTest("Can rename an org", "functionality", async (context) => {
	const new_org_name = "renamed org";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });

	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;
	const req = await request("org_rename", { org, name: new_org_name });
	verifyRequestAccepted(req, "Tried to rename an org.");

	const renamed = (await request("getOrgs")).data![0];
	if (renamed.name !== new_org_name) return "The org was not renamed properly! Current org name: " + renamed.name;

	return true;
});

createTest(nameDirtyInputTest("org_rename"), "stability", async (context) => {
	const new_org_name = "renamed org";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;
	return await testDirtyInputRequest("org_rename accepted dirty input!", { org, name: new_org_name }, (dirty_object) => ["org_rename", dirty_object]);
});

createTest("Can remove whitespaces from org name", "extra", async (context) => {
	let new_org_name = " renamed org";

	await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });

	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;

	await request("org_rename", { org, name: new_org_name });

	let renamed = (await request("getOrgs")).data![0];
	if (renamed.name.startsWith(" ")) return "The leading whitespace was not removed from org name!";

	new_org_name = "renamed org ";
	renamed = (await request("getOrgs")).data![0];
	if (renamed.name.endsWith(" ")) return "The trailing whitespace was not removed from org name!";

	new_org_name = "\n\n\t\nrenamed \t\n\torg\t\n\t\n";
	renamed = (await request("getOrgs")).data![0];
	if (renamed.name.includes("\n") || renamed.name.includes("\t")) return "Newlines and tabs were not removed from text!";
	else return true;
});


createTest("Can create, set and access the notepad", "functionality", async (context) => {
	const name = "new org";
	const timezone = "Europe/Tallinn";

	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;

	const req_new_notepad = await request("usr_newNotepad", { org });
	verifyRequestAccepted(req_new_notepad, "Tried to create a new notepad.");

	let req_get_notepad = await request("usr_getNotepads", { org });
	verifyRequestAccepted(req_get_notepad, "Tried to access user's notepads.");

	if (req_get_notepad.data![0]?.name !== "") return "Could not get correct notepad!";

	const req_set_notepad = await request("usr_setNotepad", {
		_id: req_get_notepad.data![0]._id,
		org: req_get_notepad.data![0].org,
		name: "Main Notes",
		content: "My notes",
	});
	verifyRequestAccepted(req_set_notepad, "Tried to set user's notepads.");

	req_get_notepad = await request("usr_getNotepads", { org });
	if (req_get_notepad.data![0]?.name !== "Main Notes" as any) return "Could not set content properly!";
	if (req_get_notepad.data![0]?.content !== "My notes") return "Could not set content properly!";

	return true;
});

createTest(nameDirtyInputTest("usr_newNotepad, usr_getNotepads and usr_setNotepad"), "stability", async (context) => {
	const name = "new org";
	const timezone = "Europe/Tallinn";

	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
	const org = (await request("org_new", { name, timezone, options: {} })).data as Id;

	await request("usr_newNotepad", { org });
	let dirty_input = await testDirtyInputRequest("Usr_newNotepad accepted dirty input!", { org }, (dirty_object) => ["usr_newNotepad", dirty_object]);
	if (dirty_input !== true) return dirty_input;

	const req_get_notepad = (await request("usr_getNotepads", { org })).data![0];

	dirty_input = await testDirtyInputRequest("Usr_getNotepads accepted dirty input!", { org }, (dirty_object) => ["usr_getNotepads", dirty_object]);
	if (dirty_input !== true) return dirty_input;

	dirty_input = await testDirtyInputRequest("Usr_setNotepad accepted dirty input!", {
		_id: req_get_notepad._id,
		org: req_get_notepad.org,
		name: "Main Notes",
		content: "My notes",
	}, (dirty_object) => ["usr_setNotepad", dirty_object], { content: [""] });
	return dirty_input;
});

createTest("Can an org ask to be deleted", "functionality", async (context) => {
	const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
	await request("login", user_obj);

	const org = (await request("org_new", { name, timezone, options: {} })).data!;

	// mark for deletion
	let req = await request("org_delete", { org, confirm: true });
	verifyRequestAccepted(req, "Tried to mark org to be deleted.");

	let tbd_org = (await request("getOrgs")).data![0];
	if (tbd_org.mfd < timestamp() + 27 * 86_400 * 1000) return "Org was not marked to be deleted at a correct time!\nExpected: " + new Date(timestamp() + 27 * 86_400 * 1000) + " or later\nReceived: " + new Date(tbd_org.mfd);

	// unmark for deletion
	req = await request("org_delete", { org, confirm: false });
	verifyRequestAccepted(req, "Tried to cancel org from being deleted.");

	tbd_org = (await request("getOrgs")).data![0];
	if (tbd_org.mfd !== 0) return "Org was not unmarked to be deleted!";

	// test actual deletion by hacking time
	req = await request("org_delete", { org, confirm: true });

	tbd_org = await shiftTime(user_obj, timestamp() + 32 * 86_400 * 1000, async () => (await request("getOrgs")).data![0]);
	if (tbd_org != undefined) return "Org was not actually deleted!";
	return true;
});
