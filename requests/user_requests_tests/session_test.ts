import { User } from "@/data/User";
import { admin_pwd, admin_usr_name, test_key } from "../config";
import { createTest, request, setBeforeEach } from "../request_test";
import { createUser } from "../request_utils";
import { testDirtyInputRequest, shiftTime, timestamp, nameDirtyInputTest, verifyRequestDenied, verifyRequestAccepted } from "../utils";

const fname = "user";
const pwd = "Password123";
const lname = "lastname";

setBeforeEach(async () => {
	await request(false);
});

createTest("Can register a new user", "functionality", async (context) => {
	const req = await createUser(context, fname);
	verifyRequestAccepted(req, "Tried to register a regular user.");
	return true;
});

createTest("Can log into site", "functionality", async (context) => {
	await createUser(context, "user");
	const req = await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });
	verifyRequestAccepted(req, "Tried to log in as a regular user.");
	return true;
});

createTest("Can download user profile", "functionality", async (context) => {
	await createUser(context, "user");
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });
	const req = await request("user");
	verifyRequestAccepted(req, "Tried to download user profile.");
	return req.data?.pfl.dname ? true : "Could not get user profile!";
});

createTest("Can log out", "functionality", async (context) => {
	await createUser(context, "user");
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });
	const req = await request("logout");
	verifyRequestAccepted(req, "Tried to log out.");

	const req_user = await request("user");
	return !req_user.data?.pfl.dname ? true : "Could not actually logout!";
});

createTest("Can switch users", "extra", async (context) => {
	await createUser(context + 1, "user1");
	await createUser(context + 2, "user2");
	await request("login", { usr: context + 1 + "@sharklasers.com", pwd });

	const pfl1 = (await request("user")).data!.pfl.dname;
	await request("logout");

	await request("login", { usr: context + 2 + "@sharklasers.com", pwd });
	const pfl2 = (await request("user")).data!.pfl.dname;
	if (pfl1 === pfl2) return "Switching user profiles by logging out as the old user and logging in as the new user failed!";
	return true;
});

createTest("Can not log in with wrong password", "security", async (context) => {
	const req = await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "hretggwiotygjod" });
	verifyRequestDenied(req, "Tried to log in with a wrong password.");
	return true;
});

createTest("Can not register with bad password", "security", async (context) => {
	const req = await request("register", { email: context + "@sharklasers.com", pwd: "123", key: "a", fname: "sneakyuser", lname: "sneak" });
	verifyRequestDenied(req, "Tried to register with a bad (short) password!");
	return true;
});

createTest(nameDirtyInputTest("register"), "stability", async (context) => {
	const pwd = "Password123";

	let req = await request("register", { fname: "sneakyuser", lname: "sneak", email: context + "@.", pwd, key: "a" });
	verifyRequestDenied(req, "Tried to register with email: @.");
	req = await request("register", { fname: "sneakyuser", lname: "sneak", email: context, pwd, key: "a" });
	verifyRequestDenied(req, "Tried to register with email containing only context.");
	req = await request("register", { fname: "sneakyuser", lname: "sneak", email: "", pwd, key: "a" });
	verifyRequestDenied(req, "Tried to register with email containing empty string.");
	req = await request("register", { fname: "sneakyuser", lname: "sneak", email: "@.", pwd, key: "a" });
	verifyRequestDenied(req, "User could register with email containing only @.");
	req = await request("register", { fname: "sneakyuser", lname: "sneak", email: "12345", pwd, key: "a" });
	verifyRequestDenied(req, "User could register with email containing only 12345.");
	
	return await testDirtyInputRequest("register accepted dirty input!", { email: context + "@sharklasers.com", pwd, fname, lname, key: "a" }, (dirty_input) => ["register", dirty_input]);
});

createTest("Can change password", "functionality", async (context) => {
	await createUser(context, "user");
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });

	let req = await request("changePassword", { old_pwd: pwd, new_pwd: "Qwerty123" }) as any;
	verifyRequestAccepted(req, "Tried to change password.");
	await request("logout") as any;

	req = await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });
	if (req.ok) return "Could login with old password.";

	req = await request("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Qwerty123" });
	if (!req.ok) return "Could not login with new (changed) password.";

	return true;
});

createTest(nameDirtyInputTest("changePassword"), "stability", async (context) => {
	await createUser(context, "user");
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });
	return await testDirtyInputRequest("changePassword accepted dirty input!", { old_pwd: pwd, new_pwd: "Qwerty123" }, (dirty_input) => ["changePassword", dirty_input]);
});

createTest("Password encryption method in use produces different output each time", "extra", async (context) => {
	await request("register", { email: context + 1 + "@sharklasers.com", pwd, fname: "firstname", lname: "lastname", key: "a" });
	await request("register", { email: context + 2 + "@sharklasers.com", pwd, fname: "firstname", lname: "lastname", key: "a" });

	await request("login", { admin: true, usr: admin_usr_name, pwd: admin_pwd });

	const users = (await request("get", { table: "users" })).data as User[];
	if (users[0].acc.pwd !== users[1].acc.pwd) return true;
	return "Password encryption method produced two same passwords!";
});

createTest("Can change names", "functionality", async (context) => {
	await createUser(context, "firstname");
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });

	let req = await request("changeNames", { fname: "Nimi", lname: "lastname", dname: "firstname" });
	verifyRequestAccepted(req, "Tried to change firstname.");

	req = await request("changeNames", { fname: "Nimi", lname: "Nimeste", dname: "firstname" });
	verifyRequestAccepted(req, "Tried to change lastname.");

	req = await request("changeNames", { fname: "Nimi", lname: "Nimeste", dname: "Minu nimi" });
	verifyRequestAccepted(req, "Tried to change displayname.");

	const user_info = (await request("user")).data;
	if (user_info?.pfl.fname !== "Nimi") return "Could not change firstname!";
	if (user_info?.pfl.lname !== "Nimeste") return "Could not change lastname!";
	if (user_info?.pfl.dname !== "Minu nimi") return "Could not change displayname!";

	return true;
});

createTest(nameDirtyInputTest("changeNames"), "stability", async (context) => {
	await createUser(context, "firstname");
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });
	return await testDirtyInputRequest("changeNames accepted dirty input!", { fname: "firstname", lname: "lastname", dname: "firstname" }, (dirty_input) => ["changeNames", dirty_input]);
});

createTest("Can remove whitespaces from user's names", "extra", async (context) => {
	let fname = " Nimi";
	let lname = " Nimeste";
	let dname = " Minu nimi";
	await createUser(context, "firstname");
	await request("login", { admin: false, usr: context + "@sharklasers.com", pwd });
	await request("changeNames", { fname, lname, dname });
	let user_info = (await request("user")).data;
	if (!user_info?.pfl.fname.startsWith("N")) return "The leading whitespace was not removed from firstname!";
	if (!user_info?.pfl.lname.startsWith("N")) return "The leading whitespace was not removed from lastname!";
	if (!user_info?.pfl.dname.startsWith("M")) return "The leading whitespace was not removed from displayname!";

	fname = "Nimi ";
	lname = "Nimeste ";
	dname = "Minu nimi ";
	await request("changeNames", { fname, lname, dname });
	user_info = (await request("user")).data;
	if (!user_info?.pfl.fname.endsWith("i")) return "The trailing whitespace was not removed from firstname!";
	if (!user_info?.pfl.lname.endsWith("e")) return "The trailing whitespace was not removed from lastname!";
	if (!user_info?.pfl.dname.endsWith("i")) return "The trailing whitespace was not removed from displayname!";

	fname = "\n\n\t\tNi\t\nm\t\ni\n\n\t\t";
	lname = "\n\n\t\tNi\t\nmes\t\nte\n\n\t\t ";
	dname = "\n\n\t\tMi\t\nnu \t\nnimi\n\n\t\t ";
	await request("changeNames", { fname, lname, dname });
	user_info = (await request("user")).data;
	if (user_info?.pfl.fname.includes("\n") && user_info?.pfl.fname.includes("\t")) return "Tabs and newlines were not removed from firstname!";
	if (user_info?.pfl.lname.includes("\n") && user_info?.pfl.lname.includes("\t")) return "Tabs and newlines were not removed from lastname!";
	if (user_info?.pfl.dname.includes("\n") && user_info?.pfl.dname.includes("\t")) return "Tabs and newlines were not removed from displayname!";

	return true;
});

createTest("Can a user ask to be deleted", "functionality", async (context) => {
	await createUser(context, "user");
	const user_obj = { usr: context + "@sharklasers.com", pwd };
	await request("login", user_obj);

	// mark for deletion
	let req = await request("usr_delete", { confirm: true });
	verifyRequestAccepted(req, "Tried to mark user for deletion.");

	let user = (await request("user")).data;
	if (user!.mfd < timestamp() + 27 * 86_400 * 1000) return "User was not marked to be deleted at a correct time!\nExpected: " + new Date(timestamp() + 27 * 86_400 * 1000) + " or later\nReceived: " + new Date(user!.mfd);

	// unmark for deletion
	req = await request("usr_delete", { confirm: false });
	verifyRequestAccepted(req, "Tried to cancel user deletion.");

	user = (await request("user")).data;
	if (user!.mfd !== 0) return "User was not unmarked to be deleted!";

	// test actual deletion by hacking time
	req = await request("usr_delete", { confirm: true });

	const del_log_in = await shiftTime(user_obj, timestamp() + 32 * 86_400 * 1000, async () => await request("login", { admin: false, usr: context + "@sharklasers.com", pwd }));
	if (del_log_in.ok) return "Could login as a deleted user.";

	return true;
});

createTest("User can get their wskey", ["functionality", "stability"], async (context) => {
	await createUser(context, "user");
	const user_obj = { usr: context + "@sharklasers.com", pwd };
	await request("login", user_obj);
	const req = await request("wskey");
	verifyRequestAccepted(req, "Tried to get user's wskey.");
	return typeof req.data![0] !== "string" ? "Wskey was not of type string!" : true;
});

createTest("Every user gets a unique wskey assigned to them", "security", async (context) => {
	await createUser(context, "user");
	let user_obj = { usr: context + "@sharklasers.com", pwd };
	await request("login", user_obj);
	const req1 = await request("wskey");
	await request("logout");

	await createUser(context + 1, "user");
	user_obj = { usr: context + 1 + "@sharklasers.com", pwd };
	await request("login", user_obj);
	const req2 = await request("wskey");
	verifyRequestAccepted(req2, "Tried to get second user's wskey.");

	if (req1.data === req2.data) return "Wskeys of two different sessions were the same!";
	return true;
});
