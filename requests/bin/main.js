import {createRequire} from "module";const require=createRequire(import.meta.url);
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/core/RequestValidator.ts":
/*!**************************************!*\
  !*** ./src/core/RequestValidator.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RequestValidator": () => (/* binding */ RequestValidator)
/* harmony export */ });
/* harmony import */ var _core_u__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/core/u */ "./src/core/u.ts");
/* harmony import */ var _data_User__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/data/User */ "./src/data/User.ts");
/* harmony import */ var _data_Checklist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/data/Checklist */ "./src/data/Checklist.ts");
/* harmony import */ var email_validator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! email-validator */ "email-validator");
/* harmony import */ var email_validator__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(email_validator__WEBPACK_IMPORTED_MODULE_3__);




const vvMap = "map";
const vvArray = "array";
const vvId = "id";
const vvStringAny = "string, any";
const vvStringAnyArray = "array of any strings";
const vvStringNonEmpty = "string, non-empty";
const vvStringNonEmptyArray = "array of non-empty strings";
const vvNumber = "number";
const vvBoolean = "boolean";
const marker_optional = "null or undefined or ";
const vvOptional = (x) => `${marker_optional}${x}`;
function TypeCheck(q, type_template) {
    if (!(0,_core_u__WEBPACK_IMPORTED_MODULE_0__.isRecord)(q))
        return false;
    let is_v = false;
    for (const key in type_template) {
        const x = q[key];
        const type_or_template = type_template[key];
        let type = typeof type_or_template == "string"
            ? type_or_template
            : vvMap;
        if (type.startsWith(marker_optional)) {
            if (x === undefined || x === null)
                is_v = true;
            else
                type = type.split(marker_optional)[1];
        }
        switch (type) {
            case vvMap:
                {
                    is_v = (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.isRecord)(x)
                        && (type == type_or_template || TypeCheck(x, type_or_template));
                }
                break;
            case vvStringAny:
                {
                    is_v = typeof x == "string";
                }
                break;
            case vvStringNonEmpty:
                {
                    is_v = typeof x == "string"
                        && x.trim() != "";
                }
                break;
            case vvId:
                {
                    is_v = typeof x == "string"
                        && x.startsWith("x");
                }
                break;
            case vvBoolean:
                {
                    is_v = typeof x == "boolean";
                }
                break;
            case vvNumber:
                {
                    is_v = typeof x == "number";
                }
                break;
            case vvArray:
                {
                    is_v = Array.isArray(x);
                }
                break;
            case vvStringAnyArray:
                {
                    is_v = Array.isArray(x)
                        && x.every((item) => typeof item == "string");
                }
                break;
            case vvStringNonEmptyArray:
                {
                    is_v = Array.isArray(x)
                        && x.every((item) => typeof item == "string" && item.trim() != "");
                }
                break;
            default: { }
        }
        if (!is_v)
            return false;
    }
    return true;
}
const VOID_INPUT = () => true;
const RequestValidator = {
    getCssVersion: VOID_INPUT,
    changePassword: (q) => {
        return ( true
            && typeof q.old_pwd == "string"
            && (0,_data_User__WEBPACK_IMPORTED_MODULE_1__.validatePassword)(q.new_pwd));
    },
    changeNames: (q) => {
        return ( true
            && typeof q.fname == "string"
            && typeof q.lname == "string"
            && typeof q.dname == "string"
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.fname).length > 0
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.lname).length > 0
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.dname).length > 0);
    },
    login: VOID_INPUT,
    logout: VOID_INPUT,
    register: (q) => {
        return ( true
            && typeof q.email == "string"
            && (0,email_validator__WEBPACK_IMPORTED_MODULE_3__.validate)(q.email)
            && typeof q.fname == "string"
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.fname).length >= 1
            && q.fname.length <= 16
            && typeof q.lname == "string"
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.lname).length >= 1
            && q.lname.length <= 16
            && (0,_data_User__WEBPACK_IMPORTED_MODULE_1__.validatePassword)(q.pwd));
    },
    feedback: (q) => (TypeCheck(q, {
        text: vvStringNonEmpty,
    })),
    acceptInvitation: (q) => (TypeCheck(q, {
        inv: vvId,
    })),
    org_delete: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    getInvitations: VOID_INPUT,
    getOrgs: VOID_INPUT,
    org_getUsers: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    org_getInvitations: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    grp_setAuthLvl: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
        user: vvId,
        lvl: vvNumber,
    })),
    invite: (q) => (TypeCheck(q, {
        org: vvId,
        email: vvStringAny,
    })
        && (0,email_validator__WEBPACK_IMPORTED_MODULE_3__.validate)(q.email)),
    //#region User
    user: VOID_INPUT,
    wskey: VOID_INPUT,
    usr_delete: (q) => (TypeCheck(q, {
        confirm: vvBoolean,
    })),
    usr_setOptions: (q) => (TypeCheck(q, {
        problems__sort_by_last_post: vvOptional(vvBoolean),
        problems__sort_unread_first: vvOptional(vvBoolean),
    })),
    usr_getNotepads: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    usr_setNotepad: (q) => (TypeCheck(q, {
        org: vvId,
        _id: vvId,
        name: vvStringNonEmpty,
        content: vvStringAny,
    })),
    usr_deleteNotepad: (q) => (TypeCheck(q, {
        org: vvId,
        _id: vvId,
    })),
    usr_newNotepad: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    //#endregion
    //#region Groups
    grp_new: (q) => {
        return ( true
            && typeof q.org == "string"
            && typeof q.name == "string"
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.name).length > 0);
    },
    grp_delete: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
    })),
    grp_getOfOrg: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    grp_rename: (q) => {
        return ( true
            && typeof q.org == "string"
            && typeof q.name == "string"
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.name).length > 0);
    },
    grp_setOptions: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
        options: vvMap,
    })
        && ["null", "undefined", "number"].includes(typeof q.options.problem_sleep_delay)),
    //#endregion
    //#region Checklists
    chk_new: (q) => (TypeCheck(q, {
        org: vvId,
        doc: {
            title: vvStringNonEmpty,
            grp: vvId,
            duration: vvNumber,
            activation: vvNumber,
        },
    })
        && (q.doc.schedule === undefined || typeof q.doc.schedule == "object")
        && q.doc.title.trim().length >= 1
        && Array.isArray(q.doc.items)
        && q.doc.items.every(x => TypeCheck(x, { name: vvStringNonEmpty, section: vvBoolean, key: vvNumber }))
        && q.doc.items
            .filter((x) => x.section === true)
            .every(x => (TypeCheck(x, { description: vvStringNonEmptyArray, tasks: vvArray })
            && x.tasks.every(y => TypeCheck(y, { name: vvStringNonEmpty, section: vvBoolean, key: vvNumber }))))
        && (q.doc.schedule == undefined || ( true
            && typeof q.doc.schedule == "object"
            && ( false
                || ( true
                    && q.doc.schedule.repeat == "daily"
                    && q.doc.schedule.days
                    && typeof q.doc.schedule.days == "object"
                    && Object.keys(q.doc.schedule.days).length == _data_Checklist__WEBPACK_IMPORTED_MODULE_2__.days.length
                    && !_data_Checklist__WEBPACK_IMPORTED_MODULE_2__.days.some(day => typeof q.doc.schedule.days[day] != "boolean"))
                || ( true
                    && q.doc.schedule.repeat == "monthly"
                    && typeof q.doc.schedule.from_end == "boolean"
                    && typeof q.doc.schedule.day == "number"
                    && q.doc.schedule.day >= 1
                    && q.doc.schedule.day <= 31))))
        && (q.doc.overrides_phantom == undefined || ( true
            && typeof q.doc.overrides_phantom == "object"
            && typeof q.doc.overrides_phantom._id == "string"
            && q.doc.overrides_phantom._id != ""
            && typeof q.doc.overrides_phantom.activation == "number"))),
    chk_get: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
    })),
    chk_appendTask: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        section: vvNumber,
        name: vvStringNonEmpty,
    })),
    chk_edit: (q) => {
        return ( true
            && typeof q.org == "string"
            && q.doc.title.trim().length >= 1);
    },
    chk_getOfOrg: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    chk_setItemStatus: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        section: vvNumber,
        index: vvNumber,
        status: vvStringAny,
    })
        && q.section >= -1
        && q.index > -1
        && ["new", "done", "fail"].includes(q.status)),
    chk_lock: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
    })),
    chk_getOfGroup: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
    })),
    //#endregion
    //#region Orgs
    org_new: (q) => (TypeCheck(q, {
        name: vvStringNonEmpty,
        timezone: vvStringAny,
        options: vvMap,
    })
        && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.name).length > 1),
    org_kick: (q) => (TypeCheck(q, {
        org: vvId,
        user: vvId,
    })),
    org_setAuthLvl: (q) => (TypeCheck(q, {
        org: vvId,
        user: vvId,
        lvl: vvNumber,
    })
        && [1, 5].includes(q.lvl)),
    org_rename: (q) => {
        return ( true
            && typeof q.org == "string"
            && typeof q.name == "string"
            && (0,_core_u__WEBPACK_IMPORTED_MODULE_0__.removeNameWhitespace)(q.name).length > 0);
    },
    org_setOptions: (q) => {
        return ( true
            && typeof q.org == "string"
            && q.options
            && typeof q.options == "object"
            && (q.options.use_display_names === undefined || typeof q.options.use_display_names == "boolean"));
    },
    org_getLatestCursors: (q) => (TypeCheck(q, {
        org: "id",
    })),
    //#endregion
    //#region Chat
    chat_load: (q) => (true),
    chat_post: (q) => (TypeCheck(q, {
        org: vvId,
        channel: vvStringNonEmpty,
        is_dm: vvBoolean,
        txt: vvStringNonEmpty,
    })),
    //#endregion
    //#region Problems
    tkt_new: (q) => (TypeCheck(q, {
        org: vvId,
        doc: {
            title: vvStringNonEmpty,
            grp: vvId,
            is_global: vvBoolean,
            is_priority: vvBoolean,
        },
    })
        && typeof q.doc.text == "string"
        && typeof q.doc.title == "string"
        && q.doc.title.trim() != ""),
    tkt_addReply: (q) => (TypeCheck(q, {
        id: vvId,
        org: vvId,
        text: vvStringNonEmpty,
    })),
    tkt_getOfGroup: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
    })),
    tkt_getOfMeta: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvStringNonEmpty,
    })),
    tkt_getOfOrg: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    tkt_modify: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        options: {
            is_global: vvOptional(vvBoolean),
            is_priority: vvOptional(vvBoolean),
            is_resolved: vvOptional(vvBoolean),
        },
    })),
    tkt_rename: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        name: vvStringNonEmpty,
    })),
    tkt_read: (q) => (TypeCheck(q, {
        org: vvId,
    })
        && (TypeCheck(q, { grp: vvId }) || TypeCheck(q, { tkt: vvId, key: vvNumber }))),
    //#endregion
};


/***/ }),

/***/ "./src/core/u.ts":
/*!***********************!*\
  !*** ./src/core/u.ts ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "removeNameWhitespace": () => (/* binding */ removeNameWhitespace),
/* harmony export */   "isRecord": () => (/* binding */ isRecord)
/* harmony export */ });
/* unused harmony exports log, sleep, random, timestamp, removeLastNewline, sh, ash, streamToString, date, removeFromArray, asParagraphArray, copyOptionalFields, testing_setServerTime */
/* harmony import */ var shelljs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! shelljs */ "shelljs");
/* harmony import */ var shelljs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(shelljs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var luxon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! luxon */ "luxon");
/* harmony import */ var luxon__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(luxon__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _sinonjs_fake_timers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sinonjs/fake-timers */ "@sinonjs/fake-timers");
/* harmony import */ var _sinonjs_fake_timers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_sinonjs_fake_timers__WEBPACK_IMPORTED_MODULE_2__);



function log(x) {
    const faint = "\x1b[2m";
    const reset = "\x1b[0m";
    let line = new Error().stack?.split("\n")[2].split("at ")[1];
    if (typeof x == "string") {
        const parts = line.split(".ts");
        line = parts[0].slice(parts[0].lastIndexOf("/") + 1) + ".ts";
        line += parts[1].slice(0, parts[1].lastIndexOf(":"));
        console.log(faint + line + " " + reset + x);
    }
    else
        console.log([line, x]);
}
function sleep(ms) { return new Promise(r => setTimeout(() => { r(true); }, ms)); }
function random(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }
function timestamp() { return new Date().getTime(); }
function removeLastNewline(str) {
    if (str.lastIndexOf("\n") > 0) {
        return str.substring(0, str.lastIndexOf("\n"));
    }
    else {
        return str;
    }
}
function sh(cmd, wont_fail) {
    const result = (0,shelljs__WEBPACK_IMPORTED_MODULE_0__.exec)(cmd);
    if (!wont_fail && result.code != 0) {
        console.log("+++ Tried to run sh command, but it failed.");
        console.log("Command line: " + cmd);
        console.log("Exit code: " + result.code);
        console.log("+++ Stdout:");
        console.log(result.stdout);
        console.log("+++ Stderr:");
        console.log(result.stderr);
        console.log("+++ Stacktrace:");
        console.trace();
        throw new Error("Error: sh failed!");
    }
    return result;
}
async function ash(cmd, wont_fail) {
    const result = await (0,shelljs__WEBPACK_IMPORTED_MODULE_0__.exec)(cmd, { async: true });
    if (!wont_fail && result.exitCode != 0) {
        console.log("+++ Tried to run sh command, but it failed.");
        console.log("Command line: " + cmd);
        console.log("Exit code: " + result.exitCode);
        console.log("+++ Stdout:");
        console.log(streamToString(result.stdout));
        console.log("+++ Stderr:");
        console.log(streamToString(result.stderr));
        console.log("+++ Stacktrace:");
        console.trace();
        throw new Error("Error: async sh failed!");
    }
    return result;
}
function streamToString(stream) {
    const chunks = [];
    return new Promise((resolve, reject) => {
        stream.on("data", chunk => chunks.push(chunk));
        stream.on("error", reject);
        stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
    });
}
function date(time) {
    if (typeof time != "number")
        return "invalid timestamp";
    const t = luxon__WEBPACK_IMPORTED_MODULE_1__.DateTime.fromMillis(time);
    function z(n) { return (n + "").padStart(2, "0"); }
    return `${t.year}-${z(t.month)}-${z(t.day)} ${z(t.hour)}:${z(t.minute)}`;
}
function removeFromArray(array, item) {
    const index = array.indexOf(item);
    if (index > -1) {
        array.splice(index, 1);
    }
}
function asParagraphArray(text) {
    const r = text.trim().split("\n").map(x => x.trim()).filter(x => x != "");
    if (r.length)
        return r;
    else
        return [""];
}
function removeNameWhitespace(name) {
    return name.trim().replaceAll("\n", "").replaceAll("\t", "");
}
function isRecord(x) {
    return typeof x == "object" && x != null && !Array.isArray(x);
}
function copyOptionalFields(from, to, keys) {
    for (const key of keys)
        if (from[key] != undefined)
            to[key] = from[key];
}
const testing_setServerTime = (x) => {
    testing_setServerTime.clock?.uninstall();
    if (x != "reset")
        testing_setServerTime.clock = _sinonjs_fake_timers__WEBPACK_IMPORTED_MODULE_2__.install({ now: x, shouldAdvanceTime: true });
};


/***/ }),

/***/ "./src/data/Chat.ts":
/*!**************************!*\
  !*** ./src/data/Chat.ts ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "asDMChannel": () => (/* binding */ asDMChannel)
/* harmony export */ });
function asDMChannel(users) {
    return users.sort().join("+");
}


/***/ }),

/***/ "./src/data/Checklist.ts":
/*!*******************************!*\
  !*** ./src/data/Checklist.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "days": () => (/* binding */ days)
/* harmony export */ });
/* unused harmony exports dates, getChecklistCompletion, getChecklistNextRepeat */
/* harmony import */ var luxon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! luxon */ "luxon");
/* harmony import */ var luxon__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(luxon__WEBPACK_IMPORTED_MODULE_0__);

const days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
const dates = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
function getChecklistCompletion(x) {
    let total = 0;
    let completed = 0;
    for (const item of x.items) {
        if (item.section) {
            total += item.tasks.length;
            for (const task of item.tasks)
                if (task.val != "new")
                    completed += 1;
        }
        else {
            total++;
            if (item.val != "new")
                completed += 1;
        }
    }
    const fraction = Math.min(completed / (total || 1), 1);
    return { completed, total, fraction, percent: Math.floor(fraction * 100) };
}
function getChecklistNextRepeat(x, timezone) {
    if (x.schedule) {
        if (x.schedule.repeat == "daily") {
            const day_array = [...days, ...days];
            const day = luxon__WEBPACK_IMPORTED_MODULE_0__.DateTime.fromMillis(x.activation, { zone: timezone });
            const week_start_pos = day.weekday;
            for (let i = week_start_pos; i <= week_start_pos + 6; i++) {
                if (x.schedule.days[day_array[i]])
                    return day.plus({ days: i - week_start_pos + 1 }).toMillis();
            }
            return 0;
        }
        else {
            const getTargetDay = (in_month) => {
                const { from_end, day } = x.schedule;
                return (from_end
                    ? Math.max(1, in_month.daysInMonth - day)
                    : Math.min(in_month.daysInMonth, day));
            };
            const old_month = luxon__WEBPACK_IMPORTED_MODULE_0__.DateTime.fromMillis(x.activation, { zone: timezone });
            const old_month_target = old_month.set({ day: getTargetDay(old_month) });
            if (old_month_target.toMillis() > old_month.toMillis()) {
                return old_month_target.toMillis();
            }
            else {
                const new_month = old_month.plus({ months: 1 });
                return new_month.set({ day: getTargetDay(new_month) }).toMillis();
            }
        }
    }
    else
        return 0;
}


/***/ }),

/***/ "./src/data/User.ts":
/*!**************************!*\
  !*** ./src/data/User.ts ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "validatePassword": () => (/* binding */ validatePassword)
/* harmony export */ });
function validatePassword(x) {
    return (typeof x == "string" &&
        x.length >= 8);
}


/***/ }),

/***/ "./src/data/u.ts":
/*!***********************!*\
  !*** ./src/data/u.ts ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewId": () => (/* binding */ NewId)
/* harmony export */ });
/* unused harmony exports SystemId, isId, newIdAutoMap, ListedIdMap, Narrow */
/** Placeholder Id for Documents that have not gotten a generated Id yet. */
const NewId = "new";
/** Placeholder Id to use instead of user Id when an action was performed automatically. */
const SystemId = "sys";
function isId(x) { return typeof x == "string" && x.startsWith("x"); }
function newIdAutoMap(doc_constructor) {
    return new Proxy({}, {
        get: (target, property) => {
            if (isId(property) && target[property] === undefined)
                target[property] = doc_constructor(property);
            return target[property];
        },
    });
}
class ListedIdMap {
    constructor(arg) {
        this.list = [];
        if (typeof arg == "string")
            this.reset(JSON.parse(arg));
        else
            this.reset(arg);
    }
    reset(items) {
        items ?? (items = []);
        const map = {};
        items.forEach(item => map[item._id] = item);
        this.map = map;
        this.list.splice(0, this.list.length, ...items);
    }
    get(id) {
        return this.map[id];
    }
    add(x) {
        if (this.map[x._id])
            this.list.splice(this.list.findIndex(y => y._id == x._id), 1, x);
        else
            this.list.push(x);
        this.map[x._id] = x;
    }
    remove(x) {
        if (this.map[x]) {
            this.list.splice(this.list.findIndex(y => y._id == x), 1);
            delete this.map[x];
        }
    }
    toJSON() {
        return this.list;
    }
}
/** Used to select typescript typing in if-blocks. Optimizer trims it out of production builds. */
function Narrow(x) { return true; }


/***/ }),

/***/ "./test/requests/config.ts":
/*!*********************************!*\
  !*** ./test/requests/config.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "url": () => (/* binding */ url),
/* harmony export */   "admin_usr_name": () => (/* binding */ admin_usr_name),
/* harmony export */   "admin_pwd": () => (/* binding */ admin_pwd),
/* harmony export */   "test_key": () => (/* binding */ test_key)
/* harmony export */ });
const url = "http://localhost:3001";
const admin_usr_name = "kaheksajalg@pm.me";
const admin_pwd = "asdasd";
const test_key = "poe";


/***/ }),

/***/ "./test/requests/request_test.ts":
/*!***************************************!*\
  !*** ./test/requests/request_test.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "request": () => (/* binding */ request),
/* harmony export */   "createTest": () => (/* binding */ createTest),
/* harmony export */   "setBeforeEach": () => (/* binding */ setBeforeEach)
/* harmony export */ });
/* harmony import */ var json5_lib_register__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! json5/lib/register */ "json5/lib/register");
/* harmony import */ var json5_lib_register__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(json5_lib_register__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils */ "./test/requests/utils.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./config */ "./test/requests/config.ts");
/* harmony import */ var _core_RequestValidator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/core/RequestValidator */ "./src/core/RequestValidator.ts");
/* harmony import */ var node_fetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! node-fetch */ "node-fetch");
/* harmony import */ var node_fetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(node_fetch__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _user_requests_tests_session_test__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user_requests_tests/session_test */ "./test/requests/user_requests_tests/session_test.ts");
/* harmony import */ var _user_requests_tests_org_test__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user_requests_tests/org_test */ "./test/requests/user_requests_tests/org_test.ts");
/* harmony import */ var _user_requests_tests_ticket_test__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user_requests_tests/ticket_test */ "./test/requests/user_requests_tests/ticket_test.ts");
/* harmony import */ var _user_requests_tests_misc_test__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./user_requests_tests/misc_test */ "./test/requests/user_requests_tests/misc_test.ts");
/* harmony import */ var _user_requests_tests_group_test__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user_requests_tests/group_test */ "./test/requests/user_requests_tests/group_test.ts");
/* harmony import */ var _user_requests_tests_checklist_test__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./user_requests_tests/checklist_test */ "./test/requests/user_requests_tests/checklist_test.ts");
/* harmony import */ var _user_requests_tests_chat_test__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./user_requests_tests/chat_test */ "./test/requests/user_requests_tests/chat_test.ts");
var _a;
const is_windows = process.platform == "win32";
const slash = is_windows ? "\\" : "/";
__webpack_require__(/*! source-map-support */ "source-map-support").install({ environment: "node" });

{
    const j5 = __webpack_require__(/*! json5 */ "json5");
    JSON.r = j5.parse;
    JSON.w = (x) => j5.stringify(x, { space: "\t" });
}
(_a = String.prototype).replaceAll ?? (_a.replaceAll = function (search, replacement) { return this.split(search).join(replacement); });




const fetchTimeout = (url, ms, options) => {
    const controller = new AbortController();
    const promise = node_fetch__WEBPACK_IMPORTED_MODULE_4___default()(url, { signal: controller.signal, ...options });
    const timeout = setTimeout(() => controller.abort(), ms);
    return promise.finally(() => clearTimeout(timeout));
};
let cookie = "";
let current_test_req_set;
/**
 *
 * @param action either request action or false to reset cookies
 * @param parameters
 * @param files
 */
async function request(action, parameters) {
    if (action === false) {
        cookie = "";
        return null;
    }
    return fetchTimeout(_config__WEBPACK_IMPORTED_MODULE_2__.url + ( true ? "?a=" + action : 0), 5000, {
        method: "post",
        body: JSON.stringify({ action, ...(parameters ?? {}) }),
        headers: {
            "Cookie": cookie,
            "Content-Type": "application/json",
        },
    }).then(async (response) => {
        if (response.status === 502 || response.status === 503 || response.status === 504) {
            console.log("The server or network is unavailable!");
            console.log(response.status);
            process.exit(1);
        }
        const set_cookie = response.headers.raw()["set-cookie"];
        if (set_cookie != null) {
            cookie = set_cookie[0].split(";")[0];
        }
        return new Proxy({ ...(await response.json()), is200: response.status === 200 }, {
            get: (target, step) => {
                if (step === "is200") {
                    current_test_req_set.add(action);
                }
                return target[step];
            },
        });
    }).catch((err) => {
        console.log(err);
        if (err.type === "aborted" || err.code === "ECONNREFUSED") {
            console.log("The server or network is unavailable!");
            process.exit(1);
        }
        return { ok: false, is200: false };
    });
}
const tests = [];
let current_before_each = null;
const green = "\x1b[32m";
const red = "\x1b[31m";
const reset = "\x1b[0m";
let counter = 0;
function createTest(test_name, category, test_action) {
    const test_failed = (error_msg) => {
        console.log();
        console.log(red + "Test \"" + test_name + "\" failed!" + reset);
        console.log(error_msg);
        console.log();
    };
    const before_each = current_before_each;
    const test = async () => {
        const context = "context_" + (0,_utils__WEBPACK_IMPORTED_MODULE_1__.timestamp)() + "_" + counter++;
        if (before_each) {
            await before_each(context);
        }
        try {
            const result = await test_action(context);
            if (category !== "extra" && current_test_req_set.size === 0)
                throw new Error("No 200 checks in this test!");
            if (result === true) {
                return { passed: true, category, test_name };
            }
            else {
                test_failed(result);
                return { passed: false, category, test_name };
            }
        }
        catch (err) {
            if (typeof err !== "string") {
                console.log(err.code);
                console.log("\nError in test: " + test_name);
                console.log(err);
            }
            else
                test_failed(err);
            return { passed: false, category, test_name };
        }
    };
    tests.push(test);
}
function setBeforeEach(lambda) {
    current_before_each = lambda;
}
const coverage = {};
const req_names = Object.keys(_core_RequestValidator__WEBPACK_IMPORTED_MODULE_3__.RequestValidator);
req_names.forEach(x => {
    coverage[x] = {
        functionality: 0,
        stability: 0,
        security: 0,
    };
});
async function runTests() {
    function markCorrectCoverage(current_test_req, category, test_name) {
        if (!coverage[current_test_req][category])
            coverage[current_test_req][category]++;
        else
            throw new Error("Warning! Test \"" + test_name + "\" for request \"" + current_test_req + "\" is a duplicate test in category \"" + category + "\"!");
    }
    let count = 0;
    for (const test of tests) {
        current_test_req_set = new Set();
        const test_result = await test();
        if (test_result.passed)
            count++;
        if (test_result.category === "extra")
            continue;
        for (const current_test_req of current_test_req_set) {
            if (typeof test_result.category === "string") {
                markCorrectCoverage(current_test_req, test_result.category, test_result.test_name);
            }
            else {
                for (const category of test_result.category) {
                    markCorrectCoverage(current_test_req, category, test_result.test_name);
                }
            }
        }
    }
    const color_code = count === tests.length ? green : red;
    console.log(color_code + count + " out of " + tests.length + " tests successful!" + reset);
    if (count < tests.length)
        process.exitCode = 1;
}
// imports tests and runs them
current_before_each = null;

current_before_each = null;

current_before_each = null;

current_before_each = null;

current_before_each = null;

current_before_each = null;

current_before_each = null;

async function main() {
    await runTests();
    function isMissingType(x) {
        return (!x.functionality || !x.stability || !x.security);
    }
    if (req_names.some(x => isMissingType(coverage[x]))) {
        console.log("Warning, some requests are missing tests!");
        req_names.forEach(x => {
            if (isMissingType(coverage[x])) {
                const func = (coverage[x].functionality ? green : red) + "functionality" + reset;
                const stab = (coverage[x].stability ? green : red) + "stability" + reset;
                const secu = (coverage[x].security ? green : red) + "security" + reset;
                console.log(`${func}  ${stab}  ${secu}  ${x}`);
            }
        });
    }
}
main();


/***/ }),

/***/ "./test/requests/request_utils.ts":
/*!****************************************!*\
  !*** ./test/requests/request_utils.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createUser": () => (/* binding */ createUser)
/* harmony export */ });
/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./request_test */ "./test/requests/request_test.ts");

async function createUser(email_name, display_name) {
    return (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("register", { email: email_name + "@sharklasers.com", pwd: "Password123", fname: display_name, lname: "lastname", key: "a" });
}


/***/ }),

/***/ "./test/requests/user_requests_tests/chat_test.ts":
/*!********************************************************!*\
  !*** ./test/requests/user_requests_tests/chat_test.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../request_utils */ "./test/requests/request_utils.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./test/requests/utils.ts");
/* harmony import */ var _data_Chat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/data/Chat */ "./src/data/Chat.ts");




(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.setBeforeEach)(async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)(false);
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context, "user");
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can post and load chats", "functionality", async (context) => {
    // create a second user to try and access posted chats later
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context + 1, "user1");
    const user_obj2 = { usr: context + 1 + "@sharklasers.com", pwd: "Password123" };
    const user_obj1 = { usr: context + "@sharklasers.com", pwd: "Password123" };
    const name = "my new grp";
    const txt_org = "hello, fellow chatters";
    const txt_grp = "hello, fellow chatters";
    let txt_dm = "Hello, sub!";
    // second user's id is required to give them access to the group
    const user2_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj2)).data._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    // log original user in
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj1);
    const user1_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj1)).data._id;
    // setup
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("invite", { org, email: user_obj2.usr });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj2);
    const invitations = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getInvitations")).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("acceptInvitation", { inv: invitations[0]._id });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj1);
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name });
    const grp = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_setAuthLvl", { org, grp, user: user2_id, lvl: 1 });
    // can user, who posts chats, access them?
    let req_post = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: "org", is_dm: false, txt: txt_org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req_post, "Tried to post a new chat to an org.");
    req_post = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: grp, is_dm: false, txt: txt_grp });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req_post, "Tried to post a new chat to a grp.");
    req_post = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: user2_id, is_dm: true, txt: txt_dm });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req_post, "Tried to send a dm to another user.");
    let req_load = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_load", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req_load, "Tried to load chat messages.");
    if (req_load.data[0].txt !== txt_org)
        return "Could not load a correct chat from org!";
    if (req_load.data[1].txt !== txt_grp)
        return "Could not load a correct chat from grp!";
    if (req_load.data[2].txt !== txt_dm)
        return "Could not load a correct chat from dm's!";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    // can user, who loads chats, access them?
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj2);
    req_load = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_load", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req_load, "Tried to load chat messages as second user.");
    if (req_load.data[0].txt !== txt_org)
        return "Could not load a correct chat as second user from org!";
    if (req_load.data[1].txt !== txt_grp)
        return "Could not load a correct chat as second user from grp!";
    if (req_load.data[2].txt !== txt_dm)
        return "Could not load a correct chat as second user from dm's!";
    // is dm channel correct?
    const channel = (0,_data_Chat__WEBPACK_IMPORTED_MODULE_3__.asDMChannel)([user1_id, user2_id]);
    req_load = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_load", { org });
    if (req_load.data[2].channel !== channel)
        return "The channel, in which the users send dms, was incorrect!\nExpected: " + channel + "\nReceived: " + req_load.data[2].channel;
    txt_dm = "Hello, dom!";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: user1_id, is_dm: true, txt: txt_dm });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    // can user, who originally posted chats, access dms, sent to them?
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj1);
    req_load = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_load", { org });
    return (req_load.data[3].txt !== txt_dm) ? "Could not load a correct dm chat text!" : true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("chat_post, chat_load"), "stability", async (context) => {
    // setup
    const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
    const name = "my new grp";
    const txt = "hello, fellow chatters";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj);
    const user_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("user")).data?._id;
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name });
    const grp = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data[0]._id;
    // dirty input testing
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_load", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to load an empty ChatMsg[].");
    if (req.data.length !== 0)
        return "Something was loaded into ChatMsg[], but it was supposed to be empty!";
    let dirty_input = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chat_post accepted dirty input!", { org, channel: grp, is_dm: false, txt }, (dirty_object) => ["chat_post", dirty_object], { is_dm: [true, false] });
    if (dirty_input !== true)
        return dirty_input;
    // ultra corner cases
    let bad_req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: "bad_req", is_dm: false, txt });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(bad_req, "Tried to post a chat msg into an incorrect channel (\"bad_req\").");
    bad_req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: "", is_dm: false, txt });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(bad_req, "Tried to post a chat msg containing empty strings as IDs (\"\").");
    bad_req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: "bad", is_dm: false, txt });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(bad_req, "Tried to post a chat msg containing dirty IDs (\"bad\").");
    bad_req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: user_id, is_dm: true, txt });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(bad_req, "Tried to post a chat msg to user themself.");
    if (!bad_req.is200)
        return "Server crashed due to dirty input in req chat_post field channel (user's actual id)!";
    if (bad_req.ok)
        return "Server accepted a chat message being sent to user themself!";
    bad_req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: "x0_0", is_dm: true, txt });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(bad_req, "Tried to post a chat msg being sent to a non-existing user (\"x0_0\").");
    // normal resumes here
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chat_post", { org, channel: grp, is_dm: false, txt });
    dirty_input = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chat_load accepted dirty input!", { org }, (dirty_object) => ["chat_load", dirty_object]);
    return dirty_input;
});


/***/ }),

/***/ "./test/requests/user_requests_tests/checklist_test.ts":
/*!*************************************************************!*\
  !*** ./test/requests/user_requests_tests/checklist_test.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../request_utils */ "./test/requests/request_utils.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./test/requests/utils.ts");



(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.setBeforeEach)(async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)(false);
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context, "user");
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can create a new checklist", "functionality", async (context) => {
    const title = "Tee seda!";
    const name = "Tegevus";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    const chklist = { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 1 }], items_latest: 1, activation: 1, duration: 1 } };
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", chklist);
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to create a new checklist.");
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    return (checklists[0].title !== title) ? "New checklist was not created properly! Expected title: " + title + " Received title: " + checklists[0].title : true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can create a repeating checklist without time manually set", "extra", async (context) => {
    const days = { mon: true, tue: true, wed: true, thu: true, fri: true, sat: true, sun: true };
    const title = "Tee seda!";
    const name = "Tegevus";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 1 }], items_latest: 1, activation: 1, duration: 0, schedule: { repeat: "daily", days } } });
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    for (const checklist of checklists) {
        if (checklist.status === "active") {
            if (checklist.schedule)
                return "Active checklist had an existing schedule!";
        }
        else {
            if (checklist.schedule?.repeat !== "daily")
                return "New checklist does not repeat daily! Expected repeat schedule: daily, Received repeat schedule: " + checklists[0].schedule?.repeat;
        }
    }
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can edit an existing checklist", "functionality", async (context) => {
    const title = "Tee seda!";
    const name = "Tegevus";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 1 }], items_latest: 1, activation: (0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)() - 2000, duration: 60 * 60 * 1000 } });
    const checklist = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data[0];
    const edited_time = (0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)() + 2000;
    checklist.activation = edited_time;
    checklist.duration = 15 * 60 * 1000;
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_edit", { org, doc: checklist });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to edit a checklist.");
    const req_get = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_get", { org, id: checklist._id });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req_get, "Tried to access modified checklist.");
    if (req_get.data.status === "active")
        return "Checklist's status is active!";
    if (req_get.data.activation !== edited_time)
        return "Activation time did not change correctly!";
    if (req_get.data.duration !== checklist.duration)
        return "Duration did not change correctly!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Server accepts phantom, but does not store it", "extra", async (context) => {
    const title = "Tee seda!";
    const name = "Tegevus";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    const chklist = { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, is_phantom: true, activation: 1, duration: 1 } };
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", chklist);
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to create a new checklist with an is_phantom field.");
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    return (checklists[0].is_phantom !== undefined) ? "Server stored an is_phantom field!" : true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can remove whitespaces from checklist's title", "extra", async (context) => {
    let title = " Tee seda!";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [], activation: 1, duration: 1, items_latest: 0 } });
    title = "Tee seda! ";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [], activation: 1, duration: 1, items_latest: 0 } });
    title = "Tee \n\n\t\t\n seda";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [], activation: 1, duration: 1, items_latest: 0 } });
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    if (!checklists[0].title.startsWith("T"))
        return "The leading whitespace was not removed from checklist's title!";
    if (!checklists[1].title.endsWith("!"))
        return "The trailing whitespace was not removed from checklist's title!";
    if (checklists[2].title.includes("\n") || checklists[2].title.includes("\t"))
        return "Newlines were not removed from checklist's title!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("chk_new"), "stability", async (context) => {
    const days = { mon: true, tue: true, wed: true, thu: true, fri: true, sat: true, sun: true };
    const title = "Tee seda!";
    const name = "Tegevus";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days } } });
    let dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chk_new was accepted with dirty parameters!", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days } } }, (dirty_object) => ["chk_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chk_new was accepted with dirty parameters!", { name, section: false, key: 0, val: "new" }, (dirty_object) => ["chk_new", { org, doc: { grp, title, items: [dirty_object], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days } } }], { key: [123], val: [null, "", 123, true, false, [], {}, undefined] });
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chk_new was accepted with dirty parameters!", { repeat: "daily", days }, (dirty_object) => ["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: dirty_object } }], { võti: [null, undefined] });
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chk_new was accepted with dirty parameters!", { repeat: "monthly", day: 1, from_end: false }, (dirty_object) => ["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: dirty_object } }], { võti: [null, undefined], from_end: [false, true] });
    if (dirty_result !== true)
        return dirty_result;
    const days_exclusion_map = {};
    for (const day in days) {
        days_exclusion_map[day] = [false, true];
    }
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chk_new was accepted with dirty parameters!", days, (dirty_object) => ["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 0, schedule: { repeat: "daily", days: dirty_object } } }], days_exclusion_map);
    if (dirty_result !== true)
        return dirty_result;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: 1, duration: 1, schedule: { repeat: "daily", days } } });
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    const _id = checklists[1]._id;
    const activation = checklists[1].activation;
    const overrides_phantom = { _id, activation };
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("chk_new with overrides_phantom accepted dirty input!", overrides_phantom, (dirty_input) => ["chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation, duration: 2, overrides_phantom: dirty_input } }], { võti: [null, undefined] });
    if (dirty_result !== true)
        return dirty_result;
    return dirty_result;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can override checklist phantoms", "functionality", async (context) => {
    const days = { mon: true, tue: true, wed: true, thu: true, fri: true, sat: true, sun: true };
    const title = "Tee seda!";
    const name = "Tegevus";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation: (0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)() + 100000, duration: 1, schedule: { repeat: "daily", days } } });
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    const _id = checklists[0]._id;
    const activation = checklists[0].activation;
    const overrides_phantom = { _id, activation };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", { org, doc: { grp, title, items: [{ name, val: "new", section: false, key: 0 }], items_latest: 0, activation, duration: 2, overrides_phantom } });
    const req_res = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp }));
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req_res, "Tried to get all results in group, where overrides_phantom is defined.");
    const checklist = req_res.data.pop();
    if (checklist.duration !== 2)
        return "Checklist duration was not correct. Expected: 2, Received: " + checklist.duration;
    if (!checklist.overrides_phantom)
        return "overrides_phantom does not exist on checklist! Received: " + JSON.stringify(checklist);
    if (checklist.overrides_phantom._id !== _id)
        return "overrides_phantom received an incorrect id! Expected: " + _id + ", Received: " + checklist.overrides_phantom._id;
    if (checklist.overrides_phantom.activation !== activation)
        return "overrides_phantom received an incorrect activation! Expected: " + activation + ", Received: " + checklist.overrides_phantom.activation;
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can set checklist item status", "functionality", async (context) => {
    const title = "Tee seda!";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    const chklist = {
        org, doc: {
            grp, title, items: [
                { name: "tere", val: "new", section: false, key: 1 },
                { name: "hello", val: "new", section: false, key: 2 },
                { name: "privjet", val: "new", section: false, key: 3 },
                {
                    name: "konnichiwa uwu", val: "new", section: true, key: 4, tasks: [
                        { name: "senpai", val: "new", section: false, key: 5 },
                        { name: "san", val: "new", section: false, key: 6 },
                        { name: "chan", val: "new", section: false, key: 7 },
                    ], description: ["Japanese gibberish"],
                },
            ], items_latest: 1, activation: 1, duration: 1,
        },
    };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", chklist);
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    const checklist = checklists[0];
    // initial set item status
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 0, status: "done", section: -1 });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to set checklist item status as \"done\".");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 1, status: "fail", section: -1 });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to set checklist item status as \"fail\".");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "done", section: -1 });
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "new", section: -1 });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to set checklist item status as \"new\".");
    // section
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 0, status: "done", section: 3 });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to set checklist item status as \"done\" within section.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 1, status: "fail", section: 3 });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to set checklist item status as \"fail\" within section.");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "done", section: 3 });
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_setItemStatus", { org, id: checklist._id, index: 2, status: "new", section: 3 });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to set checklist item status as \"new\" within section.");
    // final check
    const checklist_group = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data[0];
    if (checklist_group.items[0]?.val !== "done")
        return "Could not actually set checklist item value status as \"done\"!";
    if (checklist_group.items[1]?.val !== "fail")
        return "Could not actually set checklist item value status as \"fail\"!";
    if (checklist_group.items[2]?.val !== "new")
        return "Could not actually set checklist item value status as \"new\"!";
    if (checklist_group.items[3]?.tasks[0]?.val !== "done")
        return "Could not actually set checklist item value status as \"done\" within section!";
    if (checklist_group.items[3]?.tasks[1]?.val !== "fail")
        return "Could not actually set checklist item value status as \"fail\" within section!";
    if (checklist_group.items[3]?.tasks[2]?.val !== "new")
        return "Could not actually set checklist item value status as \"new\" within section!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("chk_setItemStatus"), "stability", async (context) => {
    const title = "Tee seda!";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    const chklist = {
        org, doc: {
            grp, title, items: [
                { name: "task", val: "new", section: false, key: 1 },
                {
                    name: "section", val: "new", section: true, key: 2, description: ["section"],
                    tasks: [{ name: "task1", val: "new", section: false, key: 3 }],
                },
            ], items_latest: 1, activation: 1, duration: 1,
        },
    };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_new", chklist);
    const checklists = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("chk_getOfGroup", { org, grp })).data;
    const checklist = checklists[0];
    let dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("There was a problem trying to set checklist task as completed!", { org, id: checklist._id, index: 0, status: "done", section: -1 }, (dirty_object) => ["chk_setItemStatus", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("There was a problem trying to set checklist task status within section as completed!", { org, id: checklist._id, index: 0, status: "done", section: 1 }, (dirty_object) => ["chk_setItemStatus", dirty_object]);
    return dirty_result;
});


/***/ }),

/***/ "./test/requests/user_requests_tests/group_test.ts":
/*!*********************************************************!*\
  !*** ./test/requests/user_requests_tests/group_test.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../request_utils */ "./test/requests/request_utils.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./test/requests/utils.ts");



(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.setBeforeEach)(async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)(false);
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context, "user");
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can create a group", "functionality", async (context) => {
    const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj);
    const name = "my new grp";
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to create a new grp.");
    const grp = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data[0];
    if (grp == undefined)
        return "Group was actually not created!";
    if (grp.name !== name)
        return "Group was not named correctly!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can a group be asked to be deleted", "functionality", async (context) => {
    const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj);
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp insta delete" });
    let groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    // mark for deletion
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_delete", { org, grp, confirm: true });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to mark a grp to be deleted.");
    groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    if (groups[0].mfd < (0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)() + 1.8 * 86400 * 1000)
        return "Group was not marked to be deleted at a correct time!\nExpected: " +
            new Date((0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)() + 1.8 * 86400 * 1000) + " or later\nReceived: " +
            new Date(groups[0].mfd);
    // unmark for deletion
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_delete", { org, grp, confirm: false });
    groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to cancel a grp from being deleted.");
    if (groups[0].mfd !== 0)
        return "Group was not unmarked to be deleted!";
    // test actual deletion by hacking time
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_delete", { org, grp, confirm: true });
    groups = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.shiftTime)(user_obj, (0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)() + 3 * 86400 * 1000, async () => (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data);
    if (groups[0] != undefined)
        return "Group was not actually deleted!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can delete tickets from one group without deleting tickets from other group", "stability", async (context) => {
    const title = "teine probleem";
    const text = "erakordselt teine probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp insta delete" });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "irrelevant grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id1 = groups[0]._id;
    const grp_id2 = groups[1]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id1, is_global: false, is_priority: false, title: "esimene probleem", text: "erakordselt esimene probleem" }, org: org_id });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id2, is_global: false, is_priority: false, title, text }, org: org_id });
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_delete", { org: org_id, grp: grp_id1, confirm: true });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to delete tickets from first group!");
    const req_tkt = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id2 })).data;
    return (req_tkt[0].title !== title || req_tkt[0].comments[0].text[0] !== text) ? "Deleting a group caused the entries from another group to also be deleted!" : true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can rename a group", "functionality", async (context) => {
    const name = "new group";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grup" });
    let groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_rename", { org, grp, name });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to rename a grp.");
    groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const changed = groups[0].name;
    return (name !== changed) ? "Request grp_rename did not change the name of the group!\nReceived: " + changed + "\nExpected: " + name : true;
});


/***/ }),

/***/ "./test/requests/user_requests_tests/misc_test.ts":
/*!********************************************************!*\
  !*** ./test/requests/user_requests_tests/misc_test.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../request_utils */ "./test/requests/request_utils.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./test/requests/utils.ts");



(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.setBeforeEach)(async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)(false);
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context, "user");
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can send feedback", "functionality", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("feedback", { text: "test feedback" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to send feedback.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Must not accept feedback from users that are not logged in", "security", async () => {
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("feedback", { text: "test feedback" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(req, "Tried to send feedback without being logged in.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("feedback"), "stability", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("feedback accepted dirty input!", { text: "test feedback" }, (dirty_input) => ["feedback", dirty_input]);
});


/***/ }),

/***/ "./test/requests/user_requests_tests/org_test.ts":
/*!*******************************************************!*\
  !*** ./test/requests/user_requests_tests/org_test.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _data_u__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/data/u */ "./src/data/u.ts");
/* harmony import */ var _request_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../request_utils */ "./test/requests/request_utils.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils */ "./test/requests/utils.ts");




(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.setBeforeEach)(async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)(false);
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
});
const name = "new org";
const timezone = "Europe/Tallinn";
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can create a new organization", "functionality", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to create an org.");
    return (req.data == undefined || req.data == _data_u__WEBPACK_IMPORTED_MODULE_1__.NewId) ? "Could not create a new org!" : true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can access updated problem list while internet hiked", "functionality", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    // can access latest cursors
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_getLatestCursors", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to get latest cursors.");
    if (req.data["tkt"] !== undefined)
        return "Request org_getLatestCursors did not return undefined!" + JSON.stringify(req.data);
    // should open most recently visited org
    const user = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("user")).data;
    if (user?.options.last_org !== org)
        return "User's last org was not set correctly!\nExpected: " + JSON.stringify(org) + "\nReceived: " + JSON.stringify(user?.options.last_org);
    // create a group and insert a ticket there
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "ticket", text: "text" }, org });
    const tkt = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org, grp: grp_id })).data[0];
    // were the cursors correct?
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_getLatestCursors", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to get updated latest cursors.");
    if (req.data["tkt"] !== tkt._id)
        return "Request org_getLatestCursors did not return a ticket with correct id!\nExpected: " + tkt._id + "\nReceived: " + JSON.stringify(req.data);
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("org_getLatestCursors"), "stability", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("org_getLatestCursors accepted dirty input!", { org }, (dirty_input) => ["org_getLatestCursors", dirty_input]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("org_new"), "stability", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("org_new accepted dirty input!", { name, timezone, options: {} }, (dirty_input) => ["org_new", dirty_input]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can access, send, receive and accept invitations", "functionality", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context + 1, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user0, pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    // non-existing invites
    let invitations = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_getInvitations", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(invitations, "Tried to get non-existing invites.");
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("invite", { org, email: user1 });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to send an invite.");
    invitations = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_getInvitations", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(invitations, "Tried to get sent existing invites.");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user1, pwd: "Password123" });
    req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getInvitations"));
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to get received existing invites.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("acceptInvitation", { inv: invitations.data[0]._id });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tries to accept invitations.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can kick a user from an org", "functionality", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context + 1, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user0, pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_getInvitations", { org });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("invite", { org, email: user1 });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user1, pwd: "Password123" });
    const pfl = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("user")).data;
    const invitations = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getInvitations"));
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("acceptInvitation", { inv: invitations.data[0]._id });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user0, pwd: "Password123" });
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_kick", { org, user: pfl._id });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to kick a user from an org.");
    const cur_org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0];
    if (cur_org.auth[pfl._id] !== undefined)
        return "User was not correctly removed from org auth table!";
    if (cur_org.users.includes(pfl._id))
        return "User was not correctly removed from org users list!";
    else
        return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("org_kick"), "stability", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context + 1, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user0, pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_getInvitations", { org });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("invite", { org, email: user1 });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user1, pwd: "Password123" });
    const pfl = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("user")).data;
    const invitations = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getInvitations"));
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("acceptInvitation", { inv: invitations.data[0]._id });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user0, pwd: "Password123" });
    return (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("Org_kick accepted dirty input!", { org, user: pfl._id }, (dirty_object) => ["org_kick", dirty_object]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("invite, org_getInvitations and acceptInvitation"), "stability", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context + 1, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user0, pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    let dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("Invite, which included dirty input was sent!", { org, email: user1 }, (dirty_object) => ["invite", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("invite", { org, email: user1 });
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("invite", { org, email: user1 });
    if (req.ok)
        return "Can create multiple invitations for one user!";
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("Request, which gets all invitations, accepted dirty input!", { org }, (dirty_object) => ["org_getInvitations", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user1, pwd: "Password123" });
    const req2 = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getInvitations"));
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("Invitation was accepted with dirty input!", { inv: req2.data[0]._id }, (dirty_object) => ["acceptInvitation", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can rename an org", "functionality", async (context) => {
    const new_org_name = "renamed org";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_rename", { org, name: new_org_name });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to rename an org.");
    const renamed = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0];
    if (renamed.name !== new_org_name)
        return "The org was not renamed properly! Current org name: " + renamed.name;
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("org_rename"), "stability", async (context) => {
    const new_org_name = "renamed org";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("org_rename accepted dirty input!", { org, name: new_org_name }, (dirty_object) => ["org_rename", dirty_object]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can remove whitespaces from org name", "extra", async (context) => {
    let new_org_name = " renamed org";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_rename", { org, name: new_org_name });
    let renamed = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0];
    if (renamed.name.startsWith(" "))
        return "The leading whitespace was not removed from org name!";
    new_org_name = "renamed org ";
    renamed = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0];
    if (renamed.name.endsWith(" "))
        return "The trailing whitespace was not removed from org name!";
    new_org_name = "\n\n\t\nrenamed \t\n\torg\t\n\t\n";
    renamed = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0];
    if (renamed.name.includes("\n") || renamed.name.includes("\t"))
        return "Newlines and tabs were not removed from text!";
    else
        return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can create, set and access the notepad", "functionality", async (context) => {
    const name = "new org";
    const timezone = "Europe/Tallinn";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    const req_new_notepad = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("usr_newNotepad", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req_new_notepad, "Tried to create a new notepad.");
    let req_get_notepad = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("usr_getNotepads", { org });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req_get_notepad, "Tried to access user's notepads.");
    if (req_get_notepad.data[0]?.name !== "")
        return "Could not get correct notepad!";
    const req_set_notepad = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("usr_setNotepad", {
        _id: req_get_notepad.data[0]._id,
        org: req_get_notepad.data[0].org,
        name: "Main Notes",
        content: "My notes",
    });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req_set_notepad, "Tried to set user's notepads.");
    req_get_notepad = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("usr_getNotepads", { org });
    if (req_get_notepad.data[0]?.name !== "Main Notes")
        return "Could not set content properly!";
    if (req_get_notepad.data[0]?.content !== "My notes")
        return "Could not set content properly!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("usr_newNotepad, usr_getNotepads and usr_setNotepad"), "stability", async (context) => {
    const name = "new org";
    const timezone = "Europe/Tallinn";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("usr_newNotepad", { org });
    let dirty_input = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("Usr_newNotepad accepted dirty input!", { org }, (dirty_object) => ["usr_newNotepad", dirty_object]);
    if (dirty_input !== true)
        return dirty_input;
    const req_get_notepad = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("usr_getNotepads", { org })).data[0];
    dirty_input = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("Usr_getNotepads accepted dirty input!", { org }, (dirty_object) => ["usr_getNotepads", dirty_object]);
    if (dirty_input !== true)
        return dirty_input;
    dirty_input = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("Usr_setNotepad accepted dirty input!", {
        _id: req_get_notepad._id,
        org: req_get_notepad.org,
        name: "Main Notes",
        content: "My notes",
    }, (dirty_object) => ["usr_setNotepad", dirty_object], { content: [""] });
    return dirty_input;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can an org ask to be deleted", "functionality", async (context) => {
    const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", user_obj);
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name, timezone, options: {} })).data;
    // mark for deletion
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_delete", { org, confirm: true });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to mark org to be deleted.");
    let tbd_org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0];
    if (tbd_org.mfd < (0,_utils__WEBPACK_IMPORTED_MODULE_3__.timestamp)() + 27 * 86400 * 1000)
        return "Org was not marked to be deleted at a correct time!\nExpected: " + new Date((0,_utils__WEBPACK_IMPORTED_MODULE_3__.timestamp)() + 27 * 86400 * 1000) + " or later\nReceived: " + new Date(tbd_org.mfd);
    // unmark for deletion
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_delete", { org, confirm: false });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to cancel org from being deleted.");
    tbd_org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0];
    if (tbd_org.mfd !== 0)
        return "Org was not unmarked to be deleted!";
    // test actual deletion by hacking time
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_delete", { org, confirm: true });
    tbd_org = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.shiftTime)(user_obj, (0,_utils__WEBPACK_IMPORTED_MODULE_3__.timestamp)() + 32 * 86400 * 1000, async () => (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getOrgs")).data[0]);
    if (tbd_org != undefined)
        return "Org was not actually deleted!";
    return true;
});


/***/ }),

/***/ "./test/requests/user_requests_tests/session_test.ts":
/*!***********************************************************!*\
  !*** ./test/requests/user_requests_tests/session_test.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../config */ "./test/requests/config.ts");
/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _request_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../request_utils */ "./test/requests/request_utils.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils */ "./test/requests/utils.ts");




const fname = "user";
const pwd = "Password123";
const lname = "lastname";
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.setBeforeEach)(async () => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)(false);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can register a new user", "functionality", async (context) => {
    const req = await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, fname);
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to register a regular user.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can log into site", "functionality", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to log in as a regular user.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can download user profile", "functionality", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user");
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to download user profile.");
    return req.data?.pfl.dname ? true : "Could not get user profile!";
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can log out", "functionality", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("logout");
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to log out.");
    const req_user = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user");
    return !req_user.data?.pfl.dname ? true : "Could not actually logout!";
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can switch users", "extra", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context + 1, "user1");
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context + 2, "user2");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { usr: context + 1 + "@sharklasers.com", pwd });
    const pfl1 = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data.pfl.dname;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { usr: context + 2 + "@sharklasers.com", pwd });
    const pfl2 = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data.pfl.dname;
    if (pfl1 === pfl2)
        return "Switching user profiles by logging out as the old user and logging in as the new user failed!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can not log in with wrong password", "security", async (context) => {
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "hretggwiotygjod" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestDenied)(req, "Tried to log in with a wrong password.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can not register with bad password", "security", async (context) => {
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { email: context + "@sharklasers.com", pwd: "123", key: "a", fname: "sneakyuser", lname: "sneak" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestDenied)(req, "Tried to register with a bad (short) password!");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("register"), "stability", async (context) => {
    const pwd = "Password123";
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { fname: "sneakyuser", lname: "sneak", email: context + "@.", pwd, key: "a" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestDenied)(req, "Tried to register with email: @.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { fname: "sneakyuser", lname: "sneak", email: context, pwd, key: "a" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestDenied)(req, "Tried to register with email containing only context.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { fname: "sneakyuser", lname: "sneak", email: "", pwd, key: "a" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestDenied)(req, "Tried to register with email containing empty string.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { fname: "sneakyuser", lname: "sneak", email: "@.", pwd, key: "a" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestDenied)(req, "User could register with email containing only @.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { fname: "sneakyuser", lname: "sneak", email: "12345", pwd, key: "a" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestDenied)(req, "User could register with email containing only 12345.");
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("register accepted dirty input!", { email: context + "@sharklasers.com", pwd, fname, lname, key: "a" }, (dirty_input) => ["register", dirty_input]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can change password", "functionality", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("changePassword", { old_pwd: pwd, new_pwd: "Qwerty123" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to change password.");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("logout");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    if (req.ok)
        return "Could login with old password.";
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd: "Qwerty123" });
    if (!req.ok)
        return "Could not login with new (changed) password.";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("changePassword"), "stability", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("changePassword accepted dirty input!", { old_pwd: pwd, new_pwd: "Qwerty123" }, (dirty_input) => ["changePassword", dirty_input]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Password encryption method in use produces different output each time", "extra", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { email: context + 1 + "@sharklasers.com", pwd, fname: "firstname", lname: "lastname", key: "a" });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("register", { email: context + 2 + "@sharklasers.com", pwd, fname: "firstname", lname: "lastname", key: "a" });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: true, usr: _config__WEBPACK_IMPORTED_MODULE_0__.admin_usr_name, pwd: _config__WEBPACK_IMPORTED_MODULE_0__.admin_pwd });
    const users = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("get", { table: "users" })).data;
    if (users[0].acc.pwd !== users[1].acc.pwd)
        return true;
    return "Password encryption method produced two same passwords!";
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can change names", "functionality", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "firstname");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("changeNames", { fname: "Nimi", lname: "lastname", dname: "firstname" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to change firstname.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("changeNames", { fname: "Nimi", lname: "Nimeste", dname: "firstname" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to change lastname.");
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("changeNames", { fname: "Nimi", lname: "Nimeste", dname: "Minu nimi" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to change displayname.");
    const user_info = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data;
    if (user_info?.pfl.fname !== "Nimi")
        return "Could not change firstname!";
    if (user_info?.pfl.lname !== "Nimeste")
        return "Could not change lastname!";
    if (user_info?.pfl.dname !== "Minu nimi")
        return "Could not change displayname!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_3__.nameDirtyInputTest)("changeNames"), "stability", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "firstname");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.testDirtyInputRequest)("changeNames accepted dirty input!", { fname: "firstname", lname: "lastname", dname: "firstname" }, (dirty_input) => ["changeNames", dirty_input]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can remove whitespaces from user's names", "extra", async (context) => {
    let fname = " Nimi";
    let lname = " Nimeste";
    let dname = " Minu nimi";
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "firstname");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("changeNames", { fname, lname, dname });
    let user_info = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data;
    if (!user_info?.pfl.fname.startsWith("N"))
        return "The leading whitespace was not removed from firstname!";
    if (!user_info?.pfl.lname.startsWith("N"))
        return "The leading whitespace was not removed from lastname!";
    if (!user_info?.pfl.dname.startsWith("M"))
        return "The leading whitespace was not removed from displayname!";
    fname = "Nimi ";
    lname = "Nimeste ";
    dname = "Minu nimi ";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("changeNames", { fname, lname, dname });
    user_info = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data;
    if (!user_info?.pfl.fname.endsWith("i"))
        return "The trailing whitespace was not removed from firstname!";
    if (!user_info?.pfl.lname.endsWith("e"))
        return "The trailing whitespace was not removed from lastname!";
    if (!user_info?.pfl.dname.endsWith("i"))
        return "The trailing whitespace was not removed from displayname!";
    fname = "\n\n\t\tNi\t\nm\t\ni\n\n\t\t";
    lname = "\n\n\t\tNi\t\nmes\t\nte\n\n\t\t ";
    dname = "\n\n\t\tMi\t\nnu \t\nnimi\n\n\t\t ";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("changeNames", { fname, lname, dname });
    user_info = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data;
    if (user_info?.pfl.fname.includes("\n") && user_info?.pfl.fname.includes("\t"))
        return "Tabs and newlines were not removed from firstname!";
    if (user_info?.pfl.lname.includes("\n") && user_info?.pfl.lname.includes("\t"))
        return "Tabs and newlines were not removed from lastname!";
    if (user_info?.pfl.dname.includes("\n") && user_info?.pfl.dname.includes("\t"))
        return "Tabs and newlines were not removed from displayname!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Can a user ask to be deleted", "functionality", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    const user_obj = { usr: context + "@sharklasers.com", pwd };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", user_obj);
    // mark for deletion
    let req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("usr_delete", { confirm: true });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to mark user for deletion.");
    let user = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data;
    if (user.mfd < (0,_utils__WEBPACK_IMPORTED_MODULE_3__.timestamp)() + 27 * 86400 * 1000)
        return "User was not marked to be deleted at a correct time!\nExpected: " + new Date((0,_utils__WEBPACK_IMPORTED_MODULE_3__.timestamp)() + 27 * 86400 * 1000) + " or later\nReceived: " + new Date(user.mfd);
    // unmark for deletion
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("usr_delete", { confirm: false });
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to cancel user deletion.");
    user = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("user")).data;
    if (user.mfd !== 0)
        return "User was not unmarked to be deleted!";
    // test actual deletion by hacking time
    req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("usr_delete", { confirm: true });
    const del_log_in = await (0,_utils__WEBPACK_IMPORTED_MODULE_3__.shiftTime)(user_obj, (0,_utils__WEBPACK_IMPORTED_MODULE_3__.timestamp)() + 32 * 86400 * 1000, async () => await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: false, usr: context + "@sharklasers.com", pwd }));
    if (del_log_in.ok)
        return "Could login as a deleted user.";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("User can get their wskey", ["functionality", "stability"], async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    const user_obj = { usr: context + "@sharklasers.com", pwd };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", user_obj);
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("wskey");
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req, "Tried to get user's wskey.");
    return typeof req.data[0] !== "string" ? "Wskey was not of type string!" : true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_1__.createTest)("Every user gets a unique wskey assigned to them", "security", async (context) => {
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context, "user");
    let user_obj = { usr: context + "@sharklasers.com", pwd };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", user_obj);
    const req1 = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("wskey");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("logout");
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_2__.createUser)(context + 1, "user");
    user_obj = { usr: context + 1 + "@sharklasers.com", pwd };
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", user_obj);
    const req2 = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("wskey");
    (0,_utils__WEBPACK_IMPORTED_MODULE_3__.verifyRequestAccepted)(req2, "Tried to get second user's wskey.");
    if (req1.data === req2.data)
        return "Wskeys of two different sessions were the same!";
    return true;
});


/***/ }),

/***/ "./test/requests/user_requests_tests/ticket_test.ts":
/*!**********************************************************!*\
  !*** ./test/requests/user_requests_tests/ticket_test.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {

/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _request_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../request_utils */ "./test/requests/request_utils.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./test/requests/utils.ts");



(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.setBeforeEach)(async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)(false);
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context, "user");
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("tkt_new"), "stability", async (context) => {
    let dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("Ticket was created without any user being logged in!", { doc: { grp: "x0_0", is_global: true, is_priority: false, text: "tere", title: "hello" }, org: "x0_0" }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("Ticket was created without user belonging to any org!", { doc: { grp: "x0_0", is_global: true, is_priority: false, text: "tere", title: "hello" }, org: "x0_0" }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("Ticket was created in nonexistant group!", { doc: { grp: "1", is_global: true, is_priority: false, text: "tere", title: "hello" }, org: org_id }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const group_id = groups[0]._id;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("Ticket was created with dirty input!", { doc: { grp: group_id, is_global: true, is_priority: false, text: "tere", title: "hello" }, org: org_id }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("Ticket was created with dirty input!", { grp: group_id, is_global: true, is_priority: false, text: "tere", title: "hello" }, (dirty_object) => ["tkt_new", { doc: dirty_object, org: org_id }], { is_global: [false, true], is_priority: [false, true], text: [""] });
    return dirty_result;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can successfully create a ticket", "functionality", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to create a correct ticket.");
    const tkt = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0];
    return (tkt.title !== title || tkt.comments[0].text[0] !== text) ? "Ticket did not have desired content!" : true;
});
// todo: randomly failed once and not again, wonder wth is going on
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can reply to ticket", "functionality", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    const tickets = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_addReply", { org: org_id, id: tickets[0]._id, text: "test reply" });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to add a reply to ticket.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("tkt_addReply"), "stability", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    const tickets = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    return await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("tkt_addReply received dirty input!", { org: org_id, id: tickets[0]._id, text: "Lamp" }, (dirty_object) => ["tkt_addReply", dirty_object]);
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Must check user authorization before accepting tickets", "security", async (context) => {
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context + 1, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "1@sharklasers.com", pwd: "Password123" });
    const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(req, "Tried to accept a ticket from an unauthorized user.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Must not reveal tickets to unauthorized users", "security", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await (0,_request_utils__WEBPACK_IMPORTED_MODULE_1__.createUser)(context + 1, "user");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user0, pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("invite", { org: org_id, email: user1 });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("logout");
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: user1, pwd: "Password123" });
    let req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id }));
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(req, "Tried to access tickets outside the org.");
    const invitation = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("getInvitations")).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("acceptInvitation", { inv: invitation[0]._id });
    req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id }));
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestDenied)(req, "Tried to access tickets outside the grp.");
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can get global and self made tickets", "functionality", async (context) => {
    const title_global = "väga tõsine probleem";
    const text_global = "erakordselt tõsine probleem";
    const title_self = "isiklik probleem";
    const text_self = "erakordselt isiklik probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp, is_global: true, is_priority: false, title: title_global, text: text_global }, org });
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp, is_global: false, is_priority: false, title: title_self, text: text_self }, org });
    // can access global ticket
    let req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfMeta", { org, grp: "Global" }));
    let tkt = req.data[0];
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to get global tickets.");
    if (!(tkt.title === title_global && tkt.comments[0].text[0] === text_global))
        return "Could not get a self-made ticket!";
    // can access self-made ticket
    req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfMeta", { org, grp: "Self" }));
    tkt = req.data[1];
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(req, "Tried to get self-made ticket.");
    if (!(tkt.title === title_self && tkt.comments[0].text[0] === text_self))
        return "Could not get a self-made ticket!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can modify an existing ticket", "functionality", async (context) => {
    // set up
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    let req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    let tkt = req.data[0];
    // Is it possible to modify?
    let options = { is_global: true, is_priority: true, is_resolved: true };
    let modify_req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_modify", { org: org_id, id: tkt._id, options });
    let cur_time = (0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)();
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(modify_req, "Tried to modify a ticket.");
    req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    tkt = req.data[0];
    if (tkt?.is_global !== options.is_global || tkt?.is_priority !== options.is_priority || tkt?.is_resolved !== options.is_resolved) {
        return "Could not modify ticket with new data! Expected: " + JSON.stringify({ ...tkt, ...options }) +
            ", received: " + tkt;
    }
    tkt = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0];
    // to compare them, they have to be in order and made more similar or sth idk
    let expected_action_types = JSON.stringify(["setGlobal", "setPriority", "lock"].sort());
    let received_action_types = JSON.stringify(tkt.actions.map(action => action.type).sort());
    if (expected_action_types !== received_action_types)
        return "Action types did not match!\nExpected: " + expected_action_types + "\nReceived: " + received_action_types;
    if (!(tkt.actions[0].posted.at >= cur_time - 2000 && tkt.actions[0].posted.at - cur_time < 3000)) {
        return "Timestamp was incorrect!\nExpected: " + (0,_utils__WEBPACK_IMPORTED_MODULE_2__.datec)(cur_time) +
            "\nReceived: " + (0,_utils__WEBPACK_IMPORTED_MODULE_2__.datec)(tkt.actions[0].posted.at) +
            "\nThe difference: " + JSON.stringify(Math.abs(tkt.actions[0].posted.at - cur_time) + "ms");
    }
    // set ticket resolved 
    options = { is_global: false, is_priority: false, is_resolved: false };
    modify_req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_modify", { org: org_id, id: tkt._id, options });
    cur_time = (0,_utils__WEBPACK_IMPORTED_MODULE_2__.timestamp)();
    (0,_utils__WEBPACK_IMPORTED_MODULE_2__.verifyRequestAccepted)(modify_req, "Tried to mark ticket as resolved.");
    req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    tkt = req.data[0];
    if (tkt?.is_global !== options.is_global || tkt?.is_priority !== options.is_priority || tkt?.is_resolved !== options.is_resolved) {
        return "Could not modify ticket with new data! Expected: " + JSON.stringify({ ...tkt, ...options }) + ", received: " + tkt;
    }
    // checks time log
    tkt = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0];
    expected_action_types = JSON.stringify(["setGlobal", "setPriority", "lock", "unlock", "unsetGlobal", "unsetPriority"].sort());
    received_action_types = JSON.stringify(tkt.actions.map(action => action.type).sort());
    if (expected_action_types !== received_action_types)
        return "Action types did not match!\nExpected: " + expected_action_types + "\nReceived: " + received_action_types;
    if (!(tkt.actions[0].posted.at >= cur_time - 2000 && tkt.actions[0].posted.at - cur_time < 3000)) {
        return "Timestamp was incorrect!\nExpected: " + (0,_utils__WEBPACK_IMPORTED_MODULE_2__.datec)(cur_time) +
            "\nReceived: " + (0,_utils__WEBPACK_IMPORTED_MODULE_2__.datec)(tkt.actions[0].posted.at) +
            "\nThe difference: " + JSON.stringify(Math.abs(tkt.actions[0].posted.at - cur_time) + "ms");
    }
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("tkt_modify"), "stability", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp, is_global: true, is_priority: false, title, text }, org: org_id });
    const req = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfMeta", { org: org_id, grp: "Global" }));
    const tkt = req.data[0];
    const options = { is_global: false, is_priority: true, is_resolved: true };
    let dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("There was a problem modifying a ticket!", { org: org_id, id: tkt._id, options }, (dirty_object) => ["tkt_modify", dirty_object], { options: [{}] });
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("There was a problem modifying a ticket!", options, (dirty_object) => ["tkt_modify", { org: org_id, id: tkt._id, options: dirty_object }], { is_global: [true, false, null, undefined], is_priority: [true, false, null, undefined], is_resolved: [true, false, null, undefined], võti: [{}] });
    return dirty_result;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can remove whitespaces from title", "extra", async (context) => {
    let title = " väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    title = "väga tõsine probleem ";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    title = "väga tõsine \n\n\n\n\n probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    const tickets = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    if (!tickets[0].title.startsWith("v"))
        return "The leading whitespace was not removed from title!";
    if (!tickets[1].title.endsWith("m"))
        return "The trailing whitespace was not removed from title!";
    if (tickets[2].title.includes("\n"))
        return "Newlines were not removed from title!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can remove whitespaces from text (description)", "extra", async (context) => {
    const title = "väga tõsine probleem";
    let text = " erakordselt tõsine probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    text = "erakordselt tõsine probleem ";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    text = "\n\n\n\nerakordselt \n\n\n\n tõsine \n\n\n\n\n probleem\n\n\n\n";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    const tickets = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    if (!tickets[0].comments[0].text[0].startsWith("e"))
        return "The leading whitespace was not removed from text!";
    if (!tickets[1].comments[0].text[0].endsWith("m"))
        return "The trailing whitespace was not removed from text!";
    const text_string = tickets[2].comments[0].text.join("ä");
    if (text_string.includes("\n"))
        return "Newlines were not removed from text!";
    if (text_string !== "erakordseltätõsineäprobleem")
        return "Paragraphs were not split up correctly!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)("Can remove whitespaces from reply", "extra", async (context) => {
    let text = " test reply";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org: org_id, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    const tickets = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_addReply", { org: org_id, id: tickets[0]._id, text });
    text = "test reply ";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_addReply", { org: org_id, id: tickets[0]._id, text });
    text = "\n\n\n\n test \n\n\n\n reply \n\n\n\n";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_addReply", { org: org_id, id: tickets[0]._id, text });
    const fixed_replies = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0].comments;
    if (!fixed_replies[1].text[0].startsWith("t"))
        return "The leading whitespace was not removed from reply!";
    if (!fixed_replies[2].text[0].endsWith("y"))
        return "The trailing whitespace was not removed from reply!";
    const final_string = fixed_replies[3].text.join("ä");
    if (final_string.includes("\n"))
        return "Newlines were not removed from text!";
    if (final_string !== "testäreply")
        return "Paragraphs were not split up correctly!";
    return true;
});
(0,_request_test__WEBPACK_IMPORTED_MODULE_0__.createTest)((0,_utils__WEBPACK_IMPORTED_MODULE_2__.nameDirtyInputTest)("tkt_read"), "stability", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_new", { org, name: "new grp" });
    const groups = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_new", { doc: { grp, is_global: false, is_priority: false, title, text }, org });
    const tkt = (await (0,_request_test__WEBPACK_IMPORTED_MODULE_0__.request)("tkt_getOfGroup", { org, grp })).data[0];
    let dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("Ticket read received dirty input!", { org, tkt: tkt._id, key: 1 }, (dirty_object) => ["tkt_read", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await (0,_utils__WEBPACK_IMPORTED_MODULE_2__.testDirtyInputRequest)("Ticket read received dirty input!", { org, grp }, (dirty_object) => ["tkt_read", dirty_object]);
    return dirty_result;
});


/***/ }),

/***/ "./test/requests/utils.ts":
/*!********************************!*\
  !*** ./test/requests/utils.ts ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "timestamp": () => (/* binding */ timestamp),
/* harmony export */   "datec": () => (/* binding */ datec),
/* harmony export */   "testDirtyInputRequest": () => (/* binding */ testDirtyInputRequest),
/* harmony export */   "shiftTime": () => (/* binding */ shiftTime),
/* harmony export */   "nameDirtyInputTest": () => (/* binding */ nameDirtyInputTest),
/* harmony export */   "verifyRequestAccepted": () => (/* binding */ verifyRequestAccepted),
/* harmony export */   "verifyRequestDenied": () => (/* binding */ verifyRequestDenied)
/* harmony export */ });
/* unused harmony exports sleep, combinateDirtyObjects */
/* harmony import */ var luxon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! luxon */ "luxon");
/* harmony import */ var luxon__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(luxon__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _request_test__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./request_test */ "./test/requests/request_test.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./config */ "./test/requests/config.ts");



function timestamp() { return new Date().getTime(); }
function sleep(ms) { return new Promise(r => setTimeout(() => { r(true); }, ms)); }
const dirty_values = [null, "", 123, true, false, [], {}, undefined];
const object_root_key = "võti";
async function combinateDirtyObjects(valid_object, foreach, exclude) {
    const clean_json = JSON.stringify(valid_object);
    const field_names = Object.keys(valid_object);
    for (const field_name of field_names) {
        for (const dirty_value of dirty_values) {
            if (JSON.stringify(dirty_value) === JSON.stringify(valid_object[field_name]) ||
                exclude && exclude[field_name]?.some((current) => JSON.stringify(current) === JSON.stringify(dirty_value)))
                continue;
            const dirty_object = JSON.parse(clean_json);
            dirty_object[field_name] = dirty_value;
            const result = await foreach(dirty_object);
            if (result !== true)
                return result;
        }
    }
    for (const dirty_value of dirty_values) {
        if (JSON.stringify(dirty_value) === JSON.stringify(valid_object) ||
            exclude && exclude[object_root_key]?.some((current) => JSON.stringify(current) === JSON.stringify(dirty_value)))
            continue;
        const result = await foreach(dirty_value);
        if (result !== true)
            return result;
    }
    return true;
}
function datec(time) {
    if (typeof time != "number")
        return "invalid timestamp";
    const t = luxon__WEBPACK_IMPORTED_MODULE_0__.DateTime.fromMillis(time);
    function z(n) { return (n + "").padStart(2, "0"); }
    return `${t.year}-${z(t.month)}-${z(t.day)}`;
}
async function testDirtyInputRequest(special_msg, valid_object, request_params, exclude) {
    const peculiar_string = "blä_blä_blä_blä";
    return await combinateDirtyObjects(valid_object, async (dirty_object) => {
        const all_params = request_params(dirty_object);
        const req = await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)(...all_params);
        if (!req.is200 || req.ok) {
            const req_params = request_params(peculiar_string)[1];
            let field_with_dirty_input;
            if (req_params === peculiar_string) {
                field_with_dirty_input = "(entire object)";
            }
            else {
                const weird_params = JSON.stringify(req_params).split('"' + peculiar_string + '"')[0];
                const last_index = weird_params.lastIndexOf("\"");
                const buffer = weird_params.slice(0, last_index);
                const final = buffer.lastIndexOf("\"");
                field_with_dirty_input = buffer.slice(final + 1);
            }
            const error_details = "The field containing the dirty input was \"" + field_with_dirty_input + "\" and the dirty object was\n" + JSON.stringify(dirty_object);
            if (!req.is200)
                return "Request " + all_params[0] + " has crashed the server!\n" + error_details;
            else
                return special_msg + "\nRequest was: " + all_params[0] + ". " + error_details;
        }
        else
            return true;
    }, exclude);
}
async function shiftTime(user, timestamp, action) {
    try {
        await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: true, usr: _config__WEBPACK_IMPORTED_MODULE_2__.admin_usr_name, pwd: _config__WEBPACK_IMPORTED_MODULE_2__.admin_pwd });
        await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("testing_setServerTime", { key: _config__WEBPACK_IMPORTED_MODULE_2__.test_key, timestamp });
        await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", user);
        return await action();
    }
    finally {
        await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", { admin: true, usr: _config__WEBPACK_IMPORTED_MODULE_2__.admin_usr_name, pwd: _config__WEBPACK_IMPORTED_MODULE_2__.admin_pwd });
        await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("testing_setServerTime", { key: _config__WEBPACK_IMPORTED_MODULE_2__.test_key, timestamp: "reset" });
        await (0,_request_test__WEBPACK_IMPORTED_MODULE_1__.request)("login", user);
    }
}
function nameDirtyInputTest(request_name) {
    return "Server does not accept dirty input from: " + request_name;
}
function verifyRequestAccepted(req, description) {
    if (!req.is200)
        throw "Server has crashed due to following action!\n" + description;
    if (!req.ok)
        throw "Request did not return ok!\n" + description;
}
function verifyRequestDenied(req, description) {
    if (!req.is200)
        throw "Server has crashed due to following action!\n" + description;
    if (req.ok)
        throw "Request returned ok, but it was supposed to be denied!\n" + description;
}
/*
createTest("Can access self-made tickets", "functionality", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";

    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

    await request("grp_new", { org, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org })).data!;
    const grp = groups[0]._id;
    await request("tkt_new", { doc: { grp, is_global: false, is_priority: false, title, text }, org });

    const req_get = (await request("tkt_getOfMeta", { org, grp: "Self" }));
    const tkt = req_get.data![0];
    verifyRequestAccepted(req_get, "Tried to get self-made ticket.");
    if (!(tkt.title === title && tkt.comments[0].text[0] === text)) return "Could not get a self-made ticket!";

    const req_add = await request("tkt_addReply", { org, id: tkt._id, text: "test reply" });
    verifyRequestAccepted(req_add, "Tried to send a reply to a self-made ticket.");
    
    return true;
});

security in the future
createTest("Can create and access tickets in non-joined groups", "functionality", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await createUser(context + 1, "user");
    await request("login", { usr: user0, pwd: "Password123" });

    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
    await request("invite", { org: org_id, email: user1 });
    await request("grp_new", { org: org_id, name: "new grp" });

    const groups = (await request("grp_getOfOrg", { org: org_id })).data!;
    const grp_id = groups[0]._id;
    await request("logout");

    await request("login", { usr: user1, pwd: "Password123" });
    const invitation = (await request("getInvitations")).data!;
    await request("acceptInvitation", { inv: invitation[0]._id });

    // this is okay, Raul approved
    const req = await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    verifyRequestAccepted(req, "Tried to create a ticket while being a member of an org but not a member of a grp.");

    // must always be able to access self-made tickets
    const req_meta = (await request("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    const tkt = req_meta.data![0];
    verifyRequestAccepted(req_meta, "Tried to get a self-made ticket from grp, where user does not belong.");
    if (!(tkt.title === "väga tõsine probleem" && tkt.comments[0].text[0] === "erakordselt tõsine probleem")) return "Could not get a self-made ticket from grp, where user does not belong!";

    const req_add = await request("tkt_addReply", { org: org_id, id: tkt._id, text: "test reply" });
    verifyRequestAccepted(req_add, "Tried to add a reply to a self-made ticket, but it was not added to a grp, where user does not belong to.");

    return true;
});

createTest("Everyone in org can reply to global tkt", "functionality", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await createUser(context + 1, "user");
    await request("login", { usr: user0, pwd: "Password123" });

    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
    await request("invite", { org: org_id, email: user1 });
    await request("grp_new", { org: org_id, name: "new grp" });

    const groups = (await request("grp_getOfOrg", { org: org_id })).data!;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: true, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });

    await request("logout");

    await request("login", { usr: user1, pwd: "Password123" });

    const invitation = (await request("getInvitations")).data!;
    await request("acceptInvitation", { inv: invitation[0]._id });

    const req_meta = (await request("tkt_getOfMeta", { org: org_id, grp: "Global" }));
    const tkt = req_meta.data![0];
    const req_add = await request("tkt_addReply", { org: org_id, id: tkt._id, text: "test reply" });
    verifyRequestAccepted(req_add, "Tried to reply to a global ticket.");

    return true;
});
*/


/***/ }),

/***/ "@sinonjs/fake-timers":
/*!***************************************!*\
  !*** external "@sinonjs/fake-timers" ***!
  \***************************************/
/***/ ((module) => {

module.exports = require("@sinonjs/fake-timers");

/***/ }),

/***/ "email-validator":
/*!**********************************!*\
  !*** external "email-validator" ***!
  \**********************************/
/***/ ((module) => {

module.exports = require("email-validator");

/***/ }),

/***/ "json5":
/*!************************!*\
  !*** external "json5" ***!
  \************************/
/***/ ((module) => {

module.exports = require("json5");

/***/ }),

/***/ "json5/lib/register":
/*!*************************************!*\
  !*** external "json5/lib/register" ***!
  \*************************************/
/***/ ((module) => {

module.exports = require("json5/lib/register");

/***/ }),

/***/ "luxon":
/*!************************!*\
  !*** external "luxon" ***!
  \************************/
/***/ ((module) => {

module.exports = require("luxon");

/***/ }),

/***/ "node-fetch":
/*!*****************************!*\
  !*** external "node-fetch" ***!
  \*****************************/
/***/ ((module) => {

module.exports = require("node-fetch");

/***/ }),

/***/ "shelljs":
/*!**************************!*\
  !*** external "shelljs" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("shelljs");

/***/ }),

/***/ "source-map-support":
/*!*************************************!*\
  !*** external "source-map-support" ***!
  \*************************************/
/***/ ((module) => {

module.exports = require("source-map-support");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./test/requests/request_test.ts");
/******/ 	
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQTBEO0FBRVg7QUFDVztBQUNFO0FBTTVELE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQztBQUNwQixNQUFNLE9BQU8sR0FBRyxPQUFPLENBQUM7QUFDeEIsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO0FBQ2xCLE1BQU0sV0FBVyxHQUFHLGFBQWEsQ0FBQztBQUNsQyxNQUFNLGdCQUFnQixHQUFHLHNCQUFzQixDQUFDO0FBQ2hELE1BQU0sZ0JBQWdCLEdBQUcsbUJBQW1CLENBQUM7QUFDN0MsTUFBTSxxQkFBcUIsR0FBRyw0QkFBNEIsQ0FBQztBQUMzRCxNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUM7QUFDMUIsTUFBTSxTQUFTLEdBQUcsU0FBUyxDQUFDO0FBWTVCLE1BQU0sZUFBZSxHQUFHLHVCQUF1QixDQUFDO0FBQ2hELE1BQU0sVUFBVSxHQUFHLENBQXNCLENBQUksRUFBbUMsRUFBRSxDQUFDLEdBQUcsZUFBZSxHQUFHLENBQUMsRUFBRSxDQUFDO0FBZ0I1RyxTQUFTLFNBQVMsQ0FBZ0MsQ0FBSSxFQUFFLGFBQW9DO0lBQzNGLElBQUksQ0FBQyxpREFBUSxDQUFDLENBQUMsQ0FBQztRQUFFLE9BQU8sS0FBSyxDQUFDO0lBQy9CLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztJQUVqQixLQUFLLE1BQU0sR0FBRyxJQUFJLGFBQWEsRUFBRTtRQUNoQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsTUFBTSxnQkFBZ0IsR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDNUMsSUFBSSxJQUFJLEdBQUcsT0FBTyxnQkFBZ0IsSUFBSSxRQUFRO1lBQzdDLENBQUMsQ0FBQyxnQkFBcUM7WUFDdkMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUVULElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUNyQyxJQUFJLENBQUMsS0FBSyxTQUFTLElBQUksQ0FBQyxLQUFLLElBQUk7Z0JBQUUsSUFBSSxHQUFHLElBQUksQ0FBQzs7Z0JBQzFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBYyxDQUFDO1NBQ3hEO1FBRUQsUUFBUSxJQUFJLEVBQUU7WUFDYixLQUFLLEtBQUs7Z0JBQUU7b0JBQ1gsSUFBSSxHQUFHLGlEQUFRLENBQUMsQ0FBQyxDQUFDOzJCQUNkLENBQUMsSUFBSSxJQUFJLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxDQUFRLEVBQUUsZ0JBQXVCLENBQUMsQ0FBQyxDQUFDO2lCQUMvRTtnQkFBQyxNQUFNO1lBQ1IsS0FBSyxXQUFXO2dCQUFFO29CQUNqQixJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksUUFBUSxDQUFDO2lCQUM1QjtnQkFBQyxNQUFNO1lBQ1IsS0FBSyxnQkFBZ0I7Z0JBQUU7b0JBQ3RCLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxRQUFROzJCQUN2QixDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDO2lCQUNuQjtnQkFBQyxNQUFNO1lBQ1IsS0FBSyxJQUFJO2dCQUFFO29CQUNWLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxRQUFROzJCQUN2QixDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN0QjtnQkFBQyxNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUFFO29CQUNmLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxTQUFTLENBQUM7aUJBQzdCO2dCQUFDLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQUU7b0JBQ2QsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLFFBQVEsQ0FBQztpQkFDNUI7Z0JBQUMsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFBRTtvQkFDYixJQUFJLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDeEI7Z0JBQUMsTUFBTTtZQUNSLEtBQUssZ0JBQWdCO2dCQUFFO29CQUN0QixJQUFJLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7MkJBQ25CLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFZLEVBQUUsRUFBRSxDQUFDLE9BQU8sSUFBSSxJQUFJLFFBQVEsQ0FBQyxDQUFDO2lCQUN2RDtnQkFBQyxNQUFNO1lBQ1IsS0FBSyxxQkFBcUI7Z0JBQUU7b0JBQzNCLElBQUksR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzsyQkFDbkIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQVksRUFBRSxFQUFFLENBQUMsT0FBTyxJQUFJLElBQUksUUFBUSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDNUU7Z0JBQUMsTUFBTTtZQUNSLE9BQU8sQ0FBQyxDQUFDLEdBQUc7U0FDWjtRQUVELElBQUksQ0FBQyxJQUFJO1lBQUUsT0FBTyxLQUFLLENBQUM7S0FDeEI7SUFFRCxPQUFPLElBQUksQ0FBQztBQUNiLENBQUM7QUFFRCxNQUFNLFVBQVUsR0FBRyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUM7QUFFdkIsTUFBTSxnQkFBZ0IsR0FBTztJQUNuQyxhQUFhLEVBQUUsVUFBVTtJQUN6QixjQUFjLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRTtRQUNyQixPQUFPLENBQUMsS0FBSTtlQUNSLE9BQU8sQ0FBQyxDQUFDLE9BQU8sSUFBSSxRQUFRO2VBQzVCLDREQUFnQixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FDOUIsQ0FBQztJQUNILENBQUM7SUFDRCxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRTtRQUNsQixPQUFPLENBQUMsS0FBSTtlQUNSLE9BQU8sQ0FBQyxDQUFDLEtBQUssSUFBSSxRQUFRO2VBQzFCLE9BQU8sQ0FBQyxDQUFDLEtBQUssSUFBSSxRQUFRO2VBQzFCLE9BQU8sQ0FBQyxDQUFDLEtBQUssSUFBSSxRQUFRO2VBQzFCLDZEQUFvQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQztlQUN4Qyw2REFBb0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUM7ZUFDeEMsNkRBQW9CLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQzNDLENBQUM7SUFDSCxDQUFDO0lBQ0QsS0FBSyxFQUFFLFVBQVU7SUFDakIsTUFBTSxFQUFFLFVBQVU7SUFDbEIsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUU7UUFDZixPQUFPLENBQUMsS0FBSTtlQUNSLE9BQU8sQ0FBQyxDQUFDLEtBQUssSUFBSSxRQUFRO2VBQzFCLHlEQUFhLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztlQUN0QixPQUFPLENBQUMsQ0FBQyxLQUFLLElBQUksUUFBUTtlQUMxQiw2REFBb0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUM7ZUFDekMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksRUFBRTtlQUNwQixPQUFPLENBQUMsQ0FBQyxLQUFLLElBQUksUUFBUTtlQUMxQiw2REFBb0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUM7ZUFDekMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksRUFBRTtlQUNwQiw0REFBZ0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQzFCLENBQUM7SUFDSCxDQUFDO0lBQ0QsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUNoQixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osSUFBSSxFQUFFLGdCQUFnQjtLQUN0QixDQUFDLENBQ0Y7SUFDRCxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDeEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO0tBQ1QsQ0FBQyxDQUNGO0lBQ0QsVUFBVSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUNsQixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7S0FDVCxDQUFDLENBQ0Y7SUFDRCxjQUFjLEVBQUUsVUFBVTtJQUMxQixPQUFPLEVBQUUsVUFBVTtJQUNuQixZQUFZLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ3BCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtLQUNULENBQUMsQ0FDRjtJQUNELGtCQUFrQixFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUMxQixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7S0FDVCxDQUFDLENBQ0Y7SUFDRCxjQUFjLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ3RCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtRQUNULEdBQUcsRUFBRSxJQUFJO1FBQ1QsSUFBSSxFQUFFLElBQUk7UUFDVixHQUFHLEVBQUUsUUFBUTtLQUNiLENBQUMsQ0FDRjtJQUNELE1BQU0sRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDZCxTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7UUFDVCxLQUFLLEVBQUUsV0FBVztLQUNsQixDQUFDO1dBQ0MseURBQWEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQ3pCO0lBRUQsY0FBYztJQUNkLElBQUksRUFBRSxVQUFVO0lBQ2hCLEtBQUssRUFBRSxVQUFVO0lBQ2pCLFVBQVUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDbEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLE9BQU8sRUFBRSxTQUFTO0tBQ2xCLENBQUMsQ0FDRjtJQUNELGNBQWMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDdEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLDJCQUEyQixFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUM7UUFDbEQsMkJBQTJCLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQztLQUNsRCxDQUFDLENBQ0Y7SUFDRCxlQUFlLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ3ZCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtLQUNULENBQUMsQ0FDRjtJQUNELGNBQWMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDdEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO1FBQ1QsR0FBRyxFQUFFLElBQUk7UUFDVCxJQUFJLEVBQUUsZ0JBQWdCO1FBQ3RCLE9BQU8sRUFBRSxXQUFXO0tBQ3BCLENBQUMsQ0FDRjtJQUNELGlCQUFpQixFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUN6QixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7UUFDVCxHQUFHLEVBQUUsSUFBSTtLQUNULENBQUMsQ0FDRjtJQUNELGNBQWMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDdEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO0tBQ1QsQ0FBQyxDQUNGO0lBQ0QsWUFBWTtJQUVaLGdCQUFnQjtJQUNoQixPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRTtRQUNkLE9BQU8sQ0FBQyxLQUFJO2VBQ1IsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLFFBQVE7ZUFDeEIsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLFFBQVE7ZUFDekIsNkRBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQzFDLENBQUM7SUFDSCxDQUFDO0lBQ0QsVUFBVSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUNsQixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7UUFDVCxHQUFHLEVBQUUsSUFBSTtLQUNULENBQUMsQ0FDRjtJQUNELFlBQVksRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDcEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO0tBQ1QsQ0FBQyxDQUNGO0lBQ0QsVUFBVSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUU7UUFDakIsT0FBTyxDQUFDLEtBQUk7ZUFDUixPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksUUFBUTtlQUN4QixPQUFPLENBQUMsQ0FBQyxJQUFJLElBQUksUUFBUTtlQUN6Qiw2REFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FDMUMsQ0FBQztJQUNILENBQUM7SUFDRCxjQUFjLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ3RCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtRQUNULEdBQUcsRUFBRSxJQUFJO1FBQ1QsT0FBTyxFQUFFLEtBQUs7S0FDZCxDQUFDO1dBQ0MsQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDakY7SUFDRCxZQUFZO0lBRVosb0JBQW9CO0lBQ3BCLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDZixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7UUFDVCxHQUFHLEVBQUU7WUFDSixLQUFLLEVBQUUsZ0JBQWdCO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsVUFBVSxFQUFFLFFBQVE7U0FDcEI7S0FDRCxDQUFDO1dBQ0MsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsS0FBSyxTQUFTLElBQUksT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUM7V0FDbkUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsTUFBTSxJQUFJLENBQUM7V0FDOUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztXQUMxQixDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7V0FDbkcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLO2FBQ1osTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUF5QixFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUM7YUFDeEQsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FDWCxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsV0FBVyxFQUFFLHFCQUFxQixFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsQ0FBQztlQUNqRSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUNsRyxDQUFDO1dBQ0EsQ0FDRixDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsSUFBSSxTQUFTLElBQUksQ0FDOUIsS0FBSTtlQUNELE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLElBQUksUUFBUTtlQUNqQyxDQUNGLE1BQUs7bUJBQ0YsQ0FDRixLQUFJO3VCQUNELENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxPQUFPO3VCQUNoQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJO3VCQUNuQixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxRQUFRO3VCQUN0QyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sSUFBSSx3REFBVzt1QkFDdEQsQ0FBQyxzREFBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsT0FBUSxDQUFTLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksU0FBUyxDQUFDLENBQzNFO21CQUNFLENBQ0YsS0FBSTt1QkFDRCxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksU0FBUzt1QkFDbEMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxRQUFRLElBQUksU0FBUzt1QkFDM0MsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksUUFBUTt1QkFDckMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7dUJBQ3ZCLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQzNCLENBQ0QsQ0FDRCxDQUNEO1dBQ0UsQ0FDRixDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixJQUFJLFNBQVMsSUFBSSxDQUN2QyxLQUFJO2VBQ0QsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixJQUFJLFFBQVE7ZUFDMUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsSUFBSSxRQUFRO2VBQzlDLENBQUMsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsR0FBVSxJQUFJLEVBQUU7ZUFDeEMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsSUFBSSxRQUFRLENBQ3hELENBQ0QsQ0FDRDtJQUNELE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDZixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7UUFDVCxFQUFFLEVBQUUsSUFBSTtLQUNSLENBQUMsQ0FDRjtJQUNELGNBQWMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDdEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO1FBQ1QsRUFBRSxFQUFFLElBQUk7UUFDUixPQUFPLEVBQUUsUUFBUTtRQUNqQixJQUFJLEVBQUUsZ0JBQWdCO0tBQ3RCLENBQUMsQ0FDRjtJQUNELFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFO1FBQ2YsT0FBTyxDQUFDLEtBQUk7ZUFDUixPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksUUFBUTtlQUN4QixDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUNqQyxDQUFDO0lBQ0gsQ0FBQztJQUNELFlBQVksRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDcEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO0tBQ1QsQ0FBQyxDQUNGO0lBQ0QsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ3pCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtRQUNULEVBQUUsRUFBRSxJQUFJO1FBQ1IsT0FBTyxFQUFFLFFBQVE7UUFDakIsS0FBSyxFQUFFLFFBQVE7UUFDZixNQUFNLEVBQUUsV0FBVztLQUNuQixDQUFDO1dBQ0MsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7V0FDZixDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztXQUNaLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUM3QztJQUNELFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDaEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO1FBQ1QsRUFBRSxFQUFFLElBQUk7S0FDUixDQUFDLENBQ0Y7SUFDRCxjQUFjLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ3RCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtRQUNULEdBQUcsRUFBRSxJQUFJO0tBQ1QsQ0FBQyxDQUNGO0lBQ0QsWUFBWTtJQUVaLGNBQWM7SUFDZCxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ2YsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLElBQUksRUFBRSxnQkFBZ0I7UUFDdEIsUUFBUSxFQUFFLFdBQVc7UUFDckIsT0FBTyxFQUFFLEtBQUs7S0FDZCxDQUFDO1dBQ0MsNkRBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQzFDO0lBQ0QsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUNoQixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7UUFDVCxJQUFJLEVBQUUsSUFBSTtLQUNWLENBQUMsQ0FDRjtJQUNELGNBQWMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDdEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO1FBQ1QsSUFBSSxFQUFFLElBQUk7UUFDVixHQUFHLEVBQUUsUUFBUTtLQUNiLENBQUM7V0FDQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUN6QjtJQUNELFVBQVUsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFO1FBQ2pCLE9BQU8sQ0FBQyxLQUFJO2VBQ1IsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLFFBQVE7ZUFDeEIsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLFFBQVE7ZUFDekIsNkRBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQzFDLENBQUM7SUFDSCxDQUFDO0lBQ0QsY0FBYyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUU7UUFDckIsT0FBTyxDQUFDLEtBQUk7ZUFDUixPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksUUFBUTtlQUN4QixDQUFDLENBQUMsT0FBTztlQUNULE9BQU8sQ0FBQyxDQUFDLE9BQU8sSUFBSSxRQUFRO2VBQzVCLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsS0FBSyxTQUFTLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLGlCQUFpQixJQUFJLFNBQVMsQ0FBQyxDQUNqRyxDQUFDO0lBQ0gsQ0FBQztJQUNELG9CQUFvQixFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUM1QixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7S0FDVCxDQUFDLENBQ0Y7SUFDRCxZQUFZO0lBRVosY0FBYztJQUNkLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDakIsSUFBSSxDQUNKO0lBQ0QsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUNqQixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7UUFDVCxPQUFPLEVBQUUsZ0JBQWdCO1FBQ3pCLEtBQUssRUFBRSxTQUFTO1FBQ2hCLEdBQUcsRUFBRSxnQkFBZ0I7S0FDckIsQ0FBQyxDQUNGO0lBQ0QsWUFBWTtJQUVaLGtCQUFrQjtJQUNsQixPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ2YsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO1FBQ1QsR0FBRyxFQUFFO1lBQ0osS0FBSyxFQUFFLGdCQUFnQjtZQUN2QixHQUFHLEVBQUUsSUFBSTtZQUNULFNBQVMsRUFBRSxTQUFTO1lBQ3BCLFdBQVcsRUFBRSxTQUFTO1NBQ3RCO0tBQ0QsQ0FBQztXQUNDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksUUFBUTtXQUM3QixPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxJQUFJLFFBQVE7V0FDOUIsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUMzQjtJQUNELFlBQVksRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDcEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEVBQUUsRUFBRSxJQUFJO1FBQ1IsR0FBRyxFQUFFLElBQUk7UUFDVCxJQUFJLEVBQUUsZ0JBQWdCO0tBQ3RCLENBQUMsQ0FDRjtJQUNELGNBQWMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FDdEIsU0FBUyxDQUFDLENBQUMsRUFBRTtRQUNaLEdBQUcsRUFBRSxJQUFJO1FBQ1QsR0FBRyxFQUFFLElBQUk7S0FDVCxDQUFDLENBQ0Y7SUFDRCxhQUFhLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ3JCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtRQUNULEdBQUcsRUFBRSxnQkFBZ0I7S0FDckIsQ0FBQyxDQUNGO0lBQ0QsWUFBWSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUNwQixTQUFTLENBQUMsQ0FBQyxFQUFFO1FBQ1osR0FBRyxFQUFFLElBQUk7S0FDVCxDQUFDLENBQ0Y7SUFDRCxVQUFVLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ2xCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtRQUNULEVBQUUsRUFBRSxJQUFJO1FBQ1IsT0FBTyxFQUFFO1lBQ1IsU0FBUyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUM7WUFDaEMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUM7WUFDbEMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUM7U0FDbEM7S0FDRCxDQUFDLENBQ0Y7SUFDRCxVQUFVLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ2xCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtRQUNULEVBQUUsRUFBRSxJQUFJO1FBQ1IsSUFBSSxFQUFFLGdCQUFnQjtLQUN0QixDQUFDLENBQ0Y7SUFDRCxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQ2hCLFNBQVMsQ0FBQyxDQUFDLEVBQUU7UUFDWixHQUFHLEVBQUUsSUFBSTtLQUNULENBQUM7V0FDQyxDQUNGLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FDekUsQ0FDRDtJQUNELFlBQVk7Q0FDWixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM2U2QjtBQUNFO0FBRWtCO0FBRTVDLFNBQVMsR0FBRyxDQUFDLENBQU07SUFDekIsTUFBTSxLQUFLLEdBQUcsU0FBUyxDQUFDO0lBQ3hCLE1BQU0sS0FBSyxHQUFHLFNBQVMsQ0FBQztJQUN4QixJQUFJLElBQUksR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBQzlELElBQUksT0FBTyxDQUFDLElBQUksUUFBUSxFQUFFO1FBQ3pCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDM0QsSUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxJQUFJLEdBQUcsR0FBRyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztLQUM1Qzs7UUFFQSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDekIsQ0FBQztBQUVNLFNBQVMsS0FBSyxDQUFDLEVBQVUsSUFBbUIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDMUcsU0FBUyxNQUFNLENBQUMsR0FBVyxFQUFFLEdBQVcsSUFBSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDdkcsU0FBUyxTQUFTLEtBQUssT0FBTyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztBQUVyRCxTQUFTLGlCQUFpQixDQUFDLEdBQVc7SUFDNUMsSUFBSSxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtRQUM5QixPQUFPLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUMvQztTQUNJO1FBQ0osT0FBTyxHQUFHLENBQUM7S0FDWDtBQUNGLENBQUM7QUFNTSxTQUFTLEVBQUUsQ0FBQyxHQUFXLEVBQUUsU0FBbUI7SUFDbEQsTUFBTSxNQUFNLEdBQUcsNkNBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QixJQUFJLENBQUMsU0FBUyxJQUFJLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxFQUFFO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkNBQTZDLENBQUMsQ0FBQztRQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7S0FDckM7SUFDRCxPQUFPLE1BQU0sQ0FBQztBQUNmLENBQUM7QUFFTSxLQUFLLFVBQVUsR0FBRyxDQUFDLEdBQVcsRUFBRSxTQUFtQjtJQUN6RCxNQUFNLE1BQU0sR0FBRyxNQUFNLDZDQUFJLENBQUMsR0FBRyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDaEQsSUFBSSxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsUUFBUSxJQUFJLENBQUMsRUFBRTtRQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7UUFDM0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTyxDQUFDLENBQUMsQ0FBQztRQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFPLENBQUMsQ0FBQyxDQUFDO1FBQzVDLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMvQixPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEIsTUFBTSxJQUFJLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0tBQzNDO0lBQ0QsT0FBTyxNQUFNLENBQUM7QUFDZixDQUFDO0FBRU0sU0FBUyxjQUFjLENBQUMsTUFBZ0I7SUFDOUMsTUFBTSxNQUFNLEdBQUcsRUFBVyxDQUFDO0lBQzNCLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7UUFDdEMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDL0MsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDM0IsTUFBTSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN6RSxDQUFDLENBQUMsQ0FBQztBQUNKLENBQUM7QUFFTSxTQUFTLElBQUksQ0FBQyxJQUFZO0lBQ2hDLElBQUksT0FBTyxJQUFJLElBQUksUUFBUTtRQUFFLE9BQU8sbUJBQW1CLENBQUM7SUFDeEQsTUFBTSxDQUFDLEdBQUcsc0RBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEMsU0FBUyxDQUFDLENBQUMsQ0FBUyxJQUFJLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0QsT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO0FBQzFFLENBQUM7QUFFTSxTQUFTLGVBQWUsQ0FBSSxLQUFVLEVBQUUsSUFBTztJQUNyRCxNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2xDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO1FBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FBRTtBQUM1QyxDQUFDO0FBRU0sU0FBUyxnQkFBZ0IsQ0FBQyxJQUFZO0lBQzVDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzFFLElBQUksQ0FBQyxDQUFDLE1BQU07UUFBRSxPQUFPLENBQUMsQ0FBQzs7UUFDbEIsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ2xCLENBQUM7QUFFTSxTQUFTLG9CQUFvQixDQUFDLElBQVk7SUFDaEQsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQzlELENBQUM7QUFFTSxTQUFTLFFBQVEsQ0FBQyxDQUFNO0lBQzlCLE9BQU8sT0FBTyxDQUFDLElBQUksUUFBUSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQy9ELENBQUM7QUFFTSxTQUFTLGtCQUFrQixDQUVoQyxJQUFPLEVBQUUsRUFBSyxFQUFFLElBQVU7SUFDM0IsS0FBSyxNQUFNLEdBQUcsSUFBSSxJQUFJO1FBQUUsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksU0FBUztZQUFHLEVBQUUsQ0FBQyxHQUFHLENBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDbEYsQ0FBQztBQUVNLE1BQU0scUJBQXFCLEdBQW1FLENBQUMsQ0FBQyxFQUFFLEVBQUU7SUFDMUcscUJBQXFCLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxDQUFDO0lBQ3pDLElBQUksQ0FBQyxJQUFJLE9BQU87UUFBRSxxQkFBcUIsQ0FBQyxLQUFLLEdBQUcseURBQWtCLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLGlCQUFpQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7QUFDekcsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7OztBQ3RHSyxTQUFTLFdBQVcsQ0FBQyxLQUFlO0lBQzFDLE9BQU8sS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQU8sQ0FBQztBQUNyQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ2RnQztBQUsxQixNQUFNLElBQUksR0FBRyxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBVSxDQUFDO0FBQ3hFLE1BQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFVLENBQUM7QUFrRjNJLFNBQVMsc0JBQXNCLENBQUMsQ0FBWTtJQUNsRCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDZCxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUM7SUFFbEIsS0FBSyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFO1FBQzNCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNqQixLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDM0IsS0FBSyxNQUFNLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSztnQkFBRSxJQUFJLElBQUksQ0FBQyxHQUFHLElBQUksS0FBSztvQkFBRSxTQUFTLElBQUksQ0FBQyxDQUFDO1NBQ3JFO2FBQ0k7WUFDSixLQUFLLEVBQUUsQ0FBQztZQUNSLElBQUksSUFBSSxDQUFDLEdBQUcsSUFBSSxLQUFLO2dCQUFFLFNBQVMsSUFBRyxDQUFDLENBQUM7U0FDckM7S0FDRDtJQUVELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBRXZELE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLEVBQUUsQ0FBQztBQUM1RSxDQUFDO0FBRU0sU0FBUyxzQkFBc0IsQ0FBQyxDQUFZLEVBQUUsUUFBZ0I7SUFDcEUsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFO1FBQ2YsSUFBSSxDQUFDLENBQUMsUUFBUyxDQUFDLE1BQU0sSUFBSSxPQUFPLEVBQUU7WUFDbEMsTUFBTSxTQUFTLEdBQUcsQ0FBQyxHQUFHLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQ3JDLE1BQU0sR0FBRyxHQUFHLHNEQUFtQixDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNsRSxNQUFNLGNBQWMsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDO1lBQ25DLEtBQUssSUFBSSxDQUFDLEdBQUcsY0FBYyxFQUFFLENBQUMsSUFBSSxjQUFjLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUMxRCxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFBRSxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFHLGNBQWMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2hHO1lBQ0QsT0FBTyxDQUFDLENBQUM7U0FDVDthQUNJO1lBQ0osTUFBTSxZQUFZLEdBQUcsQ0FBQyxRQUFrQixFQUFFLEVBQUU7Z0JBQzNDLE1BQU0sRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLFFBQTJCLENBQUM7Z0JBQ3hELE9BQU8sQ0FDTixRQUFRO29CQUNQLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztvQkFDekMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsQ0FDdEMsQ0FBQztZQUNILENBQUMsQ0FBQztZQUVGLE1BQU0sU0FBUyxHQUFHLHNEQUFtQixDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUN4RSxNQUFNLGdCQUFnQixHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsWUFBWSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN6RSxJQUFJLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxHQUFHLFNBQVMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDdkQsT0FBTyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNuQztpQkFDSTtnQkFDSixNQUFNLFNBQVMsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2hELE9BQU8sU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsRUFBRSxZQUFZLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2xFO1NBQ0Q7S0FDRDs7UUFDSSxPQUFPLENBQUMsQ0FBQztBQUNmLENBQUM7Ozs7Ozs7Ozs7Ozs7O0FDMUZNLFNBQVMsZ0JBQWdCLENBQUMsQ0FBVTtJQUMxQyxPQUFPLENBQ04sT0FBTyxDQUFDLElBQUksUUFBUTtRQUNwQixDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FDYixDQUFDO0FBQ0gsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FDckRELDRFQUE0RTtBQUNyRSxNQUFNLEtBQUssR0FBRyxLQUFXLENBQUM7QUFFakMsMkZBQTJGO0FBQ3BGLE1BQU0sUUFBUSxHQUFHLEtBQVcsQ0FBQztBQUs3QixTQUFTLElBQUksQ0FBQyxDQUFVLElBQWEsT0FBTyxPQUFPLENBQUMsSUFBSSxRQUFRLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFLeEYsU0FBUyxZQUFZLENBQVcsZUFBcUM7SUFDM0UsT0FBTyxJQUFJLEtBQUssQ0FBQyxFQUFpQyxFQUFFO1FBQ25ELEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsRUFBRTtZQUN6QixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssU0FBUztnQkFDbkQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM5QyxPQUFPLE1BQU0sQ0FBQyxRQUFjLENBQUMsQ0FBQztRQUMvQixDQUFDO0tBQ0QsQ0FBQyxDQUFDO0FBQ0osQ0FBQztBQUVNLE1BQU0sV0FBVztJQU12QixZQUFZLEdBQXlCO1FBSnJDLFNBQUksR0FBZSxFQUFFLENBQUM7UUFLckIsSUFBSSxPQUFPLEdBQUcsSUFBSSxRQUFRO1lBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBZSxDQUFDLENBQUM7O1lBQ2pFLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVELEtBQUssQ0FBQyxLQUFrQjtRQUN2QixLQUFLLEtBQUwsS0FBSyxHQUFLLEVBQUUsRUFBQztRQUNiLE1BQU0sR0FBRyxHQUFHLEVBQXFCLENBQUM7UUFDbEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxLQUFLLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsR0FBRyxDQUFDLEVBQU07UUFDVCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELEdBQUcsQ0FBQyxDQUFXO1FBQ2QsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7WUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7O1lBQzdELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNyQixDQUFDO0lBRUQsTUFBTSxDQUFDLENBQUs7UUFDWCxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzFELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNuQjtJQUNGLENBQUM7SUFFRCxNQUFNO1FBQ0wsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ2xCLENBQUM7Q0FDRDtBQVNELGtHQUFrRztBQUMzRixTQUFTLE1BQU0sQ0FBSSxDQUFNLElBQVksT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztBQzdFbkQsTUFBTSxHQUFHLEdBQUcsdUJBQXVCLENBQUM7QUFDcEMsTUFBTSxjQUFjLEdBQUcsbUJBQW1CLENBQUM7QUFDM0MsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDO0FBQzNCLE1BQU0sUUFBUSxHQUFHLEtBQUssQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0g5QixNQUFNLFVBQVUsR0FBRyxPQUFPLENBQUMsUUFBUSxJQUFJLE9BQU8sQ0FBQztBQUMvQyxNQUFNLEtBQUssR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0FBRXRDLDJFQUFxQyxDQUFDLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7QUFFbkM7QUFFNUI7SUFBRSxNQUFNLEVBQUUsR0FBRyxtQkFBTyxDQUFDLG9CQUFPLENBQUMsQ0FBQztJQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztJQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Q0FBRTtBQUVyRyxZQUFNLENBQUMsU0FBUyxFQUFDLFVBQVUsUUFBVixVQUFVLEdBQUssVUFBVSxNQUFXLEVBQUUsV0FBZ0IsSUFBSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDO0FBR3RGO0FBQ0w7QUFDNEI7QUFFNUI7QUFFL0IsTUFBTSxZQUFZLEdBQUcsQ0FBQyxHQUFXLEVBQUUsRUFBVSxFQUFFLE9BQVksRUFBRSxFQUFFO0lBQzlELE1BQU0sVUFBVSxHQUFHLElBQUksZUFBZSxFQUFFLENBQUM7SUFDekMsTUFBTSxPQUFPLEdBQUcsaURBQUssQ0FBQyxHQUFHLEVBQUUsRUFBRSxNQUFNLEVBQUUsVUFBVSxDQUFDLE1BQU0sRUFBRSxHQUFHLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDdEUsTUFBTSxPQUFPLEdBQUcsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6RCxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDckQsQ0FBQyxDQUFDO0FBQ0YsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO0FBQ2hCLElBQUksb0JBQWlDLENBQUM7QUFFdEM7Ozs7O0dBS0c7QUFDSSxLQUFLLFVBQVUsT0FBTyxDQUczQixNQUFzQixFQUFFLFVBQTJCO0lBQ3BELElBQUksTUFBTSxLQUFLLEtBQUssRUFBRTtRQUNyQixNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ1osT0FBTyxJQUFXLENBQUM7S0FDbkI7SUFFRCxPQUFPLFlBQVksQ0FBQyx3Q0FBRyxHQUFHLENBQUMsS0FBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUMsRUFBRSxJQUFJLEVBQUU7UUFDN0QsTUFBTSxFQUFFLE1BQU07UUFDZCxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDdkQsT0FBTyxFQUFFO1lBQ1IsUUFBUSxFQUFFLE1BQU07WUFDaEIsY0FBYyxFQUFFLGtCQUFrQjtTQUNsQztLQUNELENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLFFBQWEsRUFBRSxFQUFFO1FBQy9CLElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7WUFDbEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO1lBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDaEI7UUFDRCxNQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hELElBQUksVUFBVSxJQUFJLElBQUksRUFBRTtZQUN2QixNQUFNLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNyQztRQUNELE9BQU8sSUFBSSxLQUFLLENBQUMsRUFBRSxHQUFHLENBQUMsTUFBTSxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUUsRUFBRTtZQUNoRixHQUFHLEVBQUUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQ3JCLElBQUksSUFBSSxLQUFLLE9BQU8sRUFBRTtvQkFDckIsb0JBQW9CLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNqQztnQkFDRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixDQUFDO1NBQ0QsQ0FBQyxDQUFDO0lBQ0osQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBUSxFQUFFLEVBQUU7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssY0FBYyxFQUFFO1lBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsdUNBQXVDLENBQUMsQ0FBQztZQUNyRCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDO0lBQ3BDLENBQUMsQ0FBQyxDQUFDO0FBQ0osQ0FBQztBQUlELE1BQU0sS0FBSyxHQUF1RixFQUFFLENBQUM7QUFDckcsSUFBSSxtQkFBbUIsR0FBZ0QsSUFBSSxDQUFDO0FBQzVFLE1BQU0sS0FBSyxHQUFHLFVBQVUsQ0FBQztBQUN6QixNQUFNLEdBQUcsR0FBRyxVQUFVLENBQUM7QUFDdkIsTUFBTSxLQUFLLEdBQUcsU0FBUyxDQUFDO0FBQ3hCLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQztBQUdULFNBQVMsVUFBVSxDQUFDLFNBQWlCLEVBQUUsUUFBdUIsRUFBRSxXQUF3RDtJQUM5SCxNQUFNLFdBQVcsR0FBRyxDQUFDLFNBQWlCLEVBQUUsRUFBRTtRQUN6QyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxTQUFTLEdBQUcsU0FBUyxHQUFHLFlBQVksR0FBRyxLQUFLLENBQUMsQ0FBQztRQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQztJQUNmLENBQUMsQ0FBQztJQUVGLE1BQU0sV0FBVyxHQUFHLG1CQUFtQixDQUFDO0lBQ3hDLE1BQU0sSUFBSSxHQUFtRixLQUFLLElBQUksRUFBRTtRQUN2RyxNQUFNLE9BQU8sR0FBRyxVQUFVLEdBQUcsaURBQVMsRUFBRSxHQUFHLEdBQUcsR0FBRyxPQUFPLEVBQUUsQ0FBQztRQUMzRCxJQUFJLFdBQVcsRUFBRTtZQUNoQixNQUFNLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUMzQjtRQUVELElBQUk7WUFDSCxNQUFNLE1BQU0sR0FBRyxNQUFNLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQyxJQUFJLFFBQVEsS0FBSyxPQUFPLElBQUksb0JBQW9CLENBQUMsSUFBSSxLQUFLLENBQUM7Z0JBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1lBRTVHLElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtnQkFDcEIsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxDQUFDO2FBQzdDO2lCQUNJO2dCQUNKLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDcEIsT0FBTyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxDQUFDO2FBQzlDO1NBQ0Q7UUFDRCxPQUFPLEdBQUcsRUFBRTtZQUNYLElBQUksT0FBTyxHQUFHLEtBQUssUUFBUSxFQUFFO2dCQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsR0FBRyxTQUFTLENBQUMsQ0FBQztnQkFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNqQjs7Z0JBQ0ksV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQztTQUM5QztJQUNGLENBQUMsQ0FBQztJQUVGLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDbEIsQ0FBQztBQUVNLFNBQVMsYUFBYSxDQUFDLE1BQTBDO0lBQ3ZFLG1CQUFtQixHQUFHLE1BQU0sQ0FBQztBQUM5QixDQUFDO0FBT0QsTUFBTSxRQUFRLEdBQUcsRUFBcUMsQ0FBQztBQUN2RCxNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLG9FQUFnQixDQUFDLENBQUM7QUFDaEQsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtJQUNyQixRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUc7UUFDYixhQUFhLEVBQUUsQ0FBQztRQUNoQixTQUFTLEVBQUUsQ0FBQztRQUNaLFFBQVEsRUFBRSxDQUFDO0tBQ1gsQ0FBQztBQUNILENBQUMsQ0FBQyxDQUFDO0FBRUgsS0FBSyxVQUFVLFFBQVE7SUFDdEIsU0FBUyxtQkFBbUIsQ0FBQyxnQkFBd0IsRUFBRSxRQUF1QixFQUFFLFNBQWlCO1FBQ2hHLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFBRSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDOztZQUM3RSxNQUFNLElBQUksS0FBSyxDQUFDLGtCQUFrQixHQUFHLFNBQVMsR0FBRyxtQkFBbUIsR0FBRyxnQkFBZ0IsR0FBRyx1Q0FBdUMsR0FBRyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUM7SUFDNUosQ0FBQztJQUVELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztJQUNkLEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxFQUFFO1FBQ3pCLG9CQUFvQixHQUFHLElBQUksR0FBRyxFQUFFLENBQUM7UUFDakMsTUFBTSxXQUFXLEdBQUcsTUFBTSxJQUFJLEVBQUUsQ0FBQztRQUNqQyxJQUFJLFdBQVcsQ0FBQyxNQUFNO1lBQUUsS0FBSyxFQUFFLENBQUM7UUFDaEMsSUFBSSxXQUFXLENBQUMsUUFBUSxLQUFLLE9BQU87WUFBRSxTQUFTO1FBRS9DLEtBQUssTUFBTSxnQkFBZ0IsSUFBSSxvQkFBb0IsRUFBRTtZQUNwRCxJQUFJLE9BQU8sV0FBVyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7Z0JBQzdDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFLFdBQVcsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ25GO2lCQUNJO2dCQUNKLEtBQUssTUFBTSxRQUFRLElBQUksV0FBVyxDQUFDLFFBQVEsRUFBRTtvQkFDNUMsbUJBQW1CLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDdkU7YUFDRDtTQUNEO0tBQ0Q7SUFDRCxNQUFNLFVBQVUsR0FBRyxLQUFLLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsS0FBSyxHQUFHLFVBQVUsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLG9CQUFvQixHQUFHLEtBQUssQ0FBQyxDQUFDO0lBQzNGLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNO1FBQUUsT0FBTyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7QUFDaEQsQ0FBQztBQUVELDhCQUE4QjtBQUM5QixtQkFBbUIsR0FBRyxJQUFJLENBQUM7QUFDaUI7QUFDNUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO0FBQ2E7QUFDeEMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO0FBQ2dCO0FBQzNDLG1CQUFtQixHQUFHLElBQUksQ0FBQztBQUNjO0FBQ3pDLG1CQUFtQixHQUFHLElBQUksQ0FBQztBQUNlO0FBQzFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztBQUNtQjtBQUM5QyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7QUFDYztBQUV6QyxLQUFLLFVBQVUsSUFBSTtJQUNsQixNQUFNLFFBQVEsRUFBRSxDQUFDO0lBQ2pCLFNBQVMsYUFBYSxDQUFDLENBQWtCO1FBQ3hDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7UUFFekQsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNyQixJQUFJLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDL0IsTUFBTSxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLGVBQWUsR0FBRyxLQUFLLENBQUM7Z0JBQ2pGLE1BQU0sSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFXLEdBQUcsS0FBSyxDQUFDO2dCQUN6RSxNQUFNLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDdkUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksS0FBSyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDL0M7UUFDRixDQUFDLENBQUMsQ0FBQztLQUNIO0FBQ0YsQ0FBQztBQUVELElBQUksRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUNuTmtDO0FBRWxDLEtBQUssVUFBVSxVQUFVLENBQUMsVUFBa0IsRUFBRSxZQUFvQjtJQUN4RSxPQUFPLHNEQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFVBQVUsR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztBQUM5SSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUNMb0U7QUFDdkI7QUFDbUU7QUFFdkU7QUFFMUMsNERBQWEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDL0IsTUFBTSxzREFBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLHlCQUF5QixFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDeEUsNERBQTREO0lBQzVELE1BQU0sMERBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3ZDLE1BQU0sU0FBUyxHQUFHLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxDQUFDLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDO0lBQ2hGLE1BQU0sU0FBUyxHQUFHLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUM7SUFDNUUsTUFBTSxJQUFJLEdBQUcsWUFBWSxDQUFDO0lBQzFCLE1BQU0sT0FBTyxHQUFHLHdCQUF3QixDQUFDO0lBQ3pDLE1BQU0sT0FBTyxHQUFHLHdCQUF3QixDQUFDO0lBQ3pDLElBQUksTUFBTSxHQUFHLGFBQWEsQ0FBQztJQUUzQixnRUFBZ0U7SUFDaEUsTUFBTSxRQUFRLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDLEdBQUcsQ0FBQztJQUMvRCxNQUFNLHNEQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFeEIsdUJBQXVCO0lBQ3ZCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbEMsTUFBTSxRQUFRLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDLEdBQUcsQ0FBQztJQUUvRCxRQUFRO0lBQ1IsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFDaEgsTUFBTSxzREFBTyxDQUFDLFFBQVEsRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDdkQsTUFBTSxzREFBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBRXhCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbEMsTUFBTSxXQUFXLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUM1RCxNQUFNLHNEQUFPLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxHQUFHLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDL0QsTUFBTSxzREFBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBRXhCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbEMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3hDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQ2xFLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUV0RSwwQ0FBMEM7SUFDMUMsSUFBSSxRQUFRLEdBQUcsTUFBTSxzREFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDL0YsNkRBQXFCLENBQUMsUUFBUSxFQUFFLHFDQUFxQyxDQUFDLENBQUM7SUFDdkUsUUFBUSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ3pGLDZEQUFxQixDQUFDLFFBQVEsRUFBRSxvQ0FBb0MsQ0FBQyxDQUFDO0lBQ3RFLFFBQVEsR0FBRyxNQUFNLHNEQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUM1Riw2REFBcUIsQ0FBQyxRQUFRLEVBQUUscUNBQXFDLENBQUMsQ0FBQztJQUV2RSxJQUFJLFFBQVEsR0FBRyxNQUFNLHNEQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNuRCw2REFBcUIsQ0FBQyxRQUFRLEVBQUUsOEJBQThCLENBQUMsQ0FBQztJQUNoRSxJQUFJLFFBQVEsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLE9BQU87UUFBRSxPQUFPLHlDQUF5QyxDQUFDO0lBQ3hGLElBQUksUUFBUSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssT0FBTztRQUFFLE9BQU8seUNBQXlDLENBQUM7SUFDeEYsSUFBSSxRQUFRLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxNQUFNO1FBQUUsT0FBTywwQ0FBMEMsQ0FBQztJQUV4RixNQUFNLHNEQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFeEIsMENBQTBDO0lBQzFDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbEMsUUFBUSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLDZEQUFxQixDQUFDLFFBQVEsRUFBRSw2Q0FBNkMsQ0FBQyxDQUFDO0lBQy9FLElBQUksUUFBUSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssT0FBTztRQUFFLE9BQU8sd0RBQXdELENBQUM7SUFDdkcsSUFBSSxRQUFRLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxPQUFPO1FBQUUsT0FBTyx3REFBd0QsQ0FBQztJQUN2RyxJQUFJLFFBQVEsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLE1BQU07UUFBRSxPQUFPLHlEQUF5RCxDQUFDO0lBRXZHLHlCQUF5QjtJQUN6QixNQUFNLE9BQU8sR0FBRyx1REFBVyxDQUFDLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDbEQsUUFBUSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLElBQUksUUFBUSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssT0FBTztRQUFFLE9BQU8sc0VBQXNFLEdBQUcsT0FBTyxHQUFHLGNBQWMsR0FBRyxRQUFRLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztJQUVoTCxNQUFNLEdBQUcsYUFBYSxDQUFDO0lBQ3ZCLE1BQU0sc0RBQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ2pGLE1BQU0sc0RBQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUV4QixtRUFBbUU7SUFDbkUsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNsQyxRQUFRLEdBQUcsTUFBTSxzREFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDL0MsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0FBQzdGLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDckYsUUFBUTtJQUNSLE1BQU0sUUFBUSxHQUFHLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUM7SUFDM0UsTUFBTSxJQUFJLEdBQUcsWUFBWSxDQUFDO0lBQzFCLE1BQU0sR0FBRyxHQUFHLHdCQUF3QixDQUFDO0lBQ3JDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFFakMsTUFBTSxPQUFPLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO0lBRWxELE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBQ2hILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUN4QyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUVsRSxzQkFBc0I7SUFDdEIsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDaEQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLG1DQUFtQyxDQUFDLENBQUM7SUFFaEUsSUFBSSxHQUFHLENBQUMsSUFBSyxDQUFDLE1BQU0sS0FBSyxDQUFDO1FBQUUsT0FBTyx1RUFBdUUsQ0FBQztJQUUzRyxJQUFJLFdBQVcsR0FBRyxNQUFNLDZEQUFxQixDQUFDLGlDQUFpQyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3BNLElBQUksV0FBVyxLQUFLLElBQUk7UUFBRSxPQUFPLFdBQVcsQ0FBQztJQUU3QyxxQkFBcUI7SUFDckIsSUFBSSxPQUFPLEdBQUcsTUFBTSxzREFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsU0FBZSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUMvRiwyREFBbUIsQ0FBQyxPQUFPLEVBQUUsbUVBQW1FLENBQUMsQ0FBQztJQUVsRyxPQUFPLEdBQUcsTUFBTSxzREFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNwRiwyREFBbUIsQ0FBQyxPQUFPLEVBQUUsa0VBQWtFLENBQUMsQ0FBQztJQUVqRyxPQUFPLEdBQUcsTUFBTSxzREFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUN2RiwyREFBbUIsQ0FBQyxPQUFPLEVBQUUsMERBQTBELENBQUMsQ0FBQztJQUV6RixPQUFPLEdBQUcsTUFBTSxzREFBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsT0FBYSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUN4RiwyREFBbUIsQ0FBQyxPQUFPLEVBQUUsNENBQTRDLENBQUMsQ0FBQztJQUMzRSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7UUFBRSxPQUFPLHNGQUFzRixDQUFDO0lBQ2xILElBQUksT0FBTyxDQUFDLEVBQUU7UUFBRSxPQUFPLDZEQUE2RCxDQUFDO0lBRXJGLE9BQU8sR0FBRyxNQUFNLHNEQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxNQUFZLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZGLDJEQUFtQixDQUFDLE9BQU8sRUFBRSx3RUFBd0UsQ0FBQyxDQUFDO0lBRXZHLHNCQUFzQjtJQUN0QixNQUFNLHNEQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3JFLFdBQVcsR0FBRyxNQUFNLDZEQUFxQixDQUFDLGlDQUFpQyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDckksT0FBTyxXQUFXLENBQUM7QUFDcEIsQ0FBQyxDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7O0FDL0hrRTtBQUV2QjtBQUN5RDtBQUd2Ryw0REFBYSxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUMvQixNQUFNLHNEQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsTUFBTSwwREFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztBQUNuQyxDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsNEJBQTRCLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUMzRSxNQUFNLEtBQUssR0FBRyxXQUFXLENBQUM7SUFDMUIsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDO0lBRXZCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRWhILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDbkQsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUM5RCxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBRTFCLE1BQU0sT0FBTyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQVcsQ0FBQyxFQUFFLFlBQVksRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztJQUMxSixNQUFNLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQzlDLDZEQUFxQixDQUFDLEdBQUcsRUFBRSxrQ0FBa0MsQ0FBQyxDQUFDO0lBRS9ELE1BQU0sVUFBVSxHQUFnQixDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RGLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQywwREFBMEQsR0FBRyxLQUFLLEdBQUcsbUJBQW1CLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0FBQ2hLLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyw0REFBNEQsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ25HLE1BQU0sSUFBSSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDO0lBQzdGLE1BQU0sS0FBSyxHQUFHLFdBQVcsQ0FBQztJQUMxQixNQUFNLElBQUksR0FBRyxTQUFTLENBQUM7SUFFdkIsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDbEYsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFFaEgsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUNuRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQzlELE1BQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFFMUIsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRWhNLE1BQU0sVUFBVSxHQUFnQixDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RGLEtBQUssTUFBTSxTQUFTLElBQUksVUFBVSxFQUFFO1FBQ25DLElBQUksU0FBUyxDQUFDLE1BQU0sS0FBSyxRQUFRLEVBQUU7WUFDbEMsSUFBSSxTQUFTLENBQUMsUUFBUTtnQkFBRSxPQUFPLDRDQUE0QyxDQUFDO1NBQzVFO2FBQ0k7WUFDSixJQUFJLFNBQVMsQ0FBQyxRQUFRLEVBQUUsTUFBTSxLQUFLLE9BQU87Z0JBQUUsT0FBTyxrR0FBa0csR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQztTQUN2TDtLQUNEO0lBRUQsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsZ0NBQWdDLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUMvRSxNQUFNLEtBQUssR0FBRyxXQUFXLENBQUM7SUFDMUIsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDO0lBRXZCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRWhILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDbkQsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUM5RCxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBRTFCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsaURBQVMsRUFBRSxHQUFHLElBQUksRUFBRSxRQUFRLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDekwsTUFBTSxTQUFTLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzRSxNQUFNLFdBQVcsR0FBRyxpREFBUyxFQUFFLEdBQUcsSUFBSSxDQUFDO0lBQ3ZDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDO0lBQ25DLFNBQVMsQ0FBQyxRQUFRLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7SUFDcEMsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUMvRCw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztJQUV6RCxNQUFNLE9BQU8sR0FBRyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNyRSw2REFBcUIsQ0FBQyxPQUFPLEVBQUUscUNBQXFDLENBQUMsQ0FBQztJQUN0RSxJQUFJLE9BQU8sQ0FBQyxJQUFLLENBQUMsTUFBTSxLQUFLLFFBQVE7UUFBRSxPQUFPLCtCQUErQixDQUFDO0lBQzlFLElBQUksT0FBTyxDQUFDLElBQUssQ0FBQyxVQUFVLEtBQUssV0FBVztRQUFFLE9BQU8sMkNBQTJDLENBQUM7SUFDakcsSUFBSSxPQUFPLENBQUMsSUFBSyxDQUFDLFFBQVEsS0FBSyxTQUFTLENBQUMsUUFBUTtRQUFFLE9BQU8sb0NBQW9DLENBQUM7SUFFL0YsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsK0NBQStDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUN0RixNQUFNLEtBQUssR0FBRyxXQUFXLENBQUM7SUFDMUIsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDO0lBRXZCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRWhILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDbkQsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUM5RCxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBRTFCLE1BQU0sT0FBTyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQVcsQ0FBQyxFQUFFLFlBQVksRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO0lBQzVLLE1BQU0sR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDOUMsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDJEQUEyRCxDQUFDLENBQUM7SUFFeEYsTUFBTSxVQUFVLEdBQWdCLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDdEYsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLG9DQUFvQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7QUFDL0YsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLCtDQUErQyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDdEYsSUFBSSxLQUFLLEdBQUcsWUFBWSxDQUFDO0lBRXpCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRWhILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDbkQsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUM5RCxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBRTFCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRS9HLEtBQUssR0FBRyxZQUFZLENBQUM7SUFDckIsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFFL0csS0FBSyxHQUFHLHFCQUFxQixDQUFDO0lBQzlCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRS9HLE1BQU0sVUFBVSxHQUFnQixDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RGLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLGdFQUFnRSxDQUFDO0lBQ2xILElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLGlFQUFpRSxDQUFDO0lBQ2pILElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQUUsT0FBTyxtREFBbUQsQ0FBQztJQUV6SSxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxTQUFTLENBQUMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3hFLE1BQU0sSUFBSSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDO0lBQzdGLE1BQU0sS0FBSyxHQUFHLFdBQVcsQ0FBQztJQUMxQixNQUFNLElBQUksR0FBRyxTQUFTLENBQUM7SUFFdkIsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDbEYsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFFaEgsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUNuRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQzlELE1BQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFFMUIsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRWhNLElBQUksWUFBWSxHQUFHLE1BQU0sNkRBQXFCLENBQUMsNkNBQTZDLEVBQzNGLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQVcsQ0FBQyxFQUFFLFlBQVksRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFXLEVBQUUsRUFBRSxFQUN2TCxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUM5QyxJQUFJLFlBQVksS0FBSyxJQUFJO1FBQUUsT0FBTyxZQUFZLENBQUM7SUFFL0MsWUFBWSxHQUFHLE1BQU0sNkRBQXFCLENBQUMsNkNBQTZDLEVBQ3ZGLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFXLEVBQ3JELENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLFlBQVksRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzdPLElBQUksWUFBWSxLQUFLLElBQUk7UUFBRSxPQUFPLFlBQVksQ0FBQztJQUUvQyxZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FBQyw2Q0FBNkMsRUFDdkYsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBVyxFQUNsQyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsRUFDdkwsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzlCLElBQUksWUFBWSxLQUFLLElBQUk7UUFBRSxPQUFPLFlBQVksQ0FBQztJQUUvQyxZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FBQyw2Q0FBNkMsRUFDdkYsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBVyxFQUN2RCxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsRUFDdkwsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN2RCxJQUFJLFlBQVksS0FBSyxJQUFJO1FBQUUsT0FBTyxZQUFZLENBQUM7SUFFL0MsTUFBTSxrQkFBa0IsR0FBRyxFQUFTLENBQUM7SUFDckMsS0FBSyxNQUFNLEdBQUcsSUFBSSxJQUFJLEVBQUU7UUFDdkIsa0JBQWtCLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDeEM7SUFDRCxZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FBQyw2Q0FBNkMsRUFBRSxJQUFJLEVBQzdGLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFDbE4sa0JBQWtCLENBQUMsQ0FBQztJQUNyQixJQUFJLFlBQVksS0FBSyxJQUFJO1FBQUUsT0FBTyxZQUFZLENBQUM7SUFFL0MsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRWhNLE1BQU0sVUFBVSxHQUFnQixDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RGLE1BQU0sR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDOUIsTUFBTSxVQUFVLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztJQUM1QyxNQUFNLGlCQUFpQixHQUFHLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxDQUFDO0lBRTlDLFlBQVksR0FBRyxNQUFNLDZEQUFxQixDQUFDLHNEQUFzRCxFQUFFLGlCQUFpQixFQUFFLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FDckksQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxpQkFBaUIsRUFBRSxXQUFXLEVBQUUsRUFBRSxDQUFDLEVBQzNLLEVBQUUsSUFBSSxFQUFFLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM3QixJQUFJLFlBQVksS0FBSyxJQUFJO1FBQUUsT0FBTyxZQUFZLENBQUM7SUFFL0MsT0FBTyxZQUFZLENBQUM7QUFDckIsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLGlDQUFpQyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDaEYsTUFBTSxJQUFJLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUM7SUFDN0YsTUFBTSxLQUFLLEdBQUcsV0FBVyxDQUFDO0lBQzFCLE1BQU0sSUFBSSxHQUFHLFNBQVMsQ0FBQztJQUV2QixNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNsRixNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUVoSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO0lBQ25ELE1BQU0sTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDOUQsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUUxQixNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLFlBQVksRUFBRSxDQUFDLEVBQUUsVUFBVSxFQUFFLGlEQUFTLEVBQUUsR0FBRyxNQUFPLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRXBOLE1BQU0sVUFBVSxHQUFnQixDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RGLE1BQU0sR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDOUIsTUFBTSxVQUFVLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztJQUM1QyxNQUFNLGlCQUFpQixHQUFHLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxDQUFDO0lBRTlDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxpQkFBaUIsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUUzSyxNQUFNLE9BQU8sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDaEUsNkRBQXFCLENBQUMsT0FBTyxFQUFFLHdFQUF3RSxDQUFDLENBQUM7SUFFekcsTUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLElBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztJQUN0QyxJQUFJLFNBQVUsQ0FBQyxRQUFRLEtBQUssQ0FBQztRQUFFLE9BQU8sNkRBQTZELEdBQUcsU0FBVSxDQUFDLFFBQVEsQ0FBQztJQUMxSCxJQUFJLENBQUMsU0FBVSxDQUFDLGlCQUFpQjtRQUFFLE9BQU8sMkRBQTJELEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNsSSxJQUFJLFNBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEtBQUssR0FBRztRQUFFLE9BQU8sd0RBQXdELEdBQUcsR0FBRyxHQUFHLGNBQWMsR0FBRyxTQUFVLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDO0lBQ3hLLElBQUksU0FBVSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsS0FBSyxVQUFVO1FBQUUsT0FBTyxnRUFBZ0UsR0FBRyxVQUFVLEdBQUcsY0FBYyxHQUFHLFNBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUM7SUFFNU0sT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsK0JBQStCLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUM5RSxNQUFNLEtBQUssR0FBRyxXQUFXLENBQUM7SUFFMUIsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDbEYsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFFaEgsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUNuRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQzlELE1BQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFFMUIsTUFBTSxPQUFPLEdBQUc7UUFDZixHQUFHLEVBQUUsR0FBRyxFQUFFO1lBQ1QsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUU7Z0JBQ2xCLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBVztnQkFDN0QsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFXO2dCQUM5RCxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQVc7Z0JBQ2hFO29CQUNDLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUU7d0JBQ2pFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBVzt3QkFDL0QsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFXO3dCQUM1RCxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQVc7cUJBQzdELEVBQUUsV0FBVyxFQUFFLENBQUMsb0JBQW9CLENBQUM7aUJBQ3RDO2FBQ0QsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUM7U0FDN0I7S0FDbEIsQ0FBQztJQUNGLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFFbEMsTUFBTSxVQUFVLEdBQWdCLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDdEYsTUFBTSxTQUFTLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRWhDLDBCQUEwQjtJQUMxQixJQUFJLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsbUJBQW1CLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBZSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDekgsNkRBQXFCLENBQUMsR0FBRyxFQUFFLGlEQUFpRCxDQUFDLENBQUM7SUFFOUUsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFlLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNySCw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsaURBQWlELENBQUMsQ0FBQztJQUU5RSxNQUFNLHNEQUFPLENBQUMsbUJBQW1CLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBZSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDL0csR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFjLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNwSCw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsZ0RBQWdELENBQUMsQ0FBQztJQUU3RSxVQUFVO0lBQ1YsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFlLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDcEgsNkRBQXFCLENBQUMsR0FBRyxFQUFFLGdFQUFnRSxDQUFDLENBQUM7SUFFN0YsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFlLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDcEgsNkRBQXFCLENBQUMsR0FBRyxFQUFFLGdFQUFnRSxDQUFDLENBQUM7SUFFN0YsTUFBTSxzREFBTyxDQUFDLG1CQUFtQixFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxTQUFTLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQWUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM5RyxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLG1CQUFtQixFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxTQUFTLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQWMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNuSCw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsK0RBQStELENBQUMsQ0FBQztJQUU1RixjQUFjO0lBQ2QsTUFBTSxlQUFlLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqRixJQUFJLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxLQUFLLE1BQU07UUFBRSxPQUFPLGlFQUFpRSxDQUFDO0lBQ3ZILElBQUksZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLEtBQUssTUFBTTtRQUFFLE9BQU8saUVBQWlFLENBQUM7SUFDdkgsSUFBSSxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsS0FBSyxLQUFLO1FBQUUsT0FBTyxnRUFBZ0UsQ0FBQztJQUNySCxJQUFLLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFzQixFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLEtBQUssTUFBTTtRQUFFLE9BQU8sZ0ZBQWdGLENBQUM7SUFDdEssSUFBSyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBc0IsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxLQUFLLE1BQU07UUFBRSxPQUFPLGdGQUFnRixDQUFDO0lBQ3RLLElBQUssZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQXNCLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsS0FBSyxLQUFLO1FBQUUsT0FBTywrRUFBK0UsQ0FBQztJQUVwSyxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDbEYsTUFBTSxLQUFLLEdBQUcsV0FBVyxDQUFDO0lBRTFCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRWhILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDbkQsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUM5RCxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBRTFCLE1BQU0sT0FBTyxHQUFHO1FBQ2YsR0FBRyxFQUFFLEdBQUcsRUFBRTtZQUNULEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO2dCQUNsQixFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQVc7Z0JBQzdEO29CQUNDLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFhO29CQUN4RixLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBb0I7aUJBQ3hFO2FBQ1YsRUFBRSxZQUFZLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUM7U0FDOUM7S0FDRCxDQUFDO0lBQ0YsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUVsQyxNQUFNLFVBQVUsR0FBZ0IsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RixNQUFNLFNBQVMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFaEMsSUFBSSxZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FDN0MsZ0VBQWdFLEVBQ2hFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxTQUFTLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQWUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFDMUUsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsbUJBQW1CLEVBQUUsWUFBWSxDQUFDLENBQ3JELENBQUM7SUFDRixJQUFJLFlBQVksS0FBSyxJQUFJO1FBQUUsT0FBTyxZQUFZLENBQUM7SUFFL0MsWUFBWSxHQUFHLE1BQU0sNkRBQXFCLENBQ3pDLHNGQUFzRixFQUN0RixFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFlLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUN6RSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxtQkFBbUIsRUFBRSxZQUFZLENBQUMsQ0FDckQsQ0FBQztJQUNGLE9BQU8sWUFBWSxDQUFDO0FBQ3JCLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7OztBQ3ZVa0U7QUFFdkI7QUFDeUI7QUFFdkUsNERBQWEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDL0IsTUFBTSxzREFBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLG9CQUFvQixFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDbkUsTUFBTSxRQUFRLEdBQUcsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQztJQUMzRSxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBRWpDLE1BQU0sSUFBSSxHQUFHLFlBQVksQ0FBQztJQUMxQixNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNoSCxNQUFNLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDcEQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDRCQUE0QixDQUFDLENBQUM7SUFFekQsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM5RCxJQUFJLEdBQUcsSUFBSSxTQUFTO1FBQUUsT0FBTyxpQ0FBaUMsQ0FBQztJQUMvRCxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSTtRQUFFLE9BQU8sZ0NBQWdDLENBQUM7SUFFL0QsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsb0NBQW9DLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUNuRixNQUFNLFFBQVEsR0FBRyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDO0lBQzNFLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFFakMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFDaEgsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsc0JBQXNCLEVBQUUsQ0FBQyxDQUFDO0lBRWhFLElBQUksTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDNUQsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUUxQixvQkFBb0I7SUFDcEIsSUFBSSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDbkUsNkRBQXFCLENBQUMsR0FBRyxFQUFFLG9DQUFvQyxDQUFDLENBQUM7SUFFakUsTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDeEQsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLGlEQUFTLEVBQUUsR0FBRyxHQUFHLEdBQUcsS0FBTSxHQUFHLElBQUk7UUFBRSxPQUFPLG1FQUFtRTtZQUNoSSxJQUFJLElBQUksQ0FBQyxpREFBUyxFQUFFLEdBQUcsR0FBRyxHQUFHLEtBQU0sR0FBRyxJQUFJLENBQUMsR0FBRyx1QkFBdUI7WUFDckUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBRXpCLHNCQUFzQjtJQUN0QixHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDaEUsTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDeEQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDJDQUEyQyxDQUFDLENBQUM7SUFFeEUsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7UUFBRSxPQUFPLHVDQUF1QyxDQUFDO0lBRXhFLHVDQUF1QztJQUN2QyxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFFL0QsTUFBTSxHQUFHLE1BQU0saURBQVMsQ0FBQyxRQUFRLEVBQUUsaURBQVMsRUFBRSxHQUFHLENBQUMsR0FBRyxLQUFNLEdBQUcsSUFBSSxFQUFFLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDO0lBQ2hJLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLFNBQVM7UUFBRSxPQUFPLGlDQUFpQyxDQUFDO0lBRXJFLE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDZFQUE2RSxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDeEgsTUFBTSxLQUFLLEdBQUcsZ0JBQWdCLENBQUM7SUFDL0IsTUFBTSxJQUFJLEdBQUcsNEJBQTRCLENBQUM7SUFFMUMsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDbEYsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFDbkgsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLHNCQUFzQixFQUFFLENBQUMsQ0FBQztJQUN4RSxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO0lBRWxFLE1BQU0sTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RFLE1BQU0sT0FBTyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDOUIsTUFBTSxPQUFPLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUU5QixNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLGtCQUFrQixFQUFFLElBQUksRUFBRSw4QkFBOEIsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ3hLLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFcEgsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUN0Riw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsMkNBQTJDLENBQUMsQ0FBQztJQUV4RSxNQUFNLE9BQU8sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDdkYsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyw0RUFBNEUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0FBQ3RLLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxvQkFBb0IsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ25FLE1BQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQztJQUV6QixNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNsRixNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNoSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDO0lBRXBELElBQUksTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDNUQsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUUxQixNQUFNLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsWUFBWSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzVELDZEQUFxQixDQUFDLEdBQUcsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO0lBRXJELE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3hELE1BQU0sT0FBTyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFFL0IsT0FBTyxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsc0VBQXNFLEdBQUcsT0FBTyxHQUFHLGNBQWMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztBQUM3SSxDQUFDLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7QUNyR2tFO0FBQ3ZCO0FBQ21FO0FBRWpILDREQUFhLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQy9CLE1BQU0sc0RBQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQixNQUFNLDBEQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQ25DLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxtQkFBbUIsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ2xFLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLENBQUMsQ0FBQztJQUNqRSw2REFBcUIsQ0FBQyxHQUFHLEVBQUUseUJBQXlCLENBQUMsQ0FBQztJQUN0RCxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyw0REFBNEQsRUFBRSxVQUFVLEVBQUUsS0FBSyxJQUFJLEVBQUU7SUFDL0YsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsQ0FBQyxDQUFDO0lBQ2pFLDJEQUFtQixDQUFDLEdBQUcsRUFBRSxpREFBaUQsQ0FBQyxDQUFDO0lBQzVFLE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDBEQUFrQixDQUFDLFVBQVUsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDekUsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDbEYsT0FBTyxNQUFNLDZEQUFxQixDQUFDLGdDQUFnQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0FBQzdJLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUN6QmtFO0FBQ2hDO0FBQ1M7QUFDeUY7QUFFdkksNERBQWEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDL0IsTUFBTSxzREFBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDLENBQUM7QUFFSCxNQUFNLElBQUksR0FBRyxTQUFTLENBQUM7QUFDdkIsTUFBTSxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7QUFFbEMseURBQVUsQ0FBQywrQkFBK0IsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQzlFLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFFaEcsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDdEUsNkRBQXFCLENBQUMsR0FBRyxFQUFFLHlCQUF5QixDQUFDLENBQUM7SUFFdEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksU0FBUyxJQUFJLEdBQUcsQ0FBQyxJQUFJLElBQUksMENBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyw2QkFBNkIsRUFBQyxDQUFDLElBQUksQ0FBQztBQUMzRixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsc0RBQXNELEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUNyRyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2hHLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFFbkYsNEJBQTRCO0lBQzVCLElBQUksR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxzQkFBc0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDekQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDhCQUE4QixDQUFDLENBQUM7SUFDM0QsSUFBSSxHQUFHLENBQUMsSUFBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVM7UUFBRSxPQUFPLHdEQUF3RCxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRS9ILHdDQUF3QztJQUN4QyxNQUFNLElBQUksR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMxQyxJQUFJLElBQUksRUFBRSxPQUFPLENBQUMsUUFBUSxLQUFLLEdBQUc7UUFBRSxPQUFPLG9EQUFvRCxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsY0FBYyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUVoTCwyQ0FBMkM7SUFDM0MsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUNuRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQzlELE1BQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDN0IsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDN0gsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFN0UsNEJBQTRCO0lBQzVCLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsc0JBQXNCLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3JELDZEQUFxQixDQUFDLEdBQUcsRUFBRSxzQ0FBc0MsQ0FBQyxDQUFDO0lBQ25FLElBQUksR0FBRyxDQUFDLElBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRztRQUFFLE9BQU8sbUZBQW1GLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxjQUFjLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFbkwsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsMERBQWtCLENBQUMsc0JBQXNCLENBQUMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3JGLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDaEcsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNuRixPQUFPLE1BQU0sNkRBQXFCLENBQUMsNENBQTRDLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQyxzQkFBc0IsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0FBQ25KLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxTQUFTLENBQUMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3hFLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDaEcsT0FBTyxNQUFNLDZEQUFxQixDQUFDLCtCQUErQixFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7QUFDakosQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLGtEQUFrRCxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDakcsTUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLGtCQUFrQixDQUFDO0lBQzNDLE1BQU0sS0FBSyxHQUFHLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQztJQUU1QyxNQUFNLDBEQUFVLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN0QyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUUzRCxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRW5GLHVCQUF1QjtJQUN2QixJQUFJLFdBQVcsR0FBRyxNQUFNLHNEQUFPLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQy9ELDZEQUFxQixDQUFDLFdBQVcsRUFBRSxvQ0FBb0MsQ0FBQyxDQUFDO0lBRXpFLElBQUksR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDekQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDBCQUEwQixDQUFDLENBQUM7SUFFdkQsV0FBVyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxvQkFBb0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDM0QsNkRBQXFCLENBQUMsV0FBVyxFQUFFLHFDQUFxQyxDQUFDLENBQUM7SUFDMUUsTUFBTSxzREFBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBRXhCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQzNELEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFDeEMsNkRBQXFCLENBQUMsR0FBRyxFQUFFLHlDQUF5QyxDQUFDLENBQUM7SUFFdEUsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxXQUFXLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDM0UsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDhCQUE4QixDQUFDLENBQUM7SUFFM0QsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsNkJBQTZCLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUM1RSxNQUFNLEtBQUssR0FBRyxPQUFPLEdBQUcsa0JBQWtCLENBQUM7SUFDM0MsTUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLG1CQUFtQixDQUFDO0lBRTVDLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3RDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBRTNELE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFDbkYsTUFBTSxzREFBTyxDQUFDLG9CQUFvQixFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM3QyxNQUFNLHNEQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLE1BQU0sc0RBQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4QixNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUMzRCxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUUxQyxNQUFNLFdBQVcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFDdEQsTUFBTSxzREFBTyxDQUFDLGtCQUFrQixFQUFFLEVBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNyRSxNQUFNLHNEQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDeEIsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFFM0QsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDOUQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLG1DQUFtQyxDQUFDLENBQUM7SUFFaEUsTUFBTSxPQUFPLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDcEQsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxTQUFTO1FBQUUsT0FBTyxxREFBcUQsQ0FBQztJQUN0RyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLHFEQUFxRCxDQUFDOztRQUU3RixPQUFPLElBQUksQ0FBQztBQUNsQixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsMERBQWtCLENBQUMsVUFBVSxDQUFDLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUN6RSxNQUFNLEtBQUssR0FBRyxPQUFPLEdBQUcsa0JBQWtCLENBQUM7SUFDM0MsTUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLG1CQUFtQixDQUFDO0lBRTVDLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3RDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBRTNELE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFFbkYsTUFBTSxzREFBTyxDQUFDLG9CQUFvQixFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM3QyxNQUFNLHNEQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLE1BQU0sc0RBQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4QixNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUMzRCxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUUxQyxNQUFNLFdBQVcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7SUFFdEQsTUFBTSxzREFBTyxDQUFDLGtCQUFrQixFQUFFLEVBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNyRSxNQUFNLHNEQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDeEIsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFFM0QsT0FBTyw2REFBcUIsQ0FBQyxnQ0FBZ0MsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0FBQ3RJLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxpREFBaUQsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDaEgsTUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLGtCQUFrQixDQUFDO0lBQzNDLE1BQU0sS0FBSyxHQUFHLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQztJQUU1QyxNQUFNLDBEQUFVLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN0QyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUUzRCxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBQ25GLElBQUksWUFBWSxHQUFHLE1BQU0sNkRBQXFCLENBQUMsOENBQThDLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQ2xLLElBQUksWUFBWSxLQUFLLElBQUk7UUFBRSxPQUFPLFlBQVksQ0FBQztJQUUvQyxJQUFJLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ3pELEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ3JELElBQUksR0FBRyxDQUFDLEVBQUU7UUFBRSxPQUFPLCtDQUErQyxDQUFDO0lBRW5FLFlBQVksR0FBRyxNQUFNLDZEQUFxQixDQUFDLDREQUE0RCxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsb0JBQW9CLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUMxSyxJQUFJLFlBQVksS0FBSyxJQUFJO1FBQUUsT0FBTyxZQUFZLENBQUM7SUFFL0MsTUFBTSxzREFBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBRXhCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQzNELE1BQU0sSUFBSSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztJQUUvQyxZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FBQywyQ0FBMkMsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLGtCQUFrQixFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDMUssSUFBSSxZQUFZLEtBQUssSUFBSTtRQUFFLE9BQU8sWUFBWSxDQUFDO0lBRS9DLE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLG1CQUFtQixFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDbEUsTUFBTSxZQUFZLEdBQUcsYUFBYSxDQUFDO0lBRW5DLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBRWxGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFDbkYsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUNyRSw2REFBcUIsQ0FBQyxHQUFHLEVBQUUseUJBQXlCLENBQUMsQ0FBQztJQUV0RCxNQUFNLE9BQU8sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssWUFBWTtRQUFFLE9BQU8sc0RBQXNELEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztJQUVoSCxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxZQUFZLENBQUMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQzNFLE1BQU0sWUFBWSxHQUFHLGFBQWEsQ0FBQztJQUVuQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNsRixNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBQ25GLE9BQU8sTUFBTSw2REFBcUIsQ0FBQyxrQ0FBa0MsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLEVBQUUsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsWUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7QUFDckosQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLHNDQUFzQyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDN0UsSUFBSSxZQUFZLEdBQUcsY0FBYyxDQUFDO0lBRWxDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBRWxGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFFbkYsTUFBTSxzREFBTyxDQUFDLFlBQVksRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUV6RCxJQUFJLE9BQU8sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNsRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQztRQUFFLE9BQU8sdURBQXVELENBQUM7SUFFakcsWUFBWSxHQUFHLGNBQWMsQ0FBQztJQUM5QixPQUFPLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLHdEQUF3RCxDQUFDO0lBRWhHLFlBQVksR0FBRyxtQ0FBbUMsQ0FBQztJQUNuRCxPQUFPLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFBRSxPQUFPLCtDQUErQyxDQUFDOztRQUNsSCxPQUFPLElBQUksQ0FBQztBQUNsQixDQUFDLENBQUMsQ0FBQztBQUdILHlEQUFVLENBQUMsd0NBQXdDLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUN2RixNQUFNLElBQUksR0FBRyxTQUFTLENBQUM7SUFDdkIsTUFBTSxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7SUFFbEMsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNoRyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRW5GLE1BQU0sZUFBZSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDakUsNkRBQXFCLENBQUMsZUFBZSxFQUFFLGdDQUFnQyxDQUFDLENBQUM7SUFFekUsSUFBSSxlQUFlLEdBQUcsTUFBTSxzREFBTyxDQUFDLGlCQUFpQixFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNoRSw2REFBcUIsQ0FBQyxlQUFlLEVBQUUsa0NBQWtDLENBQUMsQ0FBQztJQUUzRSxJQUFJLGVBQWUsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxLQUFLLEVBQUU7UUFBRSxPQUFPLGdDQUFnQyxDQUFDO0lBRW5GLE1BQU0sZUFBZSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRTtRQUN2RCxHQUFHLEVBQUUsZUFBZSxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHO1FBQ2pDLEdBQUcsRUFBRSxlQUFlLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUc7UUFDakMsSUFBSSxFQUFFLFlBQVk7UUFDbEIsT0FBTyxFQUFFLFVBQVU7S0FDbkIsQ0FBQyxDQUFDO0lBQ0gsNkRBQXFCLENBQUMsZUFBZSxFQUFFLCtCQUErQixDQUFDLENBQUM7SUFFeEUsZUFBZSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDNUQsSUFBSSxlQUFlLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksS0FBSyxZQUFtQjtRQUFFLE9BQU8saUNBQWlDLENBQUM7SUFDckcsSUFBSSxlQUFlLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sS0FBSyxVQUFVO1FBQUUsT0FBTyxpQ0FBaUMsQ0FBQztJQUUvRixPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxvREFBb0QsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDbkgsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDO0lBQ3ZCLE1BQU0sUUFBUSxHQUFHLGdCQUFnQixDQUFDO0lBRWxDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDaEcsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUVuRixNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3pDLElBQUksV0FBVyxHQUFHLE1BQU0sNkRBQXFCLENBQUMsc0NBQXNDLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQ25KLElBQUksV0FBVyxLQUFLLElBQUk7UUFBRSxPQUFPLFdBQVcsQ0FBQztJQUU3QyxNQUFNLGVBQWUsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFN0UsV0FBVyxHQUFHLE1BQU0sNkRBQXFCLENBQUMsdUNBQXVDLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQ2pKLElBQUksV0FBVyxLQUFLLElBQUk7UUFBRSxPQUFPLFdBQVcsQ0FBQztJQUU3QyxXQUFXLEdBQUcsTUFBTSw2REFBcUIsQ0FBQyxzQ0FBc0MsRUFBRTtRQUNqRixHQUFHLEVBQUUsZUFBZSxDQUFDLEdBQUc7UUFDeEIsR0FBRyxFQUFFLGVBQWUsQ0FBQyxHQUFHO1FBQ3hCLElBQUksRUFBRSxZQUFZO1FBQ2xCLE9BQU8sRUFBRSxVQUFVO0tBQ25CLEVBQUUsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsWUFBWSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDMUUsT0FBTyxXQUFXLENBQUM7QUFDcEIsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDhCQUE4QixFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDN0UsTUFBTSxRQUFRLEdBQUcsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQztJQUMzRSxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBRWpDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFFOUUsb0JBQW9CO0lBQ3BCLElBQUksR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDOUQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLGtDQUFrQyxDQUFDLENBQUM7SUFFL0QsSUFBSSxPQUFPLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbEQsSUFBSSxPQUFPLENBQUMsR0FBRyxHQUFHLGlEQUFTLEVBQUUsR0FBRyxFQUFFLEdBQUcsS0FBTSxHQUFHLElBQUk7UUFBRSxPQUFPLGlFQUFpRSxHQUFHLElBQUksSUFBSSxDQUFDLGlEQUFTLEVBQUUsR0FBRyxFQUFFLEdBQUcsS0FBTSxHQUFHLElBQUksQ0FBQyxHQUFHLHVCQUF1QixHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUU1TixzQkFBc0I7SUFDdEIsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDM0QsNkRBQXFCLENBQUMsR0FBRyxFQUFFLHlDQUF5QyxDQUFDLENBQUM7SUFFdEUsT0FBTyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzlDLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQUUsT0FBTyxxQ0FBcUMsQ0FBQztJQUVwRSx1Q0FBdUM7SUFDdkMsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFFMUQsT0FBTyxHQUFHLE1BQU0saURBQVMsQ0FBQyxRQUFRLEVBQUUsaURBQVMsRUFBRSxHQUFHLEVBQUUsR0FBRyxLQUFNLEdBQUcsSUFBSSxFQUFFLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN2SCxJQUFJLE9BQU8sSUFBSSxTQUFTO1FBQUUsT0FBTywrQkFBK0IsQ0FBQztJQUNqRSxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUMzUzZEO0FBQ0s7QUFDdkI7QUFDeUY7QUFFdkksTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDO0FBQ3JCLE1BQU0sR0FBRyxHQUFHLGFBQWEsQ0FBQztBQUMxQixNQUFNLEtBQUssR0FBRyxVQUFVLENBQUM7QUFFekIsNERBQWEsQ0FBQyxLQUFLLElBQUksRUFBRTtJQUN4QixNQUFNLHNEQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDdEIsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLHlCQUF5QixFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDeEUsTUFBTSxHQUFHLEdBQUcsTUFBTSwwREFBVSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM3Qyw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsbUNBQW1DLENBQUMsQ0FBQztJQUNoRSxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxtQkFBbUIsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ2xFLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEMsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQzdGLDZEQUFxQixDQUFDLEdBQUcsRUFBRSxvQ0FBb0MsQ0FBQyxDQUFDO0lBQ2pFLE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDJCQUEyQixFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDMUUsTUFBTSwwREFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNsQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDakYsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLDZEQUFxQixDQUFDLEdBQUcsRUFBRSxpQ0FBaUMsQ0FBQyxDQUFDO0lBQzlELE9BQU8sR0FBRyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLDZCQUE2QixDQUFDO0FBQ25FLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxhQUFhLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUM1RCxNQUFNLDBEQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNqRixNQUFNLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEMsNkRBQXFCLENBQUMsR0FBRyxFQUFFLG1CQUFtQixDQUFDLENBQUM7SUFFaEQsTUFBTSxRQUFRLEdBQUcsTUFBTSxzREFBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsNEJBQTRCLENBQUM7QUFDeEUsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLGtCQUFrQixFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDekQsTUFBTSwwREFBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDdkMsTUFBTSwwREFBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDdkMsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsQ0FBQyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFFdkUsTUFBTSxJQUFJLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztJQUNyRCxNQUFNLHNEQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFeEIsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsQ0FBQyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDdkUsTUFBTSxJQUFJLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztJQUNyRCxJQUFJLElBQUksS0FBSyxJQUFJO1FBQUUsT0FBTywrRkFBK0YsQ0FBQztJQUMxSCxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxvQ0FBb0MsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQzlFLE1BQU0sR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGlCQUFpQixFQUFFLENBQUMsQ0FBQztJQUNoSCwyREFBbUIsQ0FBQyxHQUFHLEVBQUUsd0NBQXdDLENBQUMsQ0FBQztJQUNuRSxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxvQ0FBb0MsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQzlFLE1BQU0sR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzFJLDJEQUFtQixDQUFDLEdBQUcsRUFBRSxnREFBZ0QsQ0FBQyxDQUFDO0lBQzNFLE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDBEQUFrQixDQUFDLFVBQVUsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDekUsTUFBTSxHQUFHLEdBQUcsYUFBYSxDQUFDO0lBRTFCLElBQUksR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sR0FBRyxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ25ILDJEQUFtQixDQUFDLEdBQUcsRUFBRSxrQ0FBa0MsQ0FBQyxDQUFDO0lBQzdELEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3hHLDJEQUFtQixDQUFDLEdBQUcsRUFBRSx1REFBdUQsQ0FBQyxDQUFDO0lBQ2xGLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ25HLDJEQUFtQixDQUFDLEdBQUcsRUFBRSx1REFBdUQsQ0FBQyxDQUFDO0lBQ2xGLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3JHLDJEQUFtQixDQUFDLEdBQUcsRUFBRSxtREFBbUQsQ0FBQyxDQUFDO0lBQzlFLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ3hHLDJEQUFtQixDQUFDLEdBQUcsRUFBRSx1REFBdUQsQ0FBQyxDQUFDO0lBRWxGLE9BQU8sTUFBTSw2REFBcUIsQ0FBQyxnQ0FBZ0MsRUFBRSxFQUFFLEtBQUssRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0FBQ3hMLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxxQkFBcUIsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3BFLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEMsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBRWpGLElBQUksR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxDQUFRLENBQUM7SUFDekYsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDJCQUEyQixDQUFDLENBQUM7SUFDeEQsTUFBTSxzREFBTyxDQUFDLFFBQVEsQ0FBUSxDQUFDO0lBRS9CLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDdkYsSUFBSSxHQUFHLENBQUMsRUFBRTtRQUFFLE9BQU8sZ0NBQWdDLENBQUM7SUFFcEQsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDcEcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1FBQUUsT0FBTyw4Q0FBOEMsQ0FBQztJQUVuRSxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwREFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDL0UsTUFBTSwwREFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNsQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDakYsT0FBTyxNQUFNLDZEQUFxQixDQUFDLHNDQUFzQyxFQUFFLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQztBQUN0SyxDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsdUVBQXVFLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUM5RyxNQUFNLHNEQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sR0FBRyxDQUFDLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM3SCxNQUFNLHNEQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sR0FBRyxDQUFDLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUU3SCxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsbURBQWMsRUFBRSxHQUFHLEVBQUUsOENBQVMsRUFBRSxDQUFDLENBQUM7SUFFN0UsTUFBTSxLQUFLLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFjLENBQUM7SUFDeEUsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUc7UUFBRSxPQUFPLElBQUksQ0FBQztJQUN2RCxPQUFPLHlEQUF5RCxDQUFDO0FBQ2xFLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxrQkFBa0IsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ2pFLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDdkMsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBRWpGLElBQUksR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDakcsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDRCQUE0QixDQUFDLENBQUM7SUFFekQsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDNUYsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDJCQUEyQixDQUFDLENBQUM7SUFFeEQsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDNUYsNkRBQXFCLENBQUMsR0FBRyxFQUFFLDhCQUE4QixDQUFDLENBQUM7SUFFM0QsTUFBTSxTQUFTLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDL0MsSUFBSSxTQUFTLEVBQUUsR0FBRyxDQUFDLEtBQUssS0FBSyxNQUFNO1FBQUUsT0FBTyw2QkFBNkIsQ0FBQztJQUMxRSxJQUFJLFNBQVMsRUFBRSxHQUFHLENBQUMsS0FBSyxLQUFLLFNBQVM7UUFBRSxPQUFPLDRCQUE0QixDQUFDO0lBQzVFLElBQUksU0FBUyxFQUFFLEdBQUcsQ0FBQyxLQUFLLEtBQUssV0FBVztRQUFFLE9BQU8sK0JBQStCLENBQUM7SUFFakYsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsMERBQWtCLENBQUMsYUFBYSxDQUFDLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUM1RSxNQUFNLDBEQUFVLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ3ZDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNqRixPQUFPLE1BQU0sNkRBQXFCLENBQUMsbUNBQW1DLEVBQUUsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO0FBQ3ZMLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywwQ0FBMEMsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ2pGLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQztJQUNwQixJQUFJLEtBQUssR0FBRyxVQUFVLENBQUM7SUFDdkIsSUFBSSxLQUFLLEdBQUcsWUFBWSxDQUFDO0lBQ3pCLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDdkMsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBQ2pGLE1BQU0sc0RBQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDdEQsSUFBSSxTQUFTLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDN0MsSUFBSSxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLHdEQUF3RCxDQUFDO0lBQzNHLElBQUksQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDO1FBQUUsT0FBTyx1REFBdUQsQ0FBQztJQUMxRyxJQUFJLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQztRQUFFLE9BQU8sMERBQTBELENBQUM7SUFFN0csS0FBSyxHQUFHLE9BQU8sQ0FBQztJQUNoQixLQUFLLEdBQUcsVUFBVSxDQUFDO0lBQ25CLEtBQUssR0FBRyxZQUFZLENBQUM7SUFDckIsTUFBTSxzREFBTyxDQUFDLGFBQWEsRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUN0RCxTQUFTLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDekMsSUFBSSxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLHlEQUF5RCxDQUFDO0lBQzFHLElBQUksQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO1FBQUUsT0FBTyx3REFBd0QsQ0FBQztJQUN6RyxJQUFJLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUFFLE9BQU8sMkRBQTJELENBQUM7SUFFNUcsS0FBSyxHQUFHLDhCQUE4QixDQUFDO0lBQ3ZDLEtBQUssR0FBRyxrQ0FBa0MsQ0FBQztJQUMzQyxLQUFLLEdBQUcsb0NBQW9DLENBQUM7SUFDN0MsTUFBTSxzREFBTyxDQUFDLGFBQWEsRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUN0RCxTQUFTLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDekMsSUFBSSxTQUFTLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksU0FBUyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUFFLE9BQU8sb0RBQW9ELENBQUM7SUFDNUksSUFBSSxTQUFTLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksU0FBUyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUFFLE9BQU8sbURBQW1ELENBQUM7SUFDM0ksSUFBSSxTQUFTLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksU0FBUyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUFFLE9BQU8sc0RBQXNELENBQUM7SUFFOUksT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsOEJBQThCLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUM3RSxNQUFNLDBEQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLE1BQU0sUUFBUSxHQUFHLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsQ0FBQztJQUM1RCxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBRWpDLG9CQUFvQjtJQUNwQixJQUFJLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsWUFBWSxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDekQsNkRBQXFCLENBQUMsR0FBRyxFQUFFLGtDQUFrQyxDQUFDLENBQUM7SUFFL0QsSUFBSSxJQUFJLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDeEMsSUFBSSxJQUFLLENBQUMsR0FBRyxHQUFHLGlEQUFTLEVBQUUsR0FBRyxFQUFFLEdBQUcsS0FBTSxHQUFHLElBQUk7UUFBRSxPQUFPLGtFQUFrRSxHQUFHLElBQUksSUFBSSxDQUFDLGlEQUFTLEVBQUUsR0FBRyxFQUFFLEdBQUcsS0FBTSxHQUFHLElBQUksQ0FBQyxHQUFHLHVCQUF1QixHQUFHLElBQUksSUFBSSxDQUFDLElBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUV6TixzQkFBc0I7SUFDdEIsR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUN0RCw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsZ0NBQWdDLENBQUMsQ0FBQztJQUU3RCxJQUFJLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDcEMsSUFBSSxJQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7UUFBRSxPQUFPLHNDQUFzQyxDQUFDO0lBRW5FLHVDQUF1QztJQUN2QyxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFlBQVksRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBRXJELE1BQU0sVUFBVSxHQUFHLE1BQU0saURBQVMsQ0FBQyxRQUFRLEVBQUUsaURBQVMsRUFBRSxHQUFHLEVBQUUsR0FBRyxLQUFNLEdBQUcsSUFBSSxFQUFFLEtBQUssSUFBSSxFQUFFLENBQUMsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDN0ssSUFBSSxVQUFVLENBQUMsRUFBRTtRQUFFLE9BQU8sZ0NBQWdDLENBQUM7SUFFM0QsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsMEJBQTBCLEVBQUUsQ0FBQyxlQUFlLEVBQUUsV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3hGLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEMsTUFBTSxRQUFRLEdBQUcsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxDQUFDO0lBQzVELE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDakMsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLDZEQUFxQixDQUFDLEdBQUcsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDO0lBQ3pELE9BQU8sT0FBTyxHQUFHLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsK0JBQStCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztBQUNsRixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsaURBQWlELEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUMzRixNQUFNLDBEQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLElBQUksUUFBUSxHQUFHLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsQ0FBQztJQUMxRCxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ2pDLE1BQU0sSUFBSSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNwQyxNQUFNLHNEQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7SUFFeEIsTUFBTSwwREFBVSxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDdEMsUUFBUSxHQUFHLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxDQUFDLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLENBQUM7SUFDMUQsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNqQyxNQUFNLElBQUksR0FBRyxNQUFNLHNEQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDcEMsNkRBQXFCLENBQUMsSUFBSSxFQUFFLG1DQUFtQyxDQUFDLENBQUM7SUFFakUsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJO1FBQUUsT0FBTyxpREFBaUQsQ0FBQztJQUN0RixPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7OztBQzFPa0U7QUFFdkI7QUFDcUY7QUFHbkksNERBQWEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDL0IsTUFBTSxzREFBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLE1BQU0sMERBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDBEQUFrQixDQUFDLFNBQVMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDeEUsSUFBSSxZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FBQyxzREFBc0QsRUFDcEcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBWSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBWSxFQUFXLEVBQzdILENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQzlDLElBQUksWUFBWSxLQUFLLElBQUk7UUFBRSxPQUFPLFlBQVksQ0FBQztJQUUvQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNsRixZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FBQyx1REFBdUQsRUFDakcsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBWSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBWSxFQUFXLEVBQzdILENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQzlDLElBQUksWUFBWSxLQUFLLElBQUk7UUFBRSxPQUFPLFlBQVksQ0FBQztJQUUvQyxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNuSCxZQUFZLEdBQUcsTUFBTSw2REFBcUIsQ0FBQywwQ0FBMEMsRUFDcEYsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBUyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFXLEVBQ3BILENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQzlDLElBQUksWUFBWSxLQUFLLElBQUk7UUFBRSxPQUFPLFlBQVksQ0FBQztJQUUvQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUMzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RSxNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQy9CLFlBQVksR0FBRyxNQUFNLDZEQUFxQixDQUFDLHNDQUFzQyxFQUNoRixFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxRQUFjLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQVcsRUFDekgsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDOUMsSUFBSSxZQUFZLEtBQUssSUFBSTtRQUFFLE9BQU8sWUFBWSxDQUFDO0lBRS9DLFlBQVksR0FBRyxNQUFNLDZEQUFxQixDQUFDLHNDQUFzQyxFQUNoRixFQUFFLEdBQUcsRUFBRSxRQUFjLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBVyxFQUNuRyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsWUFBWSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUNqRSxFQUFFLFNBQVMsRUFBRSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxXQUFXLEVBQUUsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBRXZFLE9BQU8sWUFBWSxDQUFDO0FBQ3JCLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxrQ0FBa0MsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ2pGLE1BQU0sS0FBSyxHQUFHLHNCQUFzQixDQUFDO0lBQ3JDLE1BQU0sSUFBSSxHQUFHLDZCQUE2QixDQUFDO0lBRTNDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRW5ILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO0lBQzNELE1BQU0sTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RFLE1BQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFDN0IsTUFBTSxHQUFHLEdBQUcsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUMvSCw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsbUNBQW1DLENBQUMsQ0FBQztJQUVoRSxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDckYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEtBQUssS0FBSyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0FBQ2xILENBQUMsQ0FBQyxDQUFDO0FBRUgsbUVBQW1FO0FBQ25FLHlEQUFVLENBQUMscUJBQXFCLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUNwRSxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUVsRixNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNuSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUUzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RSxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQzdCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLDZCQUE2QixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFMUssTUFBTSxPQUFPLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RGLE1BQU0sR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsQ0FBQyxDQUFDO0lBQ25HLDZEQUFxQixDQUFDLEdBQUcsRUFBRSxpQ0FBaUMsQ0FBQyxDQUFDO0lBRTlELE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDBEQUFrQixDQUFDLGNBQWMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDN0UsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFFbEYsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFDbkgsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFFM0QsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDdEUsTUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUM3QixNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSw2QkFBNkIsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBRTFLLE1BQU0sT0FBTyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RixPQUFPLE1BQU0sNkRBQXFCLENBQUMsb0NBQW9DLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxjQUFjLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztBQUMvSyxDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsd0RBQXdELEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUNsRyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUVsRixNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNuSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUUzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RSxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBRTdCLE1BQU0sc0RBQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4QixNQUFNLDBEQUFVLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN0QyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxtQkFBbUIsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUVuRixNQUFNLEdBQUcsR0FBRyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSw2QkFBNkIsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO0lBQ3RMLDJEQUFtQixDQUFDLEdBQUcsRUFBRSxxREFBcUQsQ0FBQyxDQUFDO0lBQ2hGLE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLCtDQUErQyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDekYsTUFBTSxLQUFLLEdBQUcsT0FBTyxHQUFHLGtCQUFrQixDQUFDO0lBQzNDLE1BQU0sS0FBSyxHQUFHLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQztJQUM1QyxNQUFNLDBEQUFVLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN0QyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUUzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNuSCxNQUFNLHNEQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUN2RCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUUzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RSxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQzdCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLDZCQUE2QixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFMUssTUFBTSxzREFBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBRXhCLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQzNELElBQUksR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzFFLDJEQUFtQixDQUFDLEdBQUcsRUFBRSwwQ0FBMEMsQ0FBQyxDQUFDO0lBRXJFLE1BQU0sVUFBVSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDM0QsTUFBTSxzREFBTyxDQUFDLGtCQUFrQixFQUFFLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0lBRTlELEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN0RSwyREFBbUIsQ0FBQyxHQUFHLEVBQUUsMENBQTBDLENBQUMsQ0FBQztJQUVyRSxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxzQ0FBc0MsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3JGLE1BQU0sWUFBWSxHQUFHLHNCQUFzQixDQUFDO0lBQzVDLE1BQU0sV0FBVyxHQUFHLDZCQUE2QixDQUFDO0lBQ2xELE1BQU0sVUFBVSxHQUFHLGtCQUFrQixDQUFDO0lBQ3RDLE1BQU0sU0FBUyxHQUFHLDhCQUE4QixDQUFDO0lBRWpELE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQ2xGLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBRWhILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDbkQsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUM5RCxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQzFCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDN0gsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUUxSCwyQkFBMkI7SUFDM0IsSUFBSSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDbkUsSUFBSSxHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN2Qiw2REFBcUIsQ0FBQyxHQUFHLEVBQUUsOEJBQThCLENBQUMsQ0FBQztJQUMzRCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxLQUFLLFlBQVksSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxXQUFXLENBQUM7UUFBRSxPQUFPLG1DQUFtQyxDQUFDO0lBRXpILDhCQUE4QjtJQUM5QixHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDN0QsR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbkIsNkRBQXFCLENBQUMsR0FBRyxFQUFFLGdDQUFnQyxDQUFDLENBQUM7SUFDN0QsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssS0FBSyxVQUFVLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssU0FBUyxDQUFDO1FBQUUsT0FBTyxtQ0FBbUMsQ0FBQztJQUVySCxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQywrQkFBK0IsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQzlFLFNBQVM7SUFDVCxNQUFNLEtBQUssR0FBRyxzQkFBc0IsQ0FBQztJQUNyQyxNQUFNLElBQUksR0FBRyw2QkFBNkIsQ0FBQztJQUUzQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNsRixNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUVuSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUMzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RSxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQzdCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFbkgsSUFBSSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3pFLElBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFdkIsNEJBQTRCO0lBQzVCLElBQUksT0FBTyxHQUFHLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsQ0FBQztJQUN4RSxJQUFJLFVBQVUsR0FBRyxNQUFNLHNEQUFPLENBQUMsWUFBWSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ3BGLElBQUksUUFBUSxHQUFHLGlEQUFTLEVBQUUsQ0FBQztJQUMzQiw2REFBcUIsQ0FBQyxVQUFVLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztJQUUvRCxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3JFLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRW5CLElBQUksR0FBRyxFQUFFLFNBQVMsS0FBSyxPQUFPLENBQUMsU0FBUyxJQUFJLEdBQUcsRUFBRSxXQUFXLEtBQUssT0FBTyxDQUFDLFdBQVcsSUFBSSxHQUFHLEVBQUUsV0FBVyxLQUFLLE9BQU8sQ0FBQyxXQUFXLEVBQUU7UUFDakksT0FBTyxtREFBbUQsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxHQUFHLEVBQUUsR0FBRyxPQUFPLEVBQUUsQ0FBQztZQUNsRyxjQUFjLEdBQUcsR0FBRyxDQUFDO0tBQ3RCO0lBQ0QsR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUUvRSw2RUFBNkU7SUFDN0UsSUFBSSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsV0FBVyxFQUFFLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3hGLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBRTFGLElBQUkscUJBQXFCLEtBQUsscUJBQXFCO1FBQUUsT0FBTyx5Q0FBeUMsR0FBRyxxQkFBcUIsR0FBRyxjQUFjLEdBQUcscUJBQXFCLENBQUM7SUFDdkssSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLFFBQVEsR0FBRyxJQUFJLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxHQUFHLFFBQVEsR0FBRyxJQUFJLENBQUMsRUFBRTtRQUNqRyxPQUFPLHNDQUFzQyxHQUFHLDZDQUFLLENBQUMsUUFBUSxDQUFDO1lBQzlELGNBQWMsR0FBRyw2Q0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQztZQUNoRCxvQkFBb0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO0tBQzdGO0lBRUQsdUJBQXVCO0lBQ3ZCLE9BQU8sR0FBRyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLENBQUM7SUFDdkUsVUFBVSxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDaEYsUUFBUSxHQUFHLGlEQUFTLEVBQUUsQ0FBQztJQUN2Qiw2REFBcUIsQ0FBQyxVQUFVLEVBQUUsbUNBQW1DLENBQUMsQ0FBQztJQUV2RSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3JFLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ25CLElBQUksR0FBRyxFQUFFLFNBQVMsS0FBSyxPQUFPLENBQUMsU0FBUyxJQUFJLEdBQUcsRUFBRSxXQUFXLEtBQUssT0FBTyxDQUFDLFdBQVcsSUFBSSxHQUFHLEVBQUUsV0FBVyxLQUFLLE9BQU8sQ0FBQyxXQUFXLEVBQUU7UUFDakksT0FBTyxtREFBbUQsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsR0FBRyxHQUFHLEVBQUUsR0FBRyxPQUFPLEVBQUUsQ0FBQyxHQUFHLGNBQWMsR0FBRyxHQUFHLENBQUM7S0FDM0g7SUFFRCxrQkFBa0I7SUFDbEIsR0FBRyxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUUvRSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsV0FBVyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxlQUFlLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzlILHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUV0RixJQUFJLHFCQUFxQixLQUFLLHFCQUFxQjtRQUFFLE9BQU8seUNBQXlDLEdBQUcscUJBQXFCLEdBQUcsY0FBYyxHQUFHLHFCQUFxQixDQUFDO0lBQ3ZLLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxRQUFRLEdBQUcsSUFBSSxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsR0FBRyxRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUU7UUFDakcsT0FBTyxzQ0FBc0MsR0FBRyw2Q0FBSyxDQUFDLFFBQVEsQ0FBQztZQUM5RCxjQUFjLEdBQUcsNkNBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFDaEQsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztLQUM3RjtJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2IsQ0FBQyxDQUFDLENBQUM7QUFFSCx5REFBVSxDQUFDLDBEQUFrQixDQUFDLFlBQVksQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7SUFDM0UsTUFBTSxLQUFLLEdBQUcsc0JBQXNCLENBQUM7SUFDckMsTUFBTSxJQUFJLEdBQUcsNkJBQTZCLENBQUM7SUFFM0MsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEdBQUcsa0JBQWtCLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7SUFDbEYsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFVLENBQUM7SUFFbkgsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDM0QsTUFBTSxNQUFNLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDdEUsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUMxQixNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFMUcsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZUFBZSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzdFLE1BQU0sR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFekIsTUFBTSxPQUFPLEdBQUcsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxDQUFDO0lBRTNFLElBQUksWUFBWSxHQUFHLE1BQU0sNkRBQXFCLENBQUMseUNBQXlDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FDakosQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLEVBQUUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDbEQsSUFBSSxZQUFZLEtBQUssSUFBSTtRQUFFLE9BQU8sWUFBWSxDQUFDO0lBRS9DLFlBQVksR0FBRyxNQUFNLDZEQUFxQixDQUFDLHlDQUF5QyxFQUFFLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQy9HLENBQUMsWUFBWSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFLFNBQVMsRUFBRSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUUzTixPQUFPLFlBQVksQ0FBQztBQUNyQixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsbUNBQW1DLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUMxRSxJQUFJLEtBQUssR0FBRyx1QkFBdUIsQ0FBQztJQUNwQyxNQUFNLElBQUksR0FBRyw2QkFBNkIsQ0FBQztJQUUzQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNsRixNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUVuSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUMzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RSxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBRTdCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFbkgsS0FBSyxHQUFHLHVCQUF1QixDQUFDO0lBQ2hDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFbkgsS0FBSyxHQUFHLGlDQUFpQyxDQUFDO0lBQzFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFbkgsTUFBTSxPQUFPLEdBQWMsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ2pHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLG9EQUFvRCxDQUFDO0lBQ25HLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLHFEQUFxRCxDQUFDO0lBQ2xHLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQUUsT0FBTyx1Q0FBdUMsQ0FBQztJQUVwRixPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxnREFBZ0QsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQ3ZGLE1BQU0sS0FBSyxHQUFHLHNCQUFzQixDQUFDO0lBQ3JDLElBQUksSUFBSSxHQUFHLDhCQUE4QixDQUFDO0lBRTFDLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLEVBQUUsT0FBTyxHQUFHLGtCQUFrQixFQUFFLEdBQUcsRUFBRSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBRWxGLE1BQU0sTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBVSxDQUFDO0lBQ25ILE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO0lBRTNELE1BQU0sTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RFLE1BQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7SUFFN0IsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUVuSCxJQUFJLEdBQUcsOEJBQThCLENBQUM7SUFDdEMsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUVuSCxJQUFJLEdBQUcsaUVBQWlFLENBQUM7SUFDekUsTUFBTSxzREFBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUVuSCxNQUFNLE9BQU8sR0FBYyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFFakcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUM7UUFBRSxPQUFPLG1EQUFtRCxDQUFDO0lBRWhILElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDO1FBQUUsT0FBTyxvREFBb0QsQ0FBQztJQUUvRyxNQUFNLFdBQVcsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDMUQsSUFBSSxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUFFLE9BQU8sc0NBQXNDLENBQUM7SUFDOUUsSUFBSSxXQUFXLEtBQUssNkJBQTZCO1FBQUUsT0FBTyx5Q0FBeUMsQ0FBQztJQUVwRyxPQUFPLElBQUksQ0FBQztBQUNiLENBQUMsQ0FBQyxDQUFDO0FBRUgseURBQVUsQ0FBQyxtQ0FBbUMsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFO0lBQzFFLElBQUksSUFBSSxHQUFHLGFBQWEsQ0FBQztJQUV6QixNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUVsRixNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUNuSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUUzRCxNQUFNLE1BQU0sR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQztJQUN0RSxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO0lBQzdCLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLDZCQUE2QixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7SUFFMUssTUFBTSxPQUFPLEdBQUcsQ0FBQyxNQUFNLHNEQUFPLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDO0lBQ3RGLE1BQU0sc0RBQU8sQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFFekUsSUFBSSxHQUFHLGFBQWEsQ0FBQztJQUNyQixNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBRXpFLElBQUksR0FBRyx1Q0FBdUMsQ0FBQztJQUMvQyxNQUFNLHNEQUFPLENBQUMsY0FBYyxFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBRXpFLE1BQU0sYUFBYSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7SUFDeEcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQztRQUFFLE9BQU8sb0RBQW9ELENBQUM7SUFDM0csSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUFFLE9BQU8scURBQXFELENBQUM7SUFFMUcsTUFBTSxZQUFZLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDckQsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUFFLE9BQU8sc0NBQXNDLENBQUM7SUFDL0UsSUFBSSxZQUFZLEtBQUssWUFBWTtRQUFFLE9BQU8seUNBQXlDLENBQUM7SUFDcEYsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDLENBQUMsQ0FBQztBQUVILHlEQUFVLENBQUMsMERBQWtCLENBQUMsVUFBVSxDQUFDLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsRUFBRTtJQUN6RSxNQUFNLEtBQUssR0FBRyxzQkFBc0IsQ0FBQztJQUNyQyxNQUFNLElBQUksR0FBRyw2QkFBNkIsQ0FBQztJQUUzQyxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sR0FBRyxrQkFBa0IsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztJQUNsRixNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQVUsQ0FBQztJQUVoSCxNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO0lBQ25ELE1BQU0sTUFBTSxHQUFHLENBQUMsTUFBTSxzREFBTyxDQUFDLGNBQWMsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFLLENBQUM7SUFDOUQsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztJQUMxQixNQUFNLHNEQUFPLENBQUMsU0FBUyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUVuRyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sc0RBQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRXJFLElBQUksWUFBWSxHQUFHLE1BQU0sNkRBQXFCLENBQUMsbUNBQW1DLEVBQ2pGLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFDN0IsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsVUFBVSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDL0MsSUFBSSxZQUFZLEtBQUssSUFBSTtRQUFFLE9BQU8sWUFBWSxDQUFDO0lBRS9DLFlBQVksR0FBRyxNQUFNLDZEQUFxQixDQUFDLG1DQUFtQyxFQUM3RSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFDWixDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQyxVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUUvQyxPQUFPLFlBQVksQ0FBQztBQUNyQixDQUFDLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlYOEI7QUFDUTtBQUNzQjtBQUV4RCxTQUFTLFNBQVMsS0FBSyxPQUFPLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ3JELFNBQVMsS0FBSyxDQUFDLEVBQVUsSUFBbUIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFFakgsTUFBTSxZQUFZLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFVLENBQUM7QUFFOUUsTUFBTSxlQUFlLEdBQUcsTUFBZSxDQUFDO0FBRWpDLEtBQUssVUFBVSxxQkFBcUIsQ0FBSSxZQUFlLEVBQUUsT0FBb0QsRUFBRSxPQUEwQjtJQUMvSSxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2hELE1BQU0sV0FBVyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFFOUMsS0FBSyxNQUFNLFVBQVUsSUFBSSxXQUFXLEVBQUU7UUFDckMsS0FBSyxNQUFNLFdBQVcsSUFBSSxZQUFZLEVBQUU7WUFDdkMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUUsWUFBb0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEYsT0FBTyxJQUFLLE9BQWUsQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFvQixFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQUUsU0FBUztZQUM1SSxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxXQUFXLENBQUM7WUFDdkMsTUFBTSxNQUFNLEdBQUcsTUFBTSxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDM0MsSUFBSSxNQUFNLEtBQUssSUFBSTtnQkFBRSxPQUFPLE1BQU0sQ0FBQztTQUNuQztLQUNEO0lBQ0QsS0FBSyxNQUFNLFdBQVcsSUFBSSxZQUFZLEVBQUU7UUFDdkMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO1lBQy9ELE9BQU8sSUFBSyxPQUFlLENBQUMsZUFBZSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBb0IsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQUUsU0FBUztRQUNqSixNQUFNLE1BQU0sR0FBRyxNQUFNLE9BQU8sQ0FBQyxXQUFrQixDQUFDLENBQUM7UUFDakQsSUFBSSxNQUFNLEtBQUssSUFBSTtZQUFFLE9BQU8sTUFBTSxDQUFDO0tBQ25DO0lBQ0QsT0FBTyxJQUFJLENBQUM7QUFDYixDQUFDO0FBRU0sU0FBUyxLQUFLLENBQUMsSUFBWTtJQUNqQyxJQUFJLE9BQU8sSUFBSSxJQUFJLFFBQVE7UUFBRSxPQUFPLG1CQUFtQixDQUFDO0lBQ3hELE1BQU0sQ0FBQyxHQUFHLHNEQUFtQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3BDLFNBQVMsQ0FBQyxDQUFDLENBQVMsSUFBSSxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNELE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO0FBQzlDLENBQUM7QUFFTSxLQUFLLFVBQVUscUJBQXFCLENBQWlHLFdBQW1CLEVBQUUsWUFBZSxFQUFFLGNBQTZELEVBQUUsT0FBMEI7SUFDMVEsTUFBTSxlQUFlLEdBQUcsaUJBQWlCLENBQUM7SUFDMUMsT0FBTyxNQUFNLHFCQUFxQixDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLEVBQUU7UUFDdkUsTUFBTSxVQUFVLEdBQUcsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRWhELE1BQU0sR0FBRyxHQUFHLE1BQU0sc0RBQU8sQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxFQUFFLEVBQUU7WUFDekIsTUFBTSxVQUFVLEdBQUcsY0FBYyxDQUFDLGVBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUUsQ0FBQztZQUM5RCxJQUFJLHNCQUFzQixDQUFDO1lBQzNCLElBQUksVUFBaUIsS0FBSyxlQUFlLEVBQUU7Z0JBQzFDLHNCQUFzQixHQUFHLGlCQUFpQixDQUFDO2FBQzNDO2lCQUNJO2dCQUNKLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxlQUFlLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RGLE1BQU0sVUFBVSxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xELE1BQU0sTUFBTSxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUNqRCxNQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN2QyxzQkFBc0IsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQzthQUNqRDtZQUVELE1BQU0sYUFBYSxHQUFHLDZDQUE2QyxHQUFHLHNCQUFzQixHQUFHLCtCQUErQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDOUosSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLO2dCQUFFLE9BQU8sVUFBVSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyw0QkFBNEIsR0FBRyxhQUFhLENBQUM7O2dCQUM1RixPQUFPLFdBQVcsR0FBRyxpQkFBaUIsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxHQUFHLGFBQWEsQ0FBQztTQUNuRjs7WUFDSSxPQUFPLElBQUksQ0FBQztJQUNsQixDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFDYixDQUFDO0FBRU0sS0FBSyxVQUFVLFNBQVMsQ0FBSSxJQUFrQyxFQUFFLFNBQWlCLEVBQUUsTUFBd0I7SUFDakgsSUFBSTtRQUNILE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxtREFBYyxFQUFFLEdBQUcsRUFBRSw4Q0FBUyxFQUFFLENBQUMsQ0FBQztRQUM3RSxNQUFNLHNEQUFPLENBQUMsdUJBQXVCLEVBQUUsRUFBRSxHQUFHLEVBQUUsNkNBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1FBQ3JFLE1BQU0sc0RBQU8sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDN0IsT0FBTyxNQUFNLE1BQU0sRUFBRSxDQUFDO0tBQ3RCO1lBQ087UUFDUCxNQUFNLHNEQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsbURBQWMsRUFBRSxHQUFHLEVBQUUsOENBQVMsRUFBRSxDQUFDLENBQUM7UUFDN0UsTUFBTSxzREFBTyxDQUFDLHVCQUF1QixFQUFFLEVBQUUsR0FBRyxFQUFFLDZDQUFRLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDOUUsTUFBTSxzREFBTyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztLQUM3QjtBQUNGLENBQUM7QUFFTSxTQUFTLGtCQUFrQixDQUFDLFlBQW9CO0lBQ3RELE9BQU8sMkNBQTJDLEdBQUcsWUFBWSxDQUFDO0FBQ25FLENBQUM7QUFFTSxTQUFTLHFCQUFxQixDQUE0QyxHQUFNLEVBQUUsV0FBbUI7SUFDM0csSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLO1FBQUUsTUFBTSwrQ0FBK0MsR0FBRyxXQUFXLENBQUM7SUFDcEYsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1FBQUUsTUFBTSw4QkFBOEIsR0FBRyxXQUFXLENBQUM7QUFDakUsQ0FBQztBQUNNLFNBQVMsbUJBQW1CLENBQTRDLEdBQU0sRUFBRSxXQUFtQjtJQUN6RyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUs7UUFBRSxNQUFNLCtDQUErQyxHQUFHLFdBQVcsQ0FBQztJQUNwRixJQUFJLEdBQUcsQ0FBQyxFQUFFO1FBQUUsTUFBTSwwREFBMEQsR0FBRyxXQUFXLENBQUM7QUFDNUYsQ0FBQztBQUdEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUF1RkU7Ozs7Ozs7Ozs7O0FDekxGOzs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7O0FDQUE7Ozs7OztVQ0FBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDdEJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0E7V0FDQSxpQ0FBaUMsV0FBVztXQUM1QztXQUNBOzs7OztXQ1BBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EseUNBQXlDLHdDQUF3QztXQUNqRjtXQUNBO1dBQ0E7Ozs7O1dDUEE7Ozs7O1VFQUE7VUFDQTtVQUNBO1VBQ0EiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY29yZS9SZXF1ZXN0VmFsaWRhdG9yLnRzIiwid2VicGFjazovLy8uL3NyYy9jb3JlL3UudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2RhdGEvQ2hhdC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvZGF0YS9DaGVja2xpc3QudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2RhdGEvVXNlci50cyIsIndlYnBhY2s6Ly8vLi9zcmMvZGF0YS91LnRzIiwid2VicGFjazovLy8uL3Rlc3QvcmVxdWVzdHMvY29uZmlnLnRzIiwid2VicGFjazovLy8uL3Rlc3QvcmVxdWVzdHMvcmVxdWVzdF90ZXN0LnRzIiwid2VicGFjazovLy8uL3Rlc3QvcmVxdWVzdHMvcmVxdWVzdF91dGlscy50cyIsIndlYnBhY2s6Ly8vLi90ZXN0L3JlcXVlc3RzL3VzZXJfcmVxdWVzdHNfdGVzdHMvY2hhdF90ZXN0LnRzIiwid2VicGFjazovLy8uL3Rlc3QvcmVxdWVzdHMvdXNlcl9yZXF1ZXN0c190ZXN0cy9jaGVja2xpc3RfdGVzdC50cyIsIndlYnBhY2s6Ly8vLi90ZXN0L3JlcXVlc3RzL3VzZXJfcmVxdWVzdHNfdGVzdHMvZ3JvdXBfdGVzdC50cyIsIndlYnBhY2s6Ly8vLi90ZXN0L3JlcXVlc3RzL3VzZXJfcmVxdWVzdHNfdGVzdHMvbWlzY190ZXN0LnRzIiwid2VicGFjazovLy8uL3Rlc3QvcmVxdWVzdHMvdXNlcl9yZXF1ZXN0c190ZXN0cy9vcmdfdGVzdC50cyIsIndlYnBhY2s6Ly8vLi90ZXN0L3JlcXVlc3RzL3VzZXJfcmVxdWVzdHNfdGVzdHMvc2Vzc2lvbl90ZXN0LnRzIiwid2VicGFjazovLy8uL3Rlc3QvcmVxdWVzdHMvdXNlcl9yZXF1ZXN0c190ZXN0cy90aWNrZXRfdGVzdC50cyIsIndlYnBhY2s6Ly8vLi90ZXN0L3JlcXVlc3RzL3V0aWxzLnRzIiwid2VicGFjazovLy9leHRlcm5hbCBjb21tb25qcyBcIkBzaW5vbmpzL2Zha2UtdGltZXJzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIGNvbW1vbmpzIFwiZW1haWwtdmFsaWRhdG9yXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIGNvbW1vbmpzIFwianNvbjVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgY29tbW9uanMgXCJqc29uNS9saWIvcmVnaXN0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgY29tbW9uanMgXCJsdXhvblwiIiwid2VicGFjazovLy9leHRlcm5hbCBjb21tb25qcyBcIm5vZGUtZmV0Y2hcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgY29tbW9uanMgXCJzaGVsbGpzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIGNvbW1vbmpzIFwic291cmNlLW1hcC1zdXBwb3J0XCIiLCJ3ZWJwYWNrOi8vL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovLy93ZWJwYWNrL3J1bnRpbWUvY29tcGF0IGdldCBkZWZhdWx0IGV4cG9ydCIsIndlYnBhY2s6Ly8vd2VicGFjay9ydW50aW1lL2RlZmluZSBwcm9wZXJ0eSBnZXR0ZXJzIiwid2VicGFjazovLy93ZWJwYWNrL3J1bnRpbWUvaGFzT3duUHJvcGVydHkgc2hvcnRoYW5kIiwid2VicGFjazovLy93ZWJwYWNrL2JlZm9yZS1zdGFydHVwIiwid2VicGFjazovLy93ZWJwYWNrL3N0YXJ0dXAiLCJ3ZWJwYWNrOi8vL3dlYnBhY2svYWZ0ZXItc3RhcnR1cCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBpc1JlY29yZCwgcmVtb3ZlTmFtZVdoaXRlc3BhY2UgfSBmcm9tIFwiQC9jb3JlL3VcIjtcbmltcG9ydCB7IE5vcm1hbFJlcXVlc3RzIH0gZnJvbSBcIkAvZGF0YS9SZXF1ZXN0UHJvdG9jb2xcIjtcbmltcG9ydCB7IHZhbGlkYXRlUGFzc3dvcmQgfSBmcm9tIFwiQC9kYXRhL1VzZXJcIjtcbmltcG9ydCB7IENoZWNrbGlzdFNlY3Rpb24sIGRheXMgfSBmcm9tIFwiQC9kYXRhL0NoZWNrbGlzdFwiO1xuaW1wb3J0IHsgdmFsaWRhdGUgYXMgdmFsaWRhdGVFbWFpbCB9IGZyb20gXCJlbWFpbC12YWxpZGF0b3JcIjtcblxudHlwZSBSViA9IHtcblx0cmVhZG9ubHkgW0EgaW4gTm9ybWFsUmVxdWVzdHNbXCJhY3Rpb25cIl1dOiAoLi4ucTogUGFyYW1ldGVyczxFeHRyYWN0PE5vcm1hbFJlcXVlc3RzLCB7IGFjdGlvbjogQSB9PltcInJwZlwiXT4pID0+IGJvb2xlYW5cbn07XG5cbmNvbnN0IHZ2TWFwID0gXCJtYXBcIjtcbmNvbnN0IHZ2QXJyYXkgPSBcImFycmF5XCI7XG5jb25zdCB2dklkID0gXCJpZFwiO1xuY29uc3QgdnZTdHJpbmdBbnkgPSBcInN0cmluZywgYW55XCI7XG5jb25zdCB2dlN0cmluZ0FueUFycmF5ID0gXCJhcnJheSBvZiBhbnkgc3RyaW5nc1wiO1xuY29uc3QgdnZTdHJpbmdOb25FbXB0eSA9IFwic3RyaW5nLCBub24tZW1wdHlcIjtcbmNvbnN0IHZ2U3RyaW5nTm9uRW1wdHlBcnJheSA9IFwiYXJyYXkgb2Ygbm9uLWVtcHR5IHN0cmluZ3NcIjtcbmNvbnN0IHZ2TnVtYmVyID0gXCJudW1iZXJcIjtcbmNvbnN0IHZ2Qm9vbGVhbiA9IFwiYm9vbGVhblwiO1xudHlwZSBWYWxpZFR5cGUgPSAoXG5cdHwgdHlwZW9mIHZ2TWFwXG5cdHwgdHlwZW9mIHZ2QXJyYXlcblx0fCB0eXBlb2YgdnZJZFxuXHR8IHR5cGVvZiB2dlN0cmluZ0FueVxuXHR8IHR5cGVvZiB2dlN0cmluZ0FueUFycmF5XG5cdHwgdHlwZW9mIHZ2U3RyaW5nTm9uRW1wdHlcblx0fCB0eXBlb2YgdnZTdHJpbmdOb25FbXB0eUFycmF5XG5cdHwgdHlwZW9mIHZ2TnVtYmVyXG5cdHwgdHlwZW9mIHZ2Qm9vbGVhblxuKTtcbmNvbnN0IG1hcmtlcl9vcHRpb25hbCA9IFwibnVsbCBvciB1bmRlZmluZWQgb3IgXCI7XG5jb25zdCB2dk9wdGlvbmFsID0gPFggZXh0ZW5kcyBWYWxpZFR5cGU+KHg6IFgpOiBgJHt0eXBlb2YgbWFya2VyX29wdGlvbmFsfSR7WH1gID0+IGAke21hcmtlcl9vcHRpb25hbH0ke3h9YDtcbnR5cGUgT3B0aW9uYWxWYWxpZFR5cGUgPSBWYWxpZFR5cGUgfCBgJHt0eXBlb2YgbWFya2VyX29wdGlvbmFsfSR7VmFsaWRUeXBlfWA7XG50eXBlIE9iamVjdEFycmF5ID0gUmVjb3JkPHN0cmluZywgYW55PltdO1xudHlwZSBWYWxpZFR5cGVzVGVtcGxhdGU8WD4gPSB7XG5cdFtrIGluIGtleW9mIFhdPzogKFxuXHRcdFhba10gZXh0ZW5kcyBzdHJpbmdbXVxuXHRcdFx0P1xuXHRcdFx0fCBgJHt0eXBlb2YgbWFya2VyX29wdGlvbmFsfSR7dHlwZW9mIHZ2U3RyaW5nQW55QXJyYXkgfCB0eXBlb2YgdnZTdHJpbmdOb25FbXB0eUFycmF5fWBcblx0XHRcdHwgdHlwZW9mIHZ2U3RyaW5nQW55QXJyYXkgfCB0eXBlb2YgdnZTdHJpbmdOb25FbXB0eUFycmF5XG5cdFx0XHQ6IFhba10gZXh0ZW5kcyBPYmplY3RBcnJheVxuXHRcdFx0XHQ/IHR5cGVvZiB2dkFycmF5XG5cdFx0XHRcdDogWFtrXSBleHRlbmRzIFJlY29yZDxzdHJpbmcsIGFueT5cblx0XHRcdFx0XHQ/IHR5cGVvZiB2dk1hcCB8IFZhbGlkVHlwZXNUZW1wbGF0ZTxYW2tdPlxuXHRcdFx0XHRcdDogT3B0aW9uYWxWYWxpZFR5cGVcblx0KVxufTtcbmZ1bmN0aW9uIFR5cGVDaGVjazxYIGV4dGVuZHMgUmVjb3JkPHN0cmluZywgYW55Pj4ocTogWCwgdHlwZV90ZW1wbGF0ZTogVmFsaWRUeXBlc1RlbXBsYXRlPFg+KTogYm9vbGVhbiB7XG5cdGlmICghaXNSZWNvcmQocSkpIHJldHVybiBmYWxzZTtcblx0bGV0IGlzX3YgPSBmYWxzZTtcblxuXHRmb3IgKGNvbnN0IGtleSBpbiB0eXBlX3RlbXBsYXRlKSB7XG5cdFx0Y29uc3QgeCA9IHFba2V5XTtcblx0XHRjb25zdCB0eXBlX29yX3RlbXBsYXRlID0gdHlwZV90ZW1wbGF0ZVtrZXldO1xuXHRcdGxldCB0eXBlID0gdHlwZW9mIHR5cGVfb3JfdGVtcGxhdGUgPT0gXCJzdHJpbmdcIlxuXHRcdFx0PyB0eXBlX29yX3RlbXBsYXRlIGFzIE9wdGlvbmFsVmFsaWRUeXBlXG5cdFx0XHQ6IHZ2TWFwO1xuXG5cdFx0aWYgKHR5cGUuc3RhcnRzV2l0aChtYXJrZXJfb3B0aW9uYWwpKSB7XG5cdFx0XHRpZiAoeCA9PT0gdW5kZWZpbmVkIHx8IHggPT09IG51bGwpIGlzX3YgPSB0cnVlO1xuXHRcdFx0ZWxzZSB0eXBlID0gdHlwZS5zcGxpdChtYXJrZXJfb3B0aW9uYWwpWzFdIGFzIFZhbGlkVHlwZTtcblx0XHR9XG5cblx0XHRzd2l0Y2ggKHR5cGUpIHtcblx0XHRcdGNhc2UgdnZNYXA6IHtcblx0XHRcdFx0aXNfdiA9IGlzUmVjb3JkKHgpXG5cdFx0XHRcdFx0JiYgKHR5cGUgPT0gdHlwZV9vcl90ZW1wbGF0ZSB8fCBUeXBlQ2hlY2soeCBhcyBhbnksIHR5cGVfb3JfdGVtcGxhdGUgYXMgYW55KSk7XG5cdFx0XHR9IGJyZWFrO1xuXHRcdFx0Y2FzZSB2dlN0cmluZ0FueToge1xuXHRcdFx0XHRpc192ID0gdHlwZW9mIHggPT0gXCJzdHJpbmdcIjtcblx0XHRcdH0gYnJlYWs7XG5cdFx0XHRjYXNlIHZ2U3RyaW5nTm9uRW1wdHk6IHtcblx0XHRcdFx0aXNfdiA9IHR5cGVvZiB4ID09IFwic3RyaW5nXCJcblx0XHRcdFx0XHQmJiB4LnRyaW0oKSAhPSBcIlwiO1xuXHRcdFx0fSBicmVhaztcblx0XHRcdGNhc2UgdnZJZDoge1xuXHRcdFx0XHRpc192ID0gdHlwZW9mIHggPT0gXCJzdHJpbmdcIlxuXHRcdFx0XHRcdCYmIHguc3RhcnRzV2l0aChcInhcIik7XG5cdFx0XHR9IGJyZWFrO1xuXHRcdFx0Y2FzZSB2dkJvb2xlYW46IHtcblx0XHRcdFx0aXNfdiA9IHR5cGVvZiB4ID09IFwiYm9vbGVhblwiO1xuXHRcdFx0fSBicmVhaztcblx0XHRcdGNhc2UgdnZOdW1iZXI6IHtcblx0XHRcdFx0aXNfdiA9IHR5cGVvZiB4ID09IFwibnVtYmVyXCI7XG5cdFx0XHR9IGJyZWFrO1xuXHRcdFx0Y2FzZSB2dkFycmF5OiB7XG5cdFx0XHRcdGlzX3YgPSBBcnJheS5pc0FycmF5KHgpO1xuXHRcdFx0fSBicmVhaztcblx0XHRcdGNhc2UgdnZTdHJpbmdBbnlBcnJheToge1xuXHRcdFx0XHRpc192ID0gQXJyYXkuaXNBcnJheSh4KVxuXHRcdFx0XHRcdCYmIHguZXZlcnkoKGl0ZW06IHN0cmluZykgPT4gdHlwZW9mIGl0ZW0gPT0gXCJzdHJpbmdcIik7XG5cdFx0XHR9IGJyZWFrO1xuXHRcdFx0Y2FzZSB2dlN0cmluZ05vbkVtcHR5QXJyYXk6IHtcblx0XHRcdFx0aXNfdiA9IEFycmF5LmlzQXJyYXkoeClcblx0XHRcdFx0XHQmJiB4LmV2ZXJ5KChpdGVtOiBzdHJpbmcpID0+IHR5cGVvZiBpdGVtID09IFwic3RyaW5nXCIgJiYgaXRlbS50cmltKCkgIT0gXCJcIik7XG5cdFx0XHR9IGJyZWFrO1xuXHRcdFx0ZGVmYXVsdDogeyB9XG5cdFx0fVxuXG5cdFx0aWYgKCFpc192KSByZXR1cm4gZmFsc2U7XG5cdH1cblxuXHRyZXR1cm4gdHJ1ZTtcbn1cblxuY29uc3QgVk9JRF9JTlBVVCA9ICgpID0+IHRydWU7XG5cbmV4cG9ydCBjb25zdCBSZXF1ZXN0VmFsaWRhdG9yOiBSViA9IHtcblx0Z2V0Q3NzVmVyc2lvbjogVk9JRF9JTlBVVCxcblx0Y2hhbmdlUGFzc3dvcmQ6IChxKSA9PiB7XG5cdFx0cmV0dXJuICh0cnVlXG5cdFx0XHQmJiB0eXBlb2YgcS5vbGRfcHdkID09IFwic3RyaW5nXCJcblx0XHRcdCYmIHZhbGlkYXRlUGFzc3dvcmQocS5uZXdfcHdkKVxuXHRcdCk7XG5cdH0sXG5cdGNoYW5nZU5hbWVzOiAocSkgPT4ge1xuXHRcdHJldHVybiAodHJ1ZVxuXHRcdFx0JiYgdHlwZW9mIHEuZm5hbWUgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgdHlwZW9mIHEubG5hbWUgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgdHlwZW9mIHEuZG5hbWUgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgcmVtb3ZlTmFtZVdoaXRlc3BhY2UocS5mbmFtZSkubGVuZ3RoID4gMFxuXHRcdFx0JiYgcmVtb3ZlTmFtZVdoaXRlc3BhY2UocS5sbmFtZSkubGVuZ3RoID4gMFxuXHRcdFx0JiYgcmVtb3ZlTmFtZVdoaXRlc3BhY2UocS5kbmFtZSkubGVuZ3RoID4gMFxuXHRcdCk7XG5cdH0sXG5cdGxvZ2luOiBWT0lEX0lOUFVULFxuXHRsb2dvdXQ6IFZPSURfSU5QVVQsXG5cdHJlZ2lzdGVyOiAocSkgPT4ge1xuXHRcdHJldHVybiAodHJ1ZVxuXHRcdFx0JiYgdHlwZW9mIHEuZW1haWwgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgdmFsaWRhdGVFbWFpbChxLmVtYWlsKVxuXHRcdFx0JiYgdHlwZW9mIHEuZm5hbWUgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgcmVtb3ZlTmFtZVdoaXRlc3BhY2UocS5mbmFtZSkubGVuZ3RoID49IDFcblx0XHRcdCYmIHEuZm5hbWUubGVuZ3RoIDw9IDE2XG5cdFx0XHQmJiB0eXBlb2YgcS5sbmFtZSA9PSBcInN0cmluZ1wiXG5cdFx0XHQmJiByZW1vdmVOYW1lV2hpdGVzcGFjZShxLmxuYW1lKS5sZW5ndGggPj0gMVxuXHRcdFx0JiYgcS5sbmFtZS5sZW5ndGggPD0gMTZcblx0XHRcdCYmIHZhbGlkYXRlUGFzc3dvcmQocS5wd2QpXG5cdFx0KTtcblx0fSxcblx0ZmVlZGJhY2s6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdHRleHQ6IHZ2U3RyaW5nTm9uRW1wdHksXG5cdFx0fSlcblx0KSxcblx0YWNjZXB0SW52aXRhdGlvbjogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0aW52OiB2dklkLFxuXHRcdH0pXG5cdCksXG5cdG9yZ19kZWxldGU6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHR9KVxuXHQpLFxuXHRnZXRJbnZpdGF0aW9uczogVk9JRF9JTlBVVCxcblx0Z2V0T3JnczogVk9JRF9JTlBVVCxcblx0b3JnX2dldFVzZXJzOiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0fSlcblx0KSxcblx0b3JnX2dldEludml0YXRpb25zOiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0fSlcblx0KSxcblx0Z3JwX3NldEF1dGhMdmw6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHRcdGdycDogdnZJZCxcblx0XHRcdHVzZXI6IHZ2SWQsXG5cdFx0XHRsdmw6IHZ2TnVtYmVyLFxuXHRcdH0pXG5cdCksXG5cdGludml0ZTogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0ZW1haWw6IHZ2U3RyaW5nQW55LFxuXHRcdH0pXG5cdFx0JiYgdmFsaWRhdGVFbWFpbChxLmVtYWlsKVxuXHQpLFxuXG5cdC8vI3JlZ2lvbiBVc2VyXG5cdHVzZXI6IFZPSURfSU5QVVQsXG5cdHdza2V5OiBWT0lEX0lOUFVULFxuXHR1c3JfZGVsZXRlOiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRjb25maXJtOiB2dkJvb2xlYW4sXG5cdFx0fSlcblx0KSxcblx0dXNyX3NldE9wdGlvbnM6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdHByb2JsZW1zX19zb3J0X2J5X2xhc3RfcG9zdDogdnZPcHRpb25hbCh2dkJvb2xlYW4pLFxuXHRcdFx0cHJvYmxlbXNfX3NvcnRfdW5yZWFkX2ZpcnN0OiB2dk9wdGlvbmFsKHZ2Qm9vbGVhbiksXG5cdFx0fSlcblx0KSxcblx0dXNyX2dldE5vdGVwYWRzOiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0fSlcblx0KSxcblx0dXNyX3NldE5vdGVwYWQ6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHRcdF9pZDogdnZJZCxcblx0XHRcdG5hbWU6IHZ2U3RyaW5nTm9uRW1wdHksXG5cdFx0XHRjb250ZW50OiB2dlN0cmluZ0FueSxcblx0XHR9KVxuXHQpLFxuXHR1c3JfZGVsZXRlTm90ZXBhZDogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0X2lkOiB2dklkLFxuXHRcdH0pXG5cdCksXG5cdHVzcl9uZXdOb3RlcGFkOiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0fSlcblx0KSxcblx0Ly8jZW5kcmVnaW9uXG5cblx0Ly8jcmVnaW9uIEdyb3Vwc1xuXHRncnBfbmV3OiAocSkgPT4ge1xuXHRcdHJldHVybiAodHJ1ZVxuXHRcdFx0JiYgdHlwZW9mIHEub3JnID09IFwic3RyaW5nXCJcblx0XHRcdCYmIHR5cGVvZiBxLm5hbWUgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgcmVtb3ZlTmFtZVdoaXRlc3BhY2UocS5uYW1lKS5sZW5ndGggPiAwXG5cdFx0KTtcblx0fSxcblx0Z3JwX2RlbGV0ZTogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0Z3JwOiB2dklkLFxuXHRcdH0pXG5cdCksXG5cdGdycF9nZXRPZk9yZzogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdH0pXG5cdCksXG5cdGdycF9yZW5hbWU6IChxKSA9PiB7XG5cdFx0cmV0dXJuICh0cnVlXG5cdFx0XHQmJiB0eXBlb2YgcS5vcmcgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgdHlwZW9mIHEubmFtZSA9PSBcInN0cmluZ1wiXG5cdFx0XHQmJiByZW1vdmVOYW1lV2hpdGVzcGFjZShxLm5hbWUpLmxlbmd0aCA+IDBcblx0XHQpO1xuXHR9LFxuXHRncnBfc2V0T3B0aW9uczogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0Z3JwOiB2dklkLFxuXHRcdFx0b3B0aW9uczogdnZNYXAsXG5cdFx0fSlcblx0XHQmJiBbXCJudWxsXCIsIFwidW5kZWZpbmVkXCIsIFwibnVtYmVyXCJdLmluY2x1ZGVzKHR5cGVvZiBxLm9wdGlvbnMucHJvYmxlbV9zbGVlcF9kZWxheSlcblx0KSxcblx0Ly8jZW5kcmVnaW9uXG5cblx0Ly8jcmVnaW9uIENoZWNrbGlzdHNcblx0Y2hrX25ldzogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0ZG9jOiB7XG5cdFx0XHRcdHRpdGxlOiB2dlN0cmluZ05vbkVtcHR5LFxuXHRcdFx0XHRncnA6IHZ2SWQsXG5cdFx0XHRcdGR1cmF0aW9uOiB2dk51bWJlcixcblx0XHRcdFx0YWN0aXZhdGlvbjogdnZOdW1iZXIsXG5cdFx0XHR9LFxuXHRcdH0pXG5cdFx0JiYgKHEuZG9jLnNjaGVkdWxlID09PSB1bmRlZmluZWQgfHwgdHlwZW9mIHEuZG9jLnNjaGVkdWxlID09IFwib2JqZWN0XCIpXG5cdFx0JiYgcS5kb2MudGl0bGUudHJpbSgpLmxlbmd0aCA+PSAxXG5cdFx0JiYgQXJyYXkuaXNBcnJheShxLmRvYy5pdGVtcylcblx0XHQmJiBxLmRvYy5pdGVtcy5ldmVyeSh4ID0+IFR5cGVDaGVjayh4LCB7IG5hbWU6IHZ2U3RyaW5nTm9uRW1wdHksIHNlY3Rpb246IHZ2Qm9vbGVhbiwga2V5OiB2dk51bWJlciB9KSlcblx0XHQmJiBxLmRvYy5pdGVtc1xuXHRcdFx0LmZpbHRlcigoeCk6IHggaXMgQ2hlY2tsaXN0U2VjdGlvbiA9PiB4LnNlY3Rpb24gPT09IHRydWUpXG5cdFx0XHQuZXZlcnkoeCA9PiAoXG5cdFx0XHRcdFR5cGVDaGVjayh4LCB7IGRlc2NyaXB0aW9uOiB2dlN0cmluZ05vbkVtcHR5QXJyYXksIHRhc2tzOiB2dkFycmF5IH0pXG5cdFx0XHRcdCYmIHgudGFza3MuZXZlcnkoeSA9PiBUeXBlQ2hlY2soeSwgeyBuYW1lOiB2dlN0cmluZ05vbkVtcHR5LCBzZWN0aW9uOiB2dkJvb2xlYW4sIGtleTogdnZOdW1iZXIgfSkpXG5cdFx0XHQpKVxuXHRcdCYmIChcblx0XHRcdHEuZG9jLnNjaGVkdWxlID09IHVuZGVmaW5lZCB8fCAoXG5cdFx0XHRcdHRydWVcblx0XHRcdFx0JiYgdHlwZW9mIHEuZG9jLnNjaGVkdWxlID09IFwib2JqZWN0XCJcblx0XHRcdFx0JiYgKFxuXHRcdFx0XHRcdGZhbHNlXG5cdFx0XHRcdFx0fHwgKFxuXHRcdFx0XHRcdFx0dHJ1ZVxuXHRcdFx0XHRcdFx0JiYgcS5kb2Muc2NoZWR1bGUucmVwZWF0ID09IFwiZGFpbHlcIlxuXHRcdFx0XHRcdFx0JiYgcS5kb2Muc2NoZWR1bGUuZGF5c1xuXHRcdFx0XHRcdFx0JiYgdHlwZW9mIHEuZG9jLnNjaGVkdWxlLmRheXMgPT0gXCJvYmplY3RcIlxuXHRcdFx0XHRcdFx0JiYgT2JqZWN0LmtleXMocS5kb2Muc2NoZWR1bGUuZGF5cykubGVuZ3RoID09IGRheXMubGVuZ3RoXG5cdFx0XHRcdFx0XHQmJiAhZGF5cy5zb21lKGRheSA9PiB0eXBlb2YgKHEgYXMgYW55KS5kb2Muc2NoZWR1bGUuZGF5c1tkYXldICE9IFwiYm9vbGVhblwiKVxuXHRcdFx0XHRcdClcblx0XHRcdFx0XHR8fCAoXG5cdFx0XHRcdFx0XHR0cnVlXG5cdFx0XHRcdFx0XHQmJiBxLmRvYy5zY2hlZHVsZS5yZXBlYXQgPT0gXCJtb250aGx5XCJcblx0XHRcdFx0XHRcdCYmIHR5cGVvZiBxLmRvYy5zY2hlZHVsZS5mcm9tX2VuZCA9PSBcImJvb2xlYW5cIlxuXHRcdFx0XHRcdFx0JiYgdHlwZW9mIHEuZG9jLnNjaGVkdWxlLmRheSA9PSBcIm51bWJlclwiXG5cdFx0XHRcdFx0XHQmJiBxLmRvYy5zY2hlZHVsZS5kYXkgPj0gMVxuXHRcdFx0XHRcdFx0JiYgcS5kb2Muc2NoZWR1bGUuZGF5IDw9IDMxXG5cdFx0XHRcdFx0KVxuXHRcdFx0XHQpXG5cdFx0XHQpXG5cdFx0KVxuXHRcdCYmIChcblx0XHRcdHEuZG9jLm92ZXJyaWRlc19waGFudG9tID09IHVuZGVmaW5lZCB8fCAoXG5cdFx0XHRcdHRydWVcblx0XHRcdFx0JiYgdHlwZW9mIHEuZG9jLm92ZXJyaWRlc19waGFudG9tID09IFwib2JqZWN0XCJcblx0XHRcdFx0JiYgdHlwZW9mIHEuZG9jLm92ZXJyaWRlc19waGFudG9tLl9pZCA9PSBcInN0cmluZ1wiXG5cdFx0XHRcdCYmIHEuZG9jLm92ZXJyaWRlc19waGFudG9tLl9pZCBhcyBhbnkgIT0gXCJcIlxuXHRcdFx0XHQmJiB0eXBlb2YgcS5kb2Mub3ZlcnJpZGVzX3BoYW50b20uYWN0aXZhdGlvbiA9PSBcIm51bWJlclwiXG5cdFx0XHQpXG5cdFx0KVxuXHQpLFxuXHRjaGtfZ2V0OiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0XHRpZDogdnZJZCxcblx0XHR9KVxuXHQpLFxuXHRjaGtfYXBwZW5kVGFzazogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0aWQ6IHZ2SWQsXG5cdFx0XHRzZWN0aW9uOiB2dk51bWJlcixcblx0XHRcdG5hbWU6IHZ2U3RyaW5nTm9uRW1wdHksXG5cdFx0fSlcblx0KSxcblx0Y2hrX2VkaXQ6IChxKSA9PiB7XG5cdFx0cmV0dXJuICh0cnVlXG5cdFx0XHQmJiB0eXBlb2YgcS5vcmcgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgcS5kb2MudGl0bGUudHJpbSgpLmxlbmd0aCA+PSAxXG5cdFx0KTtcblx0fSxcblx0Y2hrX2dldE9mT3JnOiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0fSlcblx0KSxcblx0Y2hrX3NldEl0ZW1TdGF0dXM6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHRcdGlkOiB2dklkLFxuXHRcdFx0c2VjdGlvbjogdnZOdW1iZXIsXG5cdFx0XHRpbmRleDogdnZOdW1iZXIsXG5cdFx0XHRzdGF0dXM6IHZ2U3RyaW5nQW55LFxuXHRcdH0pXG5cdFx0JiYgcS5zZWN0aW9uID49IC0xXG5cdFx0JiYgcS5pbmRleCA+IC0xXG5cdFx0JiYgW1wibmV3XCIsIFwiZG9uZVwiLCBcImZhaWxcIl0uaW5jbHVkZXMocS5zdGF0dXMpXG5cdCksXG5cdGNoa19sb2NrOiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0XHRpZDogdnZJZCxcblx0XHR9KVxuXHQpLFxuXHRjaGtfZ2V0T2ZHcm91cDogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0Z3JwOiB2dklkLFxuXHRcdH0pXG5cdCksXG5cdC8vI2VuZHJlZ2lvblxuXG5cdC8vI3JlZ2lvbiBPcmdzXG5cdG9yZ19uZXc6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG5hbWU6IHZ2U3RyaW5nTm9uRW1wdHksXG5cdFx0XHR0aW1lem9uZTogdnZTdHJpbmdBbnksXG5cdFx0XHRvcHRpb25zOiB2dk1hcCxcblx0XHR9KVxuXHRcdCYmIHJlbW92ZU5hbWVXaGl0ZXNwYWNlKHEubmFtZSkubGVuZ3RoID4gMVxuXHQpLFxuXHRvcmdfa2ljazogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0dXNlcjogdnZJZCxcblx0XHR9KVxuXHQpLFxuXHRvcmdfc2V0QXV0aEx2bDogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0dXNlcjogdnZJZCxcblx0XHRcdGx2bDogdnZOdW1iZXIsXG5cdFx0fSlcblx0XHQmJiBbMSwgNV0uaW5jbHVkZXMocS5sdmwpXG5cdCksXG5cdG9yZ19yZW5hbWU6IChxKSA9PiB7XG5cdFx0cmV0dXJuICh0cnVlXG5cdFx0XHQmJiB0eXBlb2YgcS5vcmcgPT0gXCJzdHJpbmdcIlxuXHRcdFx0JiYgdHlwZW9mIHEubmFtZSA9PSBcInN0cmluZ1wiXG5cdFx0XHQmJiByZW1vdmVOYW1lV2hpdGVzcGFjZShxLm5hbWUpLmxlbmd0aCA+IDBcblx0XHQpO1xuXHR9LFxuXHRvcmdfc2V0T3B0aW9uczogKHEpID0+IHtcblx0XHRyZXR1cm4gKHRydWVcblx0XHRcdCYmIHR5cGVvZiBxLm9yZyA9PSBcInN0cmluZ1wiXG5cdFx0XHQmJiBxLm9wdGlvbnNcblx0XHRcdCYmIHR5cGVvZiBxLm9wdGlvbnMgPT0gXCJvYmplY3RcIlxuXHRcdFx0JiYgKHEub3B0aW9ucy51c2VfZGlzcGxheV9uYW1lcyA9PT0gdW5kZWZpbmVkIHx8IHR5cGVvZiBxLm9wdGlvbnMudXNlX2Rpc3BsYXlfbmFtZXMgPT0gXCJib29sZWFuXCIpXG5cdFx0KTtcblx0fSxcblx0b3JnX2dldExhdGVzdEN1cnNvcnM6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogXCJpZFwiLFxuXHRcdH0pXG5cdCksXG5cdC8vI2VuZHJlZ2lvblxuXG5cdC8vI3JlZ2lvbiBDaGF0XG5cdGNoYXRfbG9hZDogKHEpID0+IChcblx0XHR0cnVlXG5cdCksXG5cdGNoYXRfcG9zdDogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0Y2hhbm5lbDogdnZTdHJpbmdOb25FbXB0eSxcblx0XHRcdGlzX2RtOiB2dkJvb2xlYW4sXG5cdFx0XHR0eHQ6IHZ2U3RyaW5nTm9uRW1wdHksXG5cdFx0fSlcblx0KSxcblx0Ly8jZW5kcmVnaW9uXG5cblx0Ly8jcmVnaW9uIFByb2JsZW1zXG5cdHRrdF9uZXc6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHRcdGRvYzoge1xuXHRcdFx0XHR0aXRsZTogdnZTdHJpbmdOb25FbXB0eSxcblx0XHRcdFx0Z3JwOiB2dklkLFxuXHRcdFx0XHRpc19nbG9iYWw6IHZ2Qm9vbGVhbixcblx0XHRcdFx0aXNfcHJpb3JpdHk6IHZ2Qm9vbGVhbixcblx0XHRcdH0sXG5cdFx0fSlcblx0XHQmJiB0eXBlb2YgcS5kb2MudGV4dCA9PSBcInN0cmluZ1wiXG5cdFx0JiYgdHlwZW9mIHEuZG9jLnRpdGxlID09IFwic3RyaW5nXCJcblx0XHQmJiBxLmRvYy50aXRsZS50cmltKCkgIT0gXCJcIlxuXHQpLFxuXHR0a3RfYWRkUmVwbHk6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdGlkOiB2dklkLFxuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0dGV4dDogdnZTdHJpbmdOb25FbXB0eSxcblx0XHR9KVxuXHQpLFxuXHR0a3RfZ2V0T2ZHcm91cDogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdFx0Z3JwOiB2dklkLFxuXHRcdH0pXG5cdCksXG5cdHRrdF9nZXRPZk1ldGE6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHRcdGdycDogdnZTdHJpbmdOb25FbXB0eSxcblx0XHR9KVxuXHQpLFxuXHR0a3RfZ2V0T2ZPcmc6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHR9KVxuXHQpLFxuXHR0a3RfbW9kaWZ5OiAocSkgPT4gKFxuXHRcdFR5cGVDaGVjayhxLCB7XG5cdFx0XHRvcmc6IHZ2SWQsXG5cdFx0XHRpZDogdnZJZCxcblx0XHRcdG9wdGlvbnM6IHtcblx0XHRcdFx0aXNfZ2xvYmFsOiB2dk9wdGlvbmFsKHZ2Qm9vbGVhbiksXG5cdFx0XHRcdGlzX3ByaW9yaXR5OiB2dk9wdGlvbmFsKHZ2Qm9vbGVhbiksXG5cdFx0XHRcdGlzX3Jlc29sdmVkOiB2dk9wdGlvbmFsKHZ2Qm9vbGVhbiksXG5cdFx0XHR9LFxuXHRcdH0pXG5cdCksXG5cdHRrdF9yZW5hbWU6IChxKSA9PiAoXG5cdFx0VHlwZUNoZWNrKHEsIHtcblx0XHRcdG9yZzogdnZJZCxcblx0XHRcdGlkOiB2dklkLFxuXHRcdFx0bmFtZTogdnZTdHJpbmdOb25FbXB0eSxcblx0XHR9KVxuXHQpLFxuXHR0a3RfcmVhZDogKHEpID0+IChcblx0XHRUeXBlQ2hlY2socSwge1xuXHRcdFx0b3JnOiB2dklkLFxuXHRcdH0pXG5cdFx0JiYgKFxuXHRcdFx0VHlwZUNoZWNrKHEsIHsgZ3JwOiB2dklkIH0pIHx8IFR5cGVDaGVjayhxLCB7IHRrdDogdnZJZCwga2V5OiB2dk51bWJlciB9KVxuXHRcdClcblx0KSxcblx0Ly8jZW5kcmVnaW9uXG59O1xuIiwiaW1wb3J0IHsgZXhlYyB9IGZyb20gXCJzaGVsbGpzXCI7XG5pbXBvcnQgeyBEYXRlVGltZSB9IGZyb20gXCJsdXhvblwiO1xuaW1wb3J0IHsgUmVhZGFibGUgfSBmcm9tIFwic3RyZWFtXCI7XG5pbXBvcnQgKiBhcyBGYWtlVGltZXJzIGZyb20gXCJAc2lub25qcy9mYWtlLXRpbWVyc1wiO1xuXG5leHBvcnQgZnVuY3Rpb24gbG9nKHg6IGFueSkge1xuXHRjb25zdCBmYWludCA9IFwiXFx4MWJbMm1cIjtcblx0Y29uc3QgcmVzZXQgPSBcIlxceDFiWzBtXCI7XG5cdGxldCBsaW5lID0gbmV3IEVycm9yKCkuc3RhY2s/LnNwbGl0KFwiXFxuXCIpWzJdLnNwbGl0KFwiYXQgXCIpWzFdITtcblx0aWYgKHR5cGVvZiB4ID09IFwic3RyaW5nXCIpIHtcblx0XHRjb25zdCBwYXJ0cyA9IGxpbmUuc3BsaXQoXCIudHNcIik7XG5cdFx0bGluZSA9IHBhcnRzWzBdLnNsaWNlKHBhcnRzWzBdLmxhc3RJbmRleE9mKFwiL1wiKSsxKSArIFwiLnRzXCI7XG5cdFx0bGluZSArPSBwYXJ0c1sxXS5zbGljZSgwLCBwYXJ0c1sxXS5sYXN0SW5kZXhPZihcIjpcIikpO1xuXHRcdGNvbnNvbGUubG9nKGZhaW50ICsgbGluZSArIFwiIFwiICsgcmVzZXQgKyB4KTtcblx0fVxuXHRlbHNlXG5cdFx0Y29uc29sZS5sb2coW2xpbmUsIHhdKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNsZWVwKG1zOiBudW1iZXIpOiBQcm9taXNlPHRydWU+IHsgcmV0dXJuIG5ldyBQcm9taXNlKHIgPT4gc2V0VGltZW91dCgoKSA9PiB7IHIodHJ1ZSk7IH0sIG1zKSk7IH1cbmV4cG9ydCBmdW5jdGlvbiByYW5kb20obWluOiBudW1iZXIsIG1heDogbnVtYmVyKSB7IHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluICsgMSkpICsgbWluOyB9XG5leHBvcnQgZnVuY3Rpb24gdGltZXN0YW1wKCkgeyByZXR1cm4gbmV3IERhdGUoKS5nZXRUaW1lKCk7IH1cblxuZXhwb3J0IGZ1bmN0aW9uIHJlbW92ZUxhc3ROZXdsaW5lKHN0cjogc3RyaW5nKTogc3RyaW5nIHtcblx0aWYgKHN0ci5sYXN0SW5kZXhPZihcIlxcblwiKSA+IDApIHtcblx0XHRyZXR1cm4gc3RyLnN1YnN0cmluZygwLCBzdHIubGFzdEluZGV4T2YoXCJcXG5cIikpO1xuXHR9XG5cdGVsc2Uge1xuXHRcdHJldHVybiBzdHI7XG5cdH1cbn1cblxuZXhwb3J0IGludGVyZmFjZSBBc3luY05ldyB7XG5cdHJlYWR5OiBQcm9taXNlPHRoaXM+LFxufVxuXG5leHBvcnQgZnVuY3Rpb24gc2goY21kOiBzdHJpbmcsIHdvbnRfZmFpbD86IGJvb2xlYW4pIHtcblx0Y29uc3QgcmVzdWx0ID0gZXhlYyhjbWQpO1xuXHRpZiAoIXdvbnRfZmFpbCAmJiByZXN1bHQuY29kZSAhPSAwKSB7XG5cdFx0Y29uc29sZS5sb2coXCIrKysgVHJpZWQgdG8gcnVuIHNoIGNvbW1hbmQsIGJ1dCBpdCBmYWlsZWQuXCIpO1xuXHRcdGNvbnNvbGUubG9nKFwiQ29tbWFuZCBsaW5lOiBcIiArIGNtZCk7XG5cdFx0Y29uc29sZS5sb2coXCJFeGl0IGNvZGU6IFwiICsgcmVzdWx0LmNvZGUpO1xuXHRcdGNvbnNvbGUubG9nKFwiKysrIFN0ZG91dDpcIik7XG5cdFx0Y29uc29sZS5sb2cocmVzdWx0LnN0ZG91dCk7XG5cdFx0Y29uc29sZS5sb2coXCIrKysgU3RkZXJyOlwiKTtcblx0XHRjb25zb2xlLmxvZyhyZXN1bHQuc3RkZXJyKTtcblx0XHRjb25zb2xlLmxvZyhcIisrKyBTdGFja3RyYWNlOlwiKTtcblx0XHRjb25zb2xlLnRyYWNlKCk7XG5cdFx0dGhyb3cgbmV3IEVycm9yKFwiRXJyb3I6IHNoIGZhaWxlZCFcIik7XG5cdH1cblx0cmV0dXJuIHJlc3VsdDtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGFzaChjbWQ6IHN0cmluZywgd29udF9mYWlsPzogYm9vbGVhbikge1xuXHRjb25zdCByZXN1bHQgPSBhd2FpdCBleGVjKGNtZCwgeyBhc3luYzogdHJ1ZSB9KTtcblx0aWYgKCF3b250X2ZhaWwgJiYgcmVzdWx0LmV4aXRDb2RlICE9IDApIHtcblx0XHRjb25zb2xlLmxvZyhcIisrKyBUcmllZCB0byBydW4gc2ggY29tbWFuZCwgYnV0IGl0IGZhaWxlZC5cIik7XG5cdFx0Y29uc29sZS5sb2coXCJDb21tYW5kIGxpbmU6IFwiICsgY21kKTtcblx0XHRjb25zb2xlLmxvZyhcIkV4aXQgY29kZTogXCIgKyByZXN1bHQuZXhpdENvZGUpO1xuXHRcdGNvbnNvbGUubG9nKFwiKysrIFN0ZG91dDpcIik7XG5cdFx0Y29uc29sZS5sb2coc3RyZWFtVG9TdHJpbmcocmVzdWx0LnN0ZG91dCEpKTtcblx0XHRjb25zb2xlLmxvZyhcIisrKyBTdGRlcnI6XCIpO1xuXHRcdGNvbnNvbGUubG9nKHN0cmVhbVRvU3RyaW5nKHJlc3VsdC5zdGRlcnIhKSk7XG5cdFx0Y29uc29sZS5sb2coXCIrKysgU3RhY2t0cmFjZTpcIik7XG5cdFx0Y29uc29sZS50cmFjZSgpO1xuXHRcdHRocm93IG5ldyBFcnJvcihcIkVycm9yOiBhc3luYyBzaCBmYWlsZWQhXCIpO1xuXHR9XG5cdHJldHVybiByZXN1bHQ7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzdHJlYW1Ub1N0cmluZyhzdHJlYW06IFJlYWRhYmxlKSB7XG5cdGNvbnN0IGNodW5rcyA9IFtdIGFzIGFueVtdO1xuXHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdHN0cmVhbS5vbihcImRhdGFcIiwgY2h1bmsgPT4gY2h1bmtzLnB1c2goY2h1bmspKTtcblx0XHRzdHJlYW0ub24oXCJlcnJvclwiLCByZWplY3QpO1xuXHRcdHN0cmVhbS5vbihcImVuZFwiLCAoKSA9PiByZXNvbHZlKEJ1ZmZlci5jb25jYXQoY2h1bmtzKS50b1N0cmluZyhcInV0ZjhcIikpKTtcblx0fSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBkYXRlKHRpbWU6IG51bWJlcikge1xuXHRpZiAodHlwZW9mIHRpbWUgIT0gXCJudW1iZXJcIikgcmV0dXJuIFwiaW52YWxpZCB0aW1lc3RhbXBcIjtcblx0Y29uc3QgdCA9IERhdGVUaW1lLmZyb21NaWxsaXModGltZSk7XG5cdGZ1bmN0aW9uIHoobjogbnVtYmVyKSB7IHJldHVybiAobiArIFwiXCIpLnBhZFN0YXJ0KDIsIFwiMFwiKTsgfVxuXHRyZXR1cm4gYCR7dC55ZWFyfS0ke3oodC5tb250aCl9LSR7eih0LmRheSl9ICR7eih0LmhvdXIpfToke3oodC5taW51dGUpfWA7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiByZW1vdmVGcm9tQXJyYXk8VD4oYXJyYXk6IFRbXSwgaXRlbTogVCkge1xuXHRjb25zdCBpbmRleCA9IGFycmF5LmluZGV4T2YoaXRlbSk7XG5cdGlmIChpbmRleCA+IC0xKSB7IGFycmF5LnNwbGljZShpbmRleCwgMSk7IH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGFzUGFyYWdyYXBoQXJyYXkodGV4dDogc3RyaW5nKSB7XG5cdGNvbnN0IHIgPSB0ZXh0LnRyaW0oKS5zcGxpdChcIlxcblwiKS5tYXAoeCA9PiB4LnRyaW0oKSkuZmlsdGVyKHggPT4geCAhPSBcIlwiKTtcblx0aWYgKHIubGVuZ3RoKSByZXR1cm4gcjtcblx0ZWxzZSByZXR1cm4gW1wiXCJdO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlTmFtZVdoaXRlc3BhY2UobmFtZTogc3RyaW5nKSB7XG5cdHJldHVybiBuYW1lLnRyaW0oKS5yZXBsYWNlQWxsKFwiXFxuXCIsIFwiXCIpLnJlcGxhY2VBbGwoXCJcXHRcIiwgXCJcIik7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1JlY29yZCh4OiBhbnkpOiB4IGlzIFJlY29yZDxzdHJpbmcsIGFueT4ge1xuXHRyZXR1cm4gdHlwZW9mIHggPT0gXCJvYmplY3RcIiAmJiB4ICE9IG51bGwgJiYgIUFycmF5LmlzQXJyYXkoeCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjb3B5T3B0aW9uYWxGaWVsZHM8XG5cdEEsIEtleXMgZXh0ZW5kcyAoa2V5b2YgQSlbXSwgQiBleHRlbmRzIFBpY2s8QSwgS2V5c1tudW1iZXJdPlxuPihmcm9tOiBBLCB0bzogQiwga2V5czogS2V5cykge1xuXHRmb3IgKGNvbnN0IGtleSBvZiBrZXlzKSBpZiAoZnJvbVtrZXldICE9IHVuZGVmaW5lZCkgKHRvW2tleV0gYXMgYW55KSA9IGZyb21ba2V5XTtcbn1cblxuZXhwb3J0IGNvbnN0IHRlc3Rpbmdfc2V0U2VydmVyVGltZTogKCh4OiBudW1iZXIgfCBcInJlc2V0XCIpID0+IHZvaWQpICYgeyBjbG9jaz86IEZha2VUaW1lcnMuQ2xvY2sgfSA9ICh4KSA9PiB7XG5cdHRlc3Rpbmdfc2V0U2VydmVyVGltZS5jbG9jaz8udW5pbnN0YWxsKCk7XG5cdGlmICh4ICE9IFwicmVzZXRcIikgdGVzdGluZ19zZXRTZXJ2ZXJUaW1lLmNsb2NrID0gRmFrZVRpbWVycy5pbnN0YWxsKHsgbm93OiB4LCBzaG91bGRBZHZhbmNlVGltZTogdHJ1ZSB9KTtcbn07XG4iLCJpbXBvcnQgeyBJZCwgSWRlbnRpZmllZCB9IGZyb20gXCJAL2RhdGEvdVwiO1xuaW1wb3J0IHsgVXNlckFjdGlvbiB9IGZyb20gXCJAL2RhdGEvVXNlclwiO1xuXG4vKipcbiAqIGBNaWphQ2hhdHNgIGRiLCBpbiB0YWJsZXMgYnkgT3JnIGlkLlxuICovXG5leHBvcnQgdHlwZSBDaGF0TXNnID0gSWRlbnRpZmllZCAmIHtcblx0Y2hhbm5lbDogSWQgfCBcIm9yZ1wiLFxuXHR0eHQ6IHN0cmluZyxcblx0cG9zdGVkOiBVc2VyQWN0aW9uLFxufTtcblxuZXhwb3J0IGZ1bmN0aW9uIGFzRE1DaGFubmVsKHVzZXJzOiBbSWQsIElkXSk6IElkIHtcblx0cmV0dXJuIHVzZXJzLnNvcnQoKS5qb2luKFwiK1wiKSBhcyBJZDtcbn1cbiIsImltcG9ydCB7IERhdGVUaW1lIH0gZnJvbSBcImx1eG9uXCI7XG5cbmltcG9ydCB7IFVzZXJBY3Rpb24gfSBmcm9tIFwiQC9kYXRhL1VzZXJcIjtcbmltcG9ydCB7IElkLCBJZGVudGlmaWVkIH0gZnJvbSBcIi4vdVwiO1xuXG5leHBvcnQgY29uc3QgZGF5cyA9IFtcIm1vblwiLCBcInR1ZVwiLCBcIndlZFwiLCBcInRodVwiLCBcImZyaVwiLCBcInNhdFwiLCBcInN1blwiXSBhcyBjb25zdDtcbmV4cG9ydCBjb25zdCBkYXRlcyA9IFsxLCAyLCAzLCA0LCA1LCA2LCA3LCA4LCA5LCAxMCwgMTEsIDEyLCAxMywgMTQsIDE1LCAxNiwgMTcsIDE4LCAxOSwgMjAsIDIxLCAyMiwgMjMsIDI0LCAyNSwgMjYsIDI3LCAyOCwgMjksIDMwLCAzMV0gYXMgY29uc3Q7XG5cbi8qKlxuICogYE1pamFDaGVja2xpc3RzYCBkYiwgaW4gdGFibGVzIGJ5IE9yZyBpZC5cbiAqIEF1dG9yZXNldCBtZWFucyBhIENoZWNrbGlzdCBpcyBjbG9uZWQganVzdCBiZWZvcmUgaXQgZ29lcyBmcm9tIHBsYW5uZWQgdG8gYWN0aXZlLCBcbiAqIHdpdGggdGhlIGFjdGl2YXRpb24gaXRlcmF0ZWQgYnkgdGhlIHNlbGVjdGVkIGF1dG9yZXNldCBwZXJpb2QuXG4gKi9cbmV4cG9ydCB0eXBlIENoZWNrbGlzdCA9IElkZW50aWZpZWQgJiB7XG5cdGdycDogSWQsXG5cblx0dGl0bGU6IHN0cmluZyxcblx0aXRlbXM6IENoZWNrbGlzdEl0ZW1bXSxcblx0aXRlbXNfbGF0ZXN0OiBudW1iZXIsIC8vIGtleSBvZiBsYXN0IGFkZGVkIGl0ZW0sIGtleXMgY2FuJ3QgYmUgcmV1c2VkIHNvIGluY3JlbWVudCB0aGlzIHRvIGdlbiBrZXlzXG5cblx0c3RhdHVzOiBcInBsYW5uZWRcIiB8IFwiYWN0aXZlXCIgfCBcImxhdGVcIiB8IFwibG9ja2VkXCIsXG5cdGFjdGl2YXRpb246IG51bWJlciwgLy8gdGltZXN0YW1wOyBpZiBwbGFubmVkLCB3aGVuIHRvIGFjdGl2YXRlOyBlbHNlIHdoZW4gaXQgd2FzIGFjdGl2YXRlZFxuXHRkdXJhdGlvbjogbnVtYmVyLCAvLyBob3cgbG9uZyB0byB3YWl0IHRpbCBzdGF0dXMgZ29lcyBcImFjdGl2ZVwiID0+IFwibG9ja2VkXCI7IDAgPSBpbmZpbml0ZVxuXHRzY2hlZHVsZT86IFJlcGVhdFNjaGVkdWxlLCAvLyBpcyB0aGlzIGEgdGVtcGxhdGU/XG5cblx0YWN0aW9uczogQ2hlY2tsaXN0QWN0aW9uW10sXG5cblx0aXNfcGhhbnRvbT86IGJvb2xlYW4sIC8vIHRoaXMgaXMgbm90IHJlYWwsIGl0IGlzIHByb2plY3RlZCB0byBiZWNvbWUgcmVhbCBhdCBhIGZ1dHVyZSB0aW1lXG5cdG92ZXJyaWRlc19waGFudG9tPzogeyAvLyB0aGlzIHJlcGxhY2VzIGEgcGhhbnRvbT9cblx0XHRfaWQ6IElkLCAvLyBwaGFudG9tX3Jvb3QuaWRcblx0XHRhY3RpdmF0aW9uOiBudW1iZXIsIC8vIHBoYW50b20uYWN0aXZhdGlvblxuXHR9LFxufTtcblxuZXhwb3J0IHR5cGUgQ2hlY2tsaXN0Rm9ybSA9IE9taXQ8Q2hlY2tsaXN0LCBrZXlvZiBJZGVudGlmaWVkIHwgXCJzdGF0dXNcIiB8IFwiaXNfcGhhbnRvbVwiIHwgXCJhY3Rpb25zXCI+O1xuXG5leHBvcnQgdHlwZSBDaGVja2xpc3RUYXNrID0ge1xuXHRrZXk6IG51bWJlcixcblx0c2VjdGlvbjogZmFsc2UsIC8vIGlmIGluIG1haW4gaXRlbSBsaXN0LCB0YXNrJ3Mgc2VjdGlvbiBpbmRleCA9PSAtMSwgZWxzZSA+PSAwXG5cblx0bmFtZTogc3RyaW5nLFxuXHR2YWw6IFwibmV3XCIgfCBcImRvbmVcIiB8IFwiZmFpbFwiLFxufTtcblxuZXhwb3J0IHR5cGUgQ2hlY2tsaXN0U2VjdGlvbiA9IHtcblx0a2V5OiBudW1iZXIsXG5cdHNlY3Rpb246IHRydWUsXG5cblx0bmFtZTogc3RyaW5nLFxuXHR2YWw6IFwibmV3XCIgfCBcImRvbmVcIiB8IFwiZmFpbFwiLFxuXG5cdGRlc2NyaXB0aW9uOiBzdHJpbmdbXSwgLy8gcGFyYWdyYXBocywgb3IgW11cblx0dGFza3M6IENoZWNrbGlzdFRhc2tbXSxcbn07XG5cbmV4cG9ydCB0eXBlIENoZWNrbGlzdEl0ZW0gPSBDaGVja2xpc3RUYXNrIHwgQ2hlY2tsaXN0U2VjdGlvbjtcblxuZXhwb3J0IHR5cGUgQ2hlY2tsaXN0QWN0aW9uID0ge1xuXHRwb3N0ZWQ6IFVzZXJBY3Rpb24sIC8vIHVzZXIgaWQgbWF5IGJlIFwiYXV0b1wiIGluc3RlYWRcblx0dHlwZTogKFxuXHRcdHwgXCJsb2NrXCJcblx0XHR8IFwidW5sb2NrXCJcblx0XHR8IFwicmVuYW1lXCJcblx0XHR8IFwiYXBwZW5kXCJcblx0XHR8IFwiZXhwaXJlXCJcblx0XHR8IFwibW9kVGFza3NcIlxuXHRcdHwgXCJtb2RTY2hlZHVsZVwiXG5cdCksXG5cblx0cmVuYW1lX2Zyb20/OiBzdHJpbmcsXG5cdHJlbmFtZV90bz86IHN0cmluZyxcbn07XG5cbmV4cG9ydCB0eXBlIFdlZWtEYXlNYXAgPSBSZWNvcmQ8dHlwZW9mIGRheXNbbnVtYmVyXSwgYm9vbGVhbj47XG5cbnR5cGUgRGFpbHlTY2hlZHVsZSA9IHtcblx0cmVwZWF0OiBcImRhaWx5XCIsXG5cdGRheXM6IFdlZWtEYXlNYXAsIC8vIG9uIHdoaWNoIGRheXMgc2hvdWxkIGl0IHJlcGVhdD9cbn07XG5cbnR5cGUgTW9udGhseVNjaGVkdWxlID0ge1xuXHRyZXBlYXQ6IFwibW9udGhseVwiLFxuXHRkYXk6IHR5cGVvZiBkYXRlc1tudW1iZXJdLFxuXHRmcm9tX2VuZDogYm9vbGVhbiwgLy8gY291bnQgZGF5cyBmcm9tIGVuZCBvZiBtb250aD9cbn07XG5cbmV4cG9ydCB0eXBlIFJlcGVhdFNjaGVkdWxlID0gRGFpbHlTY2hlZHVsZSB8IE1vbnRobHlTY2hlZHVsZTtcblxuZXhwb3J0IGZ1bmN0aW9uIGdldENoZWNrbGlzdENvbXBsZXRpb24oeDogQ2hlY2tsaXN0KSB7XG5cdGxldCB0b3RhbCA9IDA7XG5cdGxldCBjb21wbGV0ZWQgPSAwO1xuXG5cdGZvciAoY29uc3QgaXRlbSBvZiB4Lml0ZW1zKSB7XG5cdFx0aWYgKGl0ZW0uc2VjdGlvbikge1xuXHRcdFx0dG90YWwgKz0gaXRlbS50YXNrcy5sZW5ndGg7XG5cdFx0XHRmb3IgKGNvbnN0IHRhc2sgb2YgaXRlbS50YXNrcykgaWYgKHRhc2sudmFsICE9IFwibmV3XCIpIGNvbXBsZXRlZCArPSAxO1xuXHRcdH1cblx0XHRlbHNlIHtcblx0XHRcdHRvdGFsKys7XG5cdFx0XHRpZiAoaXRlbS52YWwgIT0gXCJuZXdcIikgY29tcGxldGVkICs9MTtcblx0XHR9XG5cdH1cblxuXHRjb25zdCBmcmFjdGlvbiA9IE1hdGgubWluKGNvbXBsZXRlZCAvICh0b3RhbCB8fCAxKSwgMSk7XG5cdFxuXHRyZXR1cm4geyBjb21wbGV0ZWQsIHRvdGFsLCBmcmFjdGlvbiwgcGVyY2VudDogTWF0aC5mbG9vcihmcmFjdGlvbiAqIDEwMCkgfTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldENoZWNrbGlzdE5leHRSZXBlYXQoeDogQ2hlY2tsaXN0LCB0aW1lem9uZTogc3RyaW5nKSB7XG5cdGlmICh4LnNjaGVkdWxlKSB7XG5cdFx0aWYgKHguc2NoZWR1bGUhLnJlcGVhdCA9PSBcImRhaWx5XCIpIHtcblx0XHRcdGNvbnN0IGRheV9hcnJheSA9IFsuLi5kYXlzLCAuLi5kYXlzXTtcblx0XHRcdGNvbnN0IGRheSA9IERhdGVUaW1lLmZyb21NaWxsaXMoeC5hY3RpdmF0aW9uLCB7IHpvbmU6IHRpbWV6b25lIH0pO1xuXHRcdFx0Y29uc3Qgd2Vla19zdGFydF9wb3MgPSBkYXkud2Vla2RheTtcblx0XHRcdGZvciAobGV0IGkgPSB3ZWVrX3N0YXJ0X3BvczsgaSA8PSB3ZWVrX3N0YXJ0X3BvcyArIDY7IGkrKykge1xuXHRcdFx0XHRpZiAoeC5zY2hlZHVsZS5kYXlzW2RheV9hcnJheVtpXV0pIHJldHVybiBkYXkucGx1cyh7IGRheXM6IGkgLSB3ZWVrX3N0YXJ0X3BvcyArIDEgfSkudG9NaWxsaXMoKTtcblx0XHRcdH1cblx0XHRcdHJldHVybiAwO1xuXHRcdH1cblx0XHRlbHNlIHtcblx0XHRcdGNvbnN0IGdldFRhcmdldERheSA9IChpbl9tb250aDogRGF0ZVRpbWUpID0+IHtcblx0XHRcdFx0Y29uc3QgeyBmcm9tX2VuZCwgZGF5IH0gPSB4LnNjaGVkdWxlIGFzIE1vbnRobHlTY2hlZHVsZTtcblx0XHRcdFx0cmV0dXJuIChcblx0XHRcdFx0XHRmcm9tX2VuZFxuXHRcdFx0XHRcdFx0PyBNYXRoLm1heCgxLCBpbl9tb250aC5kYXlzSW5Nb250aCAtIGRheSlcblx0XHRcdFx0XHRcdDogTWF0aC5taW4oaW5fbW9udGguZGF5c0luTW9udGgsIGRheSlcblx0XHRcdFx0KTtcblx0XHRcdH07XG5cblx0XHRcdGNvbnN0IG9sZF9tb250aCA9IERhdGVUaW1lLmZyb21NaWxsaXMoeC5hY3RpdmF0aW9uLCB7IHpvbmU6IHRpbWV6b25lIH0pO1xuXHRcdFx0Y29uc3Qgb2xkX21vbnRoX3RhcmdldCA9IG9sZF9tb250aC5zZXQoeyBkYXk6IGdldFRhcmdldERheShvbGRfbW9udGgpIH0pO1xuXHRcdFx0aWYgKG9sZF9tb250aF90YXJnZXQudG9NaWxsaXMoKSA+IG9sZF9tb250aC50b01pbGxpcygpKSB7XG5cdFx0XHRcdHJldHVybiBvbGRfbW9udGhfdGFyZ2V0LnRvTWlsbGlzKCk7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0Y29uc3QgbmV3X21vbnRoID0gb2xkX21vbnRoLnBsdXMoeyBtb250aHM6IDEgfSk7XG5cdFx0XHRcdHJldHVybiBuZXdfbW9udGguc2V0KHsgZGF5OiBnZXRUYXJnZXREYXkobmV3X21vbnRoKSB9KS50b01pbGxpcygpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXHRlbHNlIHJldHVybiAwO1xufVxuXG4iLCJpbXBvcnQgeyBJZCwgSWRlbnRpZmllZCwgT3B0aW9ucyB9IGZyb20gXCIuL3VcIjtcblxuZXhwb3J0IHR5cGUgU2Vzc2lvbiA9IHtcblx0ZGVzdHJveTogKCkgPT4gdm9pZCB8IFByb21pc2U8dm9pZD4sXG5cdHJlcGxhY2U6ICgpID0+IHZvaWQgfCBQcm9taXNlPHZvaWQ+LFxuXHRzYXZlOiAoKSA9PiB2b2lkIHwgUHJvbWlzZTx2b2lkPixcblx0dXNlcjogSWQsXG5cdGFkbWluOiBib29sZWFuLFxuXHR3c2tleTogc3RyaW5nLFxufTtcblxuZXhwb3J0IHR5cGUgVXNlciA9IElkZW50aWZpZWQgJiB7XG5cdG1mZDogbnVtYmVyLCAvLyBtYXJrZWQgZm9yIGRlbGV0aW9uPyAwID0gZmFsc2UsIGVsc2UgdGltZXN0YW1wIHdoZW4gaXQgc2hhbGwgaGFwcGVuXG5cdGFjYzogVXNlckFjY291bnQsXG5cdHBmbDogVXNlclByb2ZpbGUsXG5cdG9wdGlvbnM6IFVzZXJPcHRpb25zLFxufTtcblxuZXhwb3J0IHR5cGUgVXNlckFjY291bnQgPSB7XG5cdGVtYWlsOiBzdHJpbmcsXG5cdHB3ZDogc3RyaW5nLFxuXHRjcmVhdGVkX2F0OiBudW1iZXIsXG59O1xuXG5leHBvcnQgdHlwZSBVc2VyUHJvZmlsZSA9IElkZW50aWZpZWQgJiB7XG5cdC8vIHByb2ZpbGUgaWQgPSB1c2VyIGlkLFxuXG5cdGZuYW1lOiBzdHJpbmcsIC8vIGZpcnN0IG5hbWVcblx0bG5hbWU6IHN0cmluZywgLy8gbGFzdCBuYW1lXG5cdGRuYW1lOiBzdHJpbmcsIC8vIGRpc3BsYXkgbmFtZSwgbWF4IGxlbmd0aCA9IDE2LCBmb3JtIG9wdGlvbmFsIGluIHRoYXQgaXQgbWF5IGJlIFwiXCIgaW4gd2hpY2ggY2FzZSByZWFsIG5hbWUgaXMgdXNlZCBpbnN0ZWFkIGJ1dCB0ZWNobmljYWxseSB0aGUgb25lIGFjdHVhbGx5IHVzZWQgaW4gVUlcblxuXHQvLyBvcHRpb25hbCBwcm9wZXJ0aWVzOlxuXHQvLyB1c2VycyBhcmUgbm90IHJlcXVpcmVkIHRvIHByb3ZpZGUgdGhpcyBpbmZvXG5cdC8vIGhvd2V2ZXIsIGVtcHR5IHN0cmluZ3Mgc2hvdWxkIG5vdCBiZSBzYXZlZFxuXHRwaG9uZT86IHN0cmluZyxcblx0YmlydGg/OiBzdHJpbmcsXG59O1xuXG5leHBvcnQgdHlwZSBVc2VyT3B0aW9ucyA9IE9wdGlvbnM8e1xuXHRwcm9ibGVtc19fc29ydF9ieV9sYXN0X3Bvc3Q6IGJvb2xlYW4sXG5cdHByb2JsZW1zX19zb3J0X3VucmVhZF9maXJzdDogYm9vbGVhbixcblx0bGFzdF9vcmc6IElkLCAvLyB3aGF0IG9yZyB3YXMgdGhlIHVzZXIgbGFzdCBhY3RpdmUgaW4sIGFzIG1lYXN1cmVkIGJ5IHVzZSBvZiBvcmdfZ2V0TGF0ZXN0Q3Vyc29yc1xufT47XG5cbmV4cG9ydCB0eXBlIFVzZXJBY3Rpb24gPSB7XG5cdC8qKiB0aW1lc3RhbXAgKi9cblx0YXQ6IG51bWJlcixcblx0LyoqIF9pZCBvZiBVc2VyICovXG5cdGJ5OiBJZCxcbn07XG5cbmV4cG9ydCBmdW5jdGlvbiB2YWxpZGF0ZVBhc3N3b3JkKHg6IHVua25vd24pIHtcblx0cmV0dXJuIChcblx0XHR0eXBlb2YgeCA9PSBcInN0cmluZ1wiICYmXG5cdFx0eC5sZW5ndGggPj0gOFxuXHQpO1xufVxuIiwiLyoqIEtleSBnZW5lcmF0ZWQgZm9yIE1vbmdvLCBvciBwbGFjZWhvbGRlciBpZiB0aGF0IGhhc24ndCBoYXBwZW5lZCB5ZXQuICovXG5leHBvcnQgdHlwZSBJZCA9IFwiYSBzdHJpbmcgZm9yIF9pZCBmaWVsZHMgb2YgZGF0YWJhc2UgZG9jdW1lbnRzXCIgfCBcIm5ld1wiIHwgXCIuLi5cIjtcblxuLyoqIFBsYWNlaG9sZGVyIElkIGZvciBEb2N1bWVudHMgdGhhdCBoYXZlIG5vdCBnb3R0ZW4gYSBnZW5lcmF0ZWQgSWQgeWV0LiAqL1xuZXhwb3J0IGNvbnN0IE5ld0lkID0gXCJuZXdcIiBhcyBJZDtcblxuLyoqIFBsYWNlaG9sZGVyIElkIHRvIHVzZSBpbnN0ZWFkIG9mIHVzZXIgSWQgd2hlbiBhbiBhY3Rpb24gd2FzIHBlcmZvcm1lZCBhdXRvbWF0aWNhbGx5LiAqL1xuZXhwb3J0IGNvbnN0IFN5c3RlbUlkID0gXCJzeXNcIiBhcyBJZDtcblxuLyoqIEJhc2UgdHlwZSBmb3IgYW55IGRvY3VtZW50IHN0b3JlZCBpbiBhIGRhdGFiYXNlIHRhYmxlLiAqL1xuZXhwb3J0IHR5cGUgSWRlbnRpZmllZCA9IHsgX2lkOiBJZCB9O1xuXG5leHBvcnQgZnVuY3Rpb24gaXNJZCh4OiB1bmtub3duKTogeCBpcyBJZCB7IHJldHVybiB0eXBlb2YgeCA9PSBcInN0cmluZ1wiICYmIHguc3RhcnRzV2l0aChcInhcIik7IH1cblxuZXhwb3J0IHR5cGUgSWRNYXA8RG9jdW1lbnQ+ID0geyBbaW5kZXggaW4gSWRdPzogRG9jdW1lbnQgfTtcbmV4cG9ydCB0eXBlIElkQXV0b01hcDxEb2N1bWVudD4gPSB7IFtpbmRleCBpbiBJZF06IERvY3VtZW50IH07XG5cbmV4cG9ydCBmdW5jdGlvbiBuZXdJZEF1dG9NYXA8RG9jdW1lbnQ+KGRvY19jb25zdHJ1Y3RvcjogKGlkOiBJZCkgPT4gRG9jdW1lbnQpIHtcblx0cmV0dXJuIG5ldyBQcm94eSh7fSBhcyB7IFtpbmRleCBpbiBJZF06IERvY3VtZW50IH0sIHtcblx0XHRnZXQ6ICh0YXJnZXQsIHByb3BlcnR5KSA9PiB7XG5cdFx0XHRpZiAoaXNJZChwcm9wZXJ0eSkgJiYgdGFyZ2V0W3Byb3BlcnR5XSA9PT0gdW5kZWZpbmVkKVxuXHRcdFx0XHR0YXJnZXRbcHJvcGVydHldID0gZG9jX2NvbnN0cnVjdG9yKHByb3BlcnR5KTtcblx0XHRcdHJldHVybiB0YXJnZXRbcHJvcGVydHkgYXMgSWRdO1xuXHRcdH0sXG5cdH0pO1xufVxuXG5leHBvcnQgY2xhc3MgTGlzdGVkSWRNYXA8RG9jdW1lbnQgZXh0ZW5kcyBJZGVudGlmaWVkPiB7XG5cdG1hcDogSWRNYXA8RG9jdW1lbnQ+O1xuXHRsaXN0OiBEb2N1bWVudFtdID0gW107XG5cblx0Y29uc3RydWN0b3IoaXRlbXM/OiBEb2N1bWVudFtdKTtcblx0Y29uc3RydWN0b3IoanNvbj86IHN0cmluZyk7XG5cdGNvbnN0cnVjdG9yKGFyZz86IHN0cmluZyB8IERvY3VtZW50W10pIHtcblx0XHRpZiAodHlwZW9mIGFyZyA9PSBcInN0cmluZ1wiKSB0aGlzLnJlc2V0KEpTT04ucGFyc2UoYXJnKSBhcyBEb2N1bWVudFtdKTtcblx0XHRlbHNlIHRoaXMucmVzZXQoYXJnKTtcblx0fVxuXG5cdHJlc2V0KGl0ZW1zPzogRG9jdW1lbnRbXSk6IHZvaWQge1xuXHRcdGl0ZW1zID8/PSBbXTtcblx0XHRjb25zdCBtYXAgPSB7fSBhcyBJZE1hcDxEb2N1bWVudD47XG5cdFx0aXRlbXMuZm9yRWFjaChpdGVtID0+IG1hcFtpdGVtLl9pZF0gPSBpdGVtKTtcblx0XHR0aGlzLm1hcCA9IG1hcDtcblx0XHR0aGlzLmxpc3Quc3BsaWNlKDAsIHRoaXMubGlzdC5sZW5ndGgsIC4uLml0ZW1zKTtcblx0fVxuXG5cdGdldChpZDogSWQpOiBEb2N1bWVudCB8IHVuZGVmaW5lZCB7XG5cdFx0cmV0dXJuIHRoaXMubWFwW2lkXTtcblx0fVxuXG5cdGFkZCh4OiBEb2N1bWVudCk6IHZvaWQge1xuXHRcdGlmICh0aGlzLm1hcFt4Ll9pZF0pXG5cdFx0XHR0aGlzLmxpc3Quc3BsaWNlKHRoaXMubGlzdC5maW5kSW5kZXgoeSA9PiB5Ll9pZCA9PSB4Ll9pZCksIDEsIHgpO1xuXHRcdGVsc2UgdGhpcy5saXN0LnB1c2goeCk7XG5cdFx0dGhpcy5tYXBbeC5faWRdID0geDtcblx0fVxuXG5cdHJlbW92ZSh4OiBJZCk6IHZvaWQge1xuXHRcdGlmICh0aGlzLm1hcFt4XSkge1xuXHRcdFx0dGhpcy5saXN0LnNwbGljZSh0aGlzLmxpc3QuZmluZEluZGV4KHkgPT4geS5faWQgPT0geCksIDEpO1xuXHRcdFx0ZGVsZXRlIHRoaXMubWFwW3hdO1xuXHRcdH1cblx0fVxuXG5cdHRvSlNPTigpIHtcblx0XHRyZXR1cm4gdGhpcy5saXN0O1xuXHR9XG59XG5cbi8qKlxuICogTWFrZSBhbGwgcHJvcGVydGllcyBpbiBUIG9wdGlvbmFsIGFuZCBudWxsYWJsZVxuICovXG5leHBvcnQgdHlwZSBPcHRpb25zPFQ+ID0ge1xuXHRbUCBpbiBrZXlvZiBUXT86IFRbUF0gfCBudWxsO1xufTtcblxuLyoqIFVzZWQgdG8gc2VsZWN0IHR5cGVzY3JpcHQgdHlwaW5nIGluIGlmLWJsb2Nrcy4gT3B0aW1pemVyIHRyaW1zIGl0IG91dCBvZiBwcm9kdWN0aW9uIGJ1aWxkcy4gKi9cbmV4cG9ydCBmdW5jdGlvbiBOYXJyb3c8VD4oeDogYW55KTogeCBpcyBUIHsgcmV0dXJuIHRydWU7IH1cblxuZXhwb3J0IHR5cGUgTWF5YmU8QSA9IGJvb2xlYW4+ID0gQSB8IG51bGwgfCB1bmRlZmluZWQ7XG4iLCJleHBvcnQgY29uc3QgdXJsID0gXCJodHRwOi8vbG9jYWxob3N0OjMwMDFcIjtcclxuZXhwb3J0IGNvbnN0IGFkbWluX3Vzcl9uYW1lID0gXCJrYWhla3NhamFsZ0BwbS5tZVwiO1xyXG5leHBvcnQgY29uc3QgYWRtaW5fcHdkID0gXCJhc2Rhc2RcIjtcclxuZXhwb3J0IGNvbnN0IHRlc3Rfa2V5ID0gXCJwb2VcIjtcclxuIiwiY29uc3QgaXNfd2luZG93cyA9IHByb2Nlc3MucGxhdGZvcm0gPT0gXCJ3aW4zMlwiO1xyXG5jb25zdCBzbGFzaCA9IGlzX3dpbmRvd3MgPyBcIlxcXFxcIiA6IFwiL1wiO1xyXG5cclxucmVxdWlyZShcInNvdXJjZS1tYXAtc3VwcG9ydFwiKS5pbnN0YWxsKHsgZW52aXJvbm1lbnQ6IFwibm9kZVwiIH0pO1xyXG5cclxuaW1wb3J0IFwianNvbjUvbGliL3JlZ2lzdGVyXCI7XHJcbmRlY2xhcmUgZ2xvYmFsIHsgaW50ZXJmYWNlIEpTT04geyByOiAoeDogc3RyaW5nKSA9PiBhbnksIHc6ICh4OiBhbnkpID0+IHN0cmluZyB9IH1cclxueyBjb25zdCBqNSA9IHJlcXVpcmUoXCJqc29uNVwiKTsgSlNPTi5yID0gajUucGFyc2U7IEpTT04udyA9ICh4KSA9PiBqNS5zdHJpbmdpZnkoeCwgeyBzcGFjZTogXCJcXHRcIiB9KTsgfVxyXG5kZWNsYXJlIGdsb2JhbCB7IGludGVyZmFjZSBTdHJpbmcgeyByZXBsYWNlQWxsKHNlYXJjaFZhbHVlOiBzdHJpbmcgfCBSZWdFeHAsIHJlcGxhY2VWYWx1ZTogc3RyaW5nKTogc3RyaW5nIH0gfVxyXG5TdHJpbmcucHJvdG90eXBlLnJlcGxhY2VBbGwgPz89IGZ1bmN0aW9uIChzZWFyY2g6IGFueSwgcmVwbGFjZW1lbnQ6IGFueSkgeyByZXR1cm4gdGhpcy5zcGxpdChzZWFyY2gpLmpvaW4ocmVwbGFjZW1lbnQpOyB9O1xyXG5cclxuaW1wb3J0IHsgQWxsUmVxdWVzdHMgfSBmcm9tIFwiQC9kYXRhL1JlcXVlc3RQcm90b2NvbFwiO1xyXG5pbXBvcnQgeyB0aW1lc3RhbXAgfSBmcm9tIFwiLi91dGlsc1wiO1xyXG5pbXBvcnQgeyB1cmwgfSBmcm9tIFwiLi9jb25maWdcIjtcclxuaW1wb3J0IHsgUmVxdWVzdFZhbGlkYXRvciB9IGZyb20gXCJAL2NvcmUvUmVxdWVzdFZhbGlkYXRvclwiO1xyXG5pbXBvcnQgeyBQcm9taXNlVHlwZSB9IGZyb20gXCJ1dGlsaXR5LXR5cGVzXCI7XHJcbmltcG9ydCBmZXRjaCBmcm9tIFwibm9kZS1mZXRjaFwiO1xyXG5cclxuY29uc3QgZmV0Y2hUaW1lb3V0ID0gKHVybDogc3RyaW5nLCBtczogbnVtYmVyLCBvcHRpb25zOiBhbnkpID0+IHtcclxuXHRjb25zdCBjb250cm9sbGVyID0gbmV3IEFib3J0Q29udHJvbGxlcigpO1xyXG5cdGNvbnN0IHByb21pc2UgPSBmZXRjaCh1cmwsIHsgc2lnbmFsOiBjb250cm9sbGVyLnNpZ25hbCwgLi4ub3B0aW9ucyB9KTtcclxuXHRjb25zdCB0aW1lb3V0ID0gc2V0VGltZW91dCgoKSA9PiBjb250cm9sbGVyLmFib3J0KCksIG1zKTtcclxuXHRyZXR1cm4gcHJvbWlzZS5maW5hbGx5KCgpID0+IGNsZWFyVGltZW91dCh0aW1lb3V0KSk7XHJcbn07XHJcbmxldCBjb29raWUgPSBcIlwiO1xyXG5sZXQgY3VycmVudF90ZXN0X3JlcV9zZXQ6IFNldDxzdHJpbmc+O1xyXG5cclxuLyoqXHJcbiAqIFxyXG4gKiBAcGFyYW0gYWN0aW9uIGVpdGhlciByZXF1ZXN0IGFjdGlvbiBvciBmYWxzZSB0byByZXNldCBjb29raWVzXHJcbiAqIEBwYXJhbSBwYXJhbWV0ZXJzIFxyXG4gKiBAcGFyYW0gZmlsZXMgXHJcbiAqL1xyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gcmVxdWVzdDxcclxuXHRBY3Rpb24gZXh0ZW5kcyBBbGxSZXF1ZXN0c1tcImFjdGlvblwiXSxcclxuXHRGb3JtIGV4dGVuZHMgRXh0cmFjdDxBbGxSZXF1ZXN0cywgeyBhY3Rpb246IEFjdGlvbiB9PixcclxuPihhY3Rpb246IEFjdGlvbiB8IGZhbHNlLCBwYXJhbWV0ZXJzPzogRm9ybVtcInBhcmFtc1wiXSk6IFByb21pc2U8UHJvbWlzZVR5cGU8UmV0dXJuVHlwZTxGb3JtW1wicnBmXCJdPj4gJiB7IGlzMjAwOiBib29sZWFuIH0+IHtcclxuXHRpZiAoYWN0aW9uID09PSBmYWxzZSkge1xyXG5cdFx0Y29va2llID0gXCJcIjtcclxuXHRcdHJldHVybiBudWxsIGFzIGFueTtcclxuXHR9XHJcblxyXG5cdHJldHVybiBmZXRjaFRpbWVvdXQodXJsICsgKHRydWUgPyBcIj9hPVwiICsgYWN0aW9uIDogXCJcIiksIDUwMDAsIHtcclxuXHRcdG1ldGhvZDogXCJwb3N0XCIsXHJcblx0XHRib2R5OiBKU09OLnN0cmluZ2lmeSh7IGFjdGlvbiwgLi4uKHBhcmFtZXRlcnMgPz8ge30pIH0pLFxyXG5cdFx0aGVhZGVyczoge1xyXG5cdFx0XHRcIkNvb2tpZVwiOiBjb29raWUsXHJcblx0XHRcdFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG5cdFx0fSxcclxuXHR9KS50aGVuKGFzeW5jIChyZXNwb25zZTogYW55KSA9PiB7XHJcblx0XHRpZiAocmVzcG9uc2Uuc3RhdHVzID09PSA1MDIgfHwgcmVzcG9uc2Uuc3RhdHVzID09PSA1MDMgfHwgcmVzcG9uc2Uuc3RhdHVzID09PSA1MDQpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coXCJUaGUgc2VydmVyIG9yIG5ldHdvcmsgaXMgdW5hdmFpbGFibGUhXCIpO1xyXG5cdFx0XHRjb25zb2xlLmxvZyhyZXNwb25zZS5zdGF0dXMpO1xyXG5cdFx0XHRwcm9jZXNzLmV4aXQoMSk7XHJcblx0XHR9XHJcblx0XHRjb25zdCBzZXRfY29va2llID0gcmVzcG9uc2UuaGVhZGVycy5yYXcoKVtcInNldC1jb29raWVcIl07XHJcblx0XHRpZiAoc2V0X2Nvb2tpZSAhPSBudWxsKSB7XHJcblx0XHRcdGNvb2tpZSA9IHNldF9jb29raWVbMF0uc3BsaXQoXCI7XCIpWzBdO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIG5ldyBQcm94eSh7IC4uLihhd2FpdCByZXNwb25zZS5qc29uKCkpLCBpczIwMDogcmVzcG9uc2Uuc3RhdHVzID09PSAyMDAgfSwge1xyXG5cdFx0XHRnZXQ6ICh0YXJnZXQsIHN0ZXApID0+IHtcclxuXHRcdFx0XHRpZiAoc3RlcCA9PT0gXCJpczIwMFwiKSB7XHJcblx0XHRcdFx0XHRjdXJyZW50X3Rlc3RfcmVxX3NldC5hZGQoYWN0aW9uKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0cmV0dXJuIHRhcmdldFtzdGVwXTtcclxuXHRcdFx0fSxcclxuXHRcdH0pO1xyXG5cdH0pLmNhdGNoKChlcnI6IGFueSkgPT4ge1xyXG5cdFx0Y29uc29sZS5sb2coZXJyKTtcclxuXHRcdGlmIChlcnIudHlwZSA9PT0gXCJhYm9ydGVkXCIgfHwgZXJyLmNvZGUgPT09IFwiRUNPTk5SRUZVU0VEXCIpIHtcclxuXHRcdFx0Y29uc29sZS5sb2coXCJUaGUgc2VydmVyIG9yIG5ldHdvcmsgaXMgdW5hdmFpbGFibGUhXCIpO1xyXG5cdFx0XHRwcm9jZXNzLmV4aXQoMSk7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4geyBvazogZmFsc2UsIGlzMjAwOiBmYWxzZSB9O1xyXG5cdH0pO1xyXG59XHJcblxyXG50eXBlIG1haW5fY2F0ZWdvcnkgPSBcImZ1bmN0aW9uYWxpdHlcIiB8IFwic3RhYmlsaXR5XCIgfCBcInNlY3VyaXR5XCI7XHJcbnR5cGUgdGVzdF9jYXRlZ29yeSA9IG1haW5fY2F0ZWdvcnkgfCBtYWluX2NhdGVnb3J5W10gfCBcImV4dHJhXCI7XHJcbmNvbnN0IHRlc3RzOiAoKCkgPT4gUHJvbWlzZTx7IHBhc3NlZDogYm9vbGVhbiwgY2F0ZWdvcnk6IHRlc3RfY2F0ZWdvcnksIHRlc3RfbmFtZTogc3RyaW5nIH0+KVtdID0gW107XHJcbmxldCBjdXJyZW50X2JlZm9yZV9lYWNoOiAoKGNvbnRleHQ6IHN0cmluZykgPT4gUHJvbWlzZTx2b2lkPikgfCBudWxsID0gbnVsbDtcclxuY29uc3QgZ3JlZW4gPSBcIlxceDFiWzMybVwiO1xyXG5jb25zdCByZWQgPSBcIlxceDFiWzMxbVwiO1xyXG5jb25zdCByZXNldCA9IFwiXFx4MWJbMG1cIjtcclxubGV0IGNvdW50ZXIgPSAwO1xyXG5cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVUZXN0KHRlc3RfbmFtZTogc3RyaW5nLCBjYXRlZ29yeTogdGVzdF9jYXRlZ29yeSwgdGVzdF9hY3Rpb246IChjb250ZXh0OiBzdHJpbmcpID0+IFByb21pc2U8dHJ1ZSB8IHN0cmluZz4pIHtcclxuXHRjb25zdCB0ZXN0X2ZhaWxlZCA9IChlcnJvcl9tc2c6IHN0cmluZykgPT4ge1xyXG5cdFx0Y29uc29sZS5sb2coKTtcclxuXHRcdGNvbnNvbGUubG9nKHJlZCArIFwiVGVzdCBcXFwiXCIgKyB0ZXN0X25hbWUgKyBcIlxcXCIgZmFpbGVkIVwiICsgcmVzZXQpO1xyXG5cdFx0Y29uc29sZS5sb2coZXJyb3JfbXNnKTtcclxuXHRcdGNvbnNvbGUubG9nKCk7XHJcblx0fTtcclxuXHJcblx0Y29uc3QgYmVmb3JlX2VhY2ggPSBjdXJyZW50X2JlZm9yZV9lYWNoO1xyXG5cdGNvbnN0IHRlc3Q6ICgpID0+IFByb21pc2U8eyBwYXNzZWQ6IGJvb2xlYW4sIGNhdGVnb3J5OiB0ZXN0X2NhdGVnb3J5LCB0ZXN0X25hbWU6IHN0cmluZyB9PiA9IGFzeW5jICgpID0+IHtcclxuXHRcdGNvbnN0IGNvbnRleHQgPSBcImNvbnRleHRfXCIgKyB0aW1lc3RhbXAoKSArIFwiX1wiICsgY291bnRlcisrO1xyXG5cdFx0aWYgKGJlZm9yZV9lYWNoKSB7XHJcblx0XHRcdGF3YWl0IGJlZm9yZV9lYWNoKGNvbnRleHQpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHRyeSB7XHJcblx0XHRcdGNvbnN0IHJlc3VsdCA9IGF3YWl0IHRlc3RfYWN0aW9uKGNvbnRleHQpO1xyXG5cdFx0XHRpZiAoY2F0ZWdvcnkgIT09IFwiZXh0cmFcIiAmJiBjdXJyZW50X3Rlc3RfcmVxX3NldC5zaXplID09PSAwKSB0aHJvdyBuZXcgRXJyb3IoXCJObyAyMDAgY2hlY2tzIGluIHRoaXMgdGVzdCFcIik7XHJcblx0XHRcdFxyXG5cdFx0XHRpZiAocmVzdWx0ID09PSB0cnVlKSB7XHJcblx0XHRcdFx0cmV0dXJuIHsgcGFzc2VkOiB0cnVlLCBjYXRlZ29yeSwgdGVzdF9uYW1lIH07XHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0dGVzdF9mYWlsZWQocmVzdWx0KTtcclxuXHRcdFx0XHRyZXR1cm4geyBwYXNzZWQ6IGZhbHNlLCBjYXRlZ29yeSwgdGVzdF9uYW1lIH07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGNhdGNoIChlcnIpIHtcclxuXHRcdFx0aWYgKHR5cGVvZiBlcnIgIT09IFwic3RyaW5nXCIpIHtcclxuXHRcdFx0XHRjb25zb2xlLmxvZyhlcnIuY29kZSk7XHJcblx0XHRcdFx0Y29uc29sZS5sb2coXCJcXG5FcnJvciBpbiB0ZXN0OiBcIiArIHRlc3RfbmFtZSk7XHJcblx0XHRcdFx0Y29uc29sZS5sb2coZXJyKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRlbHNlIHRlc3RfZmFpbGVkKGVycik7XHJcblx0XHRcdHJldHVybiB7IHBhc3NlZDogZmFsc2UsIGNhdGVnb3J5LCB0ZXN0X25hbWUgfTtcclxuXHRcdH1cclxuXHR9O1xyXG5cclxuXHR0ZXN0cy5wdXNoKHRlc3QpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2V0QmVmb3JlRWFjaChsYW1iZGE6IChjb250ZXh0OiBzdHJpbmcpID0+IFByb21pc2U8dm9pZD4pIHtcclxuXHRjdXJyZW50X2JlZm9yZV9lYWNoID0gbGFtYmRhO1xyXG59XHJcblxyXG50eXBlIFJlcXVlc3RDb3ZlcmFnZSA9IHtcclxuXHRmdW5jdGlvbmFsaXR5OiBudW1iZXIsIC8vIGRvZXMgaXQgZXZlbiB3b3JrPyBpcyB0aGUgVUkgb2theT9cclxuXHRzdGFiaWxpdHk6IG51bWJlciwgLy8gY2FuIHRoZSBzeXN0ZW0gaGFuZGxlIGRpcnR5IGlucHV0PyBkb2VzIGl0IGNyYXNoPyBpcyB0aGVyZSBhbnkgZGF0YSBjb3JydXB0aW9uP1xyXG5cdHNlY3VyaXR5OiBudW1iZXIsIC8vIGFyZSBwZXJtaXNzaW9ucyBjaGVja2VkIHByb3Blcmx5P1xyXG59O1xyXG5jb25zdCBjb3ZlcmFnZSA9IHt9IGFzIFJlY29yZDxzdHJpbmcsIFJlcXVlc3RDb3ZlcmFnZT47XHJcbmNvbnN0IHJlcV9uYW1lcyA9IE9iamVjdC5rZXlzKFJlcXVlc3RWYWxpZGF0b3IpO1xyXG5yZXFfbmFtZXMuZm9yRWFjaCh4ID0+IHtcclxuXHRjb3ZlcmFnZVt4XSA9IHtcclxuXHRcdGZ1bmN0aW9uYWxpdHk6IDAsXHJcblx0XHRzdGFiaWxpdHk6IDAsXHJcblx0XHRzZWN1cml0eTogMCxcclxuXHR9O1xyXG59KTtcclxuXHJcbmFzeW5jIGZ1bmN0aW9uIHJ1blRlc3RzKCkge1xyXG5cdGZ1bmN0aW9uIG1hcmtDb3JyZWN0Q292ZXJhZ2UoY3VycmVudF90ZXN0X3JlcTogc3RyaW5nLCBjYXRlZ29yeTogbWFpbl9jYXRlZ29yeSwgdGVzdF9uYW1lOiBzdHJpbmcpIHtcclxuXHRcdGlmICghY292ZXJhZ2VbY3VycmVudF90ZXN0X3JlcV1bY2F0ZWdvcnldKSBjb3ZlcmFnZVtjdXJyZW50X3Rlc3RfcmVxXVtjYXRlZ29yeV0rKztcclxuXHRcdGVsc2UgdGhyb3cgbmV3IEVycm9yKFwiV2FybmluZyEgVGVzdCBcXFwiXCIgKyB0ZXN0X25hbWUgKyBcIlxcXCIgZm9yIHJlcXVlc3QgXFxcIlwiICsgY3VycmVudF90ZXN0X3JlcSArIFwiXFxcIiBpcyBhIGR1cGxpY2F0ZSB0ZXN0IGluIGNhdGVnb3J5IFxcXCJcIiArIGNhdGVnb3J5ICsgXCJcXFwiIVwiKTtcclxuXHR9XHJcblxyXG5cdGxldCBjb3VudCA9IDA7XHJcblx0Zm9yIChjb25zdCB0ZXN0IG9mIHRlc3RzKSB7XHJcblx0XHRjdXJyZW50X3Rlc3RfcmVxX3NldCA9IG5ldyBTZXQoKTtcclxuXHRcdGNvbnN0IHRlc3RfcmVzdWx0ID0gYXdhaXQgdGVzdCgpO1xyXG5cdFx0aWYgKHRlc3RfcmVzdWx0LnBhc3NlZCkgY291bnQrKztcclxuXHRcdGlmICh0ZXN0X3Jlc3VsdC5jYXRlZ29yeSA9PT0gXCJleHRyYVwiKSBjb250aW51ZTtcclxuXHJcblx0XHRmb3IgKGNvbnN0IGN1cnJlbnRfdGVzdF9yZXEgb2YgY3VycmVudF90ZXN0X3JlcV9zZXQpIHtcclxuXHRcdFx0aWYgKHR5cGVvZiB0ZXN0X3Jlc3VsdC5jYXRlZ29yeSA9PT0gXCJzdHJpbmdcIikge1xyXG5cdFx0XHRcdG1hcmtDb3JyZWN0Q292ZXJhZ2UoY3VycmVudF90ZXN0X3JlcSwgdGVzdF9yZXN1bHQuY2F0ZWdvcnksIHRlc3RfcmVzdWx0LnRlc3RfbmFtZSk7XHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0Zm9yIChjb25zdCBjYXRlZ29yeSBvZiB0ZXN0X3Jlc3VsdC5jYXRlZ29yeSkge1xyXG5cdFx0XHRcdFx0bWFya0NvcnJlY3RDb3ZlcmFnZShjdXJyZW50X3Rlc3RfcmVxLCBjYXRlZ29yeSwgdGVzdF9yZXN1bHQudGVzdF9uYW1lKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0Y29uc3QgY29sb3JfY29kZSA9IGNvdW50ID09PSB0ZXN0cy5sZW5ndGggPyBncmVlbiA6IHJlZDtcclxuXHRjb25zb2xlLmxvZyhjb2xvcl9jb2RlICsgY291bnQgKyBcIiBvdXQgb2YgXCIgKyB0ZXN0cy5sZW5ndGggKyBcIiB0ZXN0cyBzdWNjZXNzZnVsIVwiICsgcmVzZXQpO1xyXG5cdGlmIChjb3VudCA8IHRlc3RzLmxlbmd0aCkgcHJvY2Vzcy5leGl0Q29kZSA9IDE7XHJcbn1cclxuXHJcbi8vIGltcG9ydHMgdGVzdHMgYW5kIHJ1bnMgdGhlbVxyXG5jdXJyZW50X2JlZm9yZV9lYWNoID0gbnVsbDtcclxuaW1wb3J0IFwiLi91c2VyX3JlcXVlc3RzX3Rlc3RzL3Nlc3Npb25fdGVzdFwiO1xyXG5jdXJyZW50X2JlZm9yZV9lYWNoID0gbnVsbDtcclxuaW1wb3J0IFwiLi91c2VyX3JlcXVlc3RzX3Rlc3RzL29yZ190ZXN0XCI7XHJcbmN1cnJlbnRfYmVmb3JlX2VhY2ggPSBudWxsO1xyXG5pbXBvcnQgXCIuL3VzZXJfcmVxdWVzdHNfdGVzdHMvdGlja2V0X3Rlc3RcIjtcclxuY3VycmVudF9iZWZvcmVfZWFjaCA9IG51bGw7XHJcbmltcG9ydCBcIi4vdXNlcl9yZXF1ZXN0c190ZXN0cy9taXNjX3Rlc3RcIjtcclxuY3VycmVudF9iZWZvcmVfZWFjaCA9IG51bGw7XHJcbmltcG9ydCBcIi4vdXNlcl9yZXF1ZXN0c190ZXN0cy9ncm91cF90ZXN0XCI7XHJcbmN1cnJlbnRfYmVmb3JlX2VhY2ggPSBudWxsO1xyXG5pbXBvcnQgXCIuL3VzZXJfcmVxdWVzdHNfdGVzdHMvY2hlY2tsaXN0X3Rlc3RcIjtcclxuY3VycmVudF9iZWZvcmVfZWFjaCA9IG51bGw7XHJcbmltcG9ydCBcIi4vdXNlcl9yZXF1ZXN0c190ZXN0cy9jaGF0X3Rlc3RcIjtcclxuXHJcbmFzeW5jIGZ1bmN0aW9uIG1haW4oKSB7XHJcblx0YXdhaXQgcnVuVGVzdHMoKTtcclxuXHRmdW5jdGlvbiBpc01pc3NpbmdUeXBlKHg6IFJlcXVlc3RDb3ZlcmFnZSkge1xyXG5cdFx0cmV0dXJuICgheC5mdW5jdGlvbmFsaXR5IHx8ICF4LnN0YWJpbGl0eSB8fCAheC5zZWN1cml0eSk7XHJcblx0fVxyXG5cclxuXHRpZiAocmVxX25hbWVzLnNvbWUoeCA9PiBpc01pc3NpbmdUeXBlKGNvdmVyYWdlW3hdKSkpIHtcclxuXHRcdGNvbnNvbGUubG9nKFwiV2FybmluZywgc29tZSByZXF1ZXN0cyBhcmUgbWlzc2luZyB0ZXN0cyFcIik7XHJcblxyXG5cdFx0cmVxX25hbWVzLmZvckVhY2goeCA9PiB7XHJcblx0XHRcdGlmIChpc01pc3NpbmdUeXBlKGNvdmVyYWdlW3hdKSkge1xyXG5cdFx0XHRcdGNvbnN0IGZ1bmMgPSAoY292ZXJhZ2VbeF0uZnVuY3Rpb25hbGl0eSA/IGdyZWVuIDogcmVkKSArIFwiZnVuY3Rpb25hbGl0eVwiICsgcmVzZXQ7XHJcblx0XHRcdFx0Y29uc3Qgc3RhYiA9IChjb3ZlcmFnZVt4XS5zdGFiaWxpdHkgPyBncmVlbiA6IHJlZCkgKyBcInN0YWJpbGl0eVwiICsgcmVzZXQ7XHJcblx0XHRcdFx0Y29uc3Qgc2VjdSA9IChjb3ZlcmFnZVt4XS5zZWN1cml0eSA/IGdyZWVuIDogcmVkKSArIFwic2VjdXJpdHlcIiArIHJlc2V0O1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKGAke2Z1bmN9ICAke3N0YWJ9ICAke3NlY3V9ICAke3h9YCk7XHJcblx0XHRcdH1cclxuXHRcdH0pO1xyXG5cdH1cclxufVxyXG5cclxubWFpbigpO1xyXG4iLCJcclxuaW1wb3J0IHsgcmVxdWVzdCB9IGZyb20gXCIuL3JlcXVlc3RfdGVzdFwiO1xyXG5cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGNyZWF0ZVVzZXIoZW1haWxfbmFtZTogc3RyaW5nLCBkaXNwbGF5X25hbWU6IHN0cmluZykge1xyXG5cdHJldHVybiByZXF1ZXN0KFwicmVnaXN0ZXJcIiwgeyBlbWFpbDogZW1haWxfbmFtZSArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiwgZm5hbWU6IGRpc3BsYXlfbmFtZSwgbG5hbWU6IFwibGFzdG5hbWVcIiwga2V5OiBcImFcIiB9KTtcclxufVxyXG4iLCJpbXBvcnQgeyBjcmVhdGVUZXN0LCByZXF1ZXN0LCBzZXRCZWZvcmVFYWNoIH0gZnJvbSBcIi4uL3JlcXVlc3RfdGVzdFwiO1xyXG5pbXBvcnQgeyBjcmVhdGVVc2VyIH0gZnJvbSBcIi4uL3JlcXVlc3RfdXRpbHNcIjtcclxuaW1wb3J0IHsgbmFtZURpcnR5SW5wdXRUZXN0LCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QsIHZlcmlmeVJlcXVlc3RBY2NlcHRlZCwgdmVyaWZ5UmVxdWVzdERlbmllZCB9IGZyb20gXCIuLi91dGlsc1wiO1xyXG5pbXBvcnQgeyBJZCB9IGZyb20gXCJAL2RhdGEvdVwiO1xyXG5pbXBvcnQgeyBhc0RNQ2hhbm5lbCB9IGZyb20gXCJAL2RhdGEvQ2hhdFwiO1xyXG5cclxuc2V0QmVmb3JlRWFjaChhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IHJlcXVlc3QoZmFsc2UpO1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCwgXCJ1c2VyXCIpO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gcG9zdCBhbmQgbG9hZCBjaGF0c1wiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHQvLyBjcmVhdGUgYSBzZWNvbmQgdXNlciB0byB0cnkgYW5kIGFjY2VzcyBwb3N0ZWQgY2hhdHMgbGF0ZXJcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQgKyAxLCBcInVzZXIxXCIpO1xyXG5cdGNvbnN0IHVzZXJfb2JqMiA9IHsgdXNyOiBjb250ZXh0ICsgMSArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9O1xyXG5cdGNvbnN0IHVzZXJfb2JqMSA9IHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH07XHJcblx0Y29uc3QgbmFtZSA9IFwibXkgbmV3IGdycFwiO1xyXG5cdGNvbnN0IHR4dF9vcmcgPSBcImhlbGxvLCBmZWxsb3cgY2hhdHRlcnNcIjtcclxuXHRjb25zdCB0eHRfZ3JwID0gXCJoZWxsbywgZmVsbG93IGNoYXR0ZXJzXCI7XHJcblx0bGV0IHR4dF9kbSA9IFwiSGVsbG8sIHN1YiFcIjtcclxuXHJcblx0Ly8gc2Vjb25kIHVzZXIncyBpZCBpcyByZXF1aXJlZCB0byBnaXZlIHRoZW0gYWNjZXNzIHRvIHRoZSBncm91cFxyXG5cdGNvbnN0IHVzZXIyX2lkID0gKGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB1c2VyX29iajIpKS5kYXRhIS5faWQ7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ291dFwiKTtcclxuXHJcblx0Ly8gbG9nIG9yaWdpbmFsIHVzZXIgaW5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgdXNlcl9vYmoxKTtcclxuXHRjb25zdCB1c2VyMV9pZCA9IChhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgdXNlcl9vYmoxKSkuZGF0YSEuX2lkO1xyXG5cclxuXHQvLyBzZXR1cFxyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwiaW52aXRlXCIsIHsgb3JnLCBlbWFpbDogdXNlcl9vYmoyLnVzciB9KTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgdXNlcl9vYmoyKTtcclxuXHRjb25zdCBpbnZpdGF0aW9ucyA9IChhd2FpdCByZXF1ZXN0KFwiZ2V0SW52aXRhdGlvbnNcIikpLmRhdGEhO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJhY2NlcHRJbnZpdGF0aW9uXCIsIHsgaW52OiBpbnZpdGF0aW9uc1swXS5faWQgfSk7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ291dFwiKTtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHVzZXJfb2JqMSk7XHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWUgfSk7XHJcblx0Y29uc3QgZ3JwID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhWzBdLl9pZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX3NldEF1dGhMdmxcIiwgeyBvcmcsIGdycCwgdXNlcjogdXNlcjJfaWQsIGx2bDogMSB9KTtcclxuXHJcblx0Ly8gY2FuIHVzZXIsIHdobyBwb3N0cyBjaGF0cywgYWNjZXNzIHRoZW0/XHJcblx0bGV0IHJlcV9wb3N0ID0gYXdhaXQgcmVxdWVzdChcImNoYXRfcG9zdFwiLCB7IG9yZywgY2hhbm5lbDogXCJvcmdcIiwgaXNfZG06IGZhbHNlLCB0eHQ6IHR4dF9vcmcgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcV9wb3N0LCBcIlRyaWVkIHRvIHBvc3QgYSBuZXcgY2hhdCB0byBhbiBvcmcuXCIpO1xyXG5cdHJlcV9wb3N0ID0gYXdhaXQgcmVxdWVzdChcImNoYXRfcG9zdFwiLCB7IG9yZywgY2hhbm5lbDogZ3JwLCBpc19kbTogZmFsc2UsIHR4dDogdHh0X2dycCB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxX3Bvc3QsIFwiVHJpZWQgdG8gcG9zdCBhIG5ldyBjaGF0IHRvIGEgZ3JwLlwiKTtcclxuXHRyZXFfcG9zdCA9IGF3YWl0IHJlcXVlc3QoXCJjaGF0X3Bvc3RcIiwgeyBvcmcsIGNoYW5uZWw6IHVzZXIyX2lkLCBpc19kbTogdHJ1ZSwgdHh0OiB0eHRfZG0gfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcV9wb3N0LCBcIlRyaWVkIHRvIHNlbmQgYSBkbSB0byBhbm90aGVyIHVzZXIuXCIpO1xyXG5cclxuXHRsZXQgcmVxX2xvYWQgPSBhd2FpdCByZXF1ZXN0KFwiY2hhdF9sb2FkXCIsIHsgb3JnIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXFfbG9hZCwgXCJUcmllZCB0byBsb2FkIGNoYXQgbWVzc2FnZXMuXCIpO1xyXG5cdGlmIChyZXFfbG9hZC5kYXRhIVswXS50eHQgIT09IHR4dF9vcmcpIHJldHVybiBcIkNvdWxkIG5vdCBsb2FkIGEgY29ycmVjdCBjaGF0IGZyb20gb3JnIVwiO1xyXG5cdGlmIChyZXFfbG9hZC5kYXRhIVsxXS50eHQgIT09IHR4dF9ncnApIHJldHVybiBcIkNvdWxkIG5vdCBsb2FkIGEgY29ycmVjdCBjaGF0IGZyb20gZ3JwIVwiO1xyXG5cdGlmIChyZXFfbG9hZC5kYXRhIVsyXS50eHQgIT09IHR4dF9kbSkgcmV0dXJuIFwiQ291bGQgbm90IGxvYWQgYSBjb3JyZWN0IGNoYXQgZnJvbSBkbSdzIVwiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cclxuXHQvLyBjYW4gdXNlciwgd2hvIGxvYWRzIGNoYXRzLCBhY2Nlc3MgdGhlbT9cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgdXNlcl9vYmoyKTtcclxuXHRyZXFfbG9hZCA9IGF3YWl0IHJlcXVlc3QoXCJjaGF0X2xvYWRcIiwgeyBvcmcgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcV9sb2FkLCBcIlRyaWVkIHRvIGxvYWQgY2hhdCBtZXNzYWdlcyBhcyBzZWNvbmQgdXNlci5cIik7XHJcblx0aWYgKHJlcV9sb2FkLmRhdGEhWzBdLnR4dCAhPT0gdHh0X29yZykgcmV0dXJuIFwiQ291bGQgbm90IGxvYWQgYSBjb3JyZWN0IGNoYXQgYXMgc2Vjb25kIHVzZXIgZnJvbSBvcmchXCI7XHJcblx0aWYgKHJlcV9sb2FkLmRhdGEhWzFdLnR4dCAhPT0gdHh0X2dycCkgcmV0dXJuIFwiQ291bGQgbm90IGxvYWQgYSBjb3JyZWN0IGNoYXQgYXMgc2Vjb25kIHVzZXIgZnJvbSBncnAhXCI7XHJcblx0aWYgKHJlcV9sb2FkLmRhdGEhWzJdLnR4dCAhPT0gdHh0X2RtKSByZXR1cm4gXCJDb3VsZCBub3QgbG9hZCBhIGNvcnJlY3QgY2hhdCBhcyBzZWNvbmQgdXNlciBmcm9tIGRtJ3MhXCI7XHJcblxyXG5cdC8vIGlzIGRtIGNoYW5uZWwgY29ycmVjdD9cclxuXHRjb25zdCBjaGFubmVsID0gYXNETUNoYW5uZWwoW3VzZXIxX2lkLCB1c2VyMl9pZF0pO1xyXG5cdHJlcV9sb2FkID0gYXdhaXQgcmVxdWVzdChcImNoYXRfbG9hZFwiLCB7IG9yZyB9KTtcclxuXHRpZiAocmVxX2xvYWQuZGF0YSFbMl0uY2hhbm5lbCAhPT0gY2hhbm5lbCkgcmV0dXJuIFwiVGhlIGNoYW5uZWwsIGluIHdoaWNoIHRoZSB1c2VycyBzZW5kIGRtcywgd2FzIGluY29ycmVjdCFcXG5FeHBlY3RlZDogXCIgKyBjaGFubmVsICsgXCJcXG5SZWNlaXZlZDogXCIgKyByZXFfbG9hZC5kYXRhIVsyXS5jaGFubmVsO1xyXG5cclxuXHR0eHRfZG0gPSBcIkhlbGxvLCBkb20hXCI7XHJcblx0YXdhaXQgcmVxdWVzdChcImNoYXRfcG9zdFwiLCB7IG9yZywgY2hhbm5lbDogdXNlcjFfaWQsIGlzX2RtOiB0cnVlLCB0eHQ6IHR4dF9kbSB9KTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cclxuXHQvLyBjYW4gdXNlciwgd2hvIG9yaWdpbmFsbHkgcG9zdGVkIGNoYXRzLCBhY2Nlc3MgZG1zLCBzZW50IHRvIHRoZW0/XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHVzZXJfb2JqMSk7XHJcblx0cmVxX2xvYWQgPSBhd2FpdCByZXF1ZXN0KFwiY2hhdF9sb2FkXCIsIHsgb3JnIH0pO1xyXG5cdHJldHVybiAocmVxX2xvYWQuZGF0YSFbM10udHh0ICE9PSB0eHRfZG0pID8gXCJDb3VsZCBub3QgbG9hZCBhIGNvcnJlY3QgZG0gY2hhdCB0ZXh0IVwiIDogdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KG5hbWVEaXJ0eUlucHV0VGVzdChcImNoYXRfcG9zdCwgY2hhdF9sb2FkXCIpLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdC8vIHNldHVwXHJcblx0Y29uc3QgdXNlcl9vYmogPSB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9O1xyXG5cdGNvbnN0IG5hbWUgPSBcIm15IG5ldyBncnBcIjtcclxuXHRjb25zdCB0eHQgPSBcImhlbGxvLCBmZWxsb3cgY2hhdHRlcnNcIjtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgdXNlcl9vYmopO1xyXG5cclxuXHRjb25zdCB1c2VyX2lkID0gKGF3YWl0IHJlcXVlc3QoXCJ1c2VyXCIpKS5kYXRhPy5faWQ7XHJcblxyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZywgbmFtZSB9KTtcclxuXHRjb25zdCBncnAgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZyB9KSkuZGF0YSFbMF0uX2lkO1xyXG5cclxuXHQvLyBkaXJ0eSBpbnB1dCB0ZXN0aW5nXHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcImNoYXRfbG9hZFwiLCB7IG9yZyB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGxvYWQgYW4gZW1wdHkgQ2hhdE1zZ1tdLlwiKTtcclxuXHJcblx0aWYgKHJlcS5kYXRhIS5sZW5ndGggIT09IDApIHJldHVybiBcIlNvbWV0aGluZyB3YXMgbG9hZGVkIGludG8gQ2hhdE1zZ1tdLCBidXQgaXQgd2FzIHN1cHBvc2VkIHRvIGJlIGVtcHR5IVwiO1xyXG5cclxuXHRsZXQgZGlydHlfaW5wdXQgPSBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJjaGF0X3Bvc3QgYWNjZXB0ZWQgZGlydHkgaW5wdXQhXCIsIHsgb3JnLCBjaGFubmVsOiBncnAsIGlzX2RtOiBmYWxzZSwgdHh0IH0sIChkaXJ0eV9vYmplY3QpID0+IFtcImNoYXRfcG9zdFwiLCBkaXJ0eV9vYmplY3RdLCB7IGlzX2RtOiBbdHJ1ZSwgZmFsc2VdIH0pO1xyXG5cdGlmIChkaXJ0eV9pbnB1dCAhPT0gdHJ1ZSkgcmV0dXJuIGRpcnR5X2lucHV0O1xyXG5cclxuXHQvLyB1bHRyYSBjb3JuZXIgY2FzZXNcclxuXHRsZXQgYmFkX3JlcSA9IGF3YWl0IHJlcXVlc3QoXCJjaGF0X3Bvc3RcIiwgeyBvcmcsIGNoYW5uZWw6IFwiYmFkX3JlcVwiIGFzIElkLCBpc19kbTogZmFsc2UsIHR4dCB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0RGVuaWVkKGJhZF9yZXEsIFwiVHJpZWQgdG8gcG9zdCBhIGNoYXQgbXNnIGludG8gYW4gaW5jb3JyZWN0IGNoYW5uZWwgKFxcXCJiYWRfcmVxXFxcIikuXCIpO1xyXG5cclxuXHRiYWRfcmVxID0gYXdhaXQgcmVxdWVzdChcImNoYXRfcG9zdFwiLCB7IG9yZywgY2hhbm5lbDogXCJcIiBhcyBJZCwgaXNfZG06IGZhbHNlLCB0eHQgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdERlbmllZChiYWRfcmVxLCBcIlRyaWVkIHRvIHBvc3QgYSBjaGF0IG1zZyBjb250YWluaW5nIGVtcHR5IHN0cmluZ3MgYXMgSURzIChcXFwiXFxcIikuXCIpO1xyXG5cclxuXHRiYWRfcmVxID0gYXdhaXQgcmVxdWVzdChcImNoYXRfcG9zdFwiLCB7IG9yZywgY2hhbm5lbDogXCJiYWRcIiBhcyBJZCwgaXNfZG06IGZhbHNlLCB0eHQgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdERlbmllZChiYWRfcmVxLCBcIlRyaWVkIHRvIHBvc3QgYSBjaGF0IG1zZyBjb250YWluaW5nIGRpcnR5IElEcyAoXFxcImJhZFxcXCIpLlwiKTtcclxuXHJcblx0YmFkX3JlcSA9IGF3YWl0IHJlcXVlc3QoXCJjaGF0X3Bvc3RcIiwgeyBvcmcsIGNoYW5uZWw6IHVzZXJfaWQgYXMgSWQsIGlzX2RtOiB0cnVlLCB0eHQgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdERlbmllZChiYWRfcmVxLCBcIlRyaWVkIHRvIHBvc3QgYSBjaGF0IG1zZyB0byB1c2VyIHRoZW1zZWxmLlwiKTtcclxuXHRpZiAoIWJhZF9yZXEuaXMyMDApIHJldHVybiBcIlNlcnZlciBjcmFzaGVkIGR1ZSB0byBkaXJ0eSBpbnB1dCBpbiByZXEgY2hhdF9wb3N0IGZpZWxkIGNoYW5uZWwgKHVzZXIncyBhY3R1YWwgaWQpIVwiO1xyXG5cdGlmIChiYWRfcmVxLm9rKSByZXR1cm4gXCJTZXJ2ZXIgYWNjZXB0ZWQgYSBjaGF0IG1lc3NhZ2UgYmVpbmcgc2VudCB0byB1c2VyIHRoZW1zZWxmIVwiO1xyXG5cclxuXHRiYWRfcmVxID0gYXdhaXQgcmVxdWVzdChcImNoYXRfcG9zdFwiLCB7IG9yZywgY2hhbm5lbDogXCJ4MF8wXCIgYXMgSWQsIGlzX2RtOiB0cnVlLCB0eHQgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdERlbmllZChiYWRfcmVxLCBcIlRyaWVkIHRvIHBvc3QgYSBjaGF0IG1zZyBiZWluZyBzZW50IHRvIGEgbm9uLWV4aXN0aW5nIHVzZXIgKFxcXCJ4MF8wXFxcIikuXCIpO1xyXG5cclxuXHQvLyBub3JtYWwgcmVzdW1lcyBoZXJlXHJcblx0YXdhaXQgcmVxdWVzdChcImNoYXRfcG9zdFwiLCB7IG9yZywgY2hhbm5lbDogZ3JwLCBpc19kbTogZmFsc2UsIHR4dCB9KTtcclxuXHRkaXJ0eV9pbnB1dCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcImNoYXRfbG9hZCBhY2NlcHRlZCBkaXJ0eSBpbnB1dCFcIiwgeyBvcmcgfSwgKGRpcnR5X29iamVjdCkgPT4gW1wiY2hhdF9sb2FkXCIsIGRpcnR5X29iamVjdF0pO1xyXG5cdHJldHVybiBkaXJ0eV9pbnB1dDtcclxufSk7XHJcbiIsImltcG9ydCB7IGNyZWF0ZVRlc3QsIHJlcXVlc3QsIHNldEJlZm9yZUVhY2ggfSBmcm9tIFwiLi4vcmVxdWVzdF90ZXN0XCI7XHJcbmltcG9ydCB7IElkIH0gZnJvbSBcIkAvZGF0YS91XCI7XHJcbmltcG9ydCB7IGNyZWF0ZVVzZXIgfSBmcm9tIFwiLi4vcmVxdWVzdF91dGlsc1wiO1xyXG5pbXBvcnQgeyBuYW1lRGlydHlJbnB1dFRlc3QsIHRlc3REaXJ0eUlucHV0UmVxdWVzdCwgdGltZXN0YW1wLCB2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQgfSBmcm9tIFwiLi4vdXRpbHNcIjtcclxuaW1wb3J0IHsgQ2hlY2tsaXN0LCBDaGVja2xpc3RGb3JtLCBDaGVja2xpc3RTZWN0aW9uLCBDaGVja2xpc3RUYXNrIH0gZnJvbSBcIkAvZGF0YS9DaGVja2xpc3RcIjtcclxuXHJcbnNldEJlZm9yZUVhY2goYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCByZXF1ZXN0KGZhbHNlKTtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQsIFwidXNlclwiKTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGNyZWF0ZSBhIG5ldyBjaGVja2xpc3RcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgdGl0bGUgPSBcIlRlZSBzZWRhIVwiO1xyXG5cdGNvbnN0IG5hbWUgPSBcIlRlZ2V2dXNcIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnAgPSBncm91cHNbMF0uX2lkO1xyXG5cclxuXHRjb25zdCBjaGtsaXN0ID0geyBvcmcsIGRvYzogeyBncnAsIHRpdGxlLCBpdGVtczogW3sgbmFtZSwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAxIH0gYXMgY29uc3RdLCBpdGVtc19sYXRlc3Q6IDEsIGFjdGl2YXRpb246IDEsIGR1cmF0aW9uOiAxIH0gfTtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwiY2hrX25ld1wiLCBjaGtsaXN0KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGNyZWF0ZSBhIG5ldyBjaGVja2xpc3QuXCIpO1xyXG5cclxuXHRjb25zdCBjaGVja2xpc3RzOiBDaGVja2xpc3RbXSA9IChhd2FpdCByZXF1ZXN0KFwiY2hrX2dldE9mR3JvdXBcIiwgeyBvcmcsIGdycCB9KSkuZGF0YSE7XHJcblx0cmV0dXJuIChjaGVja2xpc3RzWzBdLnRpdGxlICE9PSB0aXRsZSkgPyBcIk5ldyBjaGVja2xpc3Qgd2FzIG5vdCBjcmVhdGVkIHByb3Blcmx5ISBFeHBlY3RlZCB0aXRsZTogXCIgKyB0aXRsZSArIFwiIFJlY2VpdmVkIHRpdGxlOiBcIiArIGNoZWNrbGlzdHNbMF0udGl0bGUgOiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gY3JlYXRlIGEgcmVwZWF0aW5nIGNoZWNrbGlzdCB3aXRob3V0IHRpbWUgbWFudWFsbHkgc2V0XCIsIFwiZXh0cmFcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCBkYXlzID0geyBtb246IHRydWUsIHR1ZTogdHJ1ZSwgd2VkOiB0cnVlLCB0aHU6IHRydWUsIGZyaTogdHJ1ZSwgc2F0OiB0cnVlLCBzdW46IHRydWUgfTtcclxuXHRjb25zdCB0aXRsZSA9IFwiVGVlIHNlZGEhXCI7XHJcblx0Y29uc3QgbmFtZSA9IFwiVGVnZXZ1c1wiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZywgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycCA9IGdyb3Vwc1swXS5faWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJjaGtfbmV3XCIsIHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFt7IG5hbWUsIHZhbDogXCJuZXdcIiwgc2VjdGlvbjogZmFsc2UsIGtleTogMSB9XSwgaXRlbXNfbGF0ZXN0OiAxLCBhY3RpdmF0aW9uOiAxLCBkdXJhdGlvbjogMCwgc2NoZWR1bGU6IHsgcmVwZWF0OiBcImRhaWx5XCIsIGRheXMgfSB9IH0pO1xyXG5cclxuXHRjb25zdCBjaGVja2xpc3RzOiBDaGVja2xpc3RbXSA9IChhd2FpdCByZXF1ZXN0KFwiY2hrX2dldE9mR3JvdXBcIiwgeyBvcmcsIGdycCB9KSkuZGF0YSE7XHJcblx0Zm9yIChjb25zdCBjaGVja2xpc3Qgb2YgY2hlY2tsaXN0cykge1xyXG5cdFx0aWYgKGNoZWNrbGlzdC5zdGF0dXMgPT09IFwiYWN0aXZlXCIpIHtcclxuXHRcdFx0aWYgKGNoZWNrbGlzdC5zY2hlZHVsZSkgcmV0dXJuIFwiQWN0aXZlIGNoZWNrbGlzdCBoYWQgYW4gZXhpc3Rpbmcgc2NoZWR1bGUhXCI7XHJcblx0XHR9XHJcblx0XHRlbHNlIHtcclxuXHRcdFx0aWYgKGNoZWNrbGlzdC5zY2hlZHVsZT8ucmVwZWF0ICE9PSBcImRhaWx5XCIpIHJldHVybiBcIk5ldyBjaGVja2xpc3QgZG9lcyBub3QgcmVwZWF0IGRhaWx5ISBFeHBlY3RlZCByZXBlYXQgc2NoZWR1bGU6IGRhaWx5LCBSZWNlaXZlZCByZXBlYXQgc2NoZWR1bGU6IFwiICsgY2hlY2tsaXN0c1swXS5zY2hlZHVsZT8ucmVwZWF0O1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBlZGl0IGFuIGV4aXN0aW5nIGNoZWNrbGlzdFwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCB0aXRsZSA9IFwiVGVlIHNlZGEhXCI7XHJcblx0Y29uc3QgbmFtZSA9IFwiVGVnZXZ1c1wiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZywgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycCA9IGdyb3Vwc1swXS5faWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJjaGtfbmV3XCIsIHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFt7IG5hbWUsIHZhbDogXCJuZXdcIiwgc2VjdGlvbjogZmFsc2UsIGtleTogMSB9XSwgaXRlbXNfbGF0ZXN0OiAxLCBhY3RpdmF0aW9uOiB0aW1lc3RhbXAoKSAtIDIwMDAsIGR1cmF0aW9uOiA2MCAqIDYwICogMTAwMCB9IH0pO1xyXG5cdGNvbnN0IGNoZWNrbGlzdCA9IChhd2FpdCByZXF1ZXN0KFwiY2hrX2dldE9mR3JvdXBcIiwgeyBvcmcsIGdycCB9KSkuZGF0YSFbMF07XHJcblx0Y29uc3QgZWRpdGVkX3RpbWUgPSB0aW1lc3RhbXAoKSArIDIwMDA7XHJcblx0Y2hlY2tsaXN0LmFjdGl2YXRpb24gPSBlZGl0ZWRfdGltZTtcclxuXHRjaGVja2xpc3QuZHVyYXRpb24gPSAxNSAqIDYwICogMTAwMDtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwiY2hrX2VkaXRcIiwgeyBvcmcsIGRvYzogY2hlY2tsaXN0IH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gZWRpdCBhIGNoZWNrbGlzdC5cIik7XHJcblxyXG5cdGNvbnN0IHJlcV9nZXQgPSBhd2FpdCByZXF1ZXN0KFwiY2hrX2dldFwiLCB7IG9yZywgaWQ6IGNoZWNrbGlzdC5faWQgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcV9nZXQsIFwiVHJpZWQgdG8gYWNjZXNzIG1vZGlmaWVkIGNoZWNrbGlzdC5cIik7XHJcblx0aWYgKHJlcV9nZXQuZGF0YSEuc3RhdHVzID09PSBcImFjdGl2ZVwiKSByZXR1cm4gXCJDaGVja2xpc3QncyBzdGF0dXMgaXMgYWN0aXZlIVwiO1xyXG5cdGlmIChyZXFfZ2V0LmRhdGEhLmFjdGl2YXRpb24gIT09IGVkaXRlZF90aW1lKSByZXR1cm4gXCJBY3RpdmF0aW9uIHRpbWUgZGlkIG5vdCBjaGFuZ2UgY29ycmVjdGx5IVwiO1xyXG5cdGlmIChyZXFfZ2V0LmRhdGEhLmR1cmF0aW9uICE9PSBjaGVja2xpc3QuZHVyYXRpb24pIHJldHVybiBcIkR1cmF0aW9uIGRpZCBub3QgY2hhbmdlIGNvcnJlY3RseSFcIjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIlNlcnZlciBhY2NlcHRzIHBoYW50b20sIGJ1dCBkb2VzIG5vdCBzdG9yZSBpdFwiLCBcImV4dHJhXCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgdGl0bGUgPSBcIlRlZSBzZWRhIVwiO1xyXG5cdGNvbnN0IG5hbWUgPSBcIlRlZ2V2dXNcIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnAgPSBncm91cHNbMF0uX2lkO1xyXG5cclxuXHRjb25zdCBjaGtsaXN0ID0geyBvcmcsIGRvYzogeyBncnAsIHRpdGxlLCBpdGVtczogW3sgbmFtZSwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAwIH0gYXMgY29uc3RdLCBpdGVtc19sYXRlc3Q6IDAsIGlzX3BoYW50b206IHRydWUsIGFjdGl2YXRpb246IDEsIGR1cmF0aW9uOiAxIH0gfTtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwiY2hrX25ld1wiLCBjaGtsaXN0KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGNyZWF0ZSBhIG5ldyBjaGVja2xpc3Qgd2l0aCBhbiBpc19waGFudG9tIGZpZWxkLlwiKTtcclxuXHJcblx0Y29uc3QgY2hlY2tsaXN0czogQ2hlY2tsaXN0W10gPSAoYXdhaXQgcmVxdWVzdChcImNoa19nZXRPZkdyb3VwXCIsIHsgb3JnLCBncnAgfSkpLmRhdGEhO1xyXG5cdHJldHVybiAoY2hlY2tsaXN0c1swXS5pc19waGFudG9tICE9PSB1bmRlZmluZWQpID8gXCJTZXJ2ZXIgc3RvcmVkIGFuIGlzX3BoYW50b20gZmllbGQhXCIgOiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gcmVtb3ZlIHdoaXRlc3BhY2VzIGZyb20gY2hlY2tsaXN0J3MgdGl0bGVcIiwgXCJleHRyYVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGxldCB0aXRsZSA9IFwiIFRlZSBzZWRhIVwiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZywgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycCA9IGdyb3Vwc1swXS5faWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJjaGtfbmV3XCIsIHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFtdLCBhY3RpdmF0aW9uOiAxLCBkdXJhdGlvbjogMSwgaXRlbXNfbGF0ZXN0OiAwIH0gfSk7XHJcblxyXG5cdHRpdGxlID0gXCJUZWUgc2VkYSEgXCI7XHJcblx0YXdhaXQgcmVxdWVzdChcImNoa19uZXdcIiwgeyBvcmcsIGRvYzogeyBncnAsIHRpdGxlLCBpdGVtczogW10sIGFjdGl2YXRpb246IDEsIGR1cmF0aW9uOiAxLCBpdGVtc19sYXRlc3Q6IDAgfSB9KTtcclxuXHJcblx0dGl0bGUgPSBcIlRlZSBcXG5cXG5cXHRcXHRcXG4gc2VkYVwiO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJjaGtfbmV3XCIsIHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFtdLCBhY3RpdmF0aW9uOiAxLCBkdXJhdGlvbjogMSwgaXRlbXNfbGF0ZXN0OiAwIH0gfSk7XHJcblxyXG5cdGNvbnN0IGNoZWNrbGlzdHM6IENoZWNrbGlzdFtdID0gKGF3YWl0IHJlcXVlc3QoXCJjaGtfZ2V0T2ZHcm91cFwiLCB7IG9yZywgZ3JwIH0pKS5kYXRhITtcclxuXHRpZiAoIWNoZWNrbGlzdHNbMF0udGl0bGUuc3RhcnRzV2l0aChcIlRcIikpIHJldHVybiBcIlRoZSBsZWFkaW5nIHdoaXRlc3BhY2Ugd2FzIG5vdCByZW1vdmVkIGZyb20gY2hlY2tsaXN0J3MgdGl0bGUhXCI7XHJcblx0aWYgKCFjaGVja2xpc3RzWzFdLnRpdGxlLmVuZHNXaXRoKFwiIVwiKSkgcmV0dXJuIFwiVGhlIHRyYWlsaW5nIHdoaXRlc3BhY2Ugd2FzIG5vdCByZW1vdmVkIGZyb20gY2hlY2tsaXN0J3MgdGl0bGUhXCI7XHJcblx0aWYgKGNoZWNrbGlzdHNbMl0udGl0bGUuaW5jbHVkZXMoXCJcXG5cIikgfHwgY2hlY2tsaXN0c1syXS50aXRsZS5pbmNsdWRlcyhcIlxcdFwiKSkgcmV0dXJuIFwiTmV3bGluZXMgd2VyZSBub3QgcmVtb3ZlZCBmcm9tIGNoZWNrbGlzdCdzIHRpdGxlIVwiO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KG5hbWVEaXJ0eUlucHV0VGVzdChcImNoa19uZXdcIiksIFwic3RhYmlsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgZGF5cyA9IHsgbW9uOiB0cnVlLCB0dWU6IHRydWUsIHdlZDogdHJ1ZSwgdGh1OiB0cnVlLCBmcmk6IHRydWUsIHNhdDogdHJ1ZSwgc3VuOiB0cnVlIH07XHJcblx0Y29uc3QgdGl0bGUgPSBcIlRlZSBzZWRhIVwiO1xyXG5cdGNvbnN0IG5hbWUgPSBcIlRlZ2V2dXNcIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnAgPSBncm91cHNbMF0uX2lkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiY2hrX25ld1wiLCB7IG9yZywgZG9jOiB7IGdycCwgdGl0bGUsIGl0ZW1zOiBbeyBuYW1lLCB2YWw6IFwibmV3XCIsIHNlY3Rpb246IGZhbHNlLCBrZXk6IDAgfV0sIGl0ZW1zX2xhdGVzdDogMCwgYWN0aXZhdGlvbjogMSwgZHVyYXRpb246IDAsIHNjaGVkdWxlOiB7IHJlcGVhdDogXCJkYWlseVwiLCBkYXlzIH0gfSB9KTtcclxuXHJcblx0bGV0IGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcImNoa19uZXcgd2FzIGFjY2VwdGVkIHdpdGggZGlydHkgcGFyYW1ldGVycyFcIixcclxuXHRcdHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFt7IG5hbWUsIHZhbDogXCJuZXdcIiwgc2VjdGlvbjogZmFsc2UsIGtleTogMCB9IGFzIGNvbnN0XSwgaXRlbXNfbGF0ZXN0OiAwLCBhY3RpdmF0aW9uOiAxLCBkdXJhdGlvbjogMCwgc2NoZWR1bGU6IHsgcmVwZWF0OiBcImRhaWx5XCIsIGRheXMgfSBhcyBjb25zdCB9IH0sXHJcblx0XHQoZGlydHlfb2JqZWN0KSA9PiBbXCJjaGtfbmV3XCIsIGRpcnR5X29iamVjdF0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcImNoa19uZXcgd2FzIGFjY2VwdGVkIHdpdGggZGlydHkgcGFyYW1ldGVycyFcIixcclxuXHRcdHsgbmFtZSwgc2VjdGlvbjogZmFsc2UsIGtleTogMCwgdmFsOiBcIm5ld1wiIH0gYXMgY29uc3QsXHJcblx0XHQoZGlydHlfb2JqZWN0KSA9PiBbXCJjaGtfbmV3XCIsIHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFtkaXJ0eV9vYmplY3RdLCBpdGVtc19sYXRlc3Q6IDAsIGFjdGl2YXRpb246IDEsIGR1cmF0aW9uOiAwLCBzY2hlZHVsZTogeyByZXBlYXQ6IFwiZGFpbHlcIiwgZGF5cyB9IH0gfV0sIHsga2V5OiBbMTIzXSwgdmFsOiBbbnVsbCwgXCJcIiwgMTIzLCB0cnVlLCBmYWxzZSwgW10sIHt9LCB1bmRlZmluZWRdIH0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcImNoa19uZXcgd2FzIGFjY2VwdGVkIHdpdGggZGlydHkgcGFyYW1ldGVycyFcIixcclxuXHRcdHsgcmVwZWF0OiBcImRhaWx5XCIsIGRheXMgfSBhcyBjb25zdCxcclxuXHRcdChkaXJ0eV9vYmplY3QpID0+IFtcImNoa19uZXdcIiwgeyBvcmcsIGRvYzogeyBncnAsIHRpdGxlLCBpdGVtczogW3sgbmFtZSwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAwIH1dLCBpdGVtc19sYXRlc3Q6IDAsIGFjdGl2YXRpb246IDEsIGR1cmF0aW9uOiAwLCBzY2hlZHVsZTogZGlydHlfb2JqZWN0IH0gfV0sXHJcblx0XHR7IHbDtXRpOiBbbnVsbCwgdW5kZWZpbmVkXSB9KTtcclxuXHRpZiAoZGlydHlfcmVzdWx0ICE9PSB0cnVlKSByZXR1cm4gZGlydHlfcmVzdWx0O1xyXG5cclxuXHRkaXJ0eV9yZXN1bHQgPSBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJjaGtfbmV3IHdhcyBhY2NlcHRlZCB3aXRoIGRpcnR5IHBhcmFtZXRlcnMhXCIsXHJcblx0XHR7IHJlcGVhdDogXCJtb250aGx5XCIsIGRheTogMSwgZnJvbV9lbmQ6IGZhbHNlIH0gYXMgY29uc3QsXHJcblx0XHQoZGlydHlfb2JqZWN0KSA9PiBbXCJjaGtfbmV3XCIsIHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFt7IG5hbWUsIHZhbDogXCJuZXdcIiwgc2VjdGlvbjogZmFsc2UsIGtleTogMCB9XSwgaXRlbXNfbGF0ZXN0OiAwLCBhY3RpdmF0aW9uOiAxLCBkdXJhdGlvbjogMCwgc2NoZWR1bGU6IGRpcnR5X29iamVjdCB9IH1dLFxyXG5cdFx0eyB2w7V0aTogW251bGwsIHVuZGVmaW5lZF0sIGZyb21fZW5kOiBbZmFsc2UsIHRydWVdIH0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdGNvbnN0IGRheXNfZXhjbHVzaW9uX21hcCA9IHt9IGFzIGFueTtcclxuXHRmb3IgKGNvbnN0IGRheSBpbiBkYXlzKSB7XHJcblx0XHRkYXlzX2V4Y2x1c2lvbl9tYXBbZGF5XSA9IFtmYWxzZSwgdHJ1ZV07XHJcblx0fVxyXG5cdGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcImNoa19uZXcgd2FzIGFjY2VwdGVkIHdpdGggZGlydHkgcGFyYW1ldGVycyFcIiwgZGF5cyxcclxuXHRcdChkaXJ0eV9vYmplY3QpID0+IFtcImNoa19uZXdcIiwgeyBvcmcsIGRvYzogeyBncnAsIHRpdGxlLCBpdGVtczogW3sgbmFtZSwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAwIH1dLCBpdGVtc19sYXRlc3Q6IDAsIGFjdGl2YXRpb246IDEsIGR1cmF0aW9uOiAwLCBzY2hlZHVsZTogeyByZXBlYXQ6IFwiZGFpbHlcIiwgZGF5czogZGlydHlfb2JqZWN0IH0gfSB9XSxcclxuXHRcdGRheXNfZXhjbHVzaW9uX21hcCk7XHJcblx0aWYgKGRpcnR5X3Jlc3VsdCAhPT0gdHJ1ZSkgcmV0dXJuIGRpcnR5X3Jlc3VsdDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImNoa19uZXdcIiwgeyBvcmcsIGRvYzogeyBncnAsIHRpdGxlLCBpdGVtczogW3sgbmFtZSwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAwIH1dLCBpdGVtc19sYXRlc3Q6IDAsIGFjdGl2YXRpb246IDEsIGR1cmF0aW9uOiAxLCBzY2hlZHVsZTogeyByZXBlYXQ6IFwiZGFpbHlcIiwgZGF5cyB9IH0gfSk7XHJcblxyXG5cdGNvbnN0IGNoZWNrbGlzdHM6IENoZWNrbGlzdFtdID0gKGF3YWl0IHJlcXVlc3QoXCJjaGtfZ2V0T2ZHcm91cFwiLCB7IG9yZywgZ3JwIH0pKS5kYXRhITtcclxuXHRjb25zdCBfaWQgPSBjaGVja2xpc3RzWzFdLl9pZDtcclxuXHRjb25zdCBhY3RpdmF0aW9uID0gY2hlY2tsaXN0c1sxXS5hY3RpdmF0aW9uO1xyXG5cdGNvbnN0IG92ZXJyaWRlc19waGFudG9tID0geyBfaWQsIGFjdGl2YXRpb24gfTtcclxuXHJcblx0ZGlydHlfcmVzdWx0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiY2hrX25ldyB3aXRoIG92ZXJyaWRlc19waGFudG9tIGFjY2VwdGVkIGRpcnR5IGlucHV0IVwiLCBvdmVycmlkZXNfcGhhbnRvbSwgKGRpcnR5X2lucHV0KSA9PlxyXG5cdFx0W1wiY2hrX25ld1wiLCB7IG9yZywgZG9jOiB7IGdycCwgdGl0bGUsIGl0ZW1zOiBbeyBuYW1lLCB2YWw6IFwibmV3XCIsIHNlY3Rpb246IGZhbHNlLCBrZXk6IDAgfV0sIGl0ZW1zX2xhdGVzdDogMCwgYWN0aXZhdGlvbiwgZHVyYXRpb246IDIsIG92ZXJyaWRlc19waGFudG9tOiBkaXJ0eV9pbnB1dCB9IH1dLFxyXG5cdHsgdsO1dGk6IFtudWxsLCB1bmRlZmluZWRdIH0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBvdmVycmlkZSBjaGVja2xpc3QgcGhhbnRvbXNcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgZGF5cyA9IHsgbW9uOiB0cnVlLCB0dWU6IHRydWUsIHdlZDogdHJ1ZSwgdGh1OiB0cnVlLCBmcmk6IHRydWUsIHNhdDogdHJ1ZSwgc3VuOiB0cnVlIH07XHJcblx0Y29uc3QgdGl0bGUgPSBcIlRlZSBzZWRhIVwiO1xyXG5cdGNvbnN0IG5hbWUgPSBcIlRlZ2V2dXNcIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnAgPSBncm91cHNbMF0uX2lkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiY2hrX25ld1wiLCB7IG9yZywgZG9jOiB7IGdycCwgdGl0bGUsIGl0ZW1zOiBbeyBuYW1lLCB2YWw6IFwibmV3XCIsIHNlY3Rpb246IGZhbHNlLCBrZXk6IDAgfV0sIGl0ZW1zX2xhdGVzdDogMCwgYWN0aXZhdGlvbjogdGltZXN0YW1wKCkgKyAxMDBfMDAwLCBkdXJhdGlvbjogMSwgc2NoZWR1bGU6IHsgcmVwZWF0OiBcImRhaWx5XCIsIGRheXMgfSB9IH0pO1xyXG5cclxuXHRjb25zdCBjaGVja2xpc3RzOiBDaGVja2xpc3RbXSA9IChhd2FpdCByZXF1ZXN0KFwiY2hrX2dldE9mR3JvdXBcIiwgeyBvcmcsIGdycCB9KSkuZGF0YSE7XHJcblx0Y29uc3QgX2lkID0gY2hlY2tsaXN0c1swXS5faWQ7XHJcblx0Y29uc3QgYWN0aXZhdGlvbiA9IGNoZWNrbGlzdHNbMF0uYWN0aXZhdGlvbjtcclxuXHRjb25zdCBvdmVycmlkZXNfcGhhbnRvbSA9IHsgX2lkLCBhY3RpdmF0aW9uIH07XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJjaGtfbmV3XCIsIHsgb3JnLCBkb2M6IHsgZ3JwLCB0aXRsZSwgaXRlbXM6IFt7IG5hbWUsIHZhbDogXCJuZXdcIiwgc2VjdGlvbjogZmFsc2UsIGtleTogMCB9XSwgaXRlbXNfbGF0ZXN0OiAwLCBhY3RpdmF0aW9uLCBkdXJhdGlvbjogMiwgb3ZlcnJpZGVzX3BoYW50b20gfSB9KTtcclxuXHJcblx0Y29uc3QgcmVxX3JlcyA9IChhd2FpdCByZXF1ZXN0KFwiY2hrX2dldE9mR3JvdXBcIiwgeyBvcmcsIGdycCB9KSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcV9yZXMsIFwiVHJpZWQgdG8gZ2V0IGFsbCByZXN1bHRzIGluIGdyb3VwLCB3aGVyZSBvdmVycmlkZXNfcGhhbnRvbSBpcyBkZWZpbmVkLlwiKTtcclxuXHJcblx0Y29uc3QgY2hlY2tsaXN0ID0gcmVxX3Jlcy5kYXRhIS5wb3AoKTtcclxuXHRpZiAoY2hlY2tsaXN0IS5kdXJhdGlvbiAhPT0gMikgcmV0dXJuIFwiQ2hlY2tsaXN0IGR1cmF0aW9uIHdhcyBub3QgY29ycmVjdC4gRXhwZWN0ZWQ6IDIsIFJlY2VpdmVkOiBcIiArIGNoZWNrbGlzdCEuZHVyYXRpb247XHJcblx0aWYgKCFjaGVja2xpc3QhLm92ZXJyaWRlc19waGFudG9tKSByZXR1cm4gXCJvdmVycmlkZXNfcGhhbnRvbSBkb2VzIG5vdCBleGlzdCBvbiBjaGVja2xpc3QhIFJlY2VpdmVkOiBcIiArIEpTT04uc3RyaW5naWZ5KGNoZWNrbGlzdCk7XHJcblx0aWYgKGNoZWNrbGlzdCEub3ZlcnJpZGVzX3BoYW50b20uX2lkICE9PSBfaWQpIHJldHVybiBcIm92ZXJyaWRlc19waGFudG9tIHJlY2VpdmVkIGFuIGluY29ycmVjdCBpZCEgRXhwZWN0ZWQ6IFwiICsgX2lkICsgXCIsIFJlY2VpdmVkOiBcIiArIGNoZWNrbGlzdCEub3ZlcnJpZGVzX3BoYW50b20uX2lkO1xyXG5cdGlmIChjaGVja2xpc3QhLm92ZXJyaWRlc19waGFudG9tLmFjdGl2YXRpb24gIT09IGFjdGl2YXRpb24pIHJldHVybiBcIm92ZXJyaWRlc19waGFudG9tIHJlY2VpdmVkIGFuIGluY29ycmVjdCBhY3RpdmF0aW9uISBFeHBlY3RlZDogXCIgKyBhY3RpdmF0aW9uICsgXCIsIFJlY2VpdmVkOiBcIiArIGNoZWNrbGlzdCEub3ZlcnJpZGVzX3BoYW50b20uYWN0aXZhdGlvbjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBzZXQgY2hlY2tsaXN0IGl0ZW0gc3RhdHVzXCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHRpdGxlID0gXCJUZWUgc2VkYSFcIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnAgPSBncm91cHNbMF0uX2lkO1xyXG5cclxuXHRjb25zdCBjaGtsaXN0ID0ge1xyXG5cdFx0b3JnLCBkb2M6IHtcclxuXHRcdFx0Z3JwLCB0aXRsZSwgaXRlbXM6IFtcclxuXHRcdFx0XHR7IG5hbWU6IFwidGVyZVwiLCB2YWw6IFwibmV3XCIsIHNlY3Rpb246IGZhbHNlLCBrZXk6IDEgfSBhcyBjb25zdCxcclxuXHRcdFx0XHR7IG5hbWU6IFwiaGVsbG9cIiwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAyIH0gYXMgY29uc3QsXHJcblx0XHRcdFx0eyBuYW1lOiBcInByaXZqZXRcIiwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAzIH0gYXMgY29uc3QsXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0bmFtZTogXCJrb25uaWNoaXdhIHV3dVwiLCB2YWw6IFwibmV3XCIsIHNlY3Rpb246IHRydWUsIGtleTogNCwgdGFza3M6IFtcclxuXHRcdFx0XHRcdFx0eyBuYW1lOiBcInNlbnBhaVwiLCB2YWw6IFwibmV3XCIsIHNlY3Rpb246IGZhbHNlLCBrZXk6IDUgfSBhcyBjb25zdCxcclxuXHRcdFx0XHRcdFx0eyBuYW1lOiBcInNhblwiLCB2YWw6IFwibmV3XCIsIHNlY3Rpb246IGZhbHNlLCBrZXk6IDYgfSBhcyBjb25zdCxcclxuXHRcdFx0XHRcdFx0eyBuYW1lOiBcImNoYW5cIiwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiA3IH0gYXMgY29uc3QsXHJcblx0XHRcdFx0XHRdLCBkZXNjcmlwdGlvbjogW1wiSmFwYW5lc2UgZ2liYmVyaXNoXCJdLFxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdF0sIGl0ZW1zX2xhdGVzdDogMSwgYWN0aXZhdGlvbjogMSwgZHVyYXRpb246IDEsXHJcblx0XHR9IGFzIENoZWNrbGlzdEZvcm0sXHJcblx0fTtcclxuXHRhd2FpdCByZXF1ZXN0KFwiY2hrX25ld1wiLCBjaGtsaXN0KTtcclxuXHJcblx0Y29uc3QgY2hlY2tsaXN0czogQ2hlY2tsaXN0W10gPSAoYXdhaXQgcmVxdWVzdChcImNoa19nZXRPZkdyb3VwXCIsIHsgb3JnLCBncnAgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGNoZWNrbGlzdCA9IGNoZWNrbGlzdHNbMF07XHJcblxyXG5cdC8vIGluaXRpYWwgc2V0IGl0ZW0gc3RhdHVzXHJcblx0bGV0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJjaGtfc2V0SXRlbVN0YXR1c1wiLCB7IG9yZywgaWQ6IGNoZWNrbGlzdC5faWQsIGluZGV4OiAwLCBzdGF0dXM6IFwiZG9uZVwiIGFzIGNvbnN0LCBzZWN0aW9uOiAtMSB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIHNldCBjaGVja2xpc3QgaXRlbSBzdGF0dXMgYXMgXFxcImRvbmVcXFwiLlwiKTtcclxuXHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImNoa19zZXRJdGVtU3RhdHVzXCIsIHsgb3JnLCBpZDogY2hlY2tsaXN0Ll9pZCwgaW5kZXg6IDEsIHN0YXR1czogXCJmYWlsXCIgYXMgY29uc3QsIHNlY3Rpb246IC0xIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gc2V0IGNoZWNrbGlzdCBpdGVtIHN0YXR1cyBhcyBcXFwiZmFpbFxcXCIuXCIpO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiY2hrX3NldEl0ZW1TdGF0dXNcIiwgeyBvcmcsIGlkOiBjaGVja2xpc3QuX2lkLCBpbmRleDogMiwgc3RhdHVzOiBcImRvbmVcIiBhcyBjb25zdCwgc2VjdGlvbjogLTEgfSk7XHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImNoa19zZXRJdGVtU3RhdHVzXCIsIHsgb3JnLCBpZDogY2hlY2tsaXN0Ll9pZCwgaW5kZXg6IDIsIHN0YXR1czogXCJuZXdcIiBhcyBjb25zdCwgc2VjdGlvbjogLTEgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBzZXQgY2hlY2tsaXN0IGl0ZW0gc3RhdHVzIGFzIFxcXCJuZXdcXFwiLlwiKTtcclxuXHJcblx0Ly8gc2VjdGlvblxyXG5cdHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJjaGtfc2V0SXRlbVN0YXR1c1wiLCB7IG9yZywgaWQ6IGNoZWNrbGlzdC5faWQsIGluZGV4OiAwLCBzdGF0dXM6IFwiZG9uZVwiIGFzIGNvbnN0LCBzZWN0aW9uOiAzIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gc2V0IGNoZWNrbGlzdCBpdGVtIHN0YXR1cyBhcyBcXFwiZG9uZVxcXCIgd2l0aGluIHNlY3Rpb24uXCIpO1xyXG5cclxuXHRyZXEgPSBhd2FpdCByZXF1ZXN0KFwiY2hrX3NldEl0ZW1TdGF0dXNcIiwgeyBvcmcsIGlkOiBjaGVja2xpc3QuX2lkLCBpbmRleDogMSwgc3RhdHVzOiBcImZhaWxcIiBhcyBjb25zdCwgc2VjdGlvbjogMyB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIHNldCBjaGVja2xpc3QgaXRlbSBzdGF0dXMgYXMgXFxcImZhaWxcXFwiIHdpdGhpbiBzZWN0aW9uLlwiKTtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImNoa19zZXRJdGVtU3RhdHVzXCIsIHsgb3JnLCBpZDogY2hlY2tsaXN0Ll9pZCwgaW5kZXg6IDIsIHN0YXR1czogXCJkb25lXCIgYXMgY29uc3QsIHNlY3Rpb246IDMgfSk7XHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImNoa19zZXRJdGVtU3RhdHVzXCIsIHsgb3JnLCBpZDogY2hlY2tsaXN0Ll9pZCwgaW5kZXg6IDIsIHN0YXR1czogXCJuZXdcIiBhcyBjb25zdCwgc2VjdGlvbjogMyB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIHNldCBjaGVja2xpc3QgaXRlbSBzdGF0dXMgYXMgXFxcIm5ld1xcXCIgd2l0aGluIHNlY3Rpb24uXCIpO1xyXG5cclxuXHQvLyBmaW5hbCBjaGVja1xyXG5cdGNvbnN0IGNoZWNrbGlzdF9ncm91cCA9IChhd2FpdCByZXF1ZXN0KFwiY2hrX2dldE9mR3JvdXBcIiwgeyBvcmcsIGdycCB9KSkuZGF0YSFbMF07XHJcblx0aWYgKGNoZWNrbGlzdF9ncm91cC5pdGVtc1swXT8udmFsICE9PSBcImRvbmVcIikgcmV0dXJuIFwiQ291bGQgbm90IGFjdHVhbGx5IHNldCBjaGVja2xpc3QgaXRlbSB2YWx1ZSBzdGF0dXMgYXMgXFxcImRvbmVcXFwiIVwiO1xyXG5cdGlmIChjaGVja2xpc3RfZ3JvdXAuaXRlbXNbMV0/LnZhbCAhPT0gXCJmYWlsXCIpIHJldHVybiBcIkNvdWxkIG5vdCBhY3R1YWxseSBzZXQgY2hlY2tsaXN0IGl0ZW0gdmFsdWUgc3RhdHVzIGFzIFxcXCJmYWlsXFxcIiFcIjtcclxuXHRpZiAoY2hlY2tsaXN0X2dyb3VwLml0ZW1zWzJdPy52YWwgIT09IFwibmV3XCIpIHJldHVybiBcIkNvdWxkIG5vdCBhY3R1YWxseSBzZXQgY2hlY2tsaXN0IGl0ZW0gdmFsdWUgc3RhdHVzIGFzIFxcXCJuZXdcXFwiIVwiO1xyXG5cdGlmICgoY2hlY2tsaXN0X2dyb3VwLml0ZW1zWzNdIGFzIENoZWNrbGlzdFNlY3Rpb24pPy50YXNrc1swXT8udmFsICE9PSBcImRvbmVcIikgcmV0dXJuIFwiQ291bGQgbm90IGFjdHVhbGx5IHNldCBjaGVja2xpc3QgaXRlbSB2YWx1ZSBzdGF0dXMgYXMgXFxcImRvbmVcXFwiIHdpdGhpbiBzZWN0aW9uIVwiO1xyXG5cdGlmICgoY2hlY2tsaXN0X2dyb3VwLml0ZW1zWzNdIGFzIENoZWNrbGlzdFNlY3Rpb24pPy50YXNrc1sxXT8udmFsICE9PSBcImZhaWxcIikgcmV0dXJuIFwiQ291bGQgbm90IGFjdHVhbGx5IHNldCBjaGVja2xpc3QgaXRlbSB2YWx1ZSBzdGF0dXMgYXMgXFxcImZhaWxcXFwiIHdpdGhpbiBzZWN0aW9uIVwiO1xyXG5cdGlmICgoY2hlY2tsaXN0X2dyb3VwLml0ZW1zWzNdIGFzIENoZWNrbGlzdFNlY3Rpb24pPy50YXNrc1syXT8udmFsICE9PSBcIm5ld1wiKSByZXR1cm4gXCJDb3VsZCBub3QgYWN0dWFsbHkgc2V0IGNoZWNrbGlzdCBpdGVtIHZhbHVlIHN0YXR1cyBhcyBcXFwibmV3XFxcIiB3aXRoaW4gc2VjdGlvbiFcIjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChuYW1lRGlydHlJbnB1dFRlc3QoXCJjaGtfc2V0SXRlbVN0YXR1c1wiKSwgXCJzdGFiaWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCB0aXRsZSA9IFwiVGVlIHNlZGEhXCI7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lOiBcIm5ldyBvcmdcIiwgdGltZXpvbmU6IFwiRXVyb3BlL1RhbGxpbm5cIiwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnLCBuYW1lOiBcIm5ldyBncnBcIiB9KTtcclxuXHRjb25zdCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZyB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwID0gZ3JvdXBzWzBdLl9pZDtcclxuXHJcblx0Y29uc3QgY2hrbGlzdCA9IHtcclxuXHRcdG9yZywgZG9jOiB7XHJcblx0XHRcdGdycCwgdGl0bGUsIGl0ZW1zOiBbXHJcblx0XHRcdFx0eyBuYW1lOiBcInRhc2tcIiwgdmFsOiBcIm5ld1wiLCBzZWN0aW9uOiBmYWxzZSwga2V5OiAxIH0gYXMgY29uc3QsXHJcblx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0bmFtZTogXCJzZWN0aW9uXCIsIHZhbDogXCJuZXdcIiwgc2VjdGlvbjogdHJ1ZSwga2V5OiAyLCBkZXNjcmlwdGlvbjogW1wic2VjdGlvblwiXSBhcyBzdHJpbmdbXSxcclxuXHRcdFx0XHRcdHRhc2tzOiBbeyBuYW1lOiBcInRhc2sxXCIsIHZhbDogXCJuZXdcIiwgc2VjdGlvbjogZmFsc2UsIGtleTogMyB9XSBhcyBDaGVja2xpc3RUYXNrW10sXHJcblx0XHRcdFx0fSBhcyBjb25zdCxcclxuXHRcdFx0XSwgaXRlbXNfbGF0ZXN0OiAxLCBhY3RpdmF0aW9uOiAxLCBkdXJhdGlvbjogMSxcclxuXHRcdH0sXHJcblx0fTtcclxuXHRhd2FpdCByZXF1ZXN0KFwiY2hrX25ld1wiLCBjaGtsaXN0KTtcclxuXHJcblx0Y29uc3QgY2hlY2tsaXN0czogQ2hlY2tsaXN0W10gPSAoYXdhaXQgcmVxdWVzdChcImNoa19nZXRPZkdyb3VwXCIsIHsgb3JnLCBncnAgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGNoZWNrbGlzdCA9IGNoZWNrbGlzdHNbMF07XHJcblxyXG5cdGxldCBkaXJ0eV9yZXN1bHQgPSBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXHJcblx0XHRcIlRoZXJlIHdhcyBhIHByb2JsZW0gdHJ5aW5nIHRvIHNldCBjaGVja2xpc3QgdGFzayBhcyBjb21wbGV0ZWQhXCIsXHJcblx0XHR7IG9yZywgaWQ6IGNoZWNrbGlzdC5faWQsIGluZGV4OiAwLCBzdGF0dXM6IFwiZG9uZVwiIGFzIGNvbnN0LCBzZWN0aW9uOiAtMSB9LFxyXG5cdFx0KGRpcnR5X29iamVjdCkgPT4gW1wiY2hrX3NldEl0ZW1TdGF0dXNcIiwgZGlydHlfb2JqZWN0XVxyXG5cdCk7XHJcblx0aWYgKGRpcnR5X3Jlc3VsdCAhPT0gdHJ1ZSkgcmV0dXJuIGRpcnR5X3Jlc3VsdDtcclxuXHJcblx0ZGlydHlfcmVzdWx0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFxyXG5cdFx0XCJUaGVyZSB3YXMgYSBwcm9ibGVtIHRyeWluZyB0byBzZXQgY2hlY2tsaXN0IHRhc2sgc3RhdHVzIHdpdGhpbiBzZWN0aW9uIGFzIGNvbXBsZXRlZCFcIixcclxuXHRcdHsgb3JnLCBpZDogY2hlY2tsaXN0Ll9pZCwgaW5kZXg6IDAsIHN0YXR1czogXCJkb25lXCIgYXMgY29uc3QsIHNlY3Rpb246IDEgfSxcclxuXHRcdChkaXJ0eV9vYmplY3QpID0+IFtcImNoa19zZXRJdGVtU3RhdHVzXCIsIGRpcnR5X29iamVjdF1cclxuXHQpO1xyXG5cdHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcbn0pO1xyXG4iLCJpbXBvcnQgeyBjcmVhdGVUZXN0LCByZXF1ZXN0LCBzZXRCZWZvcmVFYWNoIH0gZnJvbSBcIi4uL3JlcXVlc3RfdGVzdFwiO1xyXG5pbXBvcnQgeyBJZCB9IGZyb20gXCJAL2RhdGEvdVwiO1xyXG5pbXBvcnQgeyBjcmVhdGVVc2VyIH0gZnJvbSBcIi4uL3JlcXVlc3RfdXRpbHNcIjtcclxuaW1wb3J0IHsgc2hpZnRUaW1lLCB0aW1lc3RhbXAsIHZlcmlmeVJlcXVlc3RBY2NlcHRlZCB9IGZyb20gXCIuLi91dGlsc1wiO1xyXG5cclxuc2V0QmVmb3JlRWFjaChhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IHJlcXVlc3QoZmFsc2UpO1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCwgXCJ1c2VyXCIpO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gY3JlYXRlIGEgZ3JvdXBcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgdXNlcl9vYmogPSB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9O1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB1c2VyX29iaik7XHJcblxyXG5cdGNvbnN0IG5hbWUgPSBcIm15IG5ldyBncnBcIjtcclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lOiBcIm5ldyBvcmdcIiwgdGltZXpvbmU6IFwiRXVyb3BlL1RhbGxpbm5cIiwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWUgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBjcmVhdGUgYSBuZXcgZ3JwLlwiKTtcclxuXHJcblx0Y29uc3QgZ3JwID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhWzBdO1xyXG5cdGlmIChncnAgPT0gdW5kZWZpbmVkKSByZXR1cm4gXCJHcm91cCB3YXMgYWN0dWFsbHkgbm90IGNyZWF0ZWQhXCI7XHJcblx0aWYgKGdycC5uYW1lICE9PSBuYW1lKSByZXR1cm4gXCJHcm91cCB3YXMgbm90IG5hbWVkIGNvcnJlY3RseSFcIjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBhIGdyb3VwIGJlIGFza2VkIHRvIGJlIGRlbGV0ZWRcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgdXNlcl9vYmogPSB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9O1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB1c2VyX29iaik7XHJcblxyXG5cdGNvbnN0IG9yZyA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZywgbmFtZTogXCJuZXcgZ3JwIGluc3RhIGRlbGV0ZVwiIH0pO1xyXG5cclxuXHRsZXQgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycCA9IGdyb3Vwc1swXS5faWQ7XHJcblxyXG5cdC8vIG1hcmsgZm9yIGRlbGV0aW9uXHJcblx0bGV0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJncnBfZGVsZXRlXCIsIHsgb3JnLCBncnAsIGNvbmZpcm06IHRydWUgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBtYXJrIGEgZ3JwIHRvIGJlIGRlbGV0ZWQuXCIpO1xyXG5cclxuXHRncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZyB9KSkuZGF0YSE7XHJcblx0aWYgKGdyb3Vwc1swXS5tZmQgPCB0aW1lc3RhbXAoKSArIDEuOCAqIDg2XzQwMCAqIDEwMDApIHJldHVybiBcIkdyb3VwIHdhcyBub3QgbWFya2VkIHRvIGJlIGRlbGV0ZWQgYXQgYSBjb3JyZWN0IHRpbWUhXFxuRXhwZWN0ZWQ6IFwiICtcclxuXHRcdG5ldyBEYXRlKHRpbWVzdGFtcCgpICsgMS44ICogODZfNDAwICogMTAwMCkgKyBcIiBvciBsYXRlclxcblJlY2VpdmVkOiBcIiArXHJcblx0XHRuZXcgRGF0ZShncm91cHNbMF0ubWZkKTtcclxuXHJcblx0Ly8gdW5tYXJrIGZvciBkZWxldGlvblxyXG5cdHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJncnBfZGVsZXRlXCIsIHsgb3JnLCBncnAsIGNvbmZpcm06IGZhbHNlIH0pO1xyXG5cdGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGNhbmNlbCBhIGdycCBmcm9tIGJlaW5nIGRlbGV0ZWQuXCIpO1xyXG5cclxuXHRpZiAoZ3JvdXBzWzBdLm1mZCAhPT0gMCkgcmV0dXJuIFwiR3JvdXAgd2FzIG5vdCB1bm1hcmtlZCB0byBiZSBkZWxldGVkIVwiO1xyXG5cclxuXHQvLyB0ZXN0IGFjdHVhbCBkZWxldGlvbiBieSBoYWNraW5nIHRpbWVcclxuXHRyZXEgPSBhd2FpdCByZXF1ZXN0KFwiZ3JwX2RlbGV0ZVwiLCB7IG9yZywgZ3JwLCBjb25maXJtOiB0cnVlIH0pO1xyXG5cclxuXHRncm91cHMgPSBhd2FpdCBzaGlmdFRpbWUodXNlcl9vYmosIHRpbWVzdGFtcCgpICsgMyAqIDg2XzQwMCAqIDEwMDAsIGFzeW5jICgpID0+IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhISk7XHJcblx0aWYgKGdyb3Vwc1swXSAhPSB1bmRlZmluZWQpIHJldHVybiBcIkdyb3VwIHdhcyBub3QgYWN0dWFsbHkgZGVsZXRlZCFcIjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBkZWxldGUgdGlja2V0cyBmcm9tIG9uZSBncm91cCB3aXRob3V0IGRlbGV0aW5nIHRpY2tldHMgZnJvbSBvdGhlciBncm91cFwiLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHRpdGxlID0gXCJ0ZWluZSBwcm9ibGVlbVwiO1xyXG5cdGNvbnN0IHRleHQgPSBcImVyYWtvcmRzZWx0IHRlaW5lIHByb2JsZWVtXCI7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmdfaWQgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lOiBcIm5ldyBvcmdcIiwgdGltZXpvbmU6IFwiRXVyb3BlL1RhbGxpbm5cIiwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmc6IG9yZ19pZCwgbmFtZTogXCJuZXcgZ3JwIGluc3RhIGRlbGV0ZVwiIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnOiBvcmdfaWQsIG5hbWU6IFwiaXJyZWxldmFudCBncnBcIiB9KTtcclxuXHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmc6IG9yZ19pZCB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwX2lkMSA9IGdyb3Vwc1swXS5faWQ7XHJcblx0Y29uc3QgZ3JwX2lkMiA9IGdyb3Vwc1sxXS5faWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkMSwgaXNfZ2xvYmFsOiBmYWxzZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZTogXCJlc2ltZW5lIHByb2JsZWVtXCIsIHRleHQ6IFwiZXJha29yZHNlbHQgZXNpbWVuZSBwcm9ibGVlbVwiIH0sIG9yZzogb3JnX2lkIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkMiwgaXNfZ2xvYmFsOiBmYWxzZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZSwgdGV4dCB9LCBvcmc6IG9yZ19pZCB9KTtcclxuXHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcImdycF9kZWxldGVcIiwgeyBvcmc6IG9yZ19pZCwgZ3JwOiBncnBfaWQxLCBjb25maXJtOiB0cnVlIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gZGVsZXRlIHRpY2tldHMgZnJvbSBmaXJzdCBncm91cCFcIik7XHJcblxyXG5cdGNvbnN0IHJlcV90a3QgPSAoYXdhaXQgcmVxdWVzdChcInRrdF9nZXRPZkdyb3VwXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogZ3JwX2lkMiB9KSkuZGF0YSE7XHJcblx0cmV0dXJuIChyZXFfdGt0WzBdLnRpdGxlICE9PSB0aXRsZSB8fCByZXFfdGt0WzBdLmNvbW1lbnRzWzBdLnRleHRbMF0gIT09IHRleHQpID8gXCJEZWxldGluZyBhIGdyb3VwIGNhdXNlZCB0aGUgZW50cmllcyBmcm9tIGFub3RoZXIgZ3JvdXAgdG8gYWxzbyBiZSBkZWxldGVkIVwiIDogdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIHJlbmFtZSBhIGdyb3VwXCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IG5hbWUgPSBcIm5ldyBncm91cFwiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnLCBuYW1lOiBcIm5ldyBncnVwXCIgfSk7XHJcblxyXG5cdGxldCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZyB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwID0gZ3JvdXBzWzBdLl9pZDtcclxuXHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcImdycF9yZW5hbWVcIiwgeyBvcmcsIGdycCwgbmFtZSB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIHJlbmFtZSBhIGdycC5cIik7XHJcblxyXG5cdGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHRjb25zdCBjaGFuZ2VkID0gZ3JvdXBzWzBdLm5hbWU7XHJcblx0XHJcblx0cmV0dXJuIChuYW1lICE9PSBjaGFuZ2VkKSA/IFwiUmVxdWVzdCBncnBfcmVuYW1lIGRpZCBub3QgY2hhbmdlIHRoZSBuYW1lIG9mIHRoZSBncm91cCFcXG5SZWNlaXZlZDogXCIgKyBjaGFuZ2VkICsgXCJcXG5FeHBlY3RlZDogXCIgKyBuYW1lIDogdHJ1ZTtcclxufSk7XHJcbiIsImltcG9ydCB7IGNyZWF0ZVRlc3QsIHJlcXVlc3QsIHNldEJlZm9yZUVhY2ggfSBmcm9tIFwiLi4vcmVxdWVzdF90ZXN0XCI7XHJcbmltcG9ydCB7IGNyZWF0ZVVzZXIgfSBmcm9tIFwiLi4vcmVxdWVzdF91dGlsc1wiO1xyXG5pbXBvcnQgeyB0ZXN0RGlydHlJbnB1dFJlcXVlc3QsIG5hbWVEaXJ0eUlucHV0VGVzdCwgdmVyaWZ5UmVxdWVzdEFjY2VwdGVkLCB2ZXJpZnlSZXF1ZXN0RGVuaWVkIH0gZnJvbSBcIi4uL3V0aWxzXCI7XHJcblxyXG5zZXRCZWZvcmVFYWNoKGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0YXdhaXQgcmVxdWVzdChmYWxzZSk7XHJcblx0YXdhaXQgY3JlYXRlVXNlcihjb250ZXh0LCBcInVzZXJcIik7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBzZW5kIGZlZWRiYWNrXCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwiZmVlZGJhY2tcIiwgeyB0ZXh0OiBcInRlc3QgZmVlZGJhY2tcIiB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIHNlbmQgZmVlZGJhY2suXCIpO1xyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJNdXN0IG5vdCBhY2NlcHQgZmVlZGJhY2sgZnJvbSB1c2VycyB0aGF0IGFyZSBub3QgbG9nZ2VkIGluXCIsIFwic2VjdXJpdHlcIiwgYXN5bmMgKCkgPT4ge1xyXG5cdGNvbnN0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJmZWVkYmFja1wiLCB7IHRleHQ6IFwidGVzdCBmZWVkYmFja1wiIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3REZW5pZWQocmVxLCBcIlRyaWVkIHRvIHNlbmQgZmVlZGJhY2sgd2l0aG91dCBiZWluZyBsb2dnZWQgaW4uXCIpO1xyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QobmFtZURpcnR5SW5wdXRUZXN0KFwiZmVlZGJhY2tcIiksIFwic3RhYmlsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdHJldHVybiBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJmZWVkYmFjayBhY2NlcHRlZCBkaXJ0eSBpbnB1dCFcIiwgeyB0ZXh0OiBcInRlc3QgZmVlZGJhY2tcIiB9LCAoZGlydHlfaW5wdXQpID0+IFtcImZlZWRiYWNrXCIsIGRpcnR5X2lucHV0XSk7XHJcbn0pO1xyXG4iLCJpbXBvcnQgeyBjcmVhdGVUZXN0LCByZXF1ZXN0LCBzZXRCZWZvcmVFYWNoIH0gZnJvbSBcIi4uL3JlcXVlc3RfdGVzdFwiO1xyXG5pbXBvcnQgeyBJZCwgTmV3SWQgfSBmcm9tIFwiQC9kYXRhL3VcIjtcclxuaW1wb3J0IHsgY3JlYXRlVXNlciB9IGZyb20gXCIuLi9yZXF1ZXN0X3V0aWxzXCI7XHJcbmltcG9ydCB7IHRlc3REaXJ0eUlucHV0UmVxdWVzdCwgdGltZXN0YW1wLCBzaGlmdFRpbWUsIG5hbWVEaXJ0eUlucHV0VGVzdCwgdmVyaWZ5UmVxdWVzdEFjY2VwdGVkLCB2ZXJpZnlSZXF1ZXN0RGVuaWVkIH0gZnJvbSBcIi4uL3V0aWxzXCI7XHJcblxyXG5zZXRCZWZvcmVFYWNoKGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0YXdhaXQgcmVxdWVzdChmYWxzZSk7XHJcblx0YXdhaXQgY3JlYXRlVXNlcihjb250ZXh0LCBcInVzZXJcIik7XHJcbn0pO1xyXG5cclxuY29uc3QgbmFtZSA9IFwibmV3IG9yZ1wiO1xyXG5jb25zdCB0aW1lem9uZSA9IFwiRXVyb3BlL1RhbGxpbm5cIjtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gY3JlYXRlIGEgbmV3IG9yZ2FuaXphdGlvblwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogZmFsc2UsIHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBjcmVhdGUgYW4gb3JnLlwiKTtcclxuXHJcblx0cmV0dXJuIChyZXEuZGF0YSA9PSB1bmRlZmluZWQgfHwgcmVxLmRhdGEgPT0gTmV3SWQpID8gXCJDb3VsZCBub3QgY3JlYXRlIGEgbmV3IG9yZyFcIjogdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGFjY2VzcyB1cGRhdGVkIHByb2JsZW0gbGlzdCB3aGlsZSBpbnRlcm5ldCBoaWtlZFwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogZmFsc2UsIHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblxyXG5cdC8vIGNhbiBhY2Nlc3MgbGF0ZXN0IGN1cnNvcnNcclxuXHRsZXQgcmVxID0gYXdhaXQgcmVxdWVzdChcIm9yZ19nZXRMYXRlc3RDdXJzb3JzXCIsIHsgb3JnIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gZ2V0IGxhdGVzdCBjdXJzb3JzLlwiKTtcclxuXHRpZiAocmVxLmRhdGEhW1widGt0XCJdICE9PSB1bmRlZmluZWQpIHJldHVybiBcIlJlcXVlc3Qgb3JnX2dldExhdGVzdEN1cnNvcnMgZGlkIG5vdCByZXR1cm4gdW5kZWZpbmVkIVwiICsgSlNPTi5zdHJpbmdpZnkocmVxLmRhdGEpO1xyXG5cclxuXHQvLyBzaG91bGQgb3BlbiBtb3N0IHJlY2VudGx5IHZpc2l0ZWQgb3JnXHJcblx0Y29uc3QgdXNlciA9IChhd2FpdCByZXF1ZXN0KFwidXNlclwiKSkuZGF0YTtcclxuXHRpZiAodXNlcj8ub3B0aW9ucy5sYXN0X29yZyAhPT0gb3JnKSByZXR1cm4gXCJVc2VyJ3MgbGFzdCBvcmcgd2FzIG5vdCBzZXQgY29ycmVjdGx5IVxcbkV4cGVjdGVkOiBcIiArIEpTT04uc3RyaW5naWZ5KG9yZykgKyBcIlxcblJlY2VpdmVkOiBcIiArIEpTT04uc3RyaW5naWZ5KHVzZXI/Lm9wdGlvbnMubGFzdF9vcmcpO1xyXG5cclxuXHQvLyBjcmVhdGUgYSBncm91cCBhbmQgaW5zZXJ0IGEgdGlja2V0IHRoZXJlXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmcsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnBfaWQgPSBncm91cHNbMF0uX2lkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlOiBcInRpY2tldFwiLCB0ZXh0OiBcInRleHRcIiB9LCBvcmcgfSk7XHJcblx0Y29uc3QgdGt0ID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZHcm91cFwiLCB7IG9yZywgZ3JwOiBncnBfaWQgfSkpLmRhdGEhWzBdO1xyXG5cclxuXHQvLyB3ZXJlIHRoZSBjdXJzb3JzIGNvcnJlY3Q/XHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcIm9yZ19nZXRMYXRlc3RDdXJzb3JzXCIsIHsgb3JnIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gZ2V0IHVwZGF0ZWQgbGF0ZXN0IGN1cnNvcnMuXCIpO1xyXG5cdGlmIChyZXEuZGF0YSFbXCJ0a3RcIl0gIT09IHRrdC5faWQpIHJldHVybiBcIlJlcXVlc3Qgb3JnX2dldExhdGVzdEN1cnNvcnMgZGlkIG5vdCByZXR1cm4gYSB0aWNrZXQgd2l0aCBjb3JyZWN0IGlkIVxcbkV4cGVjdGVkOiBcIiArIHRrdC5faWQgKyBcIlxcblJlY2VpdmVkOiBcIiArIEpTT04uc3RyaW5naWZ5KHJlcS5kYXRhKTtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChuYW1lRGlydHlJbnB1dFRlc3QoXCJvcmdfZ2V0TGF0ZXN0Q3Vyc29yc1wiKSwgXCJzdGFiaWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogZmFsc2UsIHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblx0cmV0dXJuIGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIm9yZ19nZXRMYXRlc3RDdXJzb3JzIGFjY2VwdGVkIGRpcnR5IGlucHV0IVwiLCB7IG9yZyB9LCAoZGlydHlfaW5wdXQpID0+IFtcIm9yZ19nZXRMYXRlc3RDdXJzb3JzXCIsIGRpcnR5X2lucHV0XSk7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChuYW1lRGlydHlJbnB1dFRlc3QoXCJvcmdfbmV3XCIpLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IGFkbWluOiBmYWxzZSwgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdHJldHVybiBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJvcmdfbmV3IGFjY2VwdGVkIGRpcnR5IGlucHV0IVwiLCB7IG5hbWUsIHRpbWV6b25lLCBvcHRpb25zOiB7fSB9LCAoZGlydHlfaW5wdXQpID0+IFtcIm9yZ19uZXdcIiwgZGlydHlfaW5wdXRdKTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGFjY2Vzcywgc2VuZCwgcmVjZWl2ZSBhbmQgYWNjZXB0IGludml0YXRpb25zXCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHVzZXIwID0gY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiO1xyXG5cdGNvbnN0IHVzZXIxID0gY29udGV4dCArIFwiMUBzaGFya2xhc2Vycy5jb21cIjtcclxuXHJcblx0YXdhaXQgY3JlYXRlVXNlcihjb250ZXh0ICsgMSwgXCJ1c2VyXCIpO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogdXNlcjAsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblxyXG5cdC8vIG5vbi1leGlzdGluZyBpbnZpdGVzXHJcblx0bGV0IGludml0YXRpb25zID0gYXdhaXQgcmVxdWVzdChcIm9yZ19nZXRJbnZpdGF0aW9uc1wiLCB7IG9yZyB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQoaW52aXRhdGlvbnMsIFwiVHJpZWQgdG8gZ2V0IG5vbi1leGlzdGluZyBpbnZpdGVzLlwiKTtcclxuXHJcblx0bGV0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJpbnZpdGVcIiwgeyBvcmcsIGVtYWlsOiB1c2VyMSB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIHNlbmQgYW4gaW52aXRlLlwiKTtcclxuXHJcblx0aW52aXRhdGlvbnMgPSBhd2FpdCByZXF1ZXN0KFwib3JnX2dldEludml0YXRpb25zXCIsIHsgb3JnIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChpbnZpdGF0aW9ucywgXCJUcmllZCB0byBnZXQgc2VudCBleGlzdGluZyBpbnZpdGVzLlwiKTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IHVzZXIxLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRyZXEgPSAoYXdhaXQgcmVxdWVzdChcImdldEludml0YXRpb25zXCIpKTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGdldCByZWNlaXZlZCBleGlzdGluZyBpbnZpdGVzLlwiKTtcclxuXHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImFjY2VwdEludml0YXRpb25cIiwgeyBpbnY6IGludml0YXRpb25zLmRhdGEhWzBdLl9pZCB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVzIHRvIGFjY2VwdCBpbnZpdGF0aW9ucy5cIik7XHJcblx0XHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBraWNrIGEgdXNlciBmcm9tIGFuIG9yZ1wiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCB1c2VyMCA9IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIjtcclxuXHRjb25zdCB1c2VyMSA9IGNvbnRleHQgKyBcIjFAc2hhcmtsYXNlcnMuY29tXCI7XHJcblxyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCArIDEsIFwidXNlclwiKTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IHVzZXIwLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZSwgdGltZXpvbmUsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJvcmdfZ2V0SW52aXRhdGlvbnNcIiwgeyBvcmcgfSk7XHJcblx0YXdhaXQgcmVxdWVzdChcImludml0ZVwiLCB7IG9yZywgZW1haWw6IHVzZXIxIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dvdXRcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiB1c2VyMSwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3QgcGZsID0gKGF3YWl0IHJlcXVlc3QoXCJ1c2VyXCIpKS5kYXRhITtcclxuXHJcblx0Y29uc3QgaW52aXRhdGlvbnMgPSAoYXdhaXQgcmVxdWVzdChcImdldEludml0YXRpb25zXCIpKTtcclxuXHRhd2FpdCByZXF1ZXN0KFwiYWNjZXB0SW52aXRhdGlvblwiLCB7IGludjogaW52aXRhdGlvbnMuZGF0YSFbMF0uX2lkIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dvdXRcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiB1c2VyMCwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblxyXG5cdGNvbnN0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJvcmdfa2lja1wiLCB7IG9yZywgdXNlcjogcGZsLl9pZCB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGtpY2sgYSB1c2VyIGZyb20gYW4gb3JnLlwiKTtcclxuXHJcblx0Y29uc3QgY3VyX29yZyA9IChhd2FpdCByZXF1ZXN0KFwiZ2V0T3Jnc1wiKSkuZGF0YSFbMF07XHJcblx0aWYgKGN1cl9vcmcuYXV0aFtwZmwuX2lkXSAhPT0gdW5kZWZpbmVkKSByZXR1cm4gXCJVc2VyIHdhcyBub3QgY29ycmVjdGx5IHJlbW92ZWQgZnJvbSBvcmcgYXV0aCB0YWJsZSFcIjtcclxuXHRpZiAoY3VyX29yZy51c2Vycy5pbmNsdWRlcyhwZmwuX2lkKSkgcmV0dXJuIFwiVXNlciB3YXMgbm90IGNvcnJlY3RseSByZW1vdmVkIGZyb20gb3JnIHVzZXJzIGxpc3QhXCI7XHJcblxyXG5cdGVsc2UgcmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChuYW1lRGlydHlJbnB1dFRlc3QoXCJvcmdfa2lja1wiKSwgXCJzdGFiaWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCB1c2VyMCA9IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIjtcclxuXHRjb25zdCB1c2VyMSA9IGNvbnRleHQgKyBcIjFAc2hhcmtsYXNlcnMuY29tXCI7XHJcblxyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCArIDEsIFwidXNlclwiKTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IHVzZXIwLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZSwgdGltZXpvbmUsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwib3JnX2dldEludml0YXRpb25zXCIsIHsgb3JnIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJpbnZpdGVcIiwgeyBvcmcsIGVtYWlsOiB1c2VyMSB9KTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogdXNlcjEsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IHBmbCA9IChhd2FpdCByZXF1ZXN0KFwidXNlclwiKSkuZGF0YSE7XHJcblxyXG5cdGNvbnN0IGludml0YXRpb25zID0gKGF3YWl0IHJlcXVlc3QoXCJnZXRJbnZpdGF0aW9uc1wiKSk7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJhY2NlcHRJbnZpdGF0aW9uXCIsIHsgaW52OiBpbnZpdGF0aW9ucy5kYXRhIVswXS5faWQgfSk7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ291dFwiKTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IHVzZXIwLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0cmV0dXJuIHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIk9yZ19raWNrIGFjY2VwdGVkIGRpcnR5IGlucHV0IVwiLCB7IG9yZywgdXNlcjogcGZsLl9pZCB9LCAoZGlydHlfb2JqZWN0KSA9PiBbXCJvcmdfa2lja1wiLCBkaXJ0eV9vYmplY3RdKTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KG5hbWVEaXJ0eUlucHV0VGVzdChcImludml0ZSwgb3JnX2dldEludml0YXRpb25zIGFuZCBhY2NlcHRJbnZpdGF0aW9uXCIpLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHVzZXIwID0gY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiO1xyXG5cdGNvbnN0IHVzZXIxID0gY29udGV4dCArIFwiMUBzaGFya2xhc2Vycy5jb21cIjtcclxuXHJcblx0YXdhaXQgY3JlYXRlVXNlcihjb250ZXh0ICsgMSwgXCJ1c2VyXCIpO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogdXNlcjAsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblx0bGV0IGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIkludml0ZSwgd2hpY2ggaW5jbHVkZWQgZGlydHkgaW5wdXQgd2FzIHNlbnQhXCIsIHsgb3JnLCBlbWFpbDogdXNlcjEgfSwgKGRpcnR5X29iamVjdCkgPT4gW1wiaW52aXRlXCIsIGRpcnR5X29iamVjdF0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdGxldCByZXEgPSBhd2FpdCByZXF1ZXN0KFwiaW52aXRlXCIsIHsgb3JnLCBlbWFpbDogdXNlcjEgfSk7XHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImludml0ZVwiLCB7IG9yZywgZW1haWw6IHVzZXIxIH0pO1xyXG5cdGlmIChyZXEub2spIHJldHVybiBcIkNhbiBjcmVhdGUgbXVsdGlwbGUgaW52aXRhdGlvbnMgZm9yIG9uZSB1c2VyIVwiO1xyXG5cclxuXHRkaXJ0eV9yZXN1bHQgPSBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJSZXF1ZXN0LCB3aGljaCBnZXRzIGFsbCBpbnZpdGF0aW9ucywgYWNjZXB0ZWQgZGlydHkgaW5wdXQhXCIsIHsgb3JnIH0sIChkaXJ0eV9vYmplY3QpID0+IFtcIm9yZ19nZXRJbnZpdGF0aW9uc1wiLCBkaXJ0eV9vYmplY3RdKTtcclxuXHRpZiAoZGlydHlfcmVzdWx0ICE9PSB0cnVlKSByZXR1cm4gZGlydHlfcmVzdWx0O1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IHVzZXIxLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCByZXEyID0gKGF3YWl0IHJlcXVlc3QoXCJnZXRJbnZpdGF0aW9uc1wiKSk7XHJcblxyXG5cdGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIkludml0YXRpb24gd2FzIGFjY2VwdGVkIHdpdGggZGlydHkgaW5wdXQhXCIsIHsgaW52OiByZXEyLmRhdGEhWzBdLl9pZCB9LCAoZGlydHlfb2JqZWN0KSA9PiBbXCJhY2NlcHRJbnZpdGF0aW9uXCIsIGRpcnR5X29iamVjdF0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gcmVuYW1lIGFuIG9yZ1wiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCBuZXdfb3JnX25hbWUgPSBcInJlbmFtZWQgb3JnXCI7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZSwgdGltZXpvbmUsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cdGNvbnN0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJvcmdfcmVuYW1lXCIsIHsgb3JnLCBuYW1lOiBuZXdfb3JnX25hbWUgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byByZW5hbWUgYW4gb3JnLlwiKTtcclxuXHJcblx0Y29uc3QgcmVuYW1lZCA9IChhd2FpdCByZXF1ZXN0KFwiZ2V0T3Jnc1wiKSkuZGF0YSFbMF07XHJcblx0aWYgKHJlbmFtZWQubmFtZSAhPT0gbmV3X29yZ19uYW1lKSByZXR1cm4gXCJUaGUgb3JnIHdhcyBub3QgcmVuYW1lZCBwcm9wZXJseSEgQ3VycmVudCBvcmcgbmFtZTogXCIgKyByZW5hbWVkLm5hbWU7XHJcblxyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QobmFtZURpcnR5SW5wdXRUZXN0KFwib3JnX3JlbmFtZVwiKSwgXCJzdGFiaWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCBuZXdfb3JnX25hbWUgPSBcInJlbmFtZWQgb3JnXCI7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblx0cmV0dXJuIGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIm9yZ19yZW5hbWUgYWNjZXB0ZWQgZGlydHkgaW5wdXQhXCIsIHsgb3JnLCBuYW1lOiBuZXdfb3JnX25hbWUgfSwgKGRpcnR5X29iamVjdCkgPT4gW1wib3JnX3JlbmFtZVwiLCBkaXJ0eV9vYmplY3RdKTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIHJlbW92ZSB3aGl0ZXNwYWNlcyBmcm9tIG9yZyBuYW1lXCIsIFwiZXh0cmFcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRsZXQgbmV3X29yZ19uYW1lID0gXCIgcmVuYW1lZCBvcmdcIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJvcmdfcmVuYW1lXCIsIHsgb3JnLCBuYW1lOiBuZXdfb3JnX25hbWUgfSk7XHJcblxyXG5cdGxldCByZW5hbWVkID0gKGF3YWl0IHJlcXVlc3QoXCJnZXRPcmdzXCIpKS5kYXRhIVswXTtcclxuXHRpZiAocmVuYW1lZC5uYW1lLnN0YXJ0c1dpdGgoXCIgXCIpKSByZXR1cm4gXCJUaGUgbGVhZGluZyB3aGl0ZXNwYWNlIHdhcyBub3QgcmVtb3ZlZCBmcm9tIG9yZyBuYW1lIVwiO1xyXG5cclxuXHRuZXdfb3JnX25hbWUgPSBcInJlbmFtZWQgb3JnIFwiO1xyXG5cdHJlbmFtZWQgPSAoYXdhaXQgcmVxdWVzdChcImdldE9yZ3NcIikpLmRhdGEhWzBdO1xyXG5cdGlmIChyZW5hbWVkLm5hbWUuZW5kc1dpdGgoXCIgXCIpKSByZXR1cm4gXCJUaGUgdHJhaWxpbmcgd2hpdGVzcGFjZSB3YXMgbm90IHJlbW92ZWQgZnJvbSBvcmcgbmFtZSFcIjtcclxuXHJcblx0bmV3X29yZ19uYW1lID0gXCJcXG5cXG5cXHRcXG5yZW5hbWVkIFxcdFxcblxcdG9yZ1xcdFxcblxcdFxcblwiO1xyXG5cdHJlbmFtZWQgPSAoYXdhaXQgcmVxdWVzdChcImdldE9yZ3NcIikpLmRhdGEhWzBdO1xyXG5cdGlmIChyZW5hbWVkLm5hbWUuaW5jbHVkZXMoXCJcXG5cIikgfHwgcmVuYW1lZC5uYW1lLmluY2x1ZGVzKFwiXFx0XCIpKSByZXR1cm4gXCJOZXdsaW5lcyBhbmQgdGFicyB3ZXJlIG5vdCByZW1vdmVkIGZyb20gdGV4dCFcIjtcclxuXHRlbHNlIHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGNyZWF0ZSwgc2V0IGFuZCBhY2Nlc3MgdGhlIG5vdGVwYWRcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgbmFtZSA9IFwibmV3IG9yZ1wiO1xyXG5cdGNvbnN0IHRpbWV6b25lID0gXCJFdXJvcGUvVGFsbGlublwiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogZmFsc2UsIHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lLCB0aW1lem9uZSwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblxyXG5cdGNvbnN0IHJlcV9uZXdfbm90ZXBhZCA9IGF3YWl0IHJlcXVlc3QoXCJ1c3JfbmV3Tm90ZXBhZFwiLCB7IG9yZyB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxX25ld19ub3RlcGFkLCBcIlRyaWVkIHRvIGNyZWF0ZSBhIG5ldyBub3RlcGFkLlwiKTtcclxuXHJcblx0bGV0IHJlcV9nZXRfbm90ZXBhZCA9IGF3YWl0IHJlcXVlc3QoXCJ1c3JfZ2V0Tm90ZXBhZHNcIiwgeyBvcmcgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcV9nZXRfbm90ZXBhZCwgXCJUcmllZCB0byBhY2Nlc3MgdXNlcidzIG5vdGVwYWRzLlwiKTtcclxuXHJcblx0aWYgKHJlcV9nZXRfbm90ZXBhZC5kYXRhIVswXT8ubmFtZSAhPT0gXCJcIikgcmV0dXJuIFwiQ291bGQgbm90IGdldCBjb3JyZWN0IG5vdGVwYWQhXCI7XHJcblxyXG5cdGNvbnN0IHJlcV9zZXRfbm90ZXBhZCA9IGF3YWl0IHJlcXVlc3QoXCJ1c3Jfc2V0Tm90ZXBhZFwiLCB7XHJcblx0XHRfaWQ6IHJlcV9nZXRfbm90ZXBhZC5kYXRhIVswXS5faWQsXHJcblx0XHRvcmc6IHJlcV9nZXRfbm90ZXBhZC5kYXRhIVswXS5vcmcsXHJcblx0XHRuYW1lOiBcIk1haW4gTm90ZXNcIixcclxuXHRcdGNvbnRlbnQ6IFwiTXkgbm90ZXNcIixcclxuXHR9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxX3NldF9ub3RlcGFkLCBcIlRyaWVkIHRvIHNldCB1c2VyJ3Mgbm90ZXBhZHMuXCIpO1xyXG5cclxuXHRyZXFfZ2V0X25vdGVwYWQgPSBhd2FpdCByZXF1ZXN0KFwidXNyX2dldE5vdGVwYWRzXCIsIHsgb3JnIH0pO1xyXG5cdGlmIChyZXFfZ2V0X25vdGVwYWQuZGF0YSFbMF0/Lm5hbWUgIT09IFwiTWFpbiBOb3Rlc1wiIGFzIGFueSkgcmV0dXJuIFwiQ291bGQgbm90IHNldCBjb250ZW50IHByb3Blcmx5IVwiO1xyXG5cdGlmIChyZXFfZ2V0X25vdGVwYWQuZGF0YSFbMF0/LmNvbnRlbnQgIT09IFwiTXkgbm90ZXNcIikgcmV0dXJuIFwiQ291bGQgbm90IHNldCBjb250ZW50IHByb3Blcmx5IVwiO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KG5hbWVEaXJ0eUlucHV0VGVzdChcInVzcl9uZXdOb3RlcGFkLCB1c3JfZ2V0Tm90ZXBhZHMgYW5kIHVzcl9zZXROb3RlcGFkXCIpLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IG5hbWUgPSBcIm5ldyBvcmdcIjtcclxuXHRjb25zdCB0aW1lem9uZSA9IFwiRXVyb3BlL1RhbGxpbm5cIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IGZhbHNlLCB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZSwgdGltZXpvbmUsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwidXNyX25ld05vdGVwYWRcIiwgeyBvcmcgfSk7XHJcblx0bGV0IGRpcnR5X2lucHV0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiVXNyX25ld05vdGVwYWQgYWNjZXB0ZWQgZGlydHkgaW5wdXQhXCIsIHsgb3JnIH0sIChkaXJ0eV9vYmplY3QpID0+IFtcInVzcl9uZXdOb3RlcGFkXCIsIGRpcnR5X29iamVjdF0pO1xyXG5cdGlmIChkaXJ0eV9pbnB1dCAhPT0gdHJ1ZSkgcmV0dXJuIGRpcnR5X2lucHV0O1xyXG5cclxuXHRjb25zdCByZXFfZ2V0X25vdGVwYWQgPSAoYXdhaXQgcmVxdWVzdChcInVzcl9nZXROb3RlcGFkc1wiLCB7IG9yZyB9KSkuZGF0YSFbMF07XHJcblxyXG5cdGRpcnR5X2lucHV0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiVXNyX2dldE5vdGVwYWRzIGFjY2VwdGVkIGRpcnR5IGlucHV0IVwiLCB7IG9yZyB9LCAoZGlydHlfb2JqZWN0KSA9PiBbXCJ1c3JfZ2V0Tm90ZXBhZHNcIiwgZGlydHlfb2JqZWN0XSk7XHJcblx0aWYgKGRpcnR5X2lucHV0ICE9PSB0cnVlKSByZXR1cm4gZGlydHlfaW5wdXQ7XHJcblxyXG5cdGRpcnR5X2lucHV0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiVXNyX3NldE5vdGVwYWQgYWNjZXB0ZWQgZGlydHkgaW5wdXQhXCIsIHtcclxuXHRcdF9pZDogcmVxX2dldF9ub3RlcGFkLl9pZCxcclxuXHRcdG9yZzogcmVxX2dldF9ub3RlcGFkLm9yZyxcclxuXHRcdG5hbWU6IFwiTWFpbiBOb3Rlc1wiLFxyXG5cdFx0Y29udGVudDogXCJNeSBub3Rlc1wiLFxyXG5cdH0sIChkaXJ0eV9vYmplY3QpID0+IFtcInVzcl9zZXROb3RlcGFkXCIsIGRpcnR5X29iamVjdF0sIHsgY29udGVudDogW1wiXCJdIH0pO1xyXG5cdHJldHVybiBkaXJ0eV9pbnB1dDtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGFuIG9yZyBhc2sgdG8gYmUgZGVsZXRlZFwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCB1c2VyX29iaiA9IHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH07XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHVzZXJfb2JqKTtcclxuXHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZSwgdGltZXpvbmUsIG9wdGlvbnM6IHt9IH0pKS5kYXRhITtcclxuXHJcblx0Ly8gbWFyayBmb3IgZGVsZXRpb25cclxuXHRsZXQgcmVxID0gYXdhaXQgcmVxdWVzdChcIm9yZ19kZWxldGVcIiwgeyBvcmcsIGNvbmZpcm06IHRydWUgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBtYXJrIG9yZyB0byBiZSBkZWxldGVkLlwiKTtcclxuXHJcblx0bGV0IHRiZF9vcmcgPSAoYXdhaXQgcmVxdWVzdChcImdldE9yZ3NcIikpLmRhdGEhWzBdO1xyXG5cdGlmICh0YmRfb3JnLm1mZCA8IHRpbWVzdGFtcCgpICsgMjcgKiA4Nl80MDAgKiAxMDAwKSByZXR1cm4gXCJPcmcgd2FzIG5vdCBtYXJrZWQgdG8gYmUgZGVsZXRlZCBhdCBhIGNvcnJlY3QgdGltZSFcXG5FeHBlY3RlZDogXCIgKyBuZXcgRGF0ZSh0aW1lc3RhbXAoKSArIDI3ICogODZfNDAwICogMTAwMCkgKyBcIiBvciBsYXRlclxcblJlY2VpdmVkOiBcIiArIG5ldyBEYXRlKHRiZF9vcmcubWZkKTtcclxuXHJcblx0Ly8gdW5tYXJrIGZvciBkZWxldGlvblxyXG5cdHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJvcmdfZGVsZXRlXCIsIHsgb3JnLCBjb25maXJtOiBmYWxzZSB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGNhbmNlbCBvcmcgZnJvbSBiZWluZyBkZWxldGVkLlwiKTtcclxuXHJcblx0dGJkX29yZyA9IChhd2FpdCByZXF1ZXN0KFwiZ2V0T3Jnc1wiKSkuZGF0YSFbMF07XHJcblx0aWYgKHRiZF9vcmcubWZkICE9PSAwKSByZXR1cm4gXCJPcmcgd2FzIG5vdCB1bm1hcmtlZCB0byBiZSBkZWxldGVkIVwiO1xyXG5cclxuXHQvLyB0ZXN0IGFjdHVhbCBkZWxldGlvbiBieSBoYWNraW5nIHRpbWVcclxuXHRyZXEgPSBhd2FpdCByZXF1ZXN0KFwib3JnX2RlbGV0ZVwiLCB7IG9yZywgY29uZmlybTogdHJ1ZSB9KTtcclxuXHJcblx0dGJkX29yZyA9IGF3YWl0IHNoaWZ0VGltZSh1c2VyX29iaiwgdGltZXN0YW1wKCkgKyAzMiAqIDg2XzQwMCAqIDEwMDAsIGFzeW5jICgpID0+IChhd2FpdCByZXF1ZXN0KFwiZ2V0T3Jnc1wiKSkuZGF0YSFbMF0pO1xyXG5cdGlmICh0YmRfb3JnICE9IHVuZGVmaW5lZCkgcmV0dXJuIFwiT3JnIHdhcyBub3QgYWN0dWFsbHkgZGVsZXRlZCFcIjtcclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcbiIsImltcG9ydCB7IFVzZXIgfSBmcm9tIFwiQC9kYXRhL1VzZXJcIjtcclxuaW1wb3J0IHsgYWRtaW5fcHdkLCBhZG1pbl91c3JfbmFtZSwgdGVzdF9rZXkgfSBmcm9tIFwiLi4vY29uZmlnXCI7XHJcbmltcG9ydCB7IGNyZWF0ZVRlc3QsIHJlcXVlc3QsIHNldEJlZm9yZUVhY2ggfSBmcm9tIFwiLi4vcmVxdWVzdF90ZXN0XCI7XHJcbmltcG9ydCB7IGNyZWF0ZVVzZXIgfSBmcm9tIFwiLi4vcmVxdWVzdF91dGlsc1wiO1xyXG5pbXBvcnQgeyB0ZXN0RGlydHlJbnB1dFJlcXVlc3QsIHNoaWZ0VGltZSwgdGltZXN0YW1wLCBuYW1lRGlydHlJbnB1dFRlc3QsIHZlcmlmeVJlcXVlc3REZW5pZWQsIHZlcmlmeVJlcXVlc3RBY2NlcHRlZCB9IGZyb20gXCIuLi91dGlsc1wiO1xyXG5cclxuY29uc3QgZm5hbWUgPSBcInVzZXJcIjtcclxuY29uc3QgcHdkID0gXCJQYXNzd29yZDEyM1wiO1xyXG5jb25zdCBsbmFtZSA9IFwibGFzdG5hbWVcIjtcclxuXHJcbnNldEJlZm9yZUVhY2goYXN5bmMgKCkgPT4ge1xyXG5cdGF3YWl0IHJlcXVlc3QoZmFsc2UpO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gcmVnaXN0ZXIgYSBuZXcgdXNlclwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCByZXEgPSBhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQsIGZuYW1lKTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIHJlZ2lzdGVyIGEgcmVndWxhciB1c2VyLlwiKTtcclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGxvZyBpbnRvIHNpdGVcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0YXdhaXQgY3JlYXRlVXNlcihjb250ZXh0LCBcInVzZXJcIik7XHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IGZhbHNlLCB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gbG9nIGluIGFzIGEgcmVndWxhciB1c2VyLlwiKTtcclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGRvd25sb2FkIHVzZXIgcHJvZmlsZVwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQsIFwidXNlclwiKTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogZmFsc2UsIHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2QgfSk7XHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcInVzZXJcIik7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBkb3dubG9hZCB1c2VyIHByb2ZpbGUuXCIpO1xyXG5cdHJldHVybiByZXEuZGF0YT8ucGZsLmRuYW1lID8gdHJ1ZSA6IFwiQ291bGQgbm90IGdldCB1c2VyIHByb2ZpbGUhXCI7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBsb2cgb3V0XCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCwgXCJ1c2VyXCIpO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IGFkbWluOiBmYWxzZSwgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZCB9KTtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gbG9nIG91dC5cIik7XHJcblxyXG5cdGNvbnN0IHJlcV91c2VyID0gYXdhaXQgcmVxdWVzdChcInVzZXJcIik7XHJcblx0cmV0dXJuICFyZXFfdXNlci5kYXRhPy5wZmwuZG5hbWUgPyB0cnVlIDogXCJDb3VsZCBub3QgYWN0dWFsbHkgbG9nb3V0IVwiO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gc3dpdGNoIHVzZXJzXCIsIFwiZXh0cmFcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQgKyAxLCBcInVzZXIxXCIpO1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCArIDIsIFwidXNlcjJcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgMSArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2QgfSk7XHJcblxyXG5cdGNvbnN0IHBmbDEgPSAoYXdhaXQgcmVxdWVzdChcInVzZXJcIikpLmRhdGEhLnBmbC5kbmFtZTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyAyICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZCB9KTtcclxuXHRjb25zdCBwZmwyID0gKGF3YWl0IHJlcXVlc3QoXCJ1c2VyXCIpKS5kYXRhIS5wZmwuZG5hbWU7XHJcblx0aWYgKHBmbDEgPT09IHBmbDIpIHJldHVybiBcIlN3aXRjaGluZyB1c2VyIHByb2ZpbGVzIGJ5IGxvZ2dpbmcgb3V0IGFzIHRoZSBvbGQgdXNlciBhbmQgbG9nZ2luZyBpbiBhcyB0aGUgbmV3IHVzZXIgZmFpbGVkIVwiO1xyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gbm90IGxvZyBpbiB3aXRoIHdyb25nIHBhc3N3b3JkXCIsIFwic2VjdXJpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogZmFsc2UsIHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiaHJldGdnd2lvdHlnam9kXCIgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdERlbmllZChyZXEsIFwiVHJpZWQgdG8gbG9nIGluIHdpdGggYSB3cm9uZyBwYXNzd29yZC5cIik7XHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBub3QgcmVnaXN0ZXIgd2l0aCBiYWQgcGFzc3dvcmRcIiwgXCJzZWN1cml0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJyZWdpc3RlclwiLCB7IGVtYWlsOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCIxMjNcIiwga2V5OiBcImFcIiwgZm5hbWU6IFwic25lYWt5dXNlclwiLCBsbmFtZTogXCJzbmVha1wiIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3REZW5pZWQocmVxLCBcIlRyaWVkIHRvIHJlZ2lzdGVyIHdpdGggYSBiYWQgKHNob3J0KSBwYXNzd29yZCFcIik7XHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChuYW1lRGlydHlJbnB1dFRlc3QoXCJyZWdpc3RlclwiKSwgXCJzdGFiaWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCBwd2QgPSBcIlBhc3N3b3JkMTIzXCI7XHJcblxyXG5cdGxldCByZXEgPSBhd2FpdCByZXF1ZXN0KFwicmVnaXN0ZXJcIiwgeyBmbmFtZTogXCJzbmVha3l1c2VyXCIsIGxuYW1lOiBcInNuZWFrXCIsIGVtYWlsOiBjb250ZXh0ICsgXCJALlwiLCBwd2QsIGtleTogXCJhXCIgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdERlbmllZChyZXEsIFwiVHJpZWQgdG8gcmVnaXN0ZXIgd2l0aCBlbWFpbDogQC5cIik7XHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcInJlZ2lzdGVyXCIsIHsgZm5hbWU6IFwic25lYWt5dXNlclwiLCBsbmFtZTogXCJzbmVha1wiLCBlbWFpbDogY29udGV4dCwgcHdkLCBrZXk6IFwiYVwiIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3REZW5pZWQocmVxLCBcIlRyaWVkIHRvIHJlZ2lzdGVyIHdpdGggZW1haWwgY29udGFpbmluZyBvbmx5IGNvbnRleHQuXCIpO1xyXG5cdHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJyZWdpc3RlclwiLCB7IGZuYW1lOiBcInNuZWFreXVzZXJcIiwgbG5hbWU6IFwic25lYWtcIiwgZW1haWw6IFwiXCIsIHB3ZCwga2V5OiBcImFcIiB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0RGVuaWVkKHJlcSwgXCJUcmllZCB0byByZWdpc3RlciB3aXRoIGVtYWlsIGNvbnRhaW5pbmcgZW1wdHkgc3RyaW5nLlwiKTtcclxuXHRyZXEgPSBhd2FpdCByZXF1ZXN0KFwicmVnaXN0ZXJcIiwgeyBmbmFtZTogXCJzbmVha3l1c2VyXCIsIGxuYW1lOiBcInNuZWFrXCIsIGVtYWlsOiBcIkAuXCIsIHB3ZCwga2V5OiBcImFcIiB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0RGVuaWVkKHJlcSwgXCJVc2VyIGNvdWxkIHJlZ2lzdGVyIHdpdGggZW1haWwgY29udGFpbmluZyBvbmx5IEAuXCIpO1xyXG5cdHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJyZWdpc3RlclwiLCB7IGZuYW1lOiBcInNuZWFreXVzZXJcIiwgbG5hbWU6IFwic25lYWtcIiwgZW1haWw6IFwiMTIzNDVcIiwgcHdkLCBrZXk6IFwiYVwiIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3REZW5pZWQocmVxLCBcIlVzZXIgY291bGQgcmVnaXN0ZXIgd2l0aCBlbWFpbCBjb250YWluaW5nIG9ubHkgMTIzNDUuXCIpO1xyXG5cdFxyXG5cdHJldHVybiBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJyZWdpc3RlciBhY2NlcHRlZCBkaXJ0eSBpbnB1dCFcIiwgeyBlbWFpbDogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2QsIGZuYW1lLCBsbmFtZSwga2V5OiBcImFcIiB9LCAoZGlydHlfaW5wdXQpID0+IFtcInJlZ2lzdGVyXCIsIGRpcnR5X2lucHV0XSk7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBjaGFuZ2UgcGFzc3dvcmRcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0YXdhaXQgY3JlYXRlVXNlcihjb250ZXh0LCBcInVzZXJcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IGZhbHNlLCB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkIH0pO1xyXG5cclxuXHRsZXQgcmVxID0gYXdhaXQgcmVxdWVzdChcImNoYW5nZVBhc3N3b3JkXCIsIHsgb2xkX3B3ZDogcHdkLCBuZXdfcHdkOiBcIlF3ZXJ0eTEyM1wiIH0pIGFzIGFueTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGNoYW5nZSBwYXNzd29yZC5cIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ291dFwiKSBhcyBhbnk7XHJcblxyXG5cdHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IGFkbWluOiBmYWxzZSwgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZCB9KTtcclxuXHRpZiAocmVxLm9rKSByZXR1cm4gXCJDb3VsZCBsb2dpbiB3aXRoIG9sZCBwYXNzd29yZC5cIjtcclxuXHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IGZhbHNlLCB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlF3ZXJ0eTEyM1wiIH0pO1xyXG5cdGlmICghcmVxLm9rKSByZXR1cm4gXCJDb3VsZCBub3QgbG9naW4gd2l0aCBuZXcgKGNoYW5nZWQpIHBhc3N3b3JkLlwiO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KG5hbWVEaXJ0eUlucHV0VGVzdChcImNoYW5nZVBhc3N3b3JkXCIpLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCwgXCJ1c2VyXCIpO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IGFkbWluOiBmYWxzZSwgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZCB9KTtcclxuXHRyZXR1cm4gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiY2hhbmdlUGFzc3dvcmQgYWNjZXB0ZWQgZGlydHkgaW5wdXQhXCIsIHsgb2xkX3B3ZDogcHdkLCBuZXdfcHdkOiBcIlF3ZXJ0eTEyM1wiIH0sIChkaXJ0eV9pbnB1dCkgPT4gW1wiY2hhbmdlUGFzc3dvcmRcIiwgZGlydHlfaW5wdXRdKTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiUGFzc3dvcmQgZW5jcnlwdGlvbiBtZXRob2QgaW4gdXNlIHByb2R1Y2VzIGRpZmZlcmVudCBvdXRwdXQgZWFjaCB0aW1lXCIsIFwiZXh0cmFcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCByZXF1ZXN0KFwicmVnaXN0ZXJcIiwgeyBlbWFpbDogY29udGV4dCArIDEgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkLCBmbmFtZTogXCJmaXJzdG5hbWVcIiwgbG5hbWU6IFwibGFzdG5hbWVcIiwga2V5OiBcImFcIiB9KTtcclxuXHRhd2FpdCByZXF1ZXN0KFwicmVnaXN0ZXJcIiwgeyBlbWFpbDogY29udGV4dCArIDIgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkLCBmbmFtZTogXCJmaXJzdG5hbWVcIiwgbG5hbWU6IFwibGFzdG5hbWVcIiwga2V5OiBcImFcIiB9KTtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IHRydWUsIHVzcjogYWRtaW5fdXNyX25hbWUsIHB3ZDogYWRtaW5fcHdkIH0pO1xyXG5cclxuXHRjb25zdCB1c2VycyA9IChhd2FpdCByZXF1ZXN0KFwiZ2V0XCIsIHsgdGFibGU6IFwidXNlcnNcIiB9KSkuZGF0YSBhcyBVc2VyW107XHJcblx0aWYgKHVzZXJzWzBdLmFjYy5wd2QgIT09IHVzZXJzWzFdLmFjYy5wd2QpIHJldHVybiB0cnVlO1xyXG5cdHJldHVybiBcIlBhc3N3b3JkIGVuY3J5cHRpb24gbWV0aG9kIHByb2R1Y2VkIHR3byBzYW1lIHBhc3N3b3JkcyFcIjtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGNoYW5nZSBuYW1lc1wiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQsIFwiZmlyc3RuYW1lXCIpO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IGFkbWluOiBmYWxzZSwgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZCB9KTtcclxuXHJcblx0bGV0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJjaGFuZ2VOYW1lc1wiLCB7IGZuYW1lOiBcIk5pbWlcIiwgbG5hbWU6IFwibGFzdG5hbWVcIiwgZG5hbWU6IFwiZmlyc3RuYW1lXCIgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBjaGFuZ2UgZmlyc3RuYW1lLlwiKTtcclxuXHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImNoYW5nZU5hbWVzXCIsIHsgZm5hbWU6IFwiTmltaVwiLCBsbmFtZTogXCJOaW1lc3RlXCIsIGRuYW1lOiBcImZpcnN0bmFtZVwiIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gY2hhbmdlIGxhc3RuYW1lLlwiKTtcclxuXHJcblx0cmVxID0gYXdhaXQgcmVxdWVzdChcImNoYW5nZU5hbWVzXCIsIHsgZm5hbWU6IFwiTmltaVwiLCBsbmFtZTogXCJOaW1lc3RlXCIsIGRuYW1lOiBcIk1pbnUgbmltaVwiIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gY2hhbmdlIGRpc3BsYXluYW1lLlwiKTtcclxuXHJcblx0Y29uc3QgdXNlcl9pbmZvID0gKGF3YWl0IHJlcXVlc3QoXCJ1c2VyXCIpKS5kYXRhO1xyXG5cdGlmICh1c2VyX2luZm8/LnBmbC5mbmFtZSAhPT0gXCJOaW1pXCIpIHJldHVybiBcIkNvdWxkIG5vdCBjaGFuZ2UgZmlyc3RuYW1lIVwiO1xyXG5cdGlmICh1c2VyX2luZm8/LnBmbC5sbmFtZSAhPT0gXCJOaW1lc3RlXCIpIHJldHVybiBcIkNvdWxkIG5vdCBjaGFuZ2UgbGFzdG5hbWUhXCI7XHJcblx0aWYgKHVzZXJfaW5mbz8ucGZsLmRuYW1lICE9PSBcIk1pbnUgbmltaVwiKSByZXR1cm4gXCJDb3VsZCBub3QgY2hhbmdlIGRpc3BsYXluYW1lIVwiO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KG5hbWVEaXJ0eUlucHV0VGVzdChcImNoYW5nZU5hbWVzXCIpLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCwgXCJmaXJzdG5hbWVcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IGZhbHNlLCB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkIH0pO1xyXG5cdHJldHVybiBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJjaGFuZ2VOYW1lcyBhY2NlcHRlZCBkaXJ0eSBpbnB1dCFcIiwgeyBmbmFtZTogXCJmaXJzdG5hbWVcIiwgbG5hbWU6IFwibGFzdG5hbWVcIiwgZG5hbWU6IFwiZmlyc3RuYW1lXCIgfSwgKGRpcnR5X2lucHV0KSA9PiBbXCJjaGFuZ2VOYW1lc1wiLCBkaXJ0eV9pbnB1dF0pO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gcmVtb3ZlIHdoaXRlc3BhY2VzIGZyb20gdXNlcidzIG5hbWVzXCIsIFwiZXh0cmFcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRsZXQgZm5hbWUgPSBcIiBOaW1pXCI7XHJcblx0bGV0IGxuYW1lID0gXCIgTmltZXN0ZVwiO1xyXG5cdGxldCBkbmFtZSA9IFwiIE1pbnUgbmltaVwiO1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCwgXCJmaXJzdG5hbWVcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IGZhbHNlLCB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJjaGFuZ2VOYW1lc1wiLCB7IGZuYW1lLCBsbmFtZSwgZG5hbWUgfSk7XHJcblx0bGV0IHVzZXJfaW5mbyA9IChhd2FpdCByZXF1ZXN0KFwidXNlclwiKSkuZGF0YTtcclxuXHRpZiAoIXVzZXJfaW5mbz8ucGZsLmZuYW1lLnN0YXJ0c1dpdGgoXCJOXCIpKSByZXR1cm4gXCJUaGUgbGVhZGluZyB3aGl0ZXNwYWNlIHdhcyBub3QgcmVtb3ZlZCBmcm9tIGZpcnN0bmFtZSFcIjtcclxuXHRpZiAoIXVzZXJfaW5mbz8ucGZsLmxuYW1lLnN0YXJ0c1dpdGgoXCJOXCIpKSByZXR1cm4gXCJUaGUgbGVhZGluZyB3aGl0ZXNwYWNlIHdhcyBub3QgcmVtb3ZlZCBmcm9tIGxhc3RuYW1lIVwiO1xyXG5cdGlmICghdXNlcl9pbmZvPy5wZmwuZG5hbWUuc3RhcnRzV2l0aChcIk1cIikpIHJldHVybiBcIlRoZSBsZWFkaW5nIHdoaXRlc3BhY2Ugd2FzIG5vdCByZW1vdmVkIGZyb20gZGlzcGxheW5hbWUhXCI7XHJcblxyXG5cdGZuYW1lID0gXCJOaW1pIFwiO1xyXG5cdGxuYW1lID0gXCJOaW1lc3RlIFwiO1xyXG5cdGRuYW1lID0gXCJNaW51IG5pbWkgXCI7XHJcblx0YXdhaXQgcmVxdWVzdChcImNoYW5nZU5hbWVzXCIsIHsgZm5hbWUsIGxuYW1lLCBkbmFtZSB9KTtcclxuXHR1c2VyX2luZm8gPSAoYXdhaXQgcmVxdWVzdChcInVzZXJcIikpLmRhdGE7XHJcblx0aWYgKCF1c2VyX2luZm8/LnBmbC5mbmFtZS5lbmRzV2l0aChcImlcIikpIHJldHVybiBcIlRoZSB0cmFpbGluZyB3aGl0ZXNwYWNlIHdhcyBub3QgcmVtb3ZlZCBmcm9tIGZpcnN0bmFtZSFcIjtcclxuXHRpZiAoIXVzZXJfaW5mbz8ucGZsLmxuYW1lLmVuZHNXaXRoKFwiZVwiKSkgcmV0dXJuIFwiVGhlIHRyYWlsaW5nIHdoaXRlc3BhY2Ugd2FzIG5vdCByZW1vdmVkIGZyb20gbGFzdG5hbWUhXCI7XHJcblx0aWYgKCF1c2VyX2luZm8/LnBmbC5kbmFtZS5lbmRzV2l0aChcImlcIikpIHJldHVybiBcIlRoZSB0cmFpbGluZyB3aGl0ZXNwYWNlIHdhcyBub3QgcmVtb3ZlZCBmcm9tIGRpc3BsYXluYW1lIVwiO1xyXG5cclxuXHRmbmFtZSA9IFwiXFxuXFxuXFx0XFx0TmlcXHRcXG5tXFx0XFxuaVxcblxcblxcdFxcdFwiO1xyXG5cdGxuYW1lID0gXCJcXG5cXG5cXHRcXHROaVxcdFxcbm1lc1xcdFxcbnRlXFxuXFxuXFx0XFx0IFwiO1xyXG5cdGRuYW1lID0gXCJcXG5cXG5cXHRcXHRNaVxcdFxcbm51IFxcdFxcbm5pbWlcXG5cXG5cXHRcXHQgXCI7XHJcblx0YXdhaXQgcmVxdWVzdChcImNoYW5nZU5hbWVzXCIsIHsgZm5hbWUsIGxuYW1lLCBkbmFtZSB9KTtcclxuXHR1c2VyX2luZm8gPSAoYXdhaXQgcmVxdWVzdChcInVzZXJcIikpLmRhdGE7XHJcblx0aWYgKHVzZXJfaW5mbz8ucGZsLmZuYW1lLmluY2x1ZGVzKFwiXFxuXCIpICYmIHVzZXJfaW5mbz8ucGZsLmZuYW1lLmluY2x1ZGVzKFwiXFx0XCIpKSByZXR1cm4gXCJUYWJzIGFuZCBuZXdsaW5lcyB3ZXJlIG5vdCByZW1vdmVkIGZyb20gZmlyc3RuYW1lIVwiO1xyXG5cdGlmICh1c2VyX2luZm8/LnBmbC5sbmFtZS5pbmNsdWRlcyhcIlxcblwiKSAmJiB1c2VyX2luZm8/LnBmbC5sbmFtZS5pbmNsdWRlcyhcIlxcdFwiKSkgcmV0dXJuIFwiVGFicyBhbmQgbmV3bGluZXMgd2VyZSBub3QgcmVtb3ZlZCBmcm9tIGxhc3RuYW1lIVwiO1xyXG5cdGlmICh1c2VyX2luZm8/LnBmbC5kbmFtZS5pbmNsdWRlcyhcIlxcblwiKSAmJiB1c2VyX2luZm8/LnBmbC5kbmFtZS5pbmNsdWRlcyhcIlxcdFwiKSkgcmV0dXJuIFwiVGFicyBhbmQgbmV3bGluZXMgd2VyZSBub3QgcmVtb3ZlZCBmcm9tIGRpc3BsYXluYW1lIVwiO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGEgdXNlciBhc2sgdG8gYmUgZGVsZXRlZFwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQsIFwidXNlclwiKTtcclxuXHRjb25zdCB1c2VyX29iaiA9IHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZCB9O1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB1c2VyX29iaik7XHJcblxyXG5cdC8vIG1hcmsgZm9yIGRlbGV0aW9uXHJcblx0bGV0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJ1c3JfZGVsZXRlXCIsIHsgY29uZmlybTogdHJ1ZSB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIG1hcmsgdXNlciBmb3IgZGVsZXRpb24uXCIpO1xyXG5cclxuXHRsZXQgdXNlciA9IChhd2FpdCByZXF1ZXN0KFwidXNlclwiKSkuZGF0YTtcclxuXHRpZiAodXNlciEubWZkIDwgdGltZXN0YW1wKCkgKyAyNyAqIDg2XzQwMCAqIDEwMDApIHJldHVybiBcIlVzZXIgd2FzIG5vdCBtYXJrZWQgdG8gYmUgZGVsZXRlZCBhdCBhIGNvcnJlY3QgdGltZSFcXG5FeHBlY3RlZDogXCIgKyBuZXcgRGF0ZSh0aW1lc3RhbXAoKSArIDI3ICogODZfNDAwICogMTAwMCkgKyBcIiBvciBsYXRlclxcblJlY2VpdmVkOiBcIiArIG5ldyBEYXRlKHVzZXIhLm1mZCk7XHJcblxyXG5cdC8vIHVubWFyayBmb3IgZGVsZXRpb25cclxuXHRyZXEgPSBhd2FpdCByZXF1ZXN0KFwidXNyX2RlbGV0ZVwiLCB7IGNvbmZpcm06IGZhbHNlIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gY2FuY2VsIHVzZXIgZGVsZXRpb24uXCIpO1xyXG5cclxuXHR1c2VyID0gKGF3YWl0IHJlcXVlc3QoXCJ1c2VyXCIpKS5kYXRhO1xyXG5cdGlmICh1c2VyIS5tZmQgIT09IDApIHJldHVybiBcIlVzZXIgd2FzIG5vdCB1bm1hcmtlZCB0byBiZSBkZWxldGVkIVwiO1xyXG5cclxuXHQvLyB0ZXN0IGFjdHVhbCBkZWxldGlvbiBieSBoYWNraW5nIHRpbWVcclxuXHRyZXEgPSBhd2FpdCByZXF1ZXN0KFwidXNyX2RlbGV0ZVwiLCB7IGNvbmZpcm06IHRydWUgfSk7XHJcblxyXG5cdGNvbnN0IGRlbF9sb2dfaW4gPSBhd2FpdCBzaGlmdFRpbWUodXNlcl9vYmosIHRpbWVzdGFtcCgpICsgMzIgKiA4Nl80MDAgKiAxMDAwLCBhc3luYyAoKSA9PiBhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogZmFsc2UsIHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2QgfSkpO1xyXG5cdGlmIChkZWxfbG9nX2luLm9rKSByZXR1cm4gXCJDb3VsZCBsb2dpbiBhcyBhIGRlbGV0ZWQgdXNlci5cIjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIlVzZXIgY2FuIGdldCB0aGVpciB3c2tleVwiLCBbXCJmdW5jdGlvbmFsaXR5XCIsIFwic3RhYmlsaXR5XCJdLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCwgXCJ1c2VyXCIpO1xyXG5cdGNvbnN0IHVzZXJfb2JqID0geyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkIH07XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHVzZXJfb2JqKTtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwid3NrZXlcIik7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBnZXQgdXNlcidzIHdza2V5LlwiKTtcclxuXHRyZXR1cm4gdHlwZW9mIHJlcS5kYXRhIVswXSAhPT0gXCJzdHJpbmdcIiA/IFwiV3NrZXkgd2FzIG5vdCBvZiB0eXBlIHN0cmluZyFcIiA6IHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkV2ZXJ5IHVzZXIgZ2V0cyBhIHVuaXF1ZSB3c2tleSBhc3NpZ25lZCB0byB0aGVtXCIsIFwic2VjdXJpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQsIFwidXNlclwiKTtcclxuXHRsZXQgdXNlcl9vYmogPSB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2QgfTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgdXNlcl9vYmopO1xyXG5cdGNvbnN0IHJlcTEgPSBhd2FpdCByZXF1ZXN0KFwid3NrZXlcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ291dFwiKTtcclxuXHJcblx0YXdhaXQgY3JlYXRlVXNlcihjb250ZXh0ICsgMSwgXCJ1c2VyXCIpO1xyXG5cdHVzZXJfb2JqID0geyB1c3I6IGNvbnRleHQgKyAxICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZCB9O1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB1c2VyX29iaik7XHJcblx0Y29uc3QgcmVxMiA9IGF3YWl0IHJlcXVlc3QoXCJ3c2tleVwiKTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxMiwgXCJUcmllZCB0byBnZXQgc2Vjb25kIHVzZXIncyB3c2tleS5cIik7XHJcblxyXG5cdGlmIChyZXExLmRhdGEgPT09IHJlcTIuZGF0YSkgcmV0dXJuIFwiV3NrZXlzIG9mIHR3byBkaWZmZXJlbnQgc2Vzc2lvbnMgd2VyZSB0aGUgc2FtZSFcIjtcclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcbiIsImltcG9ydCB7IGNyZWF0ZVRlc3QsIHJlcXVlc3QsIHNldEJlZm9yZUVhY2ggfSBmcm9tIFwiLi4vcmVxdWVzdF90ZXN0XCI7XHJcbmltcG9ydCB7IElkIH0gZnJvbSBcIkAvZGF0YS91XCI7XHJcbmltcG9ydCB7IGNyZWF0ZVVzZXIgfSBmcm9tIFwiLi4vcmVxdWVzdF91dGlsc1wiO1xyXG5pbXBvcnQgeyBkYXRlYywgdGltZXN0YW1wLCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QsIG5hbWVEaXJ0eUlucHV0VGVzdCwgdmVyaWZ5UmVxdWVzdERlbmllZCwgdmVyaWZ5UmVxdWVzdEFjY2VwdGVkIH0gZnJvbSBcIi4uL3V0aWxzXCI7XHJcbmltcG9ydCB7IFByb2JsZW0gfSBmcm9tIFwiQC9kYXRhL1Byb2JsZW1cIjtcclxuXHJcbnNldEJlZm9yZUVhY2goYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCByZXF1ZXN0KGZhbHNlKTtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQsIFwidXNlclwiKTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KG5hbWVEaXJ0eUlucHV0VGVzdChcInRrdF9uZXdcIiksIFwic3RhYmlsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0bGV0IGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIlRpY2tldCB3YXMgY3JlYXRlZCB3aXRob3V0IGFueSB1c2VyIGJlaW5nIGxvZ2dlZCBpbiFcIixcclxuXHRcdHsgZG9jOiB7IGdycDogXCJ4MF8wXCIgYXMgSWQsIGlzX2dsb2JhbDogdHJ1ZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0ZXh0OiBcInRlcmVcIiwgdGl0bGU6IFwiaGVsbG9cIiB9LCBvcmc6IFwieDBfMFwiIGFzIElkIH0gYXMgY29uc3QsXHJcblx0XHQoZGlydHlfb2JqZWN0KSA9PiBbXCJ0a3RfbmV3XCIsIGRpcnR5X29iamVjdF0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRkaXJ0eV9yZXN1bHQgPSBhd2FpdCB0ZXN0RGlydHlJbnB1dFJlcXVlc3QoXCJUaWNrZXQgd2FzIGNyZWF0ZWQgd2l0aG91dCB1c2VyIGJlbG9uZ2luZyB0byBhbnkgb3JnIVwiLFxyXG5cdFx0eyBkb2M6IHsgZ3JwOiBcIngwXzBcIiBhcyBJZCwgaXNfZ2xvYmFsOiB0cnVlLCBpc19wcmlvcml0eTogZmFsc2UsIHRleHQ6IFwidGVyZVwiLCB0aXRsZTogXCJoZWxsb1wiIH0sIG9yZzogXCJ4MF8wXCIgYXMgSWQgfSBhcyBjb25zdCxcclxuXHRcdChkaXJ0eV9vYmplY3QpID0+IFtcInRrdF9uZXdcIiwgZGlydHlfb2JqZWN0XSk7XHJcblx0aWYgKGRpcnR5X3Jlc3VsdCAhPT0gdHJ1ZSkgcmV0dXJuIGRpcnR5X3Jlc3VsdDtcclxuXHJcblx0Y29uc3Qgb3JnX2lkID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cdGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIlRpY2tldCB3YXMgY3JlYXRlZCBpbiBub25leGlzdGFudCBncm91cCFcIixcclxuXHRcdHsgZG9jOiB7IGdycDogXCIxXCIgYXMgSWQsIGlzX2dsb2JhbDogdHJ1ZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0ZXh0OiBcInRlcmVcIiwgdGl0bGU6IFwiaGVsbG9cIiB9LCBvcmc6IG9yZ19pZCB9IGFzIGNvbnN0LFxyXG5cdFx0KGRpcnR5X29iamVjdCkgPT4gW1widGt0X25ld1wiLCBkaXJ0eV9vYmplY3RdKTtcclxuXHRpZiAoZGlydHlfcmVzdWx0ICE9PSB0cnVlKSByZXR1cm4gZGlydHlfcmVzdWx0O1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZzogb3JnX2lkLCBuYW1lOiBcIm5ldyBncnBcIiB9KTtcclxuXHRjb25zdCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZzogb3JnX2lkIH0pKS5kYXRhITtcclxuXHRjb25zdCBncm91cF9pZCA9IGdyb3Vwc1swXS5faWQ7XHJcblx0ZGlydHlfcmVzdWx0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiVGlja2V0IHdhcyBjcmVhdGVkIHdpdGggZGlydHkgaW5wdXQhXCIsXHJcblx0XHR7IGRvYzogeyBncnA6IGdyb3VwX2lkIGFzIElkLCBpc19nbG9iYWw6IHRydWUsIGlzX3ByaW9yaXR5OiBmYWxzZSwgdGV4dDogXCJ0ZXJlXCIsIHRpdGxlOiBcImhlbGxvXCIgfSwgb3JnOiBvcmdfaWQgfSBhcyBjb25zdCxcclxuXHRcdChkaXJ0eV9vYmplY3QpID0+IFtcInRrdF9uZXdcIiwgZGlydHlfb2JqZWN0XSk7XHJcblx0aWYgKGRpcnR5X3Jlc3VsdCAhPT0gdHJ1ZSkgcmV0dXJuIGRpcnR5X3Jlc3VsdDtcclxuXHJcblx0ZGlydHlfcmVzdWx0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiVGlja2V0IHdhcyBjcmVhdGVkIHdpdGggZGlydHkgaW5wdXQhXCIsXHJcblx0XHR7IGdycDogZ3JvdXBfaWQgYXMgSWQsIGlzX2dsb2JhbDogdHJ1ZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0ZXh0OiBcInRlcmVcIiwgdGl0bGU6IFwiaGVsbG9cIiB9IGFzIGNvbnN0LFxyXG5cdFx0KGRpcnR5X29iamVjdCkgPT4gW1widGt0X25ld1wiLCB7IGRvYzogZGlydHlfb2JqZWN0LCBvcmc6IG9yZ19pZCB9XSxcclxuXHRcdHsgaXNfZ2xvYmFsOiBbZmFsc2UsIHRydWVdLCBpc19wcmlvcml0eTogW2ZhbHNlLCB0cnVlXSwgdGV4dDogW1wiXCJdIH0pO1xyXG5cclxuXHRyZXR1cm4gZGlydHlfcmVzdWx0O1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gc3VjY2Vzc2Z1bGx5IGNyZWF0ZSBhIHRpY2tldFwiLCBcImZ1bmN0aW9uYWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCB0aXRsZSA9IFwidsOkZ2EgdMO1c2luZSBwcm9ibGVlbVwiO1xyXG5cdGNvbnN0IHRleHQgPSBcImVyYWtvcmRzZWx0IHTDtXNpbmUgcHJvYmxlZW1cIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IG9yZ19pZCA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmc6IG9yZ19pZCwgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmc6IG9yZ19pZCB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwX2lkID0gZ3JvdXBzWzBdLl9pZDtcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwidGt0X25ld1wiLCB7IGRvYzogeyBncnA6IGdycF9pZCwgaXNfZ2xvYmFsOiBmYWxzZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZSwgdGV4dCB9LCBvcmc6IG9yZ19pZCB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGNyZWF0ZSBhIGNvcnJlY3QgdGlja2V0LlwiKTtcclxuXHJcblx0Y29uc3QgdGt0ID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZHcm91cFwiLCB7IG9yZzogb3JnX2lkLCBncnA6IGdycF9pZCB9KSkuZGF0YSFbMF07XHJcblx0cmV0dXJuICh0a3QudGl0bGUgIT09IHRpdGxlIHx8IHRrdC5jb21tZW50c1swXS50ZXh0WzBdICE9PSB0ZXh0KSA/IFwiVGlja2V0IGRpZCBub3QgaGF2ZSBkZXNpcmVkIGNvbnRlbnQhXCIgOiB0cnVlO1xyXG59KTtcclxuXHJcbi8vIHRvZG86IHJhbmRvbWx5IGZhaWxlZCBvbmNlIGFuZCBub3QgYWdhaW4sIHdvbmRlciB3dGggaXMgZ29pbmcgb25cclxuY3JlYXRlVGVzdChcIkNhbiByZXBseSB0byB0aWNrZXRcIiwgXCJmdW5jdGlvbmFsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cclxuXHRjb25zdCBvcmdfaWQgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lOiBcIm5ldyBvcmdcIiwgdGltZXpvbmU6IFwiRXVyb3BlL1RhbGxpbm5cIiwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmc6IG9yZ19pZCwgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblxyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnOiBvcmdfaWQgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycF9pZCA9IGdyb3Vwc1swXS5faWQ7XHJcblx0YXdhaXQgcmVxdWVzdChcInRrdF9uZXdcIiwgeyBkb2M6IHsgZ3JwOiBncnBfaWQsIGlzX2dsb2JhbDogZmFsc2UsIGlzX3ByaW9yaXR5OiBmYWxzZSwgdGl0bGU6IFwidsOkZ2EgdMO1c2luZSBwcm9ibGVlbVwiLCB0ZXh0OiBcImVyYWtvcmRzZWx0IHTDtXNpbmUgcHJvYmxlZW1cIiB9LCBvcmc6IG9yZ19pZCB9KTtcclxuXHJcblx0Y29uc3QgdGlja2V0cyA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mR3JvdXBcIiwgeyBvcmc6IG9yZ19pZCwgZ3JwOiBncnBfaWQgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IHJlcSA9IGF3YWl0IHJlcXVlc3QoXCJ0a3RfYWRkUmVwbHlcIiwgeyBvcmc6IG9yZ19pZCwgaWQ6IHRpY2tldHNbMF0uX2lkLCB0ZXh0OiBcInRlc3QgcmVwbHlcIiB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxLCBcIlRyaWVkIHRvIGFkZCBhIHJlcGx5IHRvIHRpY2tldC5cIik7XHJcblxyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QobmFtZURpcnR5SW5wdXRUZXN0KFwidGt0X2FkZFJlcGx5XCIpLCBcInN0YWJpbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3Qgb3JnX2lkID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnOiBvcmdfaWQsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cclxuXHRjb25zdCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZzogb3JnX2lkIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnBfaWQgPSBncm91cHNbMF0uX2lkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlOiBcInbDpGdhIHTDtXNpbmUgcHJvYmxlZW1cIiwgdGV4dDogXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtXCIgfSwgb3JnOiBvcmdfaWQgfSk7XHJcblxyXG5cdGNvbnN0IHRpY2tldHMgPSAoYXdhaXQgcmVxdWVzdChcInRrdF9nZXRPZkdyb3VwXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogZ3JwX2lkIH0pKS5kYXRhITtcclxuXHRyZXR1cm4gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwidGt0X2FkZFJlcGx5IHJlY2VpdmVkIGRpcnR5IGlucHV0IVwiLCB7IG9yZzogb3JnX2lkLCBpZDogdGlja2V0c1swXS5faWQsIHRleHQ6IFwiTGFtcFwiIH0sIChkaXJ0eV9vYmplY3QpID0+IFtcInRrdF9hZGRSZXBseVwiLCBkaXJ0eV9vYmplY3RdKTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiTXVzdCBjaGVjayB1c2VyIGF1dGhvcml6YXRpb24gYmVmb3JlIGFjY2VwdGluZyB0aWNrZXRzXCIsIFwic2VjdXJpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblxyXG5cdGNvbnN0IG9yZ19pZCA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZzogb3JnX2lkLCBuYW1lOiBcIm5ldyBncnBcIiB9KTtcclxuXHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmc6IG9yZ19pZCB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwX2lkID0gZ3JvdXBzWzBdLl9pZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ291dFwiKTtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQgKyAxLCBcInVzZXJcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCIxQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdChcInRrdF9uZXdcIiwgeyBkb2M6IHsgZ3JwOiBncnBfaWQsIGlzX2dsb2JhbDogZmFsc2UsIGlzX3ByaW9yaXR5OiBmYWxzZSwgdGl0bGU6IFwidsOkZ2EgdMO1c2luZSBwcm9ibGVlbVwiLCB0ZXh0OiBcImVyYWtvcmRzZWx0IHTDtXNpbmUgcHJvYmxlZW1cIiB9LCBvcmc6IG9yZ19pZCB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0RGVuaWVkKHJlcSwgXCJUcmllZCB0byBhY2NlcHQgYSB0aWNrZXQgZnJvbSBhbiB1bmF1dGhvcml6ZWQgdXNlci5cIik7XHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIk11c3Qgbm90IHJldmVhbCB0aWNrZXRzIHRvIHVuYXV0aG9yaXplZCB1c2Vyc1wiLCBcInNlY3VyaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgdXNlcjAgPSBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCI7XHJcblx0Y29uc3QgdXNlcjEgPSBjb250ZXh0ICsgXCIxQHNoYXJrbGFzZXJzLmNvbVwiO1xyXG5cdGF3YWl0IGNyZWF0ZVVzZXIoY29udGV4dCArIDEsIFwidXNlclwiKTtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IHVzZXIwLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3Qgb3JnX2lkID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJpbnZpdGVcIiwgeyBvcmc6IG9yZ19pZCwgZW1haWw6IHVzZXIxIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnOiBvcmdfaWQsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cclxuXHRjb25zdCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZzogb3JnX2lkIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnBfaWQgPSBncm91cHNbMF0uX2lkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlOiBcInbDpGdhIHTDtXNpbmUgcHJvYmxlZW1cIiwgdGV4dDogXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtXCIgfSwgb3JnOiBvcmdfaWQgfSk7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dvdXRcIik7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogdXNlcjEsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGxldCByZXEgPSAoYXdhaXQgcmVxdWVzdChcInRrdF9nZXRPZkdyb3VwXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogZ3JwX2lkIH0pKTtcclxuXHR2ZXJpZnlSZXF1ZXN0RGVuaWVkKHJlcSwgXCJUcmllZCB0byBhY2Nlc3MgdGlja2V0cyBvdXRzaWRlIHRoZSBvcmcuXCIpO1xyXG5cclxuXHRjb25zdCBpbnZpdGF0aW9uID0gKGF3YWl0IHJlcXVlc3QoXCJnZXRJbnZpdGF0aW9uc1wiKSkuZGF0YSE7XHJcblx0YXdhaXQgcmVxdWVzdChcImFjY2VwdEludml0YXRpb25cIiwgeyBpbnY6IGludml0YXRpb25bMF0uX2lkIH0pO1xyXG5cclxuXHRyZXEgPSAoYXdhaXQgcmVxdWVzdChcInRrdF9nZXRPZkdyb3VwXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogZ3JwX2lkIH0pKTtcclxuXHR2ZXJpZnlSZXF1ZXN0RGVuaWVkKHJlcSwgXCJUcmllZCB0byBhY2Nlc3MgdGlja2V0cyBvdXRzaWRlIHRoZSBncnAuXCIpO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIGdldCBnbG9iYWwgYW5kIHNlbGYgbWFkZSB0aWNrZXRzXCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHRpdGxlX2dsb2JhbCA9IFwidsOkZ2EgdMO1c2luZSBwcm9ibGVlbVwiO1xyXG5cdGNvbnN0IHRleHRfZ2xvYmFsID0gXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtXCI7XHJcblx0Y29uc3QgdGl0bGVfc2VsZiA9IFwiaXNpa2xpayBwcm9ibGVlbVwiO1xyXG5cdGNvbnN0IHRleHRfc2VsZiA9IFwiZXJha29yZHNlbHQgaXNpa2xpayBwcm9ibGVlbVwiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZywgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycCA9IGdyb3Vwc1swXS5faWQ7XHJcblx0YXdhaXQgcmVxdWVzdChcInRrdF9uZXdcIiwgeyBkb2M6IHsgZ3JwLCBpc19nbG9iYWw6IHRydWUsIGlzX3ByaW9yaXR5OiBmYWxzZSwgdGl0bGU6IHRpdGxlX2dsb2JhbCwgdGV4dDogdGV4dF9nbG9iYWwgfSwgb3JnIH0pO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycCwgaXNfZ2xvYmFsOiBmYWxzZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZTogdGl0bGVfc2VsZiwgdGV4dDogdGV4dF9zZWxmIH0sIG9yZyB9KTtcclxuXHJcblx0Ly8gY2FuIGFjY2VzcyBnbG9iYWwgdGlja2V0XHJcblx0bGV0IHJlcSA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mTWV0YVwiLCB7IG9yZywgZ3JwOiBcIkdsb2JhbFwiIH0pKTtcclxuXHRsZXQgdGt0ID0gcmVxLmRhdGEhWzBdO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gZ2V0IGdsb2JhbCB0aWNrZXRzLlwiKTtcclxuXHRpZiAoISh0a3QudGl0bGUgPT09IHRpdGxlX2dsb2JhbCAmJiB0a3QuY29tbWVudHNbMF0udGV4dFswXSA9PT0gdGV4dF9nbG9iYWwpKSByZXR1cm4gXCJDb3VsZCBub3QgZ2V0IGEgc2VsZi1tYWRlIHRpY2tldCFcIjtcclxuXHJcblx0Ly8gY2FuIGFjY2VzcyBzZWxmLW1hZGUgdGlja2V0XHJcblx0cmVxID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZNZXRhXCIsIHsgb3JnLCBncnA6IFwiU2VsZlwiIH0pKTtcclxuXHR0a3QgPSByZXEuZGF0YSFbMV07XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcSwgXCJUcmllZCB0byBnZXQgc2VsZi1tYWRlIHRpY2tldC5cIik7XHJcblx0aWYgKCEodGt0LnRpdGxlID09PSB0aXRsZV9zZWxmICYmIHRrdC5jb21tZW50c1swXS50ZXh0WzBdID09PSB0ZXh0X3NlbGYpKSByZXR1cm4gXCJDb3VsZCBub3QgZ2V0IGEgc2VsZi1tYWRlIHRpY2tldCFcIjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiBtb2RpZnkgYW4gZXhpc3RpbmcgdGlja2V0XCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdC8vIHNldCB1cFxyXG5cdGNvbnN0IHRpdGxlID0gXCJ2w6RnYSB0w7VzaW5lIHByb2JsZWVtXCI7XHJcblx0Y29uc3QgdGV4dCA9IFwiZXJha29yZHNlbHQgdMO1c2luZSBwcm9ibGVlbVwiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnX2lkID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZzogb3JnX2lkLCBuYW1lOiBcIm5ldyBncnBcIiB9KTtcclxuXHRjb25zdCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZzogb3JnX2lkIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnBfaWQgPSBncm91cHNbMF0uX2lkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlLCB0ZXh0IH0sIG9yZzogb3JnX2lkIH0pO1xyXG5cclxuXHRsZXQgcmVxID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZNZXRhXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogXCJTZWxmXCIgfSkpO1xyXG5cdGxldCB0a3QgPSByZXEuZGF0YSFbMF07XHJcblxyXG5cdC8vIElzIGl0IHBvc3NpYmxlIHRvIG1vZGlmeT9cclxuXHRsZXQgb3B0aW9ucyA9IHsgaXNfZ2xvYmFsOiB0cnVlLCBpc19wcmlvcml0eTogdHJ1ZSwgaXNfcmVzb2x2ZWQ6IHRydWUgfTtcclxuXHRsZXQgbW9kaWZ5X3JlcSA9IGF3YWl0IHJlcXVlc3QoXCJ0a3RfbW9kaWZ5XCIsIHsgb3JnOiBvcmdfaWQsIGlkOiB0a3QuX2lkLCBvcHRpb25zIH0pO1xyXG5cdGxldCBjdXJfdGltZSA9IHRpbWVzdGFtcCgpO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChtb2RpZnlfcmVxLCBcIlRyaWVkIHRvIG1vZGlmeSBhIHRpY2tldC5cIik7XHJcblxyXG5cdHJlcSA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mTWV0YVwiLCB7IG9yZzogb3JnX2lkLCBncnA6IFwiU2VsZlwiIH0pKTtcclxuXHR0a3QgPSByZXEuZGF0YSFbMF07XHJcblxyXG5cdGlmICh0a3Q/LmlzX2dsb2JhbCAhPT0gb3B0aW9ucy5pc19nbG9iYWwgfHwgdGt0Py5pc19wcmlvcml0eSAhPT0gb3B0aW9ucy5pc19wcmlvcml0eSB8fCB0a3Q/LmlzX3Jlc29sdmVkICE9PSBvcHRpb25zLmlzX3Jlc29sdmVkKSB7XHJcblx0XHRyZXR1cm4gXCJDb3VsZCBub3QgbW9kaWZ5IHRpY2tldCB3aXRoIG5ldyBkYXRhISBFeHBlY3RlZDogXCIgKyBKU09OLnN0cmluZ2lmeSh7IC4uLnRrdCwgLi4ub3B0aW9ucyB9KSArXHJcblx0XHRcdFwiLCByZWNlaXZlZDogXCIgKyB0a3Q7XHJcblx0fVxyXG5cdHRrdCA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mR3JvdXBcIiwgeyBvcmc6IG9yZ19pZCwgZ3JwOiBncnBfaWQgfSkpLmRhdGEhWzBdO1xyXG5cclxuXHQvLyB0byBjb21wYXJlIHRoZW0sIHRoZXkgaGF2ZSB0byBiZSBpbiBvcmRlciBhbmQgbWFkZSBtb3JlIHNpbWlsYXIgb3Igc3RoIGlka1xyXG5cdGxldCBleHBlY3RlZF9hY3Rpb25fdHlwZXMgPSBKU09OLnN0cmluZ2lmeShbXCJzZXRHbG9iYWxcIiwgXCJzZXRQcmlvcml0eVwiLCBcImxvY2tcIl0uc29ydCgpKTtcclxuXHRsZXQgcmVjZWl2ZWRfYWN0aW9uX3R5cGVzID0gSlNPTi5zdHJpbmdpZnkodGt0LmFjdGlvbnMubWFwKGFjdGlvbiA9PiBhY3Rpb24udHlwZSkuc29ydCgpKTtcclxuXHRcclxuXHRpZiAoZXhwZWN0ZWRfYWN0aW9uX3R5cGVzICE9PSByZWNlaXZlZF9hY3Rpb25fdHlwZXMpIHJldHVybiBcIkFjdGlvbiB0eXBlcyBkaWQgbm90IG1hdGNoIVxcbkV4cGVjdGVkOiBcIiArIGV4cGVjdGVkX2FjdGlvbl90eXBlcyArIFwiXFxuUmVjZWl2ZWQ6IFwiICsgcmVjZWl2ZWRfYWN0aW9uX3R5cGVzO1xyXG5cdGlmICghKHRrdC5hY3Rpb25zWzBdLnBvc3RlZC5hdCA+PSBjdXJfdGltZSAtIDIwMDAgJiYgdGt0LmFjdGlvbnNbMF0ucG9zdGVkLmF0IC0gY3VyX3RpbWUgPCAzMDAwKSkge1xyXG5cdFx0cmV0dXJuIFwiVGltZXN0YW1wIHdhcyBpbmNvcnJlY3QhXFxuRXhwZWN0ZWQ6IFwiICsgZGF0ZWMoY3VyX3RpbWUpICtcclxuXHRcdFx0XCJcXG5SZWNlaXZlZDogXCIgKyBkYXRlYyh0a3QuYWN0aW9uc1swXS5wb3N0ZWQuYXQpICtcclxuXHRcdFx0XCJcXG5UaGUgZGlmZmVyZW5jZTogXCIgKyBKU09OLnN0cmluZ2lmeShNYXRoLmFicyh0a3QuYWN0aW9uc1swXS5wb3N0ZWQuYXQgLSBjdXJfdGltZSkgKyBcIm1zXCIpO1xyXG5cdH1cclxuXHJcblx0Ly8gc2V0IHRpY2tldCByZXNvbHZlZCBcclxuXHRvcHRpb25zID0geyBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIGlzX3Jlc29sdmVkOiBmYWxzZSB9O1xyXG5cdG1vZGlmeV9yZXEgPSBhd2FpdCByZXF1ZXN0KFwidGt0X21vZGlmeVwiLCB7IG9yZzogb3JnX2lkLCBpZDogdGt0Ll9pZCwgb3B0aW9ucyB9KTtcclxuXHRjdXJfdGltZSA9IHRpbWVzdGFtcCgpO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChtb2RpZnlfcmVxLCBcIlRyaWVkIHRvIG1hcmsgdGlja2V0IGFzIHJlc29sdmVkLlwiKTtcclxuXHJcblx0cmVxID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZNZXRhXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogXCJTZWxmXCIgfSkpO1xyXG5cdHRrdCA9IHJlcS5kYXRhIVswXTtcclxuXHRpZiAodGt0Py5pc19nbG9iYWwgIT09IG9wdGlvbnMuaXNfZ2xvYmFsIHx8IHRrdD8uaXNfcHJpb3JpdHkgIT09IG9wdGlvbnMuaXNfcHJpb3JpdHkgfHwgdGt0Py5pc19yZXNvbHZlZCAhPT0gb3B0aW9ucy5pc19yZXNvbHZlZCkge1xyXG5cdFx0cmV0dXJuIFwiQ291bGQgbm90IG1vZGlmeSB0aWNrZXQgd2l0aCBuZXcgZGF0YSEgRXhwZWN0ZWQ6IFwiICsgSlNPTi5zdHJpbmdpZnkoeyAuLi50a3QsIC4uLm9wdGlvbnMgfSkgKyBcIiwgcmVjZWl2ZWQ6IFwiICsgdGt0O1xyXG5cdH1cclxuXHJcblx0Ly8gY2hlY2tzIHRpbWUgbG9nXHJcblx0dGt0ID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZHcm91cFwiLCB7IG9yZzogb3JnX2lkLCBncnA6IGdycF9pZCB9KSkuZGF0YSFbMF07XHJcblxyXG5cdGV4cGVjdGVkX2FjdGlvbl90eXBlcyA9IEpTT04uc3RyaW5naWZ5KFtcInNldEdsb2JhbFwiLCBcInNldFByaW9yaXR5XCIsIFwibG9ja1wiLCBcInVubG9ja1wiLCBcInVuc2V0R2xvYmFsXCIsIFwidW5zZXRQcmlvcml0eVwiXS5zb3J0KCkpO1xyXG5cdHJlY2VpdmVkX2FjdGlvbl90eXBlcyA9IEpTT04uc3RyaW5naWZ5KHRrdC5hY3Rpb25zLm1hcChhY3Rpb24gPT4gYWN0aW9uLnR5cGUpLnNvcnQoKSk7XHJcblx0XHJcblx0aWYgKGV4cGVjdGVkX2FjdGlvbl90eXBlcyAhPT0gcmVjZWl2ZWRfYWN0aW9uX3R5cGVzKSByZXR1cm4gXCJBY3Rpb24gdHlwZXMgZGlkIG5vdCBtYXRjaCFcXG5FeHBlY3RlZDogXCIgKyBleHBlY3RlZF9hY3Rpb25fdHlwZXMgKyBcIlxcblJlY2VpdmVkOiBcIiArIHJlY2VpdmVkX2FjdGlvbl90eXBlcztcclxuXHRpZiAoISh0a3QuYWN0aW9uc1swXS5wb3N0ZWQuYXQgPj0gY3VyX3RpbWUgLSAyMDAwICYmIHRrdC5hY3Rpb25zWzBdLnBvc3RlZC5hdCAtIGN1cl90aW1lIDwgMzAwMCkpIHtcclxuXHRcdHJldHVybiBcIlRpbWVzdGFtcCB3YXMgaW5jb3JyZWN0IVxcbkV4cGVjdGVkOiBcIiArIGRhdGVjKGN1cl90aW1lKSArXHJcblx0XHRcdFwiXFxuUmVjZWl2ZWQ6IFwiICsgZGF0ZWModGt0LmFjdGlvbnNbMF0ucG9zdGVkLmF0KSArXHJcblx0XHRcdFwiXFxuVGhlIGRpZmZlcmVuY2U6IFwiICsgSlNPTi5zdHJpbmdpZnkoTWF0aC5hYnModGt0LmFjdGlvbnNbMF0ucG9zdGVkLmF0IC0gY3VyX3RpbWUpICsgXCJtc1wiKTtcclxuXHR9XHJcblxyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QobmFtZURpcnR5SW5wdXRUZXN0KFwidGt0X21vZGlmeVwiKSwgXCJzdGFiaWxpdHlcIiwgYXN5bmMgKGNvbnRleHQpID0+IHtcclxuXHRjb25zdCB0aXRsZSA9IFwidsOkZ2EgdMO1c2luZSBwcm9ibGVlbVwiO1xyXG5cdGNvbnN0IHRleHQgPSBcImVyYWtvcmRzZWx0IHTDtXNpbmUgcHJvYmxlZW1cIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cdGNvbnN0IG9yZ19pZCA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmc6IG9yZ19pZCwgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmc6IG9yZ19pZCB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwID0gZ3JvdXBzWzBdLl9pZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwidGt0X25ld1wiLCB7IGRvYzogeyBncnAsIGlzX2dsb2JhbDogdHJ1ZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZSwgdGV4dCB9LCBvcmc6IG9yZ19pZCB9KTtcclxuXHJcblx0Y29uc3QgcmVxID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZNZXRhXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogXCJHbG9iYWxcIiB9KSk7XHJcblx0Y29uc3QgdGt0ID0gcmVxLmRhdGEhWzBdO1xyXG5cclxuXHRjb25zdCBvcHRpb25zID0geyBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogdHJ1ZSwgaXNfcmVzb2x2ZWQ6IHRydWUgfTtcclxuXHJcblx0bGV0IGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIlRoZXJlIHdhcyBhIHByb2JsZW0gbW9kaWZ5aW5nIGEgdGlja2V0IVwiLCB7IG9yZzogb3JnX2lkLCBpZDogdGt0Ll9pZCwgb3B0aW9ucyB9LCAoZGlydHlfb2JqZWN0KSA9PlxyXG5cdFx0W1widGt0X21vZGlmeVwiLCBkaXJ0eV9vYmplY3RdLCB7IG9wdGlvbnM6IFt7fV0gfSk7XHJcblx0aWYgKGRpcnR5X3Jlc3VsdCAhPT0gdHJ1ZSkgcmV0dXJuIGRpcnR5X3Jlc3VsdDtcclxuXHJcblx0ZGlydHlfcmVzdWx0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiVGhlcmUgd2FzIGEgcHJvYmxlbSBtb2RpZnlpbmcgYSB0aWNrZXQhXCIsIG9wdGlvbnMsIChkaXJ0eV9vYmplY3QpID0+XHJcblx0XHRbXCJ0a3RfbW9kaWZ5XCIsIHsgb3JnOiBvcmdfaWQsIGlkOiB0a3QuX2lkLCBvcHRpb25zOiBkaXJ0eV9vYmplY3QgfV0sIHsgaXNfZ2xvYmFsOiBbdHJ1ZSwgZmFsc2UsIG51bGwsIHVuZGVmaW5lZF0sIGlzX3ByaW9yaXR5OiBbdHJ1ZSwgZmFsc2UsIG51bGwsIHVuZGVmaW5lZF0sIGlzX3Jlc29sdmVkOiBbdHJ1ZSwgZmFsc2UsIG51bGwsIHVuZGVmaW5lZF0sIHbDtXRpOiBbe31dIH0pO1xyXG5cclxuXHRyZXR1cm4gZGlydHlfcmVzdWx0O1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QoXCJDYW4gcmVtb3ZlIHdoaXRlc3BhY2VzIGZyb20gdGl0bGVcIiwgXCJleHRyYVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGxldCB0aXRsZSA9IFwiIHbDpGdhIHTDtXNpbmUgcHJvYmxlZW1cIjtcclxuXHRjb25zdCB0ZXh0ID0gXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtXCI7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmdfaWQgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lOiBcIm5ldyBvcmdcIiwgdGltZXpvbmU6IFwiRXVyb3BlL1RhbGxpbm5cIiwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnOiBvcmdfaWQsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnOiBvcmdfaWQgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycF9pZCA9IGdyb3Vwc1swXS5faWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlLCB0ZXh0IH0sIG9yZzogb3JnX2lkIH0pO1xyXG5cclxuXHR0aXRsZSA9IFwidsOkZ2EgdMO1c2luZSBwcm9ibGVlbSBcIjtcclxuXHRhd2FpdCByZXF1ZXN0KFwidGt0X25ld1wiLCB7IGRvYzogeyBncnA6IGdycF9pZCwgaXNfZ2xvYmFsOiBmYWxzZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZSwgdGV4dCB9LCBvcmc6IG9yZ19pZCB9KTtcclxuXHJcblx0dGl0bGUgPSBcInbDpGdhIHTDtXNpbmUgXFxuXFxuXFxuXFxuXFxuIHByb2JsZWVtXCI7XHJcblx0YXdhaXQgcmVxdWVzdChcInRrdF9uZXdcIiwgeyBkb2M6IHsgZ3JwOiBncnBfaWQsIGlzX2dsb2JhbDogZmFsc2UsIGlzX3ByaW9yaXR5OiBmYWxzZSwgdGl0bGUsIHRleHQgfSwgb3JnOiBvcmdfaWQgfSk7XHJcblxyXG5cdGNvbnN0IHRpY2tldHM6IFByb2JsZW1bXSA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mR3JvdXBcIiwgeyBvcmc6IG9yZ19pZCwgZ3JwOiBncnBfaWQgfSkpLmRhdGEhO1xyXG5cdGlmICghdGlja2V0c1swXS50aXRsZS5zdGFydHNXaXRoKFwidlwiKSkgcmV0dXJuIFwiVGhlIGxlYWRpbmcgd2hpdGVzcGFjZSB3YXMgbm90IHJlbW92ZWQgZnJvbSB0aXRsZSFcIjtcclxuXHRpZiAoIXRpY2tldHNbMV0udGl0bGUuZW5kc1dpdGgoXCJtXCIpKSByZXR1cm4gXCJUaGUgdHJhaWxpbmcgd2hpdGVzcGFjZSB3YXMgbm90IHJlbW92ZWQgZnJvbSB0aXRsZSFcIjtcclxuXHRpZiAodGlja2V0c1syXS50aXRsZS5pbmNsdWRlcyhcIlxcblwiKSkgcmV0dXJuIFwiTmV3bGluZXMgd2VyZSBub3QgcmVtb3ZlZCBmcm9tIHRpdGxlIVwiO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiQ2FuIHJlbW92ZSB3aGl0ZXNwYWNlcyBmcm9tIHRleHQgKGRlc2NyaXB0aW9uKVwiLCBcImV4dHJhXCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgdGl0bGUgPSBcInbDpGdhIHTDtXNpbmUgcHJvYmxlZW1cIjtcclxuXHRsZXQgdGV4dCA9IFwiIGVyYWtvcmRzZWx0IHTDtXNpbmUgcHJvYmxlZW1cIjtcclxuXHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiBjb250ZXh0ICsgXCJAc2hhcmtsYXNlcnMuY29tXCIsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cclxuXHRjb25zdCBvcmdfaWQgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lOiBcIm5ldyBvcmdcIiwgdGltZXpvbmU6IFwiRXVyb3BlL1RhbGxpbm5cIiwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblx0YXdhaXQgcmVxdWVzdChcImdycF9uZXdcIiwgeyBvcmc6IG9yZ19pZCwgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblxyXG5cdGNvbnN0IGdyb3VwcyA9IChhd2FpdCByZXF1ZXN0KFwiZ3JwX2dldE9mT3JnXCIsIHsgb3JnOiBvcmdfaWQgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycF9pZCA9IGdyb3Vwc1swXS5faWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlLCB0ZXh0IH0sIG9yZzogb3JnX2lkIH0pO1xyXG5cclxuXHR0ZXh0ID0gXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtIFwiO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlLCB0ZXh0IH0sIG9yZzogb3JnX2lkIH0pO1xyXG5cclxuXHR0ZXh0ID0gXCJcXG5cXG5cXG5cXG5lcmFrb3Jkc2VsdCBcXG5cXG5cXG5cXG4gdMO1c2luZSBcXG5cXG5cXG5cXG5cXG4gcHJvYmxlZW1cXG5cXG5cXG5cXG5cIjtcclxuXHRhd2FpdCByZXF1ZXN0KFwidGt0X25ld1wiLCB7IGRvYzogeyBncnA6IGdycF9pZCwgaXNfZ2xvYmFsOiBmYWxzZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZSwgdGV4dCB9LCBvcmc6IG9yZ19pZCB9KTtcclxuXHJcblx0Y29uc3QgdGlja2V0czogUHJvYmxlbVtdID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZHcm91cFwiLCB7IG9yZzogb3JnX2lkLCBncnA6IGdycF9pZCB9KSkuZGF0YSE7XHJcblxyXG5cdGlmICghdGlja2V0c1swXS5jb21tZW50c1swXS50ZXh0WzBdLnN0YXJ0c1dpdGgoXCJlXCIpKSByZXR1cm4gXCJUaGUgbGVhZGluZyB3aGl0ZXNwYWNlIHdhcyBub3QgcmVtb3ZlZCBmcm9tIHRleHQhXCI7XHJcblxyXG5cdGlmICghdGlja2V0c1sxXS5jb21tZW50c1swXS50ZXh0WzBdLmVuZHNXaXRoKFwibVwiKSkgcmV0dXJuIFwiVGhlIHRyYWlsaW5nIHdoaXRlc3BhY2Ugd2FzIG5vdCByZW1vdmVkIGZyb20gdGV4dCFcIjtcclxuXHJcblx0Y29uc3QgdGV4dF9zdHJpbmcgPSB0aWNrZXRzWzJdLmNvbW1lbnRzWzBdLnRleHQuam9pbihcIsOkXCIpO1xyXG5cdGlmICh0ZXh0X3N0cmluZy5pbmNsdWRlcyhcIlxcblwiKSkgcmV0dXJuIFwiTmV3bGluZXMgd2VyZSBub3QgcmVtb3ZlZCBmcm9tIHRleHQhXCI7XHJcblx0aWYgKHRleHRfc3RyaW5nICE9PSBcImVyYWtvcmRzZWx0w6R0w7VzaW5lw6Rwcm9ibGVlbVwiKSByZXR1cm4gXCJQYXJhZ3JhcGhzIHdlcmUgbm90IHNwbGl0IHVwIGNvcnJlY3RseSFcIjtcclxuXHJcblx0cmV0dXJuIHRydWU7XHJcbn0pO1xyXG5cclxuY3JlYXRlVGVzdChcIkNhbiByZW1vdmUgd2hpdGVzcGFjZXMgZnJvbSByZXBseVwiLCBcImV4dHJhXCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0bGV0IHRleHQgPSBcIiB0ZXN0IHJlcGx5XCI7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHJcblx0Y29uc3Qgb3JnX2lkID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnOiBvcmdfaWQsIG5hbWU6IFwibmV3IGdycFwiIH0pO1xyXG5cclxuXHRjb25zdCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZzogb3JnX2lkIH0pKS5kYXRhITtcclxuXHRjb25zdCBncnBfaWQgPSBncm91cHNbMF0uX2lkO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfbmV3XCIsIHsgZG9jOiB7IGdycDogZ3JwX2lkLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlOiBcInbDpGdhIHTDtXNpbmUgcHJvYmxlZW1cIiwgdGV4dDogXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtXCIgfSwgb3JnOiBvcmdfaWQgfSk7XHJcblxyXG5cdGNvbnN0IHRpY2tldHMgPSAoYXdhaXQgcmVxdWVzdChcInRrdF9nZXRPZkdyb3VwXCIsIHsgb3JnOiBvcmdfaWQsIGdycDogZ3JwX2lkIH0pKS5kYXRhITtcclxuXHRhd2FpdCByZXF1ZXN0KFwidGt0X2FkZFJlcGx5XCIsIHsgb3JnOiBvcmdfaWQsIGlkOiB0aWNrZXRzWzBdLl9pZCwgdGV4dCB9KTtcclxuXHJcblx0dGV4dCA9IFwidGVzdCByZXBseSBcIjtcclxuXHRhd2FpdCByZXF1ZXN0KFwidGt0X2FkZFJlcGx5XCIsIHsgb3JnOiBvcmdfaWQsIGlkOiB0aWNrZXRzWzBdLl9pZCwgdGV4dCB9KTtcclxuXHJcblx0dGV4dCA9IFwiXFxuXFxuXFxuXFxuIHRlc3QgXFxuXFxuXFxuXFxuIHJlcGx5IFxcblxcblxcblxcblwiO1xyXG5cdGF3YWl0IHJlcXVlc3QoXCJ0a3RfYWRkUmVwbHlcIiwgeyBvcmc6IG9yZ19pZCwgaWQ6IHRpY2tldHNbMF0uX2lkLCB0ZXh0IH0pO1xyXG5cclxuXHRjb25zdCBmaXhlZF9yZXBsaWVzID0gKGF3YWl0IHJlcXVlc3QoXCJ0a3RfZ2V0T2ZHcm91cFwiLCB7IG9yZzogb3JnX2lkLCBncnA6IGdycF9pZCB9KSkuZGF0YSFbMF0uY29tbWVudHM7XHJcblx0aWYgKCFmaXhlZF9yZXBsaWVzWzFdLnRleHRbMF0uc3RhcnRzV2l0aChcInRcIikpIHJldHVybiBcIlRoZSBsZWFkaW5nIHdoaXRlc3BhY2Ugd2FzIG5vdCByZW1vdmVkIGZyb20gcmVwbHkhXCI7XHJcblx0aWYgKCFmaXhlZF9yZXBsaWVzWzJdLnRleHRbMF0uZW5kc1dpdGgoXCJ5XCIpKSByZXR1cm4gXCJUaGUgdHJhaWxpbmcgd2hpdGVzcGFjZSB3YXMgbm90IHJlbW92ZWQgZnJvbSByZXBseSFcIjtcclxuXHJcblx0Y29uc3QgZmluYWxfc3RyaW5nID0gZml4ZWRfcmVwbGllc1szXS50ZXh0LmpvaW4oXCLDpFwiKTtcclxuXHRpZiAoZmluYWxfc3RyaW5nLmluY2x1ZGVzKFwiXFxuXCIpKSByZXR1cm4gXCJOZXdsaW5lcyB3ZXJlIG5vdCByZW1vdmVkIGZyb20gdGV4dCFcIjtcclxuXHRpZiAoZmluYWxfc3RyaW5nICE9PSBcInRlc3TDpHJlcGx5XCIpIHJldHVybiBcIlBhcmFncmFwaHMgd2VyZSBub3Qgc3BsaXQgdXAgY29ycmVjdGx5IVwiO1xyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbmNyZWF0ZVRlc3QobmFtZURpcnR5SW5wdXRUZXN0KFwidGt0X3JlYWRcIiksIFwic3RhYmlsaXR5XCIsIGFzeW5jIChjb250ZXh0KSA9PiB7XHJcblx0Y29uc3QgdGl0bGUgPSBcInbDpGdhIHTDtXNpbmUgcHJvYmxlZW1cIjtcclxuXHRjb25zdCB0ZXh0ID0gXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtXCI7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBvcmcgPSAoYXdhaXQgcmVxdWVzdChcIm9yZ19uZXdcIiwgeyBuYW1lOiBcIm5ldyBvcmdcIiwgdGltZXpvbmU6IFwiRXVyb3BlL1RhbGxpbm5cIiwgb3B0aW9uczoge30gfSkpLmRhdGEgYXMgSWQ7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJncnBfbmV3XCIsIHsgb3JnLCBuYW1lOiBcIm5ldyBncnBcIiB9KTtcclxuXHRjb25zdCBncm91cHMgPSAoYXdhaXQgcmVxdWVzdChcImdycF9nZXRPZk9yZ1wiLCB7IG9yZyB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwID0gZ3JvdXBzWzBdLl9pZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwidGt0X25ld1wiLCB7IGRvYzogeyBncnAsIGlzX2dsb2JhbDogZmFsc2UsIGlzX3ByaW9yaXR5OiBmYWxzZSwgdGl0bGUsIHRleHQgfSwgb3JnIH0pO1xyXG5cclxuXHRjb25zdCB0a3QgPSAoYXdhaXQgcmVxdWVzdChcInRrdF9nZXRPZkdyb3VwXCIsIHsgb3JnLCBncnAgfSkpLmRhdGEhWzBdO1xyXG5cclxuXHRsZXQgZGlydHlfcmVzdWx0ID0gYXdhaXQgdGVzdERpcnR5SW5wdXRSZXF1ZXN0KFwiVGlja2V0IHJlYWQgcmVjZWl2ZWQgZGlydHkgaW5wdXQhXCIsXHJcblx0XHR7IG9yZywgdGt0OiB0a3QuX2lkLCBrZXk6IDEgfSxcclxuXHRcdChkaXJ0eV9vYmplY3QpID0+IFtcInRrdF9yZWFkXCIsIGRpcnR5X29iamVjdF0pO1xyXG5cdGlmIChkaXJ0eV9yZXN1bHQgIT09IHRydWUpIHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcblxyXG5cdGRpcnR5X3Jlc3VsdCA9IGF3YWl0IHRlc3REaXJ0eUlucHV0UmVxdWVzdChcIlRpY2tldCByZWFkIHJlY2VpdmVkIGRpcnR5IGlucHV0IVwiLFxyXG5cdFx0eyBvcmcsIGdycCB9LFxyXG5cdFx0KGRpcnR5X29iamVjdCkgPT4gW1widGt0X3JlYWRcIiwgZGlydHlfb2JqZWN0XSk7XHJcblxyXG5cdHJldHVybiBkaXJ0eV9yZXN1bHQ7XHJcbn0pO1xyXG4iLCJpbXBvcnQgeyBBbGxSZXF1ZXN0cyB9IGZyb20gXCJAL2RhdGEvUmVxdWVzdFByb3RvY29sXCI7XHJcbmltcG9ydCB7IERhdGVUaW1lIH0gZnJvbSBcImx1eG9uXCI7XHJcbmltcG9ydCB7IHJlcXVlc3QgfSBmcm9tIFwiLi9yZXF1ZXN0X3Rlc3RcIjtcclxuaW1wb3J0IHsgYWRtaW5fcHdkLCBhZG1pbl91c3JfbmFtZSwgdGVzdF9rZXkgfSBmcm9tIFwiLi9jb25maWdcIjtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiB0aW1lc3RhbXAoKSB7IHJldHVybiBuZXcgRGF0ZSgpLmdldFRpbWUoKTsgfVxyXG5leHBvcnQgZnVuY3Rpb24gc2xlZXAobXM6IG51bWJlcik6IFByb21pc2U8dHJ1ZT4geyByZXR1cm4gbmV3IFByb21pc2UociA9PiBzZXRUaW1lb3V0KCgpID0+IHsgcih0cnVlKTsgfSwgbXMpKTsgfVxyXG5cclxuY29uc3QgZGlydHlfdmFsdWVzID0gW251bGwsIFwiXCIsIDEyMywgdHJ1ZSwgZmFsc2UsIFtdLCB7fSwgdW5kZWZpbmVkXSBhcyBjb25zdDtcclxudHlwZSBkaXJ0eV92YWx1ZSA9ICh0eXBlb2YgZGlydHlfdmFsdWVzKVtudW1iZXJdO1xyXG5jb25zdCBvYmplY3Rfcm9vdF9rZXkgPSBcInbDtXRpXCIgYXMgY29uc3Q7XHJcbnR5cGUgZXhjbHVzaW9uX21hcDxUPiA9IFBhcnRpYWw8UmVjb3JkPGtleW9mIFQgfCB0eXBlb2Ygb2JqZWN0X3Jvb3Rfa2V5LCBkaXJ0eV92YWx1ZVtdPj47XHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBjb21iaW5hdGVEaXJ0eU9iamVjdHM8VD4odmFsaWRfb2JqZWN0OiBULCBmb3JlYWNoOiAoZGlydHlfb2JqZWN0OiBUKSA9PiBQcm9taXNlPHRydWUgfCBzdHJpbmc+LCBleGNsdWRlPzogZXhjbHVzaW9uX21hcDxUPikge1xyXG5cdGNvbnN0IGNsZWFuX2pzb24gPSBKU09OLnN0cmluZ2lmeSh2YWxpZF9vYmplY3QpO1xyXG5cdGNvbnN0IGZpZWxkX25hbWVzID0gT2JqZWN0LmtleXModmFsaWRfb2JqZWN0KTtcclxuXHJcblx0Zm9yIChjb25zdCBmaWVsZF9uYW1lIG9mIGZpZWxkX25hbWVzKSB7XHJcblx0XHRmb3IgKGNvbnN0IGRpcnR5X3ZhbHVlIG9mIGRpcnR5X3ZhbHVlcykge1xyXG5cdFx0XHRpZiAoSlNPTi5zdHJpbmdpZnkoZGlydHlfdmFsdWUpID09PSBKU09OLnN0cmluZ2lmeSgodmFsaWRfb2JqZWN0IGFzIGFueSlbZmllbGRfbmFtZV0pIHx8XHJcblx0XHRcdFx0ZXhjbHVkZSAmJiAoZXhjbHVkZSBhcyBhbnkpW2ZpZWxkX25hbWVdPy5zb21lKChjdXJyZW50OiBkaXJ0eV92YWx1ZSkgPT4gSlNPTi5zdHJpbmdpZnkoY3VycmVudCkgPT09IEpTT04uc3RyaW5naWZ5KGRpcnR5X3ZhbHVlKSkpIGNvbnRpbnVlO1xyXG5cdFx0XHRjb25zdCBkaXJ0eV9vYmplY3QgPSBKU09OLnBhcnNlKGNsZWFuX2pzb24pO1xyXG5cdFx0XHRkaXJ0eV9vYmplY3RbZmllbGRfbmFtZV0gPSBkaXJ0eV92YWx1ZTtcclxuXHRcdFx0Y29uc3QgcmVzdWx0ID0gYXdhaXQgZm9yZWFjaChkaXJ0eV9vYmplY3QpO1xyXG5cdFx0XHRpZiAocmVzdWx0ICE9PSB0cnVlKSByZXR1cm4gcmVzdWx0O1xyXG5cdFx0fVxyXG5cdH1cclxuXHRmb3IgKGNvbnN0IGRpcnR5X3ZhbHVlIG9mIGRpcnR5X3ZhbHVlcykge1xyXG5cdFx0aWYgKEpTT04uc3RyaW5naWZ5KGRpcnR5X3ZhbHVlKSA9PT0gSlNPTi5zdHJpbmdpZnkodmFsaWRfb2JqZWN0KSB8fFxyXG5cdFx0XHRleGNsdWRlICYmIChleGNsdWRlIGFzIGFueSlbb2JqZWN0X3Jvb3Rfa2V5XT8uc29tZSgoY3VycmVudDogZGlydHlfdmFsdWUpID0+IEpTT04uc3RyaW5naWZ5KGN1cnJlbnQpID09PSBKU09OLnN0cmluZ2lmeShkaXJ0eV92YWx1ZSkpKSBjb250aW51ZTtcclxuXHRcdGNvbnN0IHJlc3VsdCA9IGF3YWl0IGZvcmVhY2goZGlydHlfdmFsdWUgYXMgYW55KTtcclxuXHRcdGlmIChyZXN1bHQgIT09IHRydWUpIHJldHVybiByZXN1bHQ7XHJcblx0fVxyXG5cdHJldHVybiB0cnVlO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZGF0ZWModGltZTogbnVtYmVyKSB7XHJcblx0aWYgKHR5cGVvZiB0aW1lICE9IFwibnVtYmVyXCIpIHJldHVybiBcImludmFsaWQgdGltZXN0YW1wXCI7XHJcblx0Y29uc3QgdCA9IERhdGVUaW1lLmZyb21NaWxsaXModGltZSk7XHJcblx0ZnVuY3Rpb24geihuOiBudW1iZXIpIHsgcmV0dXJuIChuICsgXCJcIikucGFkU3RhcnQoMiwgXCIwXCIpOyB9XHJcblx0cmV0dXJuIGAke3QueWVhcn0tJHt6KHQubW9udGgpfS0ke3oodC5kYXkpfWA7XHJcbn1cclxuXHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiB0ZXN0RGlydHlJbnB1dFJlcXVlc3Q8VCwgQWN0aW9uIGV4dGVuZHMgQWxsUmVxdWVzdHNbXCJhY3Rpb25cIl0sIEZvcm0gZXh0ZW5kcyBFeHRyYWN0PEFsbFJlcXVlc3RzLCB7IGFjdGlvbjogQWN0aW9uIH0+PihzcGVjaWFsX21zZzogc3RyaW5nLCB2YWxpZF9vYmplY3Q6IFQsIHJlcXVlc3RfcGFyYW1zOiAoZGlydHlfb2JqZWN0OiBUKSA9PiBbQWN0aW9uLCBGb3JtW1wicGFyYW1zXCJdXSwgZXhjbHVkZT86IGV4Y2x1c2lvbl9tYXA8VD4pIHtcclxuXHRjb25zdCBwZWN1bGlhcl9zdHJpbmcgPSBcImJsw6RfYmzDpF9ibMOkX2Jsw6RcIjtcclxuXHRyZXR1cm4gYXdhaXQgY29tYmluYXRlRGlydHlPYmplY3RzKHZhbGlkX29iamVjdCwgYXN5bmMgKGRpcnR5X29iamVjdCkgPT4ge1xyXG5cdFx0Y29uc3QgYWxsX3BhcmFtcyA9IHJlcXVlc3RfcGFyYW1zKGRpcnR5X29iamVjdCk7XHJcblxyXG5cdFx0Y29uc3QgcmVxID0gYXdhaXQgcmVxdWVzdCguLi5hbGxfcGFyYW1zKTtcclxuXHRcdGlmICghcmVxLmlzMjAwIHx8IHJlcS5vaykge1xyXG5cdFx0XHRjb25zdCByZXFfcGFyYW1zID0gcmVxdWVzdF9wYXJhbXMocGVjdWxpYXJfc3RyaW5nIGFzIGFueSlbMV0hO1xyXG5cdFx0XHRsZXQgZmllbGRfd2l0aF9kaXJ0eV9pbnB1dDtcclxuXHRcdFx0aWYgKHJlcV9wYXJhbXMgYXMgYW55ID09PSBwZWN1bGlhcl9zdHJpbmcpIHtcclxuXHRcdFx0XHRmaWVsZF93aXRoX2RpcnR5X2lucHV0ID0gXCIoZW50aXJlIG9iamVjdClcIjtcclxuXHRcdFx0fVxyXG5cdFx0XHRlbHNlIHtcclxuXHRcdFx0XHRjb25zdCB3ZWlyZF9wYXJhbXMgPSBKU09OLnN0cmluZ2lmeShyZXFfcGFyYW1zKS5zcGxpdCgnXCInICsgcGVjdWxpYXJfc3RyaW5nICsgJ1wiJylbMF07XHJcblx0XHRcdFx0Y29uc3QgbGFzdF9pbmRleCA9IHdlaXJkX3BhcmFtcy5sYXN0SW5kZXhPZihcIlxcXCJcIik7XHJcblx0XHRcdFx0Y29uc3QgYnVmZmVyID0gd2VpcmRfcGFyYW1zLnNsaWNlKDAsIGxhc3RfaW5kZXgpO1xyXG5cdFx0XHRcdGNvbnN0IGZpbmFsID0gYnVmZmVyLmxhc3RJbmRleE9mKFwiXFxcIlwiKTtcclxuXHRcdFx0XHRmaWVsZF93aXRoX2RpcnR5X2lucHV0ID0gYnVmZmVyLnNsaWNlKGZpbmFsICsgMSk7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGNvbnN0IGVycm9yX2RldGFpbHMgPSBcIlRoZSBmaWVsZCBjb250YWluaW5nIHRoZSBkaXJ0eSBpbnB1dCB3YXMgXFxcIlwiICsgZmllbGRfd2l0aF9kaXJ0eV9pbnB1dCArIFwiXFxcIiBhbmQgdGhlIGRpcnR5IG9iamVjdCB3YXNcXG5cIiArIEpTT04uc3RyaW5naWZ5KGRpcnR5X29iamVjdCk7XHJcblx0XHRcdGlmICghcmVxLmlzMjAwKSByZXR1cm4gXCJSZXF1ZXN0IFwiICsgYWxsX3BhcmFtc1swXSArIFwiIGhhcyBjcmFzaGVkIHRoZSBzZXJ2ZXIhXFxuXCIgKyBlcnJvcl9kZXRhaWxzO1xyXG5cdFx0XHRlbHNlIHJldHVybiBzcGVjaWFsX21zZyArIFwiXFxuUmVxdWVzdCB3YXM6IFwiICsgYWxsX3BhcmFtc1swXSArIFwiLiBcIiArIGVycm9yX2RldGFpbHM7XHJcblx0XHR9XHJcblx0XHRlbHNlIHJldHVybiB0cnVlO1xyXG5cdH0sIGV4Y2x1ZGUpO1xyXG59XHJcblxyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gc2hpZnRUaW1lPFQ+KHVzZXI6IHsgdXNyOiBzdHJpbmcsIHB3ZDogc3RyaW5nIH0sIHRpbWVzdGFtcDogbnVtYmVyLCBhY3Rpb246ICgpID0+IFByb21pc2U8VD4pOiBQcm9taXNlPFQ+IHtcclxuXHR0cnkge1xyXG5cdFx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgYWRtaW46IHRydWUsIHVzcjogYWRtaW5fdXNyX25hbWUsIHB3ZDogYWRtaW5fcHdkIH0pO1xyXG5cdFx0YXdhaXQgcmVxdWVzdChcInRlc3Rpbmdfc2V0U2VydmVyVGltZVwiLCB7IGtleTogdGVzdF9rZXksIHRpbWVzdGFtcCB9KTtcclxuXHRcdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB1c2VyKTtcclxuXHRcdHJldHVybiBhd2FpdCBhY3Rpb24oKTtcclxuXHR9XHJcblx0ZmluYWxseSB7XHJcblx0XHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyBhZG1pbjogdHJ1ZSwgdXNyOiBhZG1pbl91c3JfbmFtZSwgcHdkOiBhZG1pbl9wd2QgfSk7XHJcblx0XHRhd2FpdCByZXF1ZXN0KFwidGVzdGluZ19zZXRTZXJ2ZXJUaW1lXCIsIHsga2V5OiB0ZXN0X2tleSwgdGltZXN0YW1wOiBcInJlc2V0XCIgfSk7XHJcblx0XHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgdXNlcik7XHJcblx0fVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbmFtZURpcnR5SW5wdXRUZXN0KHJlcXVlc3RfbmFtZTogc3RyaW5nKTogc3RyaW5nIHtcclxuXHRyZXR1cm4gXCJTZXJ2ZXIgZG9lcyBub3QgYWNjZXB0IGRpcnR5IGlucHV0IGZyb206IFwiICsgcmVxdWVzdF9uYW1lO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gdmVyaWZ5UmVxdWVzdEFjY2VwdGVkPFQgZXh0ZW5kcyB7IGlzMjAwOiBib29sZWFuLCBvazogYm9vbGVhbiB9PihyZXE6IFQsIGRlc2NyaXB0aW9uOiBzdHJpbmcpIHtcclxuXHRpZiAoIXJlcS5pczIwMCkgdGhyb3cgXCJTZXJ2ZXIgaGFzIGNyYXNoZWQgZHVlIHRvIGZvbGxvd2luZyBhY3Rpb24hXFxuXCIgKyBkZXNjcmlwdGlvbjtcclxuXHRpZiAoIXJlcS5vaykgdGhyb3cgXCJSZXF1ZXN0IGRpZCBub3QgcmV0dXJuIG9rIVxcblwiICsgZGVzY3JpcHRpb247XHJcbn1cclxuZXhwb3J0IGZ1bmN0aW9uIHZlcmlmeVJlcXVlc3REZW5pZWQ8VCBleHRlbmRzIHsgaXMyMDA6IGJvb2xlYW4sIG9rOiBib29sZWFuIH0+KHJlcTogVCwgZGVzY3JpcHRpb246IHN0cmluZykge1xyXG5cdGlmICghcmVxLmlzMjAwKSB0aHJvdyBcIlNlcnZlciBoYXMgY3Jhc2hlZCBkdWUgdG8gZm9sbG93aW5nIGFjdGlvbiFcXG5cIiArIGRlc2NyaXB0aW9uO1xyXG5cdGlmIChyZXEub2spIHRocm93IFwiUmVxdWVzdCByZXR1cm5lZCBvaywgYnV0IGl0IHdhcyBzdXBwb3NlZCB0byBiZSBkZW5pZWQhXFxuXCIgKyBkZXNjcmlwdGlvbjtcclxufVxyXG5cclxuXHJcbi8qXHJcbmNyZWF0ZVRlc3QoXCJDYW4gYWNjZXNzIHNlbGYtbWFkZSB0aWNrZXRzXCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHRpdGxlID0gXCJ2w6RnYSB0w7VzaW5lIHByb2JsZWVtXCI7XHJcblx0Y29uc3QgdGV4dCA9IFwiZXJha29yZHNlbHQgdMO1c2luZSBwcm9ibGVlbVwiO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IGNvbnRleHQgKyBcIkBzaGFya2xhc2Vycy5jb21cIiwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblx0Y29uc3Qgb3JnID0gKGF3YWl0IHJlcXVlc3QoXCJvcmdfbmV3XCIsIHsgbmFtZTogXCJuZXcgb3JnXCIsIHRpbWV6b25lOiBcIkV1cm9wZS9UYWxsaW5uXCIsIG9wdGlvbnM6IHt9IH0pKS5kYXRhIGFzIElkO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZywgbmFtZTogXCJuZXcgZ3JwXCIgfSk7XHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmcgfSkpLmRhdGEhO1xyXG5cdGNvbnN0IGdycCA9IGdyb3Vwc1swXS5faWQ7XHJcblx0YXdhaXQgcmVxdWVzdChcInRrdF9uZXdcIiwgeyBkb2M6IHsgZ3JwLCBpc19nbG9iYWw6IGZhbHNlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlLCB0ZXh0IH0sIG9yZyB9KTtcclxuXHJcblx0Y29uc3QgcmVxX2dldCA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mTWV0YVwiLCB7IG9yZywgZ3JwOiBcIlNlbGZcIiB9KSk7XHJcblx0Y29uc3QgdGt0ID0gcmVxX2dldC5kYXRhIVswXTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxX2dldCwgXCJUcmllZCB0byBnZXQgc2VsZi1tYWRlIHRpY2tldC5cIik7XHJcblx0aWYgKCEodGt0LnRpdGxlID09PSB0aXRsZSAmJiB0a3QuY29tbWVudHNbMF0udGV4dFswXSA9PT0gdGV4dCkpIHJldHVybiBcIkNvdWxkIG5vdCBnZXQgYSBzZWxmLW1hZGUgdGlja2V0IVwiO1xyXG5cclxuXHRjb25zdCByZXFfYWRkID0gYXdhaXQgcmVxdWVzdChcInRrdF9hZGRSZXBseVwiLCB7IG9yZywgaWQ6IHRrdC5faWQsIHRleHQ6IFwidGVzdCByZXBseVwiIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXFfYWRkLCBcIlRyaWVkIHRvIHNlbmQgYSByZXBseSB0byBhIHNlbGYtbWFkZSB0aWNrZXQuXCIpO1xyXG5cdFxyXG5cdHJldHVybiB0cnVlO1xyXG59KTtcclxuXHJcbnNlY3VyaXR5IGluIHRoZSBmdXR1cmVcclxuY3JlYXRlVGVzdChcIkNhbiBjcmVhdGUgYW5kIGFjY2VzcyB0aWNrZXRzIGluIG5vbi1qb2luZWQgZ3JvdXBzXCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHVzZXIwID0gY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiO1xyXG5cdGNvbnN0IHVzZXIxID0gY29udGV4dCArIFwiMUBzaGFya2xhc2Vycy5jb21cIjtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQgKyAxLCBcInVzZXJcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiB1c2VyMCwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblxyXG5cdGNvbnN0IG9yZ19pZCA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwiaW52aXRlXCIsIHsgb3JnOiBvcmdfaWQsIGVtYWlsOiB1c2VyMSB9KTtcclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZzogb3JnX2lkLCBuYW1lOiBcIm5ldyBncnBcIiB9KTtcclxuXHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmc6IG9yZ19pZCB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwX2lkID0gZ3JvdXBzWzBdLl9pZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwibG9nb3V0XCIpO1xyXG5cclxuXHRhd2FpdCByZXF1ZXN0KFwibG9naW5cIiwgeyB1c3I6IHVzZXIxLCBwd2Q6IFwiUGFzc3dvcmQxMjNcIiB9KTtcclxuXHRjb25zdCBpbnZpdGF0aW9uID0gKGF3YWl0IHJlcXVlc3QoXCJnZXRJbnZpdGF0aW9uc1wiKSkuZGF0YSE7XHJcblx0YXdhaXQgcmVxdWVzdChcImFjY2VwdEludml0YXRpb25cIiwgeyBpbnY6IGludml0YXRpb25bMF0uX2lkIH0pO1xyXG5cclxuXHQvLyB0aGlzIGlzIG9rYXksIFJhdWwgYXBwcm92ZWRcclxuXHRjb25zdCByZXEgPSBhd2FpdCByZXF1ZXN0KFwidGt0X25ld1wiLCB7IGRvYzogeyBncnA6IGdycF9pZCwgaXNfZ2xvYmFsOiBmYWxzZSwgaXNfcHJpb3JpdHk6IGZhbHNlLCB0aXRsZTogXCJ2w6RnYSB0w7VzaW5lIHByb2JsZWVtXCIsIHRleHQ6IFwiZXJha29yZHNlbHQgdMO1c2luZSBwcm9ibGVlbVwiIH0sIG9yZzogb3JnX2lkIH0pO1xyXG5cdHZlcmlmeVJlcXVlc3RBY2NlcHRlZChyZXEsIFwiVHJpZWQgdG8gY3JlYXRlIGEgdGlja2V0IHdoaWxlIGJlaW5nIGEgbWVtYmVyIG9mIGFuIG9yZyBidXQgbm90IGEgbWVtYmVyIG9mIGEgZ3JwLlwiKTtcclxuXHJcblx0Ly8gbXVzdCBhbHdheXMgYmUgYWJsZSB0byBhY2Nlc3Mgc2VsZi1tYWRlIHRpY2tldHNcclxuXHRjb25zdCByZXFfbWV0YSA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mTWV0YVwiLCB7IG9yZzogb3JnX2lkLCBncnA6IFwiU2VsZlwiIH0pKTtcclxuXHRjb25zdCB0a3QgPSByZXFfbWV0YS5kYXRhIVswXTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxX21ldGEsIFwiVHJpZWQgdG8gZ2V0IGEgc2VsZi1tYWRlIHRpY2tldCBmcm9tIGdycCwgd2hlcmUgdXNlciBkb2VzIG5vdCBiZWxvbmcuXCIpO1xyXG5cdGlmICghKHRrdC50aXRsZSA9PT0gXCJ2w6RnYSB0w7VzaW5lIHByb2JsZWVtXCIgJiYgdGt0LmNvbW1lbnRzWzBdLnRleHRbMF0gPT09IFwiZXJha29yZHNlbHQgdMO1c2luZSBwcm9ibGVlbVwiKSkgcmV0dXJuIFwiQ291bGQgbm90IGdldCBhIHNlbGYtbWFkZSB0aWNrZXQgZnJvbSBncnAsIHdoZXJlIHVzZXIgZG9lcyBub3QgYmVsb25nIVwiO1xyXG5cclxuXHRjb25zdCByZXFfYWRkID0gYXdhaXQgcmVxdWVzdChcInRrdF9hZGRSZXBseVwiLCB7IG9yZzogb3JnX2lkLCBpZDogdGt0Ll9pZCwgdGV4dDogXCJ0ZXN0IHJlcGx5XCIgfSk7XHJcblx0dmVyaWZ5UmVxdWVzdEFjY2VwdGVkKHJlcV9hZGQsIFwiVHJpZWQgdG8gYWRkIGEgcmVwbHkgdG8gYSBzZWxmLW1hZGUgdGlja2V0LCBidXQgaXQgd2FzIG5vdCBhZGRlZCB0byBhIGdycCwgd2hlcmUgdXNlciBkb2VzIG5vdCBiZWxvbmcgdG8uXCIpO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcblxyXG5jcmVhdGVUZXN0KFwiRXZlcnlvbmUgaW4gb3JnIGNhbiByZXBseSB0byBnbG9iYWwgdGt0XCIsIFwiZnVuY3Rpb25hbGl0eVwiLCBhc3luYyAoY29udGV4dCkgPT4ge1xyXG5cdGNvbnN0IHVzZXIwID0gY29udGV4dCArIFwiQHNoYXJrbGFzZXJzLmNvbVwiO1xyXG5cdGNvbnN0IHVzZXIxID0gY29udGV4dCArIFwiMUBzaGFya2xhc2Vycy5jb21cIjtcclxuXHRhd2FpdCBjcmVhdGVVc2VyKGNvbnRleHQgKyAxLCBcInVzZXJcIik7XHJcblx0YXdhaXQgcmVxdWVzdChcImxvZ2luXCIsIHsgdXNyOiB1c2VyMCwgcHdkOiBcIlBhc3N3b3JkMTIzXCIgfSk7XHJcblxyXG5cdGNvbnN0IG9yZ19pZCA9IChhd2FpdCByZXF1ZXN0KFwib3JnX25ld1wiLCB7IG5hbWU6IFwibmV3IG9yZ1wiLCB0aW1lem9uZTogXCJFdXJvcGUvVGFsbGlublwiLCBvcHRpb25zOiB7fSB9KSkuZGF0YSBhcyBJZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwiaW52aXRlXCIsIHsgb3JnOiBvcmdfaWQsIGVtYWlsOiB1c2VyMSB9KTtcclxuXHRhd2FpdCByZXF1ZXN0KFwiZ3JwX25ld1wiLCB7IG9yZzogb3JnX2lkLCBuYW1lOiBcIm5ldyBncnBcIiB9KTtcclxuXHJcblx0Y29uc3QgZ3JvdXBzID0gKGF3YWl0IHJlcXVlc3QoXCJncnBfZ2V0T2ZPcmdcIiwgeyBvcmc6IG9yZ19pZCB9KSkuZGF0YSE7XHJcblx0Y29uc3QgZ3JwX2lkID0gZ3JvdXBzWzBdLl9pZDtcclxuXHRhd2FpdCByZXF1ZXN0KFwidGt0X25ld1wiLCB7IGRvYzogeyBncnA6IGdycF9pZCwgaXNfZ2xvYmFsOiB0cnVlLCBpc19wcmlvcml0eTogZmFsc2UsIHRpdGxlOiBcInbDpGdhIHTDtXNpbmUgcHJvYmxlZW1cIiwgdGV4dDogXCJlcmFrb3Jkc2VsdCB0w7VzaW5lIHByb2JsZWVtXCIgfSwgb3JnOiBvcmdfaWQgfSk7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dvdXRcIik7XHJcblxyXG5cdGF3YWl0IHJlcXVlc3QoXCJsb2dpblwiLCB7IHVzcjogdXNlcjEsIHB3ZDogXCJQYXNzd29yZDEyM1wiIH0pO1xyXG5cclxuXHRjb25zdCBpbnZpdGF0aW9uID0gKGF3YWl0IHJlcXVlc3QoXCJnZXRJbnZpdGF0aW9uc1wiKSkuZGF0YSE7XHJcblx0YXdhaXQgcmVxdWVzdChcImFjY2VwdEludml0YXRpb25cIiwgeyBpbnY6IGludml0YXRpb25bMF0uX2lkIH0pO1xyXG5cclxuXHRjb25zdCByZXFfbWV0YSA9IChhd2FpdCByZXF1ZXN0KFwidGt0X2dldE9mTWV0YVwiLCB7IG9yZzogb3JnX2lkLCBncnA6IFwiR2xvYmFsXCIgfSkpO1xyXG5cdGNvbnN0IHRrdCA9IHJlcV9tZXRhLmRhdGEhWzBdO1xyXG5cdGNvbnN0IHJlcV9hZGQgPSBhd2FpdCByZXF1ZXN0KFwidGt0X2FkZFJlcGx5XCIsIHsgb3JnOiBvcmdfaWQsIGlkOiB0a3QuX2lkLCB0ZXh0OiBcInRlc3QgcmVwbHlcIiB9KTtcclxuXHR2ZXJpZnlSZXF1ZXN0QWNjZXB0ZWQocmVxX2FkZCwgXCJUcmllZCB0byByZXBseSB0byBhIGdsb2JhbCB0aWNrZXQuXCIpO1xyXG5cclxuXHRyZXR1cm4gdHJ1ZTtcclxufSk7XHJcbiovXHJcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBzaW5vbmpzL2Zha2UtdGltZXJzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImVtYWlsLXZhbGlkYXRvclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJqc29uNVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJqc29uNS9saWIvcmVnaXN0ZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibHV4b25cIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibm9kZS1mZXRjaFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzaGVsbGpzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInNvdXJjZS1tYXAtc3VwcG9ydFwiKTsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiLy8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbl9fd2VicGFja19yZXF1aXJlX18ubiA9IChtb2R1bGUpID0+IHtcblx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG5cdFx0KCkgPT4gKG1vZHVsZVsnZGVmYXVsdCddKSA6XG5cdFx0KCkgPT4gKG1vZHVsZSk7XG5cdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsIHsgYTogZ2V0dGVyIH0pO1xuXHRyZXR1cm4gZ2V0dGVyO1xufTsiLCIvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9ucyBmb3IgaGFybW9ueSBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSAoZXhwb3J0cywgZGVmaW5pdGlvbikgPT4ge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSAob2JqLCBwcm9wKSA9PiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCkpIiwiIiwiLy8gc3RhcnR1cFxuLy8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4vLyBUaGlzIGVudHJ5IG1vZHVsZSBpcyByZWZlcmVuY2VkIGJ5IG90aGVyIG1vZHVsZXMgc28gaXQgY2FuJ3QgYmUgaW5saW5lZFxudmFyIF9fd2VicGFja19leHBvcnRzX18gPSBfX3dlYnBhY2tfcmVxdWlyZV9fKFwiLi90ZXN0L3JlcXVlc3RzL3JlcXVlc3RfdGVzdC50c1wiKTtcbiIsIiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==