import { DateTime } from "luxon";
export const days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
export const dates = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
export function getChecklistCompletion(x) {
    let total = 0;
    let completed = 0;
    for (const item of x.items) {
        if (item.section) {
            total += item.tasks.length;
            for (const task of item.tasks)
                if (task.val != "new")
                    completed += 1;
        }
        else {
            total++;
            if (item.val != "new")
                completed += 1;
        }
    }
    const fraction = Math.min(completed / (total || 1), 1);
    return { completed, total, fraction, percent: Math.floor(fraction * 100) };
}
export function getChecklistNextRepeat(x, timezone) {
    if (x.schedule) {
        if (x.schedule.repeat == "daily") {
            const day_array = [...days, ...days];
            const day = DateTime.fromMillis(x.activation, { zone: timezone });
            const week_start_pos = day.weekday;
            for (let i = week_start_pos; i <= week_start_pos + 6; i++) {
                if (x.schedule.days[day_array[i]])
                    return day.plus({ days: i - week_start_pos + 1 }).toMillis();
            }
            return 0;
        }
        else {
            const getTargetDay = (in_month) => {
                const { from_end, day } = x.schedule;
                return (from_end
                    ? Math.max(1, in_month.daysInMonth - day)
                    : Math.min(in_month.daysInMonth, day));
            };
            const old_month = DateTime.fromMillis(x.activation, { zone: timezone });
            const old_month_target = old_month.set({ day: getTargetDay(old_month) });
            if (old_month_target.toMillis() > old_month.toMillis()) {
                return old_month_target.toMillis();
            }
            else {
                const new_month = old_month.plus({ months: 1 });
                return new_month.set({ day: getTargetDay(new_month) }).toMillis();
            }
        }
    }
    else
        return 0;
}
//# sourceMappingURL=Checklist.js.map