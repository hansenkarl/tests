/** Placeholder Id for Documents that have not gotten a generated Id yet. */
export const NewId = "new";
/** Placeholder Id to use instead of user Id when an action was performed automatically. */
export const SystemId = "sys";
export function isId(x) { return typeof x == "string" && x.startsWith("x"); }
export function newIdAutoMap(doc_constructor) {
    return new Proxy({}, {
        get: (target, property) => {
            if (isId(property) && target[property] === undefined)
                target[property] = doc_constructor(property);
            return target[property];
        },
    });
}
export class ListedIdMap {
    constructor(arg) {
        this.list = [];
        if (typeof arg == "string")
            this.reset(JSON.parse(arg));
        else
            this.reset(arg);
    }
    reset(items) {
        items ?? (items = []);
        const map = {};
        items.forEach(item => map[item._id] = item);
        this.map = map;
        this.list.splice(0, this.list.length, ...items);
    }
    get(id) {
        return this.map[id];
    }
    add(x) {
        if (this.map[x._id])
            this.list.splice(this.list.findIndex(y => y._id == x._id), 1, x);
        else
            this.list.push(x);
        this.map[x._id] = x;
    }
    remove(x) {
        if (this.map[x]) {
            this.list.splice(this.list.findIndex(y => y._id == x), 1);
            delete this.map[x];
        }
    }
    toJSON() {
        return this.list;
    }
}
/** Used to select typescript typing in if-blocks. Optimizer trims it out of production builds. */
export function Narrow(x) { return true; }
//# sourceMappingURL=u.js.map