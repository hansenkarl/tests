import { isRecord, removeNameWhitespace } from "@/core/u";
import { validatePassword } from "@/data/User";
import { days } from "@/data/Checklist";
import { validate as validateEmail } from "email-validator";
const vvMap = "map";
const vvArray = "array";
const vvId = "id";
const vvStringAny = "string, any";
const vvStringAnyArray = "array of any strings";
const vvStringNonEmpty = "string, non-empty";
const vvStringNonEmptyArray = "array of non-empty strings";
const vvNumber = "number";
const vvBoolean = "boolean";
const marker_optional = "null or undefined or ";
const vvOptional = (x) => `${marker_optional}${x}`;
function TypeCheck(q, type_template) {
    if (!isRecord(q))
        return false;
    let is_v = false;
    for (const key in type_template) {
        const x = q[key];
        const type_or_template = type_template[key];
        let type = typeof type_or_template == "string"
            ? type_or_template
            : vvMap;
        if (type.startsWith(marker_optional)) {
            if (x === undefined || x === null)
                is_v = true;
            else
                type = type.split(marker_optional)[1];
        }
        switch (type) {
            case vvMap:
                {
                    is_v = isRecord(x)
                        && (type == type_or_template || TypeCheck(x, type_or_template));
                }
                break;
            case vvStringAny:
                {
                    is_v = typeof x == "string";
                }
                break;
            case vvStringNonEmpty:
                {
                    is_v = typeof x == "string"
                        && x.trim() != "";
                }
                break;
            case vvId:
                {
                    is_v = typeof x == "string"
                        && x.startsWith("x");
                }
                break;
            case vvBoolean:
                {
                    is_v = typeof x == "boolean";
                }
                break;
            case vvNumber:
                {
                    is_v = typeof x == "number";
                }
                break;
            case vvArray:
                {
                    is_v = Array.isArray(x);
                }
                break;
            case vvStringAnyArray:
                {
                    is_v = Array.isArray(x)
                        && x.every((item) => typeof item == "string");
                }
                break;
            case vvStringNonEmptyArray:
                {
                    is_v = Array.isArray(x)
                        && x.every((item) => typeof item == "string" && item.trim() != "");
                }
                break;
            default: { }
        }
        if (!is_v)
            return false;
    }
    return true;
}
const VOID_INPUT = () => true;
export const RequestValidator = {
    getCssVersion: VOID_INPUT,
    changePassword: (q) => {
        return (true
            && typeof q.old_pwd == "string"
            && validatePassword(q.new_pwd));
    },
    changeNames: (q) => {
        return (true
            && typeof q.fname == "string"
            && typeof q.lname == "string"
            && typeof q.dname == "string"
            && removeNameWhitespace(q.fname).length > 0
            && removeNameWhitespace(q.lname).length > 0
            && removeNameWhitespace(q.dname).length > 0);
    },
    login: VOID_INPUT,
    logout: VOID_INPUT,
    register: (q) => {
        return (true
            && typeof q.email == "string"
            && validateEmail(q.email)
            && typeof q.fname == "string"
            && removeNameWhitespace(q.fname).length >= 1
            && q.fname.length <= 16
            && typeof q.lname == "string"
            && removeNameWhitespace(q.lname).length >= 1
            && q.lname.length <= 16
            && validatePassword(q.pwd));
    },
    feedback: (q) => (TypeCheck(q, {
        text: vvStringNonEmpty,
    })),
    acceptInvitation: (q) => (TypeCheck(q, {
        inv: vvId,
    })),
    org_delete: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    getInvitations: VOID_INPUT,
    getOrgs: VOID_INPUT,
    org_getUsers: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    org_getInvitations: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    grp_setAuthLvl: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
        user: vvId,
        lvl: vvNumber,
    })),
    invite: (q) => (TypeCheck(q, {
        org: vvId,
        email: vvStringAny,
    })
        && validateEmail(q.email)),
    //#region User
    user: VOID_INPUT,
    wskey: VOID_INPUT,
    usr_delete: (q) => (TypeCheck(q, {
        confirm: vvBoolean,
    })),
    usr_setOptions: (q) => (TypeCheck(q, {
        problems__sort_by_last_post: vvOptional(vvBoolean),
        problems__sort_unread_first: vvOptional(vvBoolean),
    })),
    usr_getNotepads: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    usr_setNotepad: (q) => (TypeCheck(q, {
        org: vvId,
        _id: vvId,
        name: vvStringNonEmpty,
        content: vvStringAny,
    })),
    usr_deleteNotepad: (q) => (TypeCheck(q, {
        org: vvId,
        _id: vvId,
    })),
    usr_newNotepad: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    //#endregion
    //#region Groups
    grp_new: (q) => {
        return (true
            && typeof q.org == "string"
            && typeof q.name == "string"
            && removeNameWhitespace(q.name).length > 0);
    },
    grp_delete: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
    })),
    grp_getOfOrg: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    grp_rename: (q) => {
        return (true
            && typeof q.org == "string"
            && typeof q.name == "string"
            && removeNameWhitespace(q.name).length > 0);
    },
    grp_setOptions: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
        options: vvMap,
    })
        && ["null", "undefined", "number"].includes(typeof q.options.problem_sleep_delay)),
    //#endregion
    //#region Checklists
    chk_new: (q) => (TypeCheck(q, {
        org: vvId,
        doc: {
            title: vvStringNonEmpty,
            grp: vvId,
            duration: vvNumber,
            activation: vvNumber,
        },
    })
        && (q.doc.schedule === undefined || typeof q.doc.schedule == "object")
        && q.doc.title.trim().length >= 1
        && Array.isArray(q.doc.items)
        && q.doc.items.every(x => TypeCheck(x, { name: vvStringNonEmpty, section: vvBoolean, key: vvNumber }))
        && q.doc.items
            .filter((x) => x.section === true)
            .every(x => (TypeCheck(x, { description: vvStringNonEmptyArray, tasks: vvArray })
            && x.tasks.every(y => TypeCheck(y, { name: vvStringNonEmpty, section: vvBoolean, key: vvNumber }))))
        && (q.doc.schedule == undefined || (true
            && typeof q.doc.schedule == "object"
            && (false
                || (true
                    && q.doc.schedule.repeat == "daily"
                    && q.doc.schedule.days
                    && typeof q.doc.schedule.days == "object"
                    && Object.keys(q.doc.schedule.days).length == days.length
                    && !days.some(day => typeof q.doc.schedule.days[day] != "boolean"))
                || (true
                    && q.doc.schedule.repeat == "monthly"
                    && typeof q.doc.schedule.from_end == "boolean"
                    && typeof q.doc.schedule.day == "number"
                    && q.doc.schedule.day >= 1
                    && q.doc.schedule.day <= 31))))
        && (q.doc.overrides_phantom == undefined || (true
            && typeof q.doc.overrides_phantom == "object"
            && typeof q.doc.overrides_phantom._id == "string"
            && q.doc.overrides_phantom._id != ""
            && typeof q.doc.overrides_phantom.activation == "number"))),
    chk_get: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
    })),
    chk_appendTask: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        section: vvNumber,
        name: vvStringNonEmpty,
    })),
    chk_edit: (q) => {
        return (true
            && typeof q.org == "string"
            && q.doc.title.trim().length >= 1);
    },
    chk_getOfOrg: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    chk_setItemStatus: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        section: vvNumber,
        index: vvNumber,
        status: vvStringAny,
    })
        && q.section >= -1
        && q.index > -1
        && ["new", "done", "fail"].includes(q.status)),
    chk_lock: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
    })),
    chk_getOfGroup: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
    })),
    //#endregion
    //#region Orgs
    org_new: (q) => (TypeCheck(q, {
        name: vvStringNonEmpty,
        timezone: vvStringAny,
        options: vvMap,
    })
        && removeNameWhitespace(q.name).length > 1),
    org_kick: (q) => (TypeCheck(q, {
        org: vvId,
        user: vvId,
    })),
    org_setAuthLvl: (q) => (TypeCheck(q, {
        org: vvId,
        user: vvId,
        lvl: vvNumber,
    })
        && [1, 5].includes(q.lvl)),
    org_rename: (q) => {
        return (true
            && typeof q.org == "string"
            && typeof q.name == "string"
            && removeNameWhitespace(q.name).length > 0);
    },
    org_setOptions: (q) => {
        return (true
            && typeof q.org == "string"
            && q.options
            && typeof q.options == "object"
            && (q.options.use_display_names === undefined || typeof q.options.use_display_names == "boolean"));
    },
    org_getLatestCursors: (q) => (TypeCheck(q, {
        org: "id",
    })),
    //#endregion
    //#region Chat
    chat_load: (q) => (true),
    chat_post: (q) => (TypeCheck(q, {
        org: vvId,
        channel: vvStringNonEmpty,
        is_dm: vvBoolean,
        txt: vvStringNonEmpty,
    })),
    //#endregion
    //#region Problems
    tkt_new: (q) => (TypeCheck(q, {
        org: vvId,
        doc: {
            title: vvStringNonEmpty,
            grp: vvId,
            is_global: vvBoolean,
            is_priority: vvBoolean,
        },
    })
        && typeof q.doc.text == "string"
        && typeof q.doc.title == "string"
        && q.doc.title.trim() != ""),
    tkt_addReply: (q) => (TypeCheck(q, {
        id: vvId,
        org: vvId,
        text: vvStringNonEmpty,
    })),
    tkt_getOfGroup: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvId,
    })),
    tkt_getOfMeta: (q) => (TypeCheck(q, {
        org: vvId,
        grp: vvStringNonEmpty,
    })),
    tkt_getOfOrg: (q) => (TypeCheck(q, {
        org: vvId,
    })),
    tkt_modify: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        options: {
            is_global: vvOptional(vvBoolean),
            is_priority: vvOptional(vvBoolean),
            is_resolved: vvOptional(vvBoolean),
        },
    })),
    tkt_rename: (q) => (TypeCheck(q, {
        org: vvId,
        id: vvId,
        name: vvStringNonEmpty,
    })),
    tkt_read: (q) => (TypeCheck(q, {
        org: vvId,
    })
        && (TypeCheck(q, { grp: vvId }) || TypeCheck(q, { tkt: vvId, key: vvNumber }))),
    //#endregion
};
//# sourceMappingURL=RequestValidator.js.map