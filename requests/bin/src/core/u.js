import { exec } from "shelljs";
import { DateTime } from "luxon";
import * as FakeTimers from "@sinonjs/fake-timers";
export function log(x) {
    const faint = "\x1b[2m";
    const reset = "\x1b[0m";
    let line = new Error().stack?.split("\n")[2].split("at ")[1];
    if (typeof x == "string") {
        const parts = line.split(".ts");
        line = parts[0].slice(parts[0].lastIndexOf("/") + 1) + ".ts";
        line += parts[1].slice(0, parts[1].lastIndexOf(":"));
        console.log(faint + line + " " + reset + x);
    }
    else
        console.log([line, x]);
}
export function sleep(ms) { return new Promise(r => setTimeout(() => { r(true); }, ms)); }
export function random(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }
export function timestamp() { return new Date().getTime(); }
export function removeLastNewline(str) {
    if (str.lastIndexOf("\n") > 0) {
        return str.substring(0, str.lastIndexOf("\n"));
    }
    else {
        return str;
    }
}
export function sh(cmd, wont_fail) {
    const result = exec(cmd);
    if (!wont_fail && result.code != 0) {
        console.log("+++ Tried to run sh command, but it failed.");
        console.log("Command line: " + cmd);
        console.log("Exit code: " + result.code);
        console.log("+++ Stdout:");
        console.log(result.stdout);
        console.log("+++ Stderr:");
        console.log(result.stderr);
        console.log("+++ Stacktrace:");
        console.trace();
        throw new Error("Error: sh failed!");
    }
    return result;
}
export async function ash(cmd, wont_fail) {
    const result = await exec(cmd, { async: true });
    if (!wont_fail && result.exitCode != 0) {
        console.log("+++ Tried to run sh command, but it failed.");
        console.log("Command line: " + cmd);
        console.log("Exit code: " + result.exitCode);
        console.log("+++ Stdout:");
        console.log(streamToString(result.stdout));
        console.log("+++ Stderr:");
        console.log(streamToString(result.stderr));
        console.log("+++ Stacktrace:");
        console.trace();
        throw new Error("Error: async sh failed!");
    }
    return result;
}
export function streamToString(stream) {
    const chunks = [];
    return new Promise((resolve, reject) => {
        stream.on("data", chunk => chunks.push(chunk));
        stream.on("error", reject);
        stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
    });
}
export function date(time) {
    if (typeof time != "number")
        return "invalid timestamp";
    const t = DateTime.fromMillis(time);
    function z(n) { return (n + "").padStart(2, "0"); }
    return `${t.year}-${z(t.month)}-${z(t.day)} ${z(t.hour)}:${z(t.minute)}`;
}
export function removeFromArray(array, item) {
    const index = array.indexOf(item);
    if (index > -1) {
        array.splice(index, 1);
    }
}
export function asParagraphArray(text) {
    const r = text.trim().split("\n").map(x => x.trim()).filter(x => x != "");
    if (r.length)
        return r;
    else
        return [""];
}
export function removeNameWhitespace(name) {
    return name.trim().replaceAll("\n", "").replaceAll("\t", "");
}
export function isRecord(x) {
    return typeof x == "object" && x != null && !Array.isArray(x);
}
export function copyOptionalFields(from, to, keys) {
    for (const key of keys)
        if (from[key] != undefined)
            to[key] = from[key];
}
export const testing_setServerTime = (x) => {
    testing_setServerTime.clock?.uninstall();
    if (x != "reset")
        testing_setServerTime.clock = FakeTimers.install({ now: x, shouldAdvanceTime: true });
};
//# sourceMappingURL=u.js.map