var _a;
const is_windows = process.platform == "win32";
const slash = is_windows ? "\\" : "/";
require("source-map-support").install({ environment: "node" });
require("module-alias").addAlias("@", __dirname + `${slash}..${slash}..${slash}src`);
import "json5/lib/register.js";
{
    const j5 = require("json5");
    JSON.r = j5.parse;
    JSON.w = (x) => j5.stringify(x, { space: "\t" });
}
(_a = String.prototype).replaceAll ?? (_a.replaceAll = function (search, replacement) { return this.split(search).join(replacement); });
import { timestamp } from "./utils.js";
import { url } from "./config.js";
import { RequestValidator } from "@/core/RequestValidator";
import fetch from "node-fetch";
const fetchTimeout = (url, ms, options) => {
    const controller = new AbortController();
    const promise = fetch(url, { signal: controller.signal, ...options });
    const timeout = setTimeout(() => controller.abort(), ms);
    return promise.finally(() => clearTimeout(timeout));
};
let cookie = "";
let current_test_req_set;
/**
 *
 * @param action either request action or false to reset cookies
 * @param parameters
 * @param files
 */
export async function request(action, parameters) {
    if (action === false) {
        cookie = "";
        return null;
    }
    return fetchTimeout(url + (true ? "?a=" + action : ""), 5000, {
        method: "post",
        body: JSON.stringify({ action, ...(parameters ?? {}) }),
        headers: {
            "Cookie": cookie,
            "Content-Type": "application/json",
        },
    }).then(async (response) => {
        if (response.status === 502 || response.status === 503 || response.status === 504) {
            console.log("The server or network is unavailable!");
            console.log(response.status);
            process.exit(1);
        }
        const set_cookie = response.headers.raw()["set-cookie"];
        if (set_cookie != null) {
            cookie = set_cookie[0].split(";")[0];
        }
        return new Proxy({ ...(await response.json()), is200: response.status === 200 }, {
            get: (target, step) => {
                if (step === "is200") {
                    current_test_req_set.add(action);
                }
                return target[step];
            },
        });
    }).catch((err) => {
        console.log(err);
        if (err.type === "aborted" || err.code === "ECONNREFUSED") {
            console.log("The server or network is unavailable!");
            process.exit(1);
        }
        return { ok: false, is200: false };
    });
}
const tests = [];
let current_before_each = null;
const green = "\x1b[32m";
const red = "\x1b[31m";
const reset = "\x1b[0m";
let counter = 0;
export function createTest(test_name, category, test_action) {
    const test_failed = (error_msg) => {
        console.log();
        console.log(red + "Test \"" + test_name + "\" failed!" + reset);
        console.log(error_msg);
        console.log();
    };
    const before_each = current_before_each;
    const test = async () => {
        const context = "context_" + timestamp() + "_" + counter++;
        if (before_each) {
            await before_each(context);
        }
        try {
            const result = await test_action(context);
            if (category !== "extra" && current_test_req_set.size === 0)
                throw new Error("No 200 checks in this test!");
            if (result === true) {
                return { passed: true, category, test_name };
            }
            else {
                test_failed(result);
                return { passed: false, category, test_name };
            }
        }
        catch (err) {
            if (typeof err !== "string") {
                console.log(err.code);
                console.log("\nError in test: " + test_name);
                console.log(err);
            }
            else
                test_failed(err);
            return { passed: false, category, test_name };
        }
    };
    tests.push(test);
}
export function setBeforeEach(lambda) {
    current_before_each = lambda;
}
const coverage = {};
const req_names = Object.keys(RequestValidator);
req_names.forEach(x => {
    coverage[x] = {
        functionality: 0,
        stability: 0,
        security: 0,
    };
});
async function runTests() {
    function markCorrectCoverage(current_test_req, category, test_name) {
        if (!coverage[current_test_req][category])
            coverage[current_test_req][category]++;
        else
            throw new Error("Warning! Test \"" + test_name + "\" for request \"" + current_test_req + "\" is a duplicate test in category \"" + category + "\"!");
    }
    let count = 0;
    for (const test of tests) {
        current_test_req_set = new Set();
        const test_result = await test();
        if (test_result.passed)
            count++;
        if (test_result.category === "extra")
            continue;
        for (const current_test_req of current_test_req_set) {
            if (typeof test_result.category === "string") {
                markCorrectCoverage(current_test_req, test_result.category, test_result.test_name);
            }
            else {
                for (const category of test_result.category) {
                    markCorrectCoverage(current_test_req, category, test_result.test_name);
                }
            }
        }
    }
    const color_code = count === tests.length ? green : red;
    console.log(color_code + count + " out of " + tests.length + " tests successful!" + reset);
    if (count < tests.length)
        process.exitCode = 1;
}
// imports tests and runs them
current_before_each = null;
import "./user_requests_tests/session_test";
current_before_each = null;
import "./user_requests_tests/org_test";
current_before_each = null;
import "./user_requests_tests/ticket_test";
current_before_each = null;
import "./user_requests_tests/misc_test";
current_before_each = null;
import "./user_requests_tests/group_test";
current_before_each = null;
import "./user_requests_tests/checklist_test";
current_before_each = null;
import "./user_requests_tests/chat_test";
async function main() {
    await runTests();
    function isMissingType(x) {
        return (!x.functionality || !x.stability || !x.security);
    }
    if (req_names.some(x => isMissingType(coverage[x]))) {
        console.log("Warning, some requests are missing tests!");
        req_names.forEach(x => {
            if (isMissingType(coverage[x])) {
                const func = (coverage[x].functionality ? green : red) + "functionality" + reset;
                const stab = (coverage[x].stability ? green : red) + "stability" + reset;
                const secu = (coverage[x].security ? green : red) + "security" + reset;
                console.log(`${func}  ${stab}  ${secu}  ${x}`);
            }
        });
    }
}
main();
//# sourceMappingURL=request_test.js.map