import { createTest, request, setBeforeEach } from "../request_test";
import { createUser } from "../request_utils";
import { testDirtyInputRequest, nameDirtyInputTest, verifyRequestAccepted, verifyRequestDenied } from "../utils";
setBeforeEach(async (context) => {
    await request(false);
    await createUser(context, "user");
});
createTest("Can send feedback", "functionality", async (context) => {
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const req = await request("feedback", { text: "test feedback" });
    verifyRequestAccepted(req, "Tried to send feedback.");
    return true;
});
createTest("Must not accept feedback from users that are not logged in", "security", async () => {
    const req = await request("feedback", { text: "test feedback" });
    verifyRequestDenied(req, "Tried to send feedback without being logged in.");
    return true;
});
createTest(nameDirtyInputTest("feedback"), "stability", async (context) => {
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    return await testDirtyInputRequest("feedback accepted dirty input!", { text: "test feedback" }, (dirty_input) => ["feedback", dirty_input]);
});
//# sourceMappingURL=misc_test.js.map