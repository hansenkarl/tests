import { createTest, request, setBeforeEach } from "../request_test";
import { createUser } from "../request_utils";
import { nameDirtyInputTest, testDirtyInputRequest, verifyRequestAccepted, verifyRequestDenied } from "../utils";
import { asDMChannel } from "@/data/Chat";
setBeforeEach(async (context) => {
    await request(false);
    await createUser(context, "user");
});
createTest("Can post and load chats", "functionality", async (context) => {
    // create a second user to try and access posted chats later
    await createUser(context + 1, "user1");
    const user_obj2 = { usr: context + 1 + "@sharklasers.com", pwd: "Password123" };
    const user_obj1 = { usr: context + "@sharklasers.com", pwd: "Password123" };
    const name = "my new grp";
    const txt_org = "hello, fellow chatters";
    const txt_grp = "hello, fellow chatters";
    let txt_dm = "Hello, sub!";
    // second user's id is required to give them access to the group
    const user2_id = (await request("login", user_obj2)).data._id;
    await request("logout");
    // log original user in
    await request("login", user_obj1);
    const user1_id = (await request("login", user_obj1)).data._id;
    // setup
    const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("invite", { org, email: user_obj2.usr });
    await request("logout");
    await request("login", user_obj2);
    const invitations = (await request("getInvitations")).data;
    await request("acceptInvitation", { inv: invitations[0]._id });
    await request("logout");
    await request("login", user_obj1);
    await request("grp_new", { org, name });
    const grp = (await request("grp_getOfOrg", { org })).data[0]._id;
    await request("grp_setAuthLvl", { org, grp, user: user2_id, lvl: 1 });
    // can user, who posts chats, access them?
    let req_post = await request("chat_post", { org, channel: "org", is_dm: false, txt: txt_org });
    verifyRequestAccepted(req_post, "Tried to post a new chat to an org.");
    req_post = await request("chat_post", { org, channel: grp, is_dm: false, txt: txt_grp });
    verifyRequestAccepted(req_post, "Tried to post a new chat to a grp.");
    req_post = await request("chat_post", { org, channel: user2_id, is_dm: true, txt: txt_dm });
    verifyRequestAccepted(req_post, "Tried to send a dm to another user.");
    let req_load = await request("chat_load", { org });
    verifyRequestAccepted(req_load, "Tried to load chat messages.");
    if (req_load.data[0].txt !== txt_org)
        return "Could not load a correct chat from org!";
    if (req_load.data[1].txt !== txt_grp)
        return "Could not load a correct chat from grp!";
    if (req_load.data[2].txt !== txt_dm)
        return "Could not load a correct chat from dm's!";
    await request("logout");
    // can user, who loads chats, access them?
    await request("login", user_obj2);
    req_load = await request("chat_load", { org });
    verifyRequestAccepted(req_load, "Tried to load chat messages as second user.");
    if (req_load.data[0].txt !== txt_org)
        return "Could not load a correct chat as second user from org!";
    if (req_load.data[1].txt !== txt_grp)
        return "Could not load a correct chat as second user from grp!";
    if (req_load.data[2].txt !== txt_dm)
        return "Could not load a correct chat as second user from dm's!";
    // is dm channel correct?
    const channel = asDMChannel([user1_id, user2_id]);
    req_load = await request("chat_load", { org });
    if (req_load.data[2].channel !== channel)
        return "The channel, in which the users send dms, was incorrect!\nExpected: " + channel + "\nReceived: " + req_load.data[2].channel;
    txt_dm = "Hello, dom!";
    await request("chat_post", { org, channel: user1_id, is_dm: true, txt: txt_dm });
    await request("logout");
    // can user, who originally posted chats, access dms, sent to them?
    await request("login", user_obj1);
    req_load = await request("chat_load", { org });
    return (req_load.data[3].txt !== txt_dm) ? "Could not load a correct dm chat text!" : true;
});
createTest(nameDirtyInputTest("chat_post, chat_load"), "stability", async (context) => {
    // setup
    const user_obj = { usr: context + "@sharklasers.com", pwd: "Password123" };
    const name = "my new grp";
    const txt = "hello, fellow chatters";
    await request("login", user_obj);
    const user_id = (await request("user")).data?._id;
    const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org, name });
    const grp = (await request("grp_getOfOrg", { org })).data[0]._id;
    // dirty input testing
    const req = await request("chat_load", { org });
    verifyRequestAccepted(req, "Tried to load an empty ChatMsg[].");
    if (req.data.length !== 0)
        return "Something was loaded into ChatMsg[], but it was supposed to be empty!";
    let dirty_input = await testDirtyInputRequest("chat_post accepted dirty input!", { org, channel: grp, is_dm: false, txt }, (dirty_object) => ["chat_post", dirty_object], { is_dm: [true, false] });
    if (dirty_input !== true)
        return dirty_input;
    // ultra corner cases
    let bad_req = await request("chat_post", { org, channel: "bad_req", is_dm: false, txt });
    verifyRequestDenied(bad_req, "Tried to post a chat msg into an incorrect channel (\"bad_req\").");
    bad_req = await request("chat_post", { org, channel: "", is_dm: false, txt });
    verifyRequestDenied(bad_req, "Tried to post a chat msg containing empty strings as IDs (\"\").");
    bad_req = await request("chat_post", { org, channel: "bad", is_dm: false, txt });
    verifyRequestDenied(bad_req, "Tried to post a chat msg containing dirty IDs (\"bad\").");
    bad_req = await request("chat_post", { org, channel: user_id, is_dm: true, txt });
    verifyRequestDenied(bad_req, "Tried to post a chat msg to user themself.");
    if (!bad_req.is200)
        return "Server crashed due to dirty input in req chat_post field channel (user's actual id)!";
    if (bad_req.ok)
        return "Server accepted a chat message being sent to user themself!";
    bad_req = await request("chat_post", { org, channel: "x0_0", is_dm: true, txt });
    verifyRequestDenied(bad_req, "Tried to post a chat msg being sent to a non-existing user (\"x0_0\").");
    // normal resumes here
    await request("chat_post", { org, channel: grp, is_dm: false, txt });
    dirty_input = await testDirtyInputRequest("chat_load accepted dirty input!", { org }, (dirty_object) => ["chat_load", dirty_object]);
    return dirty_input;
});
//# sourceMappingURL=chat_test.js.map