import { createTest, request, setBeforeEach } from "../request_test";
import { createUser } from "../request_utils";
import { datec, timestamp, testDirtyInputRequest, nameDirtyInputTest, verifyRequestDenied, verifyRequestAccepted } from "../utils";
setBeforeEach(async (context) => {
    await request(false);
    await createUser(context, "user");
});
createTest(nameDirtyInputTest("tkt_new"), "stability", async (context) => {
    let dirty_result = await testDirtyInputRequest("Ticket was created without any user being logged in!", { doc: { grp: "x0_0", is_global: true, is_priority: false, text: "tere", title: "hello" }, org: "x0_0" }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    dirty_result = await testDirtyInputRequest("Ticket was created without user belonging to any org!", { doc: { grp: "x0_0", is_global: true, is_priority: false, text: "tere", title: "hello" }, org: "x0_0" }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    dirty_result = await testDirtyInputRequest("Ticket was created in nonexistant group!", { doc: { grp: "1", is_global: true, is_priority: false, text: "tere", title: "hello" }, org: org_id }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const group_id = groups[0]._id;
    dirty_result = await testDirtyInputRequest("Ticket was created with dirty input!", { doc: { grp: group_id, is_global: true, is_priority: false, text: "tere", title: "hello" }, org: org_id }, (dirty_object) => ["tkt_new", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await testDirtyInputRequest("Ticket was created with dirty input!", { grp: group_id, is_global: true, is_priority: false, text: "tere", title: "hello" }, (dirty_object) => ["tkt_new", { doc: dirty_object, org: org_id }], { is_global: [false, true], is_priority: [false, true], text: [""] });
    return dirty_result;
});
createTest("Can successfully create a ticket", "functionality", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    const req = await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    verifyRequestAccepted(req, "Tried to create a correct ticket.");
    const tkt = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0];
    return (tkt.title !== title || tkt.comments[0].text[0] !== text) ? "Ticket did not have desired content!" : true;
});
// todo: randomly failed once and not again, wonder wth is going on
createTest("Can reply to ticket", "functionality", async (context) => {
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    const tickets = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    const req = await request("tkt_addReply", { org: org_id, id: tickets[0]._id, text: "test reply" });
    verifyRequestAccepted(req, "Tried to add a reply to ticket.");
    return true;
});
createTest(nameDirtyInputTest("tkt_addReply"), "stability", async (context) => {
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    const tickets = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    return await testDirtyInputRequest("tkt_addReply received dirty input!", { org: org_id, id: tickets[0]._id, text: "Lamp" }, (dirty_object) => ["tkt_addReply", dirty_object]);
});
createTest("Must check user authorization before accepting tickets", "security", async (context) => {
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("logout");
    await createUser(context + 1, "user");
    await request("login", { usr: context + "1@sharklasers.com", pwd: "Password123" });
    const req = await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    verifyRequestDenied(req, "Tried to accept a ticket from an unauthorized user.");
    return true;
});
createTest("Must not reveal tickets to unauthorized users", "security", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await createUser(context + 1, "user");
    await request("login", { usr: user0, pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("invite", { org: org_id, email: user1 });
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    await request("logout");
    await request("login", { usr: user1, pwd: "Password123" });
    let req = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id }));
    verifyRequestDenied(req, "Tried to access tickets outside the org.");
    const invitation = (await request("getInvitations")).data;
    await request("acceptInvitation", { inv: invitation[0]._id });
    req = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id }));
    verifyRequestDenied(req, "Tried to access tickets outside the grp.");
    return true;
});
createTest("Can get global and self made tickets", "functionality", async (context) => {
    const title_global = "väga tõsine probleem";
    const text_global = "erakordselt tõsine probleem";
    const title_self = "isiklik probleem";
    const text_self = "erakordselt isiklik probleem";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await request("tkt_new", { doc: { grp, is_global: true, is_priority: false, title: title_global, text: text_global }, org });
    await request("tkt_new", { doc: { grp, is_global: false, is_priority: false, title: title_self, text: text_self }, org });
    // can access global ticket
    let req = (await request("tkt_getOfMeta", { org, grp: "Global" }));
    let tkt = req.data[0];
    verifyRequestAccepted(req, "Tried to get global tickets.");
    if (!(tkt.title === title_global && tkt.comments[0].text[0] === text_global))
        return "Could not get a self-made ticket!";
    // can access self-made ticket
    req = (await request("tkt_getOfMeta", { org, grp: "Self" }));
    tkt = req.data[1];
    verifyRequestAccepted(req, "Tried to get self-made ticket.");
    if (!(tkt.title === title_self && tkt.comments[0].text[0] === text_self))
        return "Could not get a self-made ticket!";
    return true;
});
createTest("Can modify an existing ticket", "functionality", async (context) => {
    // set up
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    let req = (await request("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    let tkt = req.data[0];
    // Is it possible to modify?
    let options = { is_global: true, is_priority: true, is_resolved: true };
    let modify_req = await request("tkt_modify", { org: org_id, id: tkt._id, options });
    let cur_time = timestamp();
    verifyRequestAccepted(modify_req, "Tried to modify a ticket.");
    req = (await request("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    tkt = req.data[0];
    if (tkt?.is_global !== options.is_global || tkt?.is_priority !== options.is_priority || tkt?.is_resolved !== options.is_resolved) {
        return "Could not modify ticket with new data! Expected: " + JSON.stringify({ ...tkt, ...options }) +
            ", received: " + tkt;
    }
    tkt = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0];
    // to compare them, they have to be in order and made more similar or sth idk
    let expected_action_types = JSON.stringify(["setGlobal", "setPriority", "lock"].sort());
    let received_action_types = JSON.stringify(tkt.actions.map(action => action.type).sort());
    if (expected_action_types !== received_action_types)
        return "Action types did not match!\nExpected: " + expected_action_types + "\nReceived: " + received_action_types;
    if (!(tkt.actions[0].posted.at >= cur_time - 2000 && tkt.actions[0].posted.at - cur_time < 3000)) {
        return "Timestamp was incorrect!\nExpected: " + datec(cur_time) +
            "\nReceived: " + datec(tkt.actions[0].posted.at) +
            "\nThe difference: " + JSON.stringify(Math.abs(tkt.actions[0].posted.at - cur_time) + "ms");
    }
    // set ticket resolved 
    options = { is_global: false, is_priority: false, is_resolved: false };
    modify_req = await request("tkt_modify", { org: org_id, id: tkt._id, options });
    cur_time = timestamp();
    verifyRequestAccepted(modify_req, "Tried to mark ticket as resolved.");
    req = (await request("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    tkt = req.data[0];
    if (tkt?.is_global !== options.is_global || tkt?.is_priority !== options.is_priority || tkt?.is_resolved !== options.is_resolved) {
        return "Could not modify ticket with new data! Expected: " + JSON.stringify({ ...tkt, ...options }) + ", received: " + tkt;
    }
    // checks time log
    tkt = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0];
    expected_action_types = JSON.stringify(["setGlobal", "setPriority", "lock", "unlock", "unsetGlobal", "unsetPriority"].sort());
    received_action_types = JSON.stringify(tkt.actions.map(action => action.type).sort());
    if (expected_action_types !== received_action_types)
        return "Action types did not match!\nExpected: " + expected_action_types + "\nReceived: " + received_action_types;
    if (!(tkt.actions[0].posted.at >= cur_time - 2000 && tkt.actions[0].posted.at - cur_time < 3000)) {
        return "Timestamp was incorrect!\nExpected: " + datec(cur_time) +
            "\nReceived: " + datec(tkt.actions[0].posted.at) +
            "\nThe difference: " + JSON.stringify(Math.abs(tkt.actions[0].posted.at - cur_time) + "ms");
    }
    return true;
});
createTest(nameDirtyInputTest("tkt_modify"), "stability", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp = groups[0]._id;
    await request("tkt_new", { doc: { grp, is_global: true, is_priority: false, title, text }, org: org_id });
    const req = (await request("tkt_getOfMeta", { org: org_id, grp: "Global" }));
    const tkt = req.data[0];
    const options = { is_global: false, is_priority: true, is_resolved: true };
    let dirty_result = await testDirtyInputRequest("There was a problem modifying a ticket!", { org: org_id, id: tkt._id, options }, (dirty_object) => ["tkt_modify", dirty_object], { options: [{}] });
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await testDirtyInputRequest("There was a problem modifying a ticket!", options, (dirty_object) => ["tkt_modify", { org: org_id, id: tkt._id, options: dirty_object }], { is_global: [true, false, null, undefined], is_priority: [true, false, null, undefined], is_resolved: [true, false, null, undefined], võti: [{}] });
    return dirty_result;
});
createTest("Can remove whitespaces from title", "extra", async (context) => {
    let title = " väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    title = "väga tõsine probleem ";
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    title = "väga tõsine \n\n\n\n\n probleem";
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    const tickets = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    if (!tickets[0].title.startsWith("v"))
        return "The leading whitespace was not removed from title!";
    if (!tickets[1].title.endsWith("m"))
        return "The trailing whitespace was not removed from title!";
    if (tickets[2].title.includes("\n"))
        return "Newlines were not removed from title!";
    return true;
});
createTest("Can remove whitespaces from text (description)", "extra", async (context) => {
    const title = "väga tõsine probleem";
    let text = " erakordselt tõsine probleem";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    text = "erakordselt tõsine probleem ";
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    text = "\n\n\n\nerakordselt \n\n\n\n tõsine \n\n\n\n\n probleem\n\n\n\n";
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title, text }, org: org_id });
    const tickets = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    if (!tickets[0].comments[0].text[0].startsWith("e"))
        return "The leading whitespace was not removed from text!";
    if (!tickets[1].comments[0].text[0].endsWith("m"))
        return "The trailing whitespace was not removed from text!";
    const text_string = tickets[2].comments[0].text.join("ä");
    if (text_string.includes("\n"))
        return "Newlines were not removed from text!";
    if (text_string !== "erakordseltätõsineäprobleem")
        return "Paragraphs were not split up correctly!";
    return true;
});
createTest("Can remove whitespaces from reply", "extra", async (context) => {
    let text = " test reply";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org: org_id, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org: org_id })).data;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    const tickets = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data;
    await request("tkt_addReply", { org: org_id, id: tickets[0]._id, text });
    text = "test reply ";
    await request("tkt_addReply", { org: org_id, id: tickets[0]._id, text });
    text = "\n\n\n\n test \n\n\n\n reply \n\n\n\n";
    await request("tkt_addReply", { org: org_id, id: tickets[0]._id, text });
    const fixed_replies = (await request("tkt_getOfGroup", { org: org_id, grp: grp_id })).data[0].comments;
    if (!fixed_replies[1].text[0].startsWith("t"))
        return "The leading whitespace was not removed from reply!";
    if (!fixed_replies[2].text[0].endsWith("y"))
        return "The trailing whitespace was not removed from reply!";
    const final_string = fixed_replies[3].text.join("ä");
    if (final_string.includes("\n"))
        return "Newlines were not removed from text!";
    if (final_string !== "testäreply")
        return "Paragraphs were not split up correctly!";
    return true;
});
createTest(nameDirtyInputTest("tkt_read"), "stability", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";
    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data;
    await request("grp_new", { org, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org })).data;
    const grp = groups[0]._id;
    await request("tkt_new", { doc: { grp, is_global: false, is_priority: false, title, text }, org });
    const tkt = (await request("tkt_getOfGroup", { org, grp })).data[0];
    let dirty_result = await testDirtyInputRequest("Ticket read received dirty input!", { org, tkt: tkt._id, key: 1 }, (dirty_object) => ["tkt_read", dirty_object]);
    if (dirty_result !== true)
        return dirty_result;
    dirty_result = await testDirtyInputRequest("Ticket read received dirty input!", { org, grp }, (dirty_object) => ["tkt_read", dirty_object]);
    return dirty_result;
});
//# sourceMappingURL=ticket_test.js.map