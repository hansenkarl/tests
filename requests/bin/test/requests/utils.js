import { DateTime } from "luxon";
import { request } from "./request_test";
import { admin_pwd, admin_usr_name, test_key } from "./config";
export function timestamp() { return new Date().getTime(); }
export function sleep(ms) { return new Promise(r => setTimeout(() => { r(true); }, ms)); }
const dirty_values = [null, "", 123, true, false, [], {}, undefined];
const object_root_key = "võti";
export async function combinateDirtyObjects(valid_object, foreach, exclude) {
    const clean_json = JSON.stringify(valid_object);
    const field_names = Object.keys(valid_object);
    for (const field_name of field_names) {
        for (const dirty_value of dirty_values) {
            if (JSON.stringify(dirty_value) === JSON.stringify(valid_object[field_name]) ||
                exclude && exclude[field_name]?.some((current) => JSON.stringify(current) === JSON.stringify(dirty_value)))
                continue;
            const dirty_object = JSON.parse(clean_json);
            dirty_object[field_name] = dirty_value;
            const result = await foreach(dirty_object);
            if (result !== true)
                return result;
        }
    }
    for (const dirty_value of dirty_values) {
        if (JSON.stringify(dirty_value) === JSON.stringify(valid_object) ||
            exclude && exclude[object_root_key]?.some((current) => JSON.stringify(current) === JSON.stringify(dirty_value)))
            continue;
        const result = await foreach(dirty_value);
        if (result !== true)
            return result;
    }
    return true;
}
export function datec(time) {
    if (typeof time != "number")
        return "invalid timestamp";
    const t = DateTime.fromMillis(time);
    function z(n) { return (n + "").padStart(2, "0"); }
    return `${t.year}-${z(t.month)}-${z(t.day)}`;
}
export async function testDirtyInputRequest(special_msg, valid_object, request_params, exclude) {
    const peculiar_string = "blä_blä_blä_blä";
    return await combinateDirtyObjects(valid_object, async (dirty_object) => {
        const all_params = request_params(dirty_object);
        const req = await request(...all_params);
        if (!req.is200 || req.ok) {
            const req_params = request_params(peculiar_string)[1];
            let field_with_dirty_input;
            if (req_params === peculiar_string) {
                field_with_dirty_input = "(entire object)";
            }
            else {
                const weird_params = JSON.stringify(req_params).split('"' + peculiar_string + '"')[0];
                const last_index = weird_params.lastIndexOf("\"");
                const buffer = weird_params.slice(0, last_index);
                const final = buffer.lastIndexOf("\"");
                field_with_dirty_input = buffer.slice(final + 1);
            }
            const error_details = "The field containing the dirty input was \"" + field_with_dirty_input + "\" and the dirty object was\n" + JSON.stringify(dirty_object);
            if (!req.is200)
                return "Request " + all_params[0] + " has crashed the server!\n" + error_details;
            else
                return special_msg + "\nRequest was: " + all_params[0] + ". " + error_details;
        }
        else
            return true;
    }, exclude);
}
export async function shiftTime(user, timestamp, action) {
    try {
        await request("login", { admin: true, usr: admin_usr_name, pwd: admin_pwd });
        await request("testing_setServerTime", { key: test_key, timestamp });
        await request("login", user);
        return await action();
    }
    finally {
        await request("login", { admin: true, usr: admin_usr_name, pwd: admin_pwd });
        await request("testing_setServerTime", { key: test_key, timestamp: "reset" });
        await request("login", user);
    }
}
export function nameDirtyInputTest(request_name) {
    return "Server does not accept dirty input from: " + request_name;
}
export function verifyRequestAccepted(req, description) {
    if (!req.is200)
        throw "Server has crashed due to following action!\n" + description;
    if (!req.ok)
        throw "Request did not return ok!\n" + description;
}
export function verifyRequestDenied(req, description) {
    if (!req.is200)
        throw "Server has crashed due to following action!\n" + description;
    if (req.ok)
        throw "Request returned ok, but it was supposed to be denied!\n" + description;
}
/*
createTest("Can access self-made tickets", "functionality", async (context) => {
    const title = "väga tõsine probleem";
    const text = "erakordselt tõsine probleem";

    await request("login", { usr: context + "@sharklasers.com", pwd: "Password123" });
    const org = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;

    await request("grp_new", { org, name: "new grp" });
    const groups = (await request("grp_getOfOrg", { org })).data!;
    const grp = groups[0]._id;
    await request("tkt_new", { doc: { grp, is_global: false, is_priority: false, title, text }, org });

    const req_get = (await request("tkt_getOfMeta", { org, grp: "Self" }));
    const tkt = req_get.data![0];
    verifyRequestAccepted(req_get, "Tried to get self-made ticket.");
    if (!(tkt.title === title && tkt.comments[0].text[0] === text)) return "Could not get a self-made ticket!";

    const req_add = await request("tkt_addReply", { org, id: tkt._id, text: "test reply" });
    verifyRequestAccepted(req_add, "Tried to send a reply to a self-made ticket.");
    
    return true;
});

security in the future
createTest("Can create and access tickets in non-joined groups", "functionality", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await createUser(context + 1, "user");
    await request("login", { usr: user0, pwd: "Password123" });

    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
    await request("invite", { org: org_id, email: user1 });
    await request("grp_new", { org: org_id, name: "new grp" });

    const groups = (await request("grp_getOfOrg", { org: org_id })).data!;
    const grp_id = groups[0]._id;
    await request("logout");

    await request("login", { usr: user1, pwd: "Password123" });
    const invitation = (await request("getInvitations")).data!;
    await request("acceptInvitation", { inv: invitation[0]._id });

    // this is okay, Raul approved
    const req = await request("tkt_new", { doc: { grp: grp_id, is_global: false, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });
    verifyRequestAccepted(req, "Tried to create a ticket while being a member of an org but not a member of a grp.");

    // must always be able to access self-made tickets
    const req_meta = (await request("tkt_getOfMeta", { org: org_id, grp: "Self" }));
    const tkt = req_meta.data![0];
    verifyRequestAccepted(req_meta, "Tried to get a self-made ticket from grp, where user does not belong.");
    if (!(tkt.title === "väga tõsine probleem" && tkt.comments[0].text[0] === "erakordselt tõsine probleem")) return "Could not get a self-made ticket from grp, where user does not belong!";

    const req_add = await request("tkt_addReply", { org: org_id, id: tkt._id, text: "test reply" });
    verifyRequestAccepted(req_add, "Tried to add a reply to a self-made ticket, but it was not added to a grp, where user does not belong to.");

    return true;
});

createTest("Everyone in org can reply to global tkt", "functionality", async (context) => {
    const user0 = context + "@sharklasers.com";
    const user1 = context + "1@sharklasers.com";
    await createUser(context + 1, "user");
    await request("login", { usr: user0, pwd: "Password123" });

    const org_id = (await request("org_new", { name: "new org", timezone: "Europe/Tallinn", options: {} })).data as Id;
    await request("invite", { org: org_id, email: user1 });
    await request("grp_new", { org: org_id, name: "new grp" });

    const groups = (await request("grp_getOfOrg", { org: org_id })).data!;
    const grp_id = groups[0]._id;
    await request("tkt_new", { doc: { grp: grp_id, is_global: true, is_priority: false, title: "väga tõsine probleem", text: "erakordselt tõsine probleem" }, org: org_id });

    await request("logout");

    await request("login", { usr: user1, pwd: "Password123" });

    const invitation = (await request("getInvitations")).data!;
    await request("acceptInvitation", { inv: invitation[0]._id });

    const req_meta = (await request("tkt_getOfMeta", { org: org_id, grp: "Global" }));
    const tkt = req_meta.data![0];
    const req_add = await request("tkt_addReply", { org: org_id, id: tkt._id, text: "test reply" });
    verifyRequestAccepted(req_add, "Tried to reply to a global ticket.");

    return true;
});
*/
//# sourceMappingURL=utils.js.map